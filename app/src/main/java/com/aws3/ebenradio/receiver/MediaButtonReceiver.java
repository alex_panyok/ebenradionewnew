package com.aws3.ebenradio.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.aws3.ebenradio.model.event.radio.ChangeVolumeEvent;
import com.aws3.ebenradio.service.RadioService;
import com.aws3.ebenradio.util.radio.RadioPlayerState;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Dell on 10.11.2016.
 */
public class MediaButtonReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(Intent.ACTION_MEDIA_BUTTON)){
            Intent intentService = new Intent(context,RadioService.class);
            intentService.setAction(RadioService.ACTION_TOGGLE);
            context.startService(intentService);
        }
    }
}
