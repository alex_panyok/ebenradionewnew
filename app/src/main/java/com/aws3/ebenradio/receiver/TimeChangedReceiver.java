package com.aws3.ebenradio.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.aws3.ebenradio.util.alarm.AlarmUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;

/**
 * Created by Dell on 21.11.2016.
 */
public class TimeChangedReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        AlarmUtil.startAlarmBroadcast(context, PrefUtils.getRepeatAlarm(), PrefUtils.getAlarmStatus());
    }
}
