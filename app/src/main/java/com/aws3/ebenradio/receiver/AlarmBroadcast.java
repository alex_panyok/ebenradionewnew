package com.aws3.ebenradio.receiver;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import com.aws3.ebenradio.model.alarm.WeekDay;
import com.aws3.ebenradio.service.RadioService;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.aws3.ebenradio.util.prefs.PrefUtils;


import java.util.Calendar;
import java.util.List;


/**
 * Created by Dell on 20.09.2016.
 */
public class AlarmBroadcast extends BroadcastReceiver implements EbenKeys {

    public static final String ACTION_ON= "alarm_on";
    public static final String ACTION_OFF= "alarm_off";

    private Context context;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        if(intent.getAction().equals(ACTION_ON))
            setUpAlarms(context, PrefUtils.getWeekDays(), intent.getBooleanExtra(REPEAT,false),true);
        else if(intent.getAction().equals(ACTION_OFF))
            setUpAlarms(context,  PrefUtils.getWeekDays(),intent.getBooleanExtra(REPEAT,false),false);
    }

    private void scheduleOneShotAlarm(Context context, Calendar calendar,  boolean status) {
        // Check we aren't setting it in the past which would trigger it to fire instantly
        if(calendar.getTimeInMillis() < System.currentTimeMillis())
            calendar.add(Calendar.DAY_OF_YEAR, 1);
            //calendar.add(Calendar.SECOND, 50);
        Intent intent = new Intent(context, RadioService.class);
        intent.putExtra(REPEAT, false);
        intent.setAction(RadioService.ACTION_ALARM_FIRE);
        PendingIntent pending = PendingIntent.getService(context,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pending);
        if(status)
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT)
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),pending);
            else
                alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),pending);


    }


    private void scheduleRepeatAlarm(Context context, Calendar calendar, int calendarDay, boolean status) {
        calendar.set(Calendar.DAY_OF_WEEK, calendarDay);
        // Check we aren't setting it in the past which would trigger it to fire instantly
        if(calendar.getTimeInMillis() < System.currentTimeMillis()) {
            calendar.add(Calendar.DAY_OF_YEAR, 7);
            //calendar.add(Calendar.SECOND, 50);

        }
        Intent intent = new Intent(context, RadioService.class);
        intent.putExtra(REPEAT, true);
        intent.setAction(RadioService.ACTION_ALARM_FIRE);
        PendingIntent pending = PendingIntent.getService(context,
                calendarDay,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pending);
        if(status)
            if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.KITKAT)
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),pending);
            else
                alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),pending);

    }

    private void cancelReceiver(){
        ComponentName receiver = new ComponentName(context, AlarmBroadcast.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);

    }

    private void setUpAlarms(Context context, List<WeekDay> days, boolean repeat, boolean status) {
        if(PrefUtils.getAlarmTime()!=null) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, PrefUtils.getAlarmTime().hour);
            calendar.set(Calendar.MINUTE, PrefUtils.getAlarmTime().minute);
            calendar.set(Calendar.SECOND, 0);
            if (repeat)
                for (WeekDay day : days) {
                    calendar = Calendar.getInstance();
                    calendar.set(Calendar.HOUR_OF_DAY, PrefUtils.getAlarmTime().hour);
                    calendar.set(Calendar.MINUTE, PrefUtils.getAlarmTime().minute);
                    calendar.set(Calendar.SECOND, 0);
                    if (day.isChecked)
                        scheduleRepeatAlarm(context, calendar, day.calendarDay, status);
                }
            else
                scheduleOneShotAlarm(context, calendar, status);
        }

        if(!status)
            cancelReceiver();
    }
}
