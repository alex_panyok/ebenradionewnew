package com.aws3.ebenradio.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.aws3.ebenradio.service.ChatService;
import com.aws3.ebenradio.util.alarm.AlarmUtil;
import com.aws3.ebenradio.util.general.ToastUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.user.UserDataPref;

/**
 * Created by Dell on 04.11.2016.
 */
public class AutoBootChatReceiver extends BroadcastReceiver {


    public void onReceive(Context context, Intent intent) {
        AlarmUtil.startAlarmBroadcast(context, PrefUtils.getRepeatAlarm(), PrefUtils.getAlarmStatus());
//        if(UserDataPref.isAuthorized(context))
//            context.startService(new Intent(context, ChatService.class));
    }
}
