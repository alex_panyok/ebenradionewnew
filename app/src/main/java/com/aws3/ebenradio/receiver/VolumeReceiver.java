package com.aws3.ebenradio.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.aws3.ebenradio.model.event.radio.ChangeVolumeEvent;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Dell on 19.10.2016.
 */
public class VolumeReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.media.VOLUME_CHANGED_ACTION")) {
            EventBus.getDefault().postSticky(new ChangeVolumeEvent());
        }
    }
}
