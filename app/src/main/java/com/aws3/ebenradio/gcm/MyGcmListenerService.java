package com.aws3.ebenradio.gcm;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.chat.NewMessageChat;
import com.aws3.ebenradio.model.chat.PushChatMessage;
import com.aws3.ebenradio.model.event.chat.NewMessageEvent;
import com.aws3.ebenradio.model.event_er.PushMessage;
import com.aws3.ebenradio.service.ChatService;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.aws3.ebenradio.util.keys.EbenRetroKeys;
import com.aws3.ebenradio.util.skins.ColorUtil;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.view.activity.ChatActivity_;
import com.aws3.ebenradio.view.activity.MainActivity_;
import com.google.android.gms.gcm.GcmListenerService;

import org.greenrobot.eventbus.EventBus;


public class MyGcmListenerService extends GcmListenerService implements EbenKeys, EbenRetroKeys {

    private static final String TAG = "MyGcmListenerService";
    private static final int PENDING_INTENT_REQUEST_CODE = 110;

    @Override
    public void onMessageReceived(String from, Bundle data) {
//        sendNotification(data.getString(TYPE),data.getString(ID), data.getString(MESSAGE), this);
        if(data.getString(TYPE)!=null)
            sendNotification(new PushMessage(data.getString(TYPE),data.getString(STREAM_ID),data.getString(URL),data.getString(EVENT_ID), data.getString(MESSAGE)));
        else
            sendNotificationMessage(new PushChatMessage(data.getString("from_user_name"), data.getString("from_user_id"), data.getString("message")));
    }


    private void sendNotification(PushMessage message) {
            Intent intent = new Intent(this, MainActivity_.class);
            intent.putExtra(TYPE, message.type);
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.setAction(EVENT_PUSH);
            switch (Integer.parseInt(message.type)){
                case 1:
                    intent.putExtra(URL, message.url);
                    break;
                case 3:
                    intent.putExtra(STREAM_ID, Integer.parseInt(message.stream_id));
                    break;
            }


        PendingIntent pendingIntent = PendingIntent.getActivity(this, PENDING_INTENT_REQUEST_CODE, intent,
                PendingIntent.FLAG_ONE_SHOT);

        int color = ColorUtil.getColorById(SkinPrefUtils.getSkin().color, this);

        boolean whiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        int notificationIconRes =R.drawable.ic_stat_notify;

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        android.support.v4.app.NotificationCompat.Builder notificationBuilder = new android.support.v4.app.NotificationCompat.Builder(this)
                    .setSmallIcon(notificationIconRes)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText(message.message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.eben_radio_logo_squere))
                    .setStyle(new android.support.v4.app.NotificationCompat.BigTextStyle().bigText(message.message))
                    .setColor(color);
//        }

        notificationBuilder.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_SOUND);

//        notification.defaults |= Notification.DEFAULT_VIBRATE;

        NotificationManagerCompat.from(this).notify(Integer.parseInt(message.type), notificationBuilder.build());
    }




    private void sendNotificationMessage(PushChatMessage messageChat){
        if(!ChatService.isChatAcitivityRunning || ChatService.idChat !=Integer.parseInt(messageChat.from_user_id)) {
            EventBus.getDefault().postSticky(new NewMessageEvent(new NewMessageChat(messageChat.message, Integer.parseInt(messageChat.from_user_id))));
            Intent intent = new Intent(this, ChatActivity_.class);
            intent.setAction("CHAT");
            intent.putExtra(ID, String.valueOf(messageChat.from_user_id));
            intent.putExtra(NAME, messageChat.from_user_name);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, Integer.parseInt(messageChat.from_user_id), intent,
                    PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            RemoteInput remoteInput = new RemoteInput.Builder("key_text_reply")
//                    .setLabel(getString(R.string.type_message))
//                    .build();

//            NotificationCompat.Action action =
//                    new NotificationCompat.Action.Builder(R.drawable.send, getString(R.string.type_message), pendingIntent)
//                            .addRemoteInput(remoteInput)
//                            .setAllowGeneratedReplies(true)
//                            .build();
            android.support.v4.app.NotificationCompat.Builder notificationBuilder;
            notificationBuilder = new android.support.v7.app.NotificationCompat.Builder(this)
                    .setStyle(new NotificationCompat.MessagingStyle(messageChat.from_user_name)
                            .addMessage(messageChat.message, 0, messageChat.from_user_name))
                    .setSmallIcon(R.drawable.ic_stat_notify_letter)
                    .setContentTitle("New message from " + messageChat.from_user_name)
                    .setContentText(messageChat.message)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setColor(ColorUtil.getColorById(SkinPrefUtils.getSkin().color, this))
                    .setVibrate(new long[]{100, 500, 300, 500})
                    .setLights(ColorUtil.getColorById(SkinPrefUtils.getSkin().color, this), 500, 200)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.eben_radio_logo_squere))
                    .setContentIntent(pendingIntent)
//                    .addAction(action)
                    .setPriority(Notification.PRIORITY_MAX);


            ;
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            Notification notification = notificationBuilder.build();
            // context.getSharedPreferences(SHARED_PREF,Context.MODE_PRIVATE).edit().putInt(NOTIFCOUNT,(context.getSharedPreferences(SHARED_PREF,Context.MODE_PRIVATE).getInt(NOTIFCOUNT,0)+1)).apply();
            notificationManager.notify(Integer.parseInt(messageChat.from_user_id), notification);
//        }
        }
    }


}