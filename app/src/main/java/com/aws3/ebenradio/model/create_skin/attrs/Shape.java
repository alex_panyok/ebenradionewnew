package com.aws3.ebenradio.model.create_skin.attrs;

/**
 * Created by Alex on 15.10.2016.
 */

public class Shape extends AttrItem{

    public int shapeId;

    public Shape(int id, int shapeId, int resId) {
        this.id = id;
        this.shapeId = shapeId;
        this.resId = resId;
    }
}
