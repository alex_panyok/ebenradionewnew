package com.aws3.ebenradio.model.create_skin.attrs;

import android.net.Uri;

/**
 * Created by Alex on 15.10.2016.
 */

public class Background extends AttrItem {

    public String pathId;

    public Background(int id, int resId, String pathId) {
        this.resId = resId;
        this.pathId = pathId;
        this.id= id;
    }
}
