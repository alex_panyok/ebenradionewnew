package com.aws3.ebenradio.model.follow;


import com.aws3.ebenradio.model.networking.EbenResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FollowResponse extends EbenResponse {

    @SerializedName("following")
    public List<FollowPerson> followingList;

    @SerializedName("followers")
    public List<FollowPerson> followersList;



}
