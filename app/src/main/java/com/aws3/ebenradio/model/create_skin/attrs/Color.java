package com.aws3.ebenradio.model.create_skin.attrs;

/**
 * Created by Alex on 15.10.2016.
 */

public class Color extends AttrItem {


    public int color;

    public Color(int id, int resId, int color) {
        this.resId = resId;
        this.color = color;
        this.id = id;
    }
}
