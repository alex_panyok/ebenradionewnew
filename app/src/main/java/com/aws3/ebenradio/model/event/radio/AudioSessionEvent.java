package com.aws3.ebenradio.model.event.radio;

/**
 * Created by Dell on 20.10.2016.
 */
public class AudioSessionEvent {

    public int sessionId;

    public AudioSessionEvent(int sessionId) {
        this.sessionId = sessionId;
    }
}
