package com.aws3.ebenradio.model.rss;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Dell on 28.10.2016.
 */
@Root(name = "rss", strict = false)
public class Feed {

    @Element(name = "channel")
    public Channel channel;


}
