package com.aws3.ebenradio.model.ads;

/**
 * Created by Dell on 04.11.2016.
 */
public class Ad {
    public String image;

    public String url;

    public Ad(String image, String url) {
        this.image = image;
        this.url = url;
    }
}
