package com.aws3.ebenradio.model.device;

import com.google.gson.annotations.SerializedName;

public class SaveDeviceBody {

    @SerializedName("device_id")
    private String deviceId;

    @SerializedName("device_token")
    private String deviceToken;

    public SaveDeviceBody(String deviceId, String deviceToken) {
        this.deviceId = deviceId;
        this.deviceToken = deviceToken;
    }
}
