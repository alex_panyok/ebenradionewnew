package com.aws3.ebenradio.model.event_er;

/**
 * Created by Dell on 10.11.2016.
 */
public class PushMessage {

    public String type;

    public String stream_id;

    public String url;

    public String event_id;

    public String message;

    public PushMessage(String type, String stream_id, String url, String event_id, String message) {
        this.type = type;
        this.stream_id = stream_id;
        this.url = url;
        this.event_id = event_id;
        this.message = message;
    }
}
