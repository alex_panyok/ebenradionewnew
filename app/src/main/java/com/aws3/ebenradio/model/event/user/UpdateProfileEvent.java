package com.aws3.ebenradio.model.event.user;

import com.aws3.ebenradio.model.user.User;

/**
 * Created by Dell on 15.09.2016.
 */
public class UpdateProfileEvent {

    public User user;

    public UpdateProfileEvent(User user) {
        this.user = user;
    }
}
