package com.aws3.ebenradio.model.networking;

import com.google.gson.annotations.SerializedName;

public class EbenResponse {

    private static final String STATUS_OK = "ok";

    @SerializedName("status")
    public String status;

    @SerializedName("message")
    public String message;

    public boolean isStatusOk() {
        return status.equalsIgnoreCase(STATUS_OK);
    }

}
