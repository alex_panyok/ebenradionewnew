package com.aws3.ebenradio.model.contact;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 21.04.2016.
 */
public class Contact {

    @SerializedName("name")
    public String name;

    @SerializedName("email")
    public String email;

    @SerializedName("message")
    public String message;

    public Contact(String name, String email, String message){
        this.name = name;
        this.email = email;
        this.message = message;
    }
}
