package com.aws3.ebenradio.model.user;



import com.aws3.ebenradio.model.networking.EbenResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UsersFromSearchResponse extends EbenResponse {

    @SerializedName("users")
    public List<UserFromSearch> users;

    @SerializedName("users_total_count_label")
    public String userTotal;

    public UsersFromSearchResponse(List<UserFromSearch> users) {
        this.users = users;
    }
}
