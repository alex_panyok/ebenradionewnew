package com.aws3.ebenradio.model.chat;

import com.google.gson.annotations.SerializedName;

public class MessageBody {

    @SerializedName("to_user_id")
    String toUserId;

    @SerializedName("message")
    String message;

    public MessageBody(String receiverId, String message) {
        this.toUserId = receiverId;
        this.message = message;
    }
}
