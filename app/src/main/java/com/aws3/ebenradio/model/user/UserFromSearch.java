package com.aws3.ebenradio.model.user;

import com.google.gson.annotations.SerializedName;


public class UserFromSearch {

    @SerializedName("id")
    public  String id;

    @SerializedName("name")
    public String name;

    @SerializedName("country")
    public String country;

    @SerializedName("photo")
    public String photo;

    @SerializedName("lat")
    public float lat;

    @SerializedName("lng")
    public int lng;

    @SerializedName("distance")
    public String distance;

    @SerializedName("is_follow")
    public String isFollow;

    public UserFromSearch() {
    }
//
//    public boolean isFollowing() {
//        return isFollow != null && isFollow.equalsIgnoreCase("1");
//    }
//
//    public void setFollowing(boolean isFollowing) {
//        isFollow = isFollowing ? "1" : "0";
//    }
//
//    public String getId() {
//        return id;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public String getCountry() {
//        return country;
//    }
//
//    public String getPhotoUrl() {
//        return photoUrl;
//    }
//
//    public int getAge() {
//        return age;
//    }
//
//    public String getDistance() {
//        return distance;
//    }

//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        UserFromSearchParcelablePlease.writeToParcel(this, dest, flags);
//    }
//
//    public static final Creator<UserFromSearch> CREATOR = new Creator<UserFromSearch>() {
//        public UserFromSearch createFromParcel(Parcel source) {
//            UserFromSearch target = new UserFromSearch();
//            UserFromSearchParcelablePlease.readFromParcel(target, source);
//            return target;
//        }
//
//        public UserFromSearch[] newArray(int size) {
//            return new UserFromSearch[size];
//        }
//    };

}
