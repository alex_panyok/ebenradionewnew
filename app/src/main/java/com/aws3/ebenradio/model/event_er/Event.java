package com.aws3.ebenradio.model.event_er;

import java.io.Serializable;

/**
 * Created by Dell on 29.07.2016.
 */
public class Event implements Serializable {
    public String id;

    public String title;

    public String location;

    public String date_event;

    public String image;

    public String description;

    public String url;

    public int active;

    public boolean viewed;

    public String price;
}
