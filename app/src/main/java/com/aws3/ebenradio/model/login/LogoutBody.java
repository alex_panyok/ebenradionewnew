package com.aws3.ebenradio.model.login;

/**
 * Created by Dell on 30.12.2016.
 */
public class LogoutBody {

    public String deviceId;

    public LogoutBody(String deviceId) {
        this.deviceId = deviceId;
    }
}
