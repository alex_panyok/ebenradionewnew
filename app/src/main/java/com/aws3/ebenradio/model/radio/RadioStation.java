package com.aws3.ebenradio.model.radio;

/**
 * Created by Dell on 14.10.2016.
 */
public class RadioStation {

    public int id;

    public String title;

    public String url;

    public int imageRes;

    public String urlTitle;

    public RadioStation(int id, String title, String url, int imageRes, String urlTitle) {
        this.id = id;
        this.title = title;
        this.url = url;
        this.imageRes = imageRes;
        this.urlTitle = urlTitle;
    }


}
