package com.aws3.ebenradio.model.chat;

import com.google.gson.annotations.SerializedName;

public class MessageData {

    @SerializedName("message")
    private MessageItem messageItem;

    public MessageItem getMessageItem() {
        return messageItem;
    }
}
