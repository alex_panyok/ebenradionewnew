package com.aws3.ebenradio.model.login;

import com.google.gson.annotations.SerializedName;

public class LoginBody {


    public String email;


    public String password;

    public LoginBody(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
