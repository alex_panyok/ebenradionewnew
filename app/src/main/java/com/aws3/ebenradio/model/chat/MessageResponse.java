package com.aws3.ebenradio.model.chat;


import com.aws3.ebenradio.model.networking.EbenResponse;
import com.google.gson.annotations.SerializedName;

public class MessageResponse extends EbenResponse {

    @SerializedName("data")
    private MessageData messageData;

    public MessageData getMessageData() {
        return messageData;
    }

}
