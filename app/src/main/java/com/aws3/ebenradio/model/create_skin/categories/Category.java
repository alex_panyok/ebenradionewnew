package com.aws3.ebenradio.model.create_skin.categories;

import android.support.v7.widget.RecyclerView;

import com.aws3.ebenradio.view.adapter.create_skin.AdapterAttr;

/**
 * Created by Alex on 15.10.2016.
 */

public class Category {

    public int id;


    public String title;

    public AdapterAttr adapter;

    public Category(int id, String title, AdapterAttr adapter) {
        this.id = id;
        this.title = title;
        this.adapter = adapter;
    }


    @Override
    public boolean equals(Object obj) {
        return title.equals(((Category)obj).title);
    }
}
