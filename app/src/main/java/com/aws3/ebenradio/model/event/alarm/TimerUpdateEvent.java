package com.aws3.ebenradio.model.event.alarm;

/**
 * Created by Dell on 20.09.2016.
 */
public class TimerUpdateEvent {
    public int leftTime;

    public TimerUpdateEvent(int leftTime) {
        this.leftTime = leftTime;
    }
}
