package com.aws3.ebenradio.model.skin;


import android.net.Uri;

import com.aws3.ebenradio.util.colors.ColorsUtil;

import java.io.Serializable;

public class Skin implements Serializable {


    public String name;

    public Integer previewRes;

    public String previewPath;

    public int typefaceId;

    public String backgroundPath;

    public Integer backgroundRes;

    public PlayerControls controls;

    public Integer color;

    public int blur;

    public int shapeId;

    public int volume;

    public int visualizer;

    public boolean isDefault;



    public Skin(String name, boolean isDefault, int previewRes, int typefaceId, String backgroundPath, int backgroundRes, PlayerControls controls, int color, int blur, int shapeId, int volume, int visualizer) {
        this.name = name;
        this.isDefault = isDefault;
        this.previewRes = previewRes;
        this.typefaceId = typefaceId;
        this.backgroundPath = backgroundPath;
        this.backgroundRes = backgroundRes;
        this.controls = controls;
        this.color = color;
        this.blur = blur;
        this.shapeId = shapeId;
        this.volume = volume;
        this.visualizer = visualizer;
    }
}
