package com.aws3.ebenradio.model.event.skin;

import com.aws3.ebenradio.model.skin.Skin;

/**
 * Created by Dell on 13.10.2016.
 */
public class ApplySkinEvent {
    public Skin skin;

    public ApplySkinEvent(Skin skin) {
        this.skin = skin;
    }
}
