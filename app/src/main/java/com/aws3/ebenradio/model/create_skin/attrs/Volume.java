package com.aws3.ebenradio.model.create_skin.attrs;

/**
 * Created by Alex on 15.10.2016.
 */

public class Volume extends AttrItem {

    public int volId;


    public Volume(int id, int volId, int resId) {
        this.id = id;
        this.volId = volId;
        this.resId = resId;

    }
}
