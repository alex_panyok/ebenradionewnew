package com.aws3.ebenradio.model.rss;

import android.text.Html;

import org.jsoup.Jsoup;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * Created by Dell on 28.10.2016.
 */
@Root(name = "item", strict = false)
public class Item implements Serializable {

    @Element(name = "title")
    public String title;

    @Element(name = "description")
    public String descriptionHtml;

    @Element(name = "pubDate")
    public String pubDate;

    public String image;

    public String description;



    public void parseHtml(){
        image = Jsoup.parseBodyFragment(descriptionHtml).getElementsByTag("img").attr("src");
        description = Jsoup.parseBodyFragment(descriptionHtml).getElementsByTag("div").get(0).text();
    }

}
