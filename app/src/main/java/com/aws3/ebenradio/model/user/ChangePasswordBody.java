package com.aws3.ebenradio.model.user;

import com.google.gson.annotations.SerializedName;

public class ChangePasswordBody {

    @SerializedName("old_password")
    public String oldPassword;

    @SerializedName("new_password")
    public String newPassword;

    public ChangePasswordBody(String oldPass, String newPass) {
        this.oldPassword = oldPass;
        this.newPassword = newPass;
    }


}
