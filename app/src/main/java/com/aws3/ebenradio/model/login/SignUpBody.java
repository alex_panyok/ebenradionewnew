package com.aws3.ebenradio.model.login;

import com.google.gson.annotations.SerializedName;

public class SignUpBody {

    @SerializedName("email")
    public String email;

    @SerializedName("full_name")
    public String fullName;

    @SerializedName("birthday_year")
    public String birthdayYear;

    @SerializedName("country")
    public String country;

    @SerializedName("gender")
    public String gender;

    @SerializedName("password")
    public String password;

    public SignUpBody(String email, String fullName, String birthdayYear, String country, String gender, String password) {
        this.email = email;
        this.fullName = fullName;
        this.birthdayYear = birthdayYear;
        this.country = country;
        this.gender = gender;
        this.password = password;
    }



}
