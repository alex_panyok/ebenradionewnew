package com.aws3.ebenradio.model.user;

import com.google.gson.annotations.SerializedName;

public class UserPublicProfile {

    @SerializedName("user_id")
    public String id;

    @SerializedName("photo")
    public String photo;

    @SerializedName("name")
    public String name;

    @SerializedName("country")
    public String country;

    @SerializedName("about")
    public String about;

    @SerializedName("lat")
    public String lat;

    @SerializedName("lng")
    public String lng;

    @SerializedName("age")
    public int age;

    @SerializedName("followers")
    public String followers;

    @SerializedName("following")
    public String following;


}
