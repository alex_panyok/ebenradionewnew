package com.aws3.ebenradio.model.login;

/**
 * Created by Dell on 13.09.2016.
 */
public class Male {

    public String title;

    public int type;

    public Male(String title, int type) {
        this.title = title;
        this.type = type;
    }

    @Override
    public String toString() {
        return title;
    }
}
