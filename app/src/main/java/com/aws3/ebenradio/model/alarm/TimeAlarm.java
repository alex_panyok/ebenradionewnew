package com.aws3.ebenradio.model.alarm;

/**
 * Created by Dell on 21.09.2016.
 */
public class TimeAlarm {

    public int hour;

    public int minute;

    public TimeAlarm(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
    }
}
