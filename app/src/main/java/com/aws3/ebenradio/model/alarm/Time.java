package com.aws3.ebenradio.model.alarm;

/**
 * Created by Dell on 19.09.2016.
 */
public class Time {

    public boolean isChecked;

    public boolean isActive;

    public int time;

    public Time(boolean isChecked, boolean isActive, int time) {
        this.isChecked = isChecked;
        this.isActive = isActive;
        this.time = time;
    }
}
