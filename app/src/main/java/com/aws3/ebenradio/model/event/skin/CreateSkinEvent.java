package com.aws3.ebenradio.model.event.skin;

import com.aws3.ebenradio.model.skin.Skin;

/**
 * Created by Dell on 18.10.2016.
 */
public class CreateSkinEvent {
    public Skin createdSkin;

    public CreateSkinEvent(Skin createdSkin) {
        this.createdSkin = createdSkin;
    }
}
