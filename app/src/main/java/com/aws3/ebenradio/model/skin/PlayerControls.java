package com.aws3.ebenradio.model.skin;

import java.io.Serializable;

/**
 * Created by Dell on 11.10.2016.
 */
public class PlayerControls implements Serializable {
    public int prevId;

    public int nextId;

    public int playId;

    public int pauseId;

    public PlayerControls(int prevId, int nextId, int playId, int pauseId) {
        this.prevId = prevId;
        this.nextId = nextId;
        this.playId = playId;
        this.pauseId = pauseId;
    }


    @Override
    public boolean equals(Object obj) {
        return playId==((PlayerControls)obj).playId;
    }
}
