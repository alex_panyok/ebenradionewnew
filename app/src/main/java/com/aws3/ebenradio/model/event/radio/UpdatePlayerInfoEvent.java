package com.aws3.ebenradio.model.event.radio;

/**
 * Created by Dell on 14.10.2016.
 */
public class UpdatePlayerInfoEvent {

    public String titleSong;

    public String coverSong;

    public boolean update;

    public UpdatePlayerInfoEvent(String titleSong, String coverSong, boolean update) {
        this.titleSong = titleSong;
        this.coverSong = coverSong;
        this.update = update;
    }
}
