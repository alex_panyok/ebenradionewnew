package com.aws3.ebenradio.model.follow;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 14.09.2016.
 */
public class FollowPerson {


    @SerializedName("id")
    public String id;

    @SerializedName("name")
    public String name;

    @SerializedName("photo")
    public String photo;


    public FollowPerson(String id, String name, String photo) {
        this.id = id;
        this.name = name;
        this.photo = photo;
    }
}
