package com.aws3.ebenradio.model.chat;


import android.provider.ContactsContract;

import com.aws3.ebenradio.model.create_skin.attrs.Shape;
import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MessageItem extends Message {

    @SerializedName("id")
    public String id;

    @SerializedName("post_date")
    public String postDate;

    @SerializedName("message")
    public String message;

    @SerializedName("user_id")
    public String userId;

    @SerializedName("name")
    public String name;

    @SerializedName("photo")
    public String photoUrl;

    @SerializedName("is_read")
    public int isRead;

    @SerializedName("delivery_date")
    public String delivery_date;


    public Date date;


    public MessageItem(Date date) {
        this.date = date;
    }
}
