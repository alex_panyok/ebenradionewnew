package com.aws3.ebenradio.model.chat;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MessagesData {

    @SerializedName("page")
    public int page;

    @SerializedName("total_pages")
    public int totalPager;

    @SerializedName("messages")
    public List<MessageItem> messages;

//    public List<Message> getMessages() {
//        return messages;
//    }
//
//    public int getPage() {
//        return page;
//    }
//
//    public int getTotalPager() {
//        return totalPager;
//    }
}
