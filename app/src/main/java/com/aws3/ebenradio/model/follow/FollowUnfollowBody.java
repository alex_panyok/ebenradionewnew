package com.aws3.ebenradio.model.follow;

import com.google.gson.annotations.SerializedName;

public class FollowUnfollowBody {

    @SerializedName("follow_id")
    public  String followId;

    public FollowUnfollowBody(String followId) {
        this.followId = followId;
    }

}
