package com.aws3.ebenradio.model.chat;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 31.10.2016.
 */
public class UserChat {


    @SerializedName("with_user_id")
    public String userWithId;

    @SerializedName("last_msg")
    public String lastMessageTime;

    @SerializedName("last_msg_content")
    public String lastMessageContent;

    @SerializedName("unread_cnt")
    public int unreadCount;

    @SerializedName("username")
    public String userName;

    @SerializedName("photo")
    public String photoUrl;



}
