package com.aws3.ebenradio.model.event.search;

/**
 * Created by Alex on 17.09.2016.
 */
public class SearchPeopleEvent {

    public String searchText;

    public SearchPeopleEvent(String searchText) {
        this.searchText = searchText;
    }
}
