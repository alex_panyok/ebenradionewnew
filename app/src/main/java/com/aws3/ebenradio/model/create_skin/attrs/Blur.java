package com.aws3.ebenradio.model.create_skin.attrs;

/**
 * Created by Dell on 18.10.2016.
 */
public class Blur extends AttrItem {

    public int blurId;


    public Blur(int id, int resId,int blurId) {
        this.id = id;
        this.resId =resId;
        this.blurId = blurId;
    }
}
