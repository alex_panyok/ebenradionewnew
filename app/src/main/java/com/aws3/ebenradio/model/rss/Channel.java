package com.aws3.ebenradio.model.rss;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * Created by Dell on 28.10.2016.
 */
@Root(strict = false)
public class Channel {

    @ElementList(inline = true)
    public List<Item> list;

    public List<Item> parseHtml(){
        for(Item item : list){
            item.parseHtml();
        }
        return list;
    }


}
