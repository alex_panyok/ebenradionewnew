package com.aws3.ebenradio.model.create_skin.attrs;

import com.aws3.ebenradio.model.skin.PlayerControls;

/**
 * Created by Alex on 15.10.2016.
 */

public class Navigation extends AttrItem {

    public PlayerControls controls;

    public Navigation(int id,int resId, PlayerControls controls) {
        this.resId = resId;
        this.controls = controls;
        this.id = id;
    }
}
