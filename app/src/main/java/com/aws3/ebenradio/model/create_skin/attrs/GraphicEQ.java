package com.aws3.ebenradio.model.create_skin.attrs;

/**
 * Created by Alex on 15.10.2016.
 */

public class GraphicEQ extends AttrItem {


    public int eqId;

    public GraphicEQ(int id, int eqId, int resId) {
        this.id = id;
        this.eqId = eqId;
        this.resId = resId;
    }
}
