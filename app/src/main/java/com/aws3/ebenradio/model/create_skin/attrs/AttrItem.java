package com.aws3.ebenradio.model.create_skin.attrs;

/**
 * Created by Alex on 15.10.2016.
 */

public class AttrItem {

    public int id;

    public Integer resId;

    public boolean isSelected;
}
