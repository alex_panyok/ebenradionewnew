package com.aws3.ebenradio.model.alarm;

import java.io.Serializable;
import java.util.Calendar;

/**
 * Created by Dell on 19.09.2016.
 */
public class WeekDay implements Serializable {

    public int id;

    public String day;

    public String code;

    public boolean isChecked;

    public int calendarDay;

    public WeekDay(int id, String day, String code, boolean isChecked) {
        this.id = id;
        this.day = day;
        this.code = code;
        this.isChecked = isChecked;
        this.calendarDay = switchDays(id);
    }

    @Override
    public boolean equals(Object obj) {
        return id==((WeekDay)obj).id;
    }

    private int switchDays(int id){
        switch (id){
            case 0:
                return Calendar.MONDAY;

            case 1:
                return Calendar.TUESDAY;

            case 2:
                return Calendar.WEDNESDAY;

            case 3:
                return Calendar.THURSDAY;

            case 4:
                return Calendar.FRIDAY;

            case 5:
                return Calendar.SATURDAY;

            case 6:
                return Calendar.SUNDAY;


        }
        return 0;
    }

}
