package com.aws3.ebenradio.model.event.chat;

import com.aws3.ebenradio.model.chat.NewMessageChat;

/**
 * Created by Dell on 01.11.2016.
 */
public class NewMessageEvent {


    public NewMessageChat messageChat;

    public NewMessageEvent(NewMessageChat messageChat) {
        this.messageChat = messageChat;
    }
}
