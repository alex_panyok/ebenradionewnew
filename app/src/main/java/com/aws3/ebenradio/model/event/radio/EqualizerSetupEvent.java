package com.aws3.ebenradio.model.event.radio;

import android.media.audiofx.Equalizer;

/**
 * Created by Dell on 22.08.2016.
 */
public class EqualizerSetupEvent {
   public Equalizer equalizer;

    public EqualizerSetupEvent(Equalizer equalizer) {
        this.equalizer = equalizer;
    }
}
