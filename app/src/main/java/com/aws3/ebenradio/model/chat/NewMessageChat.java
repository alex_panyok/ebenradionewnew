package com.aws3.ebenradio.model.chat;

import com.aws3.ebenradio.model.create_skin.attrs.Shape;

/**
 * Created by Dell on 02.11.2016.
 */
public class NewMessageChat {

    public String text;

    public int badge;

    public int from_user_id;

    public NewMessageChat(String text, int from_user_id) {
        this.text = text;
        this.from_user_id = from_user_id;
    }
}
