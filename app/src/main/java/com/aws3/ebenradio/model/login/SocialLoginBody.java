package com.aws3.ebenradio.model.login;

import com.google.gson.annotations.SerializedName;

public class SocialLoginBody {

    @SerializedName("facebook_id")
    public String facebookId;

    @SerializedName("full_name")
    public String fullName;

    @SerializedName("birthday_year")
    public String birthdayYear;

    @SerializedName("country_code")
    public String countryCode;

    @SerializedName("gender")
    public String gender;

    @SerializedName("email")
    public String email;


    @SerializedName("is_automatic")
    public int isAutomatic = 1;


    public SocialLoginBody(String facebookId, String fullName, String gender, String email) {
        this.facebookId = facebookId;
        this.fullName = fullName;
        this.gender = gender;
        this.email = email;
    }
}
