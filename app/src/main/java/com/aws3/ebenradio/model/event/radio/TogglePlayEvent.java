package com.aws3.ebenradio.model.event.radio;

/**
 * Created by Dell on 13.10.2016.
 */
public class TogglePlayEvent {
    public boolean isPlay;

    public TogglePlayEvent(boolean isPlay) {
        this.isPlay = isPlay;
    }
}
