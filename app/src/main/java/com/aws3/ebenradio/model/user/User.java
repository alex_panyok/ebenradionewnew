package com.aws3.ebenradio.model.user;


import com.aws3.ebenradio.model.networking.EbenResponse;

/**
 * Created by Dell on 07.03.2016.
 */
public class User extends EbenResponse {
    public String email;
    public String full_name;
    public String country;
    public String social_id;
    public String photo;
    public String about_you;
    public float lat;
    public float lng;
    public String country_code;
    public String birthday_year;
    public int gender;
    public String followers;
    public String following;
    public boolean isFacebook;
    public int unread;

}
