package com.aws3.ebenradio.model.player;

import java.util.List;

/**
 * Created by Dell on 20.12.2016.
 */
public class SongStreamResponse {

    public List<SongStreamResponseItem> results;
}
