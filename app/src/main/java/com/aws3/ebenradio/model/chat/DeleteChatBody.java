package com.aws3.ebenradio.model.chat;

/**
 * Created by Dell on 24.11.2016.
 */
public class DeleteChatBody {

    public String with_user_id;

    public DeleteChatBody(String with_user_id) {
        this.with_user_id = with_user_id;
    }
}
