package com.aws3.ebenradio.model.user;

import com.google.gson.annotations.SerializedName;

public class ForgetPasswordBody {

    @SerializedName("email")
    private String email;

    public ForgetPasswordBody(String email) {
        this.email = email;
    }

}
