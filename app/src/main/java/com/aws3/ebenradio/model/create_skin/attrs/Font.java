package com.aws3.ebenradio.model.create_skin.attrs;

/**
 * Created by Alex on 15.10.2016.
 */

public class Font extends AttrItem {

    public int fontId;

    public Font(int id, int fontId, int resId) {
        this.id = id;
        this.resId = resId;
        this.fontId = fontId;
    }
}
