package com.aws3.ebenradio.model.chat;

/**
 * Created by Dell on 30.01.2017.
 */

public class PushChatMessage {

    public String from_user_name;

    public String from_user_id;

    public String message;

    public PushChatMessage(String from_usr_name, String from_user_id, String message) {
        this.from_user_name = from_usr_name;
        this.from_user_id = from_user_id;
        this.message = message;
    }
}
