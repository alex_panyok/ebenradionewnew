package com.aws3.ebenradio.model.user;



import com.aws3.ebenradio.model.networking.EbenResponse;
import com.google.gson.annotations.SerializedName;

public class UserUpdatedProfileResponse extends EbenResponse {

    @SerializedName("user")
    public User user;


}
