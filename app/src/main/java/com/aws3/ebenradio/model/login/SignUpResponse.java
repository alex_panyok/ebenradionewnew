package com.aws3.ebenradio.model.login;



import com.aws3.ebenradio.model.networking.EbenResponse;
import com.aws3.ebenradio.model.user.User;
import com.google.gson.annotations.SerializedName;

public class SignUpResponse extends EbenResponse {

    @SerializedName("user")
   public User user;


}
