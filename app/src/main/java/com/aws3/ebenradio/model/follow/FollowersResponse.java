package com.aws3.ebenradio.model.follow;


import com.aws3.ebenradio.model.networking.EbenResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FollowersResponse extends EbenResponse {

    @SerializedName("followers")
    public  List<FollowPerson> followers;

    @SerializedName("following")
    public  List<FollowPerson> following;

}
