package com.aws3.ebenradio.model.chat;


import com.aws3.ebenradio.model.networking.EbenResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ChatResponse extends EbenResponse {

    @SerializedName("data")
    public List<UserChat> chats;

}
