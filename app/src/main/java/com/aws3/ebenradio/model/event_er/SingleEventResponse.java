package com.aws3.ebenradio.model.event_er;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Dell on 02.08.2016.
 */
public class SingleEventResponse {
    @SerializedName("Event")
    public Event event;
}
