package com.aws3.ebenradio.model.event_er;

import java.io.Serializable;

/**
 * Created by Dell on 02.08.2016.
 */
public class Banner implements Serializable {
    public String id;

    public int type;

    public String url;

    public int stream_id;

    public String image;

    public boolean viewed;
}
