package com.aws3.ebenradio.model.chat;


import com.aws3.ebenradio.model.networking.EbenResponse;
import com.google.gson.annotations.SerializedName;

public class MessagesResponse extends EbenResponse {

    @SerializedName("data")
    private MessagesData data;

    public MessagesData getData() {
        return data;
    }
}
