package com.aws3.ebenradio.presenter.fragment;

import android.content.Context;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.alarm.TimeAlarm;
import com.aws3.ebenradio.model.alarm.WeekDay;
import com.aws3.ebenradio.model.radio.RadioStation;
import com.aws3.ebenradio.util.alarm.AlarmUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.radio.RadioStationUtil;
import com.aws3.ebenradio.view.views.fragment.FragmentAlarmView;
import com.aws3.ebenradio.view.views.fragment.FragmentSleepTimerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Dell on 24.10.2016.
 */
public class FragmentAlarmPresenter extends BaseFragmentPresenter<FragmentAlarmView> {

    public FragmentAlarmPresenter(Context context) {
        super(context);
    }




    public void setUpWeekDays(List<WeekDay> days){
        StringBuilder builder = new StringBuilder();
        if(days!=null) {
            for (WeekDay day : days) {
                if (day.isChecked) {
                    if (builder.toString().isEmpty())
                        builder.append( Arrays.asList(context.getResources().getStringArray(R.array.week_day_code)).get(day.id));
                    else
                        builder.append(String.format(context.getString(R.string.week_day_with_coma),  Arrays.asList(context.getResources().getStringArray(R.array.week_day_code)).get(day.id)));
                }
            }

//            setUpAlarms(days);
        }
        if (isViewAttached())
            getView().setUpWeekDays(builder.toString());
    }



    public void setHourAndMinute(int hour, int minute){
        PrefUtils.setAlarmTime(new TimeAlarm(hour,minute));
    }


    public void setUpStream(RadioStation radioStation){
        PrefUtils.setAlarmStation(radioStation);
        if(isViewAttached())
            getView().setUpStream(radioStation.title);
    }




    public List<RadioStation> fillStationsArray() {
        return RadioStationUtil.getRadioStationItems(context);
    }


    public void setUpBroadcastAndSend(boolean isRepeat, boolean status){
        AlarmUtil.startAlarmBroadcast(context,isRepeat,status);
    }
}
