package com.aws3.ebenradio.presenter.fragment;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;

import com.aws3.ebenradio.model.user.UserFromSearch;

import com.aws3.ebenradio.model.user.UsersFromSearchResponse;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.view.activity.ProfilePersonActivity_;
import com.aws3.ebenradio.view.views.fragment.FragmentSearchPeopleView;


import org.androidannotations.annotations.InjectMenu;

import java.util.ArrayList;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 19.08.2016.
 */
public class FragmentSearchPeoplePresenter extends BaseFragmentPresenter<FragmentSearchPeopleView> {




    public FragmentSearchPeoplePresenter(Context context) {
        super(context);
    }

    public boolean loaded;


    private List<UserFromSearch> listUsers;

    private SearchPeople searchPeopleAsync;

    public void getPeople(int distance){
            getUsersByDistance(String.valueOf(distance));
    }



//    private void getAllPeople(){
//        subscription.add(EbenRadioRetroWorker.getInstance().getAllPeople(GeneralUtil.getBasicAuthHeader(), PrefUtils.getCookies(),"30000")
//                            .subscribeOn(Schedulers.io())
//                            .observeOn(AndroidSchedulers.mainThread())
//                            .subscribe(new Action1<UsersFromSearchResponse>() {
//                                @Override
//                                public void call(UsersFromSearchResponse usersFromSearchResponse) {
//                                        if(usersFromSearchResponse.isStatusOk()){
//                                            if(isViewAttached()){
//                                                getView().setUpSearchPeople(usersFromSearchResponse.users);
//                                            }
//                                        }
//                                    if(isViewAttached())
//                                        getView().endLoadingData();
//                                }
//                            }, new Action1<Throwable>() {
//                                @Override
//                                public void call(Throwable throwable) {
//                                    if(checkView())
//                                        getView().endLoadingData();
//                                }
//                            }));
//    }
//



    public void getUsersByDistance(String distance){
        subscription.add(EbenRadioRetroWorker.getInstance().writeClassAndMethod(this,distance).getUsersByDistance(GeneralUtil.getBasicAuthHeader(), PrefUtils.getCookies(), distance, "30000")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<UsersFromSearchResponse>() {
                    @Override
                    public void call(UsersFromSearchResponse usersFromSearchResponse) {

                        if (usersFromSearchResponse.isStatusOk()) {
                            loaded = true;
                            if (isViewAttached()) {
                                listUsers = usersFromSearchResponse.users;
                                getView().setUpSearchPeople(usersFromSearchResponse);
                            }
                        }
//                        if (checkView())
//                            getView().endLoadingData();

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
//                        if (checkView())
//                            getView().endLoadingData();
                    }
                }));
    }

    /**
     * @// TODO: 28.10.2016 Переписать на RxJava
     */
    public class SearchPeople extends AsyncTask<String,Void,List<UserFromSearch>>{

        String searchText;

        public SearchPeople(String searchText) {
            this.searchText = searchText;
        }

        @Override
        protected List<UserFromSearch> doInBackground(String... params) {
            List<UserFromSearch> searches = new ArrayList<>();
            if(listUsers!=null) {
                for (UserFromSearch user : listUsers) {
                    if (user.name.toLowerCase().contains(searchText.toLowerCase())) {
                        searches.add(user);
                    }
                }
            }
            return searches;
        }


        @Override
        protected void onPostExecute(List<UserFromSearch> userFromSearches) {
            super.onPostExecute(userFromSearches);
            if(isViewAttached())
                getView().setUpSearchPeople(new UsersFromSearchResponse(userFromSearches));
        }
    }


    public void searchPeople(String searchText){
        if(searchPeopleAsync==null){
            searchPeopleAsync = new SearchPeople(searchText);
            searchPeopleAsync.execute();
        }else {
            cancelTask();
            searchPeopleAsync = new SearchPeople(searchText);
            searchPeopleAsync.execute();
        }
    }




    private void cancelTask() {
        if (searchPeopleAsync == null) return;
        searchPeopleAsync.cancel(false);
    }









}
