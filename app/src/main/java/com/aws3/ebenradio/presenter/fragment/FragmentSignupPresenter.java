package com.aws3.ebenradio.presenter.fragment;

import android.content.Context;
import android.widget.Toast;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.app.EbenRadioApplication;
import com.aws3.ebenradio.model.event.user.LoginEvent;
import com.aws3.ebenradio.model.event.user.UnreadCountEvent;
import com.aws3.ebenradio.model.login.Male;
import com.aws3.ebenradio.model.login.SignUpBody;
import com.aws3.ebenradio.model.login.SignUpResponse;
import com.aws3.ebenradio.model.user.User;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.util.user.UserDataPref;
import com.aws3.ebenradio.view.views.fragment.FragmentLoginView;
import com.aws3.ebenradio.view.views.fragment.FragmentSignupView;
import com.hannesdorfmann.mosby.mvp.MvpActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 25.10.2016.
 */
public class FragmentSignupPresenter extends BaseFragmentPresenter<FragmentSignupView> {
    public FragmentSignupPresenter(Context context) {
        super(context);
    }



    public void setUpMaleArray(){
        List<Male> list  = new ArrayList<>();
        list.add(new Male(context.getString(R.string.choose_gender),-1));
        list.add(new Male(context.getString(R.string.male),1));
        list.add(new Male(context.getString(R.string.female), 0));
        if(isViewAttached()){
                getView().setUpMaleSpinner(list);
        }
    }


    public void setUpYearArray(){
        List<String> list  = new ArrayList<>();
        int year =  Calendar.getInstance().get(Calendar.YEAR);
        list.add(context.getString(R.string.choose_year));
        for(int  i = year;i>=year-98;i--){
            list.add(String.valueOf(i));
        }
        if(isViewAttached()){
                getView().setUpYearSpinner(list);
        }
    }



    public void signUp(String email, String fullName, String birthdayYear, String country, String gender, String password){
        subscription.add(EbenRadioRetroWorker.getInstance().writeClassAndMethod(this,email,fullName,birthdayYear,country,gender,password).signUp(GeneralUtil.getBasicAuthHeader(),new SignUpBody(email, fullName, birthdayYear, country, gender, password))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<SignUpResponse>() {
                    @Override
                    public void call(SignUpResponse signUpResponse) {
                        Toast.makeText(context, signUpResponse.message, Toast.LENGTH_SHORT).show();
                        if(signUpResponse.isStatusOk()) {
                            writeUserData(signUpResponse.user);
                            EventBus.getDefault().postSticky(new LoginEvent());
                            if (isViewAttached())
                                    getView().registrationComplete();
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                }));
    }


    private void writeUserData(User user){
        user.isFacebook = false;
        if(user.unread>0)
            EventBus.getDefault().postSticky(new UnreadCountEvent());
        EbenRadioApplication.refreshToken(context);
        UserDataPref.setAuthorized(context);
        UserDataPref.saveUserData(context, user);
        EventBus.getDefault().postSticky(new LoginEvent());
        ((MvpActivity)context).onBackPressed();
    }

}
