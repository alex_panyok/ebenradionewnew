package com.aws3.ebenradio.presenter.activity;

import android.content.Context;

import com.aws3.ebenradio.model.event_er.Event;
import com.aws3.ebenradio.model.event_er.SingleEventResponse;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.view.views.activity.SingleEventActivityView;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 03.11.2016.
 */
public class SingleEventActivityPresenter extends BaseActivityPresenter<SingleEventActivityView> {
    public SingleEventActivityPresenter(Context context) {
        super(context);
    }



    private Event event;

    public void getEvent(String id){
        subscription.add(EbenRadioRetroWorker.getInstance().writeClassAndMethod(this,id).getEvent(id, GeneralUtil.getDeviceId(context))
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new Action1<SingleEventResponse>() {
                                    @Override
                                    public void call(SingleEventResponse singleEventResponse) {
                                        if(isViewAttached()) {
                                            event = singleEventResponse.event;
                                            getView().getEvent(singleEventResponse.event);
                                        }
                                    }
                                }, new Action1<Throwable>() {
                                    @Override
                                    public void call(Throwable throwable) {

                                    }
                                }));
    }



    public Event getEvent() {
        return event;
    }
}
