package com.aws3.ebenradio.presenter.fragment;

import android.content.Context;
import android.widget.Toast;

import com.aws3.ebenradio.model.networking.EbenResponse;
import com.aws3.ebenradio.model.user.ChangePasswordBody;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.view.views.fragment.FragmentChangePassView;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 08.11.2016.
 */
public class FragmentChangePassPresenter extends BaseFragmentPresenter<FragmentChangePassView> {
    public FragmentChangePassPresenter(Context context) {
        super(context);
    }



    public void changePassword(String oldPassword, String newPassword){
        subscription.add(EbenRadioRetroWorker.getInstance().writeClassAndMethod(this, oldPassword, newPassword).changePassword(GeneralUtil.getBasicAuthHeader(), PrefUtils.getCookies(), new ChangePasswordBody(oldPassword, newPassword))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<EbenResponse>() {
                                @Override
                                public void call(EbenResponse ebenResponse) {
                                        Toast.makeText(context, ebenResponse.message, Toast.LENGTH_SHORT).show();
                                        if(ebenResponse.status.equals("0") && isViewAttached()) {
                                            getView().changePass();
                                        }
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {

                                }
                            }));
    }
}
