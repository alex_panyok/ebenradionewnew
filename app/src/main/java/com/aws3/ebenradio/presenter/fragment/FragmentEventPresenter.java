package com.aws3.ebenradio.presenter.fragment;

import android.content.Context;

import com.aws3.ebenradio.model.event_er.Event;
import com.aws3.ebenradio.model.event_er.EventResponse;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.view.activity.SingleEventActivity;
import com.aws3.ebenradio.view.activity.SingleEventActivity_;
import com.aws3.ebenradio.view.views.fragment.FragmentEventView;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 03.11.2016.
 */
public class FragmentEventPresenter extends BaseFragmentPresenter<FragmentEventView> {


    public FragmentEventPresenter(Context context) {
        super(context);
    }



    public void getEvents(){
        subscription.add(EbenRadioRetroWorker.getInstance().writeClassAndMethod(this).getEvents(GeneralUtil.getDeviceId(context))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<EventResponse>() {
                                @Override
                                public void call(EventResponse eventResponse) {
                                        if(isViewAttached())
                                            getView().setEvents(eventResponse.events);
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {

                                }
                            }));
    }


    public void startDetail(Event event){
        SingleEventActivity_.intent(context).extra(ID, event.id).start();
    }




}
