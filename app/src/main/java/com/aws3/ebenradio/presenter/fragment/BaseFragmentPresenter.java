package com.aws3.ebenradio.presenter.fragment;

import android.content.Context;

import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.util.colors.ColorsUtil;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.aws3.ebenradio.util.keys.EbenRetroKeys;
import com.aws3.ebenradio.util.skins.ColorUtil;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.view.activity.ProfilePersonActivity_;
import com.aws3.ebenradio.view.views.fragment.BaseFragmentView;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by Dell on 11.10.2016.
 */
public  class BaseFragmentPresenter<V extends BaseFragmentView> extends MvpBasePresenter<V> implements EbenKeys, EbenRetroKeys{

    protected Context context;

    protected CompositeSubscription subscription;



    public BaseFragmentPresenter(Context context) {
        this.context = context;
        subscription = new CompositeSubscription();
    }


    public void unsubscribeSubscription(){
        if(!subscription.isUnsubscribed()){
            subscription.unsubscribe();
            subscription = new CompositeSubscription();
        }
    }


    public Skin getSkin() {
        return SkinPrefUtils.getSkin();
    }

    public void setUpColor(){
        if(isViewAttached())
            getView().setUpColor(ColorUtil.getColorById(getSkin().color, context), ColorsUtil.makeLighter(getSkin().color,ColorsUtil.LIGHT_PERCENT,context));
    }


    public void startPersonProfile(String userId){
        ProfilePersonActivity_.intent(context).extra(ID, userId).start();
    }




}
