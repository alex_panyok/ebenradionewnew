package com.aws3.ebenradio.presenter.fragment;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.aws3.ebenradio.app.EbenRadioApplication;
import com.aws3.ebenradio.model.event.user.LoginEvent;
import com.aws3.ebenradio.model.event.user.UnreadCountEvent;
import com.aws3.ebenradio.model.login.LoginBody;
import com.aws3.ebenradio.model.login.SocialLoginBody;
import com.aws3.ebenradio.model.user.User;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.aws3.ebenradio.util.keys.EbenRetroKeys;
import com.aws3.ebenradio.util.user.UserDataPref;
import com.aws3.ebenradio.view.views.fragment.FragmentLoginView;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.hannesdorfmann.mosby.mvp.MvpActivity;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 25.10.2016.
 */
public class FragmentLoginPresenter extends BaseFragmentPresenter<FragmentLoginView> implements EbenKeys, EbenRetroKeys {
    public FragmentLoginPresenter(Context context) {
        super(context);
    }





    public void validate(){
        if(isViewAttached())
            getView().validate();
    }


    public void logIn(String email, String name){
        subscription.add(EbenRadioRetroWorker.getInstance().writeClassAndMethod(this, email, name).logIn(GeneralUtil.getBasicAuthHeader(),new LoginBody(email, name))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<User>() {
                    @Override
                    public void call(User user) {
                        if(user.isStatusOk()){

                            writeUserData(user, false);
                        }else
                            Toast.makeText(context, user.message, Toast.LENGTH_SHORT).show();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                }));

    }


    public void socialLogin(String facebookId, String email, String name, String gender){
        subscription.add(EbenRadioRetroWorker.getInstance().writeClassAndMethod(this, facebookId, email, name, gender).socialLogin(GeneralUtil.getBasicAuthHeader(), new SocialLoginBody(facebookId, name==null?"":name,gender==null?"":gender,email==null?"":email))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<User>() {
                    @Override
                    public void call(User loginResponse) {
                        if(loginResponse.isStatusOk()) {
                            if(loginResponse.social_id!=null)
                                writeUserData(loginResponse, true);
                            else
                                if(isViewAttached())
                                    getView().performFacebookClick();
                        }else
                            Toast.makeText(context, loginResponse.message, Toast.LENGTH_SHORT).show();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                }));

    }





    public CallbackManager initFacebook(){
       CallbackManager callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code
                        getFacebookData(loginResult.getAccessToken());


                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });

        return callbackManager;
    }



    public void getFacebookData(final AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        // Insert your code here

                        signOutFacebook();
                        String facebookId = null;
                        String name= null;
                        String email= null;
                        String gender = null;
                        try {
                            facebookId = object.getString(ID);
                            name = object.getString(NAME);
                            email = object.getString(EMAIL);
                            facebookId = object.getString(ID);
                            gender = object.getString(GENDER);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        socialLogin(facebookId,email, name,gender);

                    }

                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender");
        request.setParameters(parameters);
        request.executeAsync();
    }



    private void signOutFacebook() {
        LoginManager.getInstance().logOut();
    }



    private void writeUserData(User user, boolean isFacebook){
        if(user.unread>0)
            EventBus.getDefault().postSticky(new UnreadCountEvent());
        user.isFacebook = isFacebook;
        EbenRadioApplication.refreshToken(context);
        UserDataPref.setAuthorized(context);
        UserDataPref.saveUserData(context, user);
        EventBus.getDefault().postSticky(new LoginEvent());
        ((MvpActivity)context).onBackPressed();
    }

}
