package com.aws3.ebenradio.presenter.activity;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.aws3.ebenradio.model.rss.Item;
import com.aws3.ebenradio.view.views.activity.DetailActivityNewsView;
import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.hannesdorfmann.mosby.mvp.MvpPresenter;

/**
 * Created by Alex on 29.10.2016.
 */
public class DetailNewsActivityPresenter extends BaseActivityPresenter<DetailActivityNewsView> {
    public DetailNewsActivityPresenter(Context context) {
        super(context);
    }


    public void getItem(Intent intent){
        if(isViewAttached())
            getView().setItem((Item) intent.getSerializableExtra(ARTICLE));
    }


    public void back(){
        if(isViewAttached())
            getView().back();
    }
}
