package com.aws3.ebenradio.presenter.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.radio.UpdatePlayerInfoEvent;
import com.aws3.ebenradio.model.event.user.UpdateProfileEvent;
import com.aws3.ebenradio.model.login.Male;
import com.aws3.ebenradio.model.user.User;
import com.aws3.ebenradio.model.user.UserUpdatedProfileResponse;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.file.RealPathUtil;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.util.general.ToastUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.user.UserDataPref;
import com.aws3.ebenradio.view.views.activity.EditProfileActivityView;
import com.hannesdorfmann.mosby.mvp.MvpActivity;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 28.10.2016.
 */
public class EdiProfileActivityPresenter extends BaseActivityPresenter<EditProfileActivityView>{

    private User user ;


    private Uri takePhotoUri;


    private ArrayList<Male> genderList;

    private ArrayList<String> yearList;

    int genderPos;

    private boolean isPhotoUpdated;


    public EdiProfileActivityPresenter(Context context) {
        super(context);
    }






    public void setUpUserData(){
        user  = UserDataPref.getUserFromPrefs(context);
        for(int i = 0; i< genderList.size();i++){
            if( genderList.get(i).type==user.gender){
                genderPos = i;
                break;
            }
        }
        if(isViewAttached()){
            getView().setUpUser(user, Arrays.asList(context.getResources().getStringArray(R.array.countries)).indexOf(user.country), genderPos,yearList.indexOf(user.birthday_year));
        }
    }



    public void setUpMaleArray(){
        genderList  = new ArrayList<>();
        genderList.add(new Male(context.getString(R.string.choose_gender),-1));
        genderList.add(new Male(context.getString(R.string.male),1));
        genderList.add(new Male(context.getString(R.string.female), 0));
        if(isViewAttached()){
            getView().setUpMaleSpinner(genderList);
        }

    }




    public void setUpYearArray(){
        yearList  = new ArrayList<>();
        yearList.add(context.getString(R.string.choose_year));
        int year =  Calendar.getInstance().get(Calendar.YEAR);
        for(int  i = year;i>=year-98;i--){
            yearList.add(String.valueOf(i));
        }
        if(isViewAttached()){
            getView().setUpYearSpinner(yearList);
        }
    }


    public void takePhoto(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            File ebenImageDirectory = new File(Environment.getExternalStorageDirectory(), "eben_images");
            if(!ebenImageDirectory.exists()){
                ebenImageDirectory.mkdirs();
            }else {
                if(ebenImageDirectory.listFiles().length>=5){
                    for(File file:ebenImageDirectory.listFiles()){
                        file.delete();
                    }
                }
            }
            File destination = new File(Environment
                    .getExternalStorageDirectory(), "eben_images/"+"eben"+System.currentTimeMillis()/1000+ ".jpg");

            takePhotoUri = Uri.fromFile(destination);
            takePictureIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
                    takePhotoUri);
            takePictureIntent.putExtra("return-data", true);
            ((MvpActivity)context).startActivityForResult(takePictureIntent, TAKE_PHOTO_CODE);

        }
    }


    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        ((MvpActivity)context).startActivityForResult(intent, PICK_PHOTO);
    }

    public Uri getTakePhotoUri() {
        return takePhotoUri;
    }

    public void getPhoto(Uri uri){
        isPhotoUpdated = true;
        try {
            user.photo = GeneralUtil.encodeToBase64(getImage(RealPathUtil.getRealPath(context, uri)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }





    public void deletePhoto(){
        isPhotoUpdated = true;
        user.photo =  GeneralUtil.encodeToBase64(BitmapFactory.decodeResource(context.getResources(),R.drawable.no_photo_bg));
    }


    public Bitmap getImage(String path) throws IOException
    {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        int srcWidth = options.outWidth;
        int srcHeight = options.outHeight;
        int[] newWH =  new int[2];
        newWH[0] = srcWidth/2;
        newWH[1] = (newWH[0]*srcHeight)/srcWidth;

        int inSampleSize = 1;
        while(srcWidth / 2 >= newWH[0]){
            srcWidth /= 2;
            srcHeight /= 2;
            inSampleSize *= 2;
        }

        //      float desiredScale = (float) newWH[0] / srcWidth;
        // Decode with inSampleSize
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inSampleSize = inSampleSize;
        options.inScaled = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        Bitmap sampledSrcBitmap = BitmapFactory.decodeFile(path,options);
            ExifInterface ei = new ExifInterface(path);

            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            Matrix matrix = new Matrix();
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    matrix.postRotate(90);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    matrix.postRotate(180);
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    matrix.postRotate(270);
                    break;
            }

        return Bitmap.createBitmap(sampledSrcBitmap , 0, 0, sampledSrcBitmap .getWidth(), sampledSrcBitmap.getHeight(), matrix, true);
    }

    public void updateUserProfile(String name, String email,String country, String about, int gender, String year){

        user.full_name = name;
        user.country = country;
        user.email = email;
        user.gender = gender;
        user.about_you = about;
        user.birthday_year = year;
        if(!isPhotoUpdated)
            user.photo="";
//        if(isViewAttached())
//            getView().startLoadingData();
        subscription.add(EbenRadioRetroWorker.getInstance().writeClassAndMethod(this,name,email,country, about, gender,year).updateUserProfile(GeneralUtil.getBasicAuthHeader(), PrefUtils.getCookies(),user)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<UserUpdatedProfileResponse>() {
                    @Override
                    public void call(UserUpdatedProfileResponse userUpdatedProfileResponse) {
                        if(userUpdatedProfileResponse.status.equals("0")){
                            User user =  userUpdatedProfileResponse.user;
                            user.isFacebook = EdiProfileActivityPresenter.this.user.isFacebook;
                            UserDataPref.saveUserData(context, user);
                            EventBus.getDefault().postSticky(new UpdateProfileEvent(userUpdatedProfileResponse.user));
                        }
                        ToastUtil.toast(context, userUpdatedProfileResponse.message);
//                        if(checkView()) {
//                            getView().updateComplete(userUpdatedProfileResponse.status.equals("0"), userUpdatedProfileResponse.user);
//                            getView().endLoadingData();
//                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
//                        if(checkView()) {
//                            getView().updateComplete(false, null);
//                            getView().endLoadingData();
//                        }
                    }

                }));
    }
}
