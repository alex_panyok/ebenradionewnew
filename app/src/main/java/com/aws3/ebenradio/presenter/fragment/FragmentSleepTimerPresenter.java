package com.aws3.ebenradio.presenter.fragment;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.aws3.ebenradio.model.alarm.Time;
import com.aws3.ebenradio.model.event.alarm.TimerUpdateEvent;
import com.aws3.ebenradio.model.event.alarm.TurnOffMusicEvent;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.view.views.fragment.FragmentAlarmTimerView;
import com.aws3.ebenradio.view.views.fragment.FragmentSleepTimerView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 24.10.2016.
 */
public class FragmentSleepTimerPresenter extends BaseFragmentPresenter<FragmentSleepTimerView> {

    public FragmentSleepTimerPresenter(Context context) {
        super(context);
    }


    private int time;

    private int timeLeft;

    public Handler handler;

    private Runnable runnableUpdate;

    private Runnable runnable;

    private int[] times  = new int[]{5,15,30,45,60};

    public boolean isTimerStarted = false;


    public void setUpTime(int time){
        PrefUtils.setTimer(time);
        this.time =time;
        if(isViewAttached())
            getView().newTime(time);
    }



    public void turnOnTimer(){
        PrefUtils.setTimerStatus(true);
        timeLeft = time;
        if(isViewAttached())
            getView().updateTimer(timeLeft);
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                EventBus.getDefault().postSticky(new TurnOffMusicEvent());
            }
        };

        runnableUpdate = new Runnable() {
            @Override
            public void run() {
                timeLeft--;
                EventBus.getDefault().postSticky(new TimerUpdateEvent(timeLeft));
                if(timeLeft>0) {
                    handler.postDelayed(runnableUpdate, 60000);
                }
            }
        };
        handler.postDelayed(runnableUpdate,60000);
        handler.postDelayed(runnable, time  * 60000);


    }


    public List<Time> generateListTimer(){
        List<Time> list = new ArrayList<>();
        for(int time : times) {
            list.add(new Time(time== PrefUtils.getTimer(),!PrefUtils.getTimerStatus(), time));
        }
        return list;
    }


    public void turnOffTimer(){
        isTimerStarted = false;
        PrefUtils.setTimerStatus(false);
        if(handler!=null) {
            handler.removeCallbacks(runnable);
            handler.removeCallbacks(runnableUpdate);
        }

    }
}
