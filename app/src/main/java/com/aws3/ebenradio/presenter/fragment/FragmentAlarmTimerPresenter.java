package com.aws3.ebenradio.presenter.fragment;

import android.content.Context;

import com.aws3.ebenradio.view.views.fragment.FragmentAlarmTimerView;

/**
 * Created by Dell on 24.10.2016.
 */
public class FragmentAlarmTimerPresenter extends BaseFragmentPresenter<FragmentAlarmTimerView> {

    public FragmentAlarmTimerPresenter(Context context) {
        super(context);
    }
}
