package com.aws3.ebenradio.presenter.fragment;

import android.content.Context;
import android.content.Intent;

import com.aws3.ebenradio.model.event.user.LoginEvent;
import com.aws3.ebenradio.model.follow.FollowPerson;
import com.aws3.ebenradio.model.follow.FollowResponse;
import com.aws3.ebenradio.model.login.LogoutBody;
import com.aws3.ebenradio.model.networking.EbenResponse;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.service.ChatService;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.util.general.ToastUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.user.UserDataPref;
import com.aws3.ebenradio.view.activity.EditProfileActivity_;
import com.aws3.ebenradio.view.activity.ProfilePersonActivity;
import com.aws3.ebenradio.view.activity.ProfilePersonActivity_;
import com.aws3.ebenradio.view.activity.SearchPeopleActivity;
import com.aws3.ebenradio.view.activity.SearchPeopleActivity_;
import com.aws3.ebenradio.view.views.fragment.FragmentProfileView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 26.10.2016.
 */
public class FragmentProfilePresenter extends BaseFragmentPresenter<FragmentProfileView> {
    public FragmentProfilePresenter(Context context) {
        super(context);
    }




    public void getFollow(){
        EbenRadioRetroWorker.getInstance().writeClassAndMethod(this);
        getFollowers();
        getFollowing();
    }




    public void getFollowers(){
        subscription.add(EbenRadioRetroWorker.getInstance().getFollowers(GeneralUtil.getBasicAuthHeader(), PrefUtils.getCookies())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<FollowResponse>() {
                    @Override
                    public void call(FollowResponse followResponse) {
                        if (followResponse.isStatusOk()) {
                            if (isViewAttached()) {
                                getView().setUpFollowers(followResponse);
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                }));
    }

    public void getFollowing(){
        subscription.add(EbenRadioRetroWorker.getInstance().getFollowing(GeneralUtil.getBasicAuthHeader(),PrefUtils.getCookies())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<FollowResponse>() {
                    @Override
                    public void call(FollowResponse followResponse) {
                        if(followResponse.isStatusOk()){
                            if(isViewAttached()){
                                getView().setUpFollowing(followResponse);
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                }));
    }



    public void startPersonProfile(String id){
        ProfilePersonActivity_.intent(context).extra(ID,id).start();
    }


    public void startSearchPeople(){
        SearchPeopleActivity_.intent(context).start();
    }





    public void selectFollowers(){
        if(isViewAttached())
            getView().selectFollowers();
    }


    public void selectFollowing(){
        if(isViewAttached())
            getView().selectFollowing();
    }

    public void logOut(){
        logOutRequest();
//        context.stopService(new Intent(context, ChatService.class));
        UserDataPref.setNotAuthorized(context);
        EventBus.getDefault().postSticky(new LoginEvent());
    }

    public void logOutRequest(){
        subscription.add(EbenRadioRetroWorker.getInstance().writeClassAndMethod(this).logOut(GeneralUtil.getBasicAuthHeader(), PrefUtils.getCookies(), new LogoutBody(GeneralUtil.getDeviceId(context)))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<EbenResponse>() {
                                @Override
                                public void call(EbenResponse ebenResponse) {
                                    ToastUtil.toast(context, ebenResponse.message);
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {

                                }
                            }));
    }


    public void edit(){
        EditProfileActivity_.intent(context).start();
    }


}
