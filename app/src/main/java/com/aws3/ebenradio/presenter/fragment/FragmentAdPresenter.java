package com.aws3.ebenradio.presenter.fragment;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.webkit.URLUtil;
import android.widget.Toast;

import com.aws3.ebenradio.model.ads.Ad;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.view.views.fragment.FragmentAdView;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 04.11.2016.
 */
public class FragmentAdPresenter extends BaseFragmentPresenter<FragmentAdView> {
    public FragmentAdPresenter(Context context) {
        super(context);
    }




    public void getAds(){
        subscription.add(EbenRadioRetroWorker.getInstance().getAds("http://api.ebenradio.com/api/ebenradio")
                            .repeatWhen(new Func1<Observable<? extends Void>, Observable<?>>() {
                                @Override
                                public Observable<?> call(Observable<? extends Void> completed) {
                                    return completed.delay(20, TimeUnit.SECONDS);
                                }
                            })
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<ResponseBody>() {
                                @Override
                                public void call(ResponseBody responseBody) {
                                    if(isViewAttached())
                                        try {
                                            getView().setAd(parseAd(responseBody.string()));
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
//                                    try {
//                                        currentAd = parseAd(responseBody.string());
//                                    } catch (IOException e) {
//                                        e.printStackTrace();
//                                    }
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {

                                }
                            }));
    }


    private Ad parseAd(String ad){
        String adArr[] = ad.split(";");
        return new Ad(adArr[1], adArr[2]);
    }


    public void startUrl(String url){
        if(url.isEmpty())
            return;
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URLUtil.isValidUrl(url)?url:"http://"+url)));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, "Can`t open this url", Toast.LENGTH_SHORT).show();
        }
    }


}
