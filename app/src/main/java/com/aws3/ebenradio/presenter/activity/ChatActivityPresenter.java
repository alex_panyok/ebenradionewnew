package com.aws3.ebenradio.presenter.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.aws3.ebenradio.model.chat.JoinEmit;
import com.aws3.ebenradio.model.chat.MessageBody;
import com.aws3.ebenradio.model.chat.MessageItem;
import com.aws3.ebenradio.model.chat.MessageResponse;
import com.aws3.ebenradio.model.chat.MessagesResponse;
import com.aws3.ebenradio.model.event.chat.RemoveUnreadDotEvent;
import com.aws3.ebenradio.model.event.chat.UpdateChatListEvent;
import com.aws3.ebenradio.model.event.user.UpdateProfileEvent;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.view.views.activity.ChatActivityView;
import com.google.gson.Gson;
import com.paginate.Paginate;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 31.10.2016.
 */
public class ChatActivityPresenter extends BaseActivityPresenter<ChatActivityView>  {
    public ChatActivityPresenter(Context context) {
        super(context);
    }



    private int currentPage;

    private int totalPage;

    private String userId;

    private List<MessageItem> messageItems;

    int page = 1;


    private Handler handler;

    private Runnable updateRunnable;

    public void getMessages(String userId, int page, final boolean isNewMessage){
        this.userId = userId;
        subscription.add(EbenRadioRetroWorker.getInstance().getMessages(GeneralUtil.getBasicAuthHeader(), PrefUtils.getCookies(), userId, page)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<MessagesResponse>() {
                                @Override
                                public void call(MessagesResponse messagesResponse) {
                                        if(messagesResponse.isStatusOk() && isViewAttached()) {
                                            initHandler();
                                            EventBus.getDefault().postSticky(new UpdateChatListEvent());
                                            EventBus.getDefault().postSticky(new RemoveUnreadDotEvent());
                                            if (!isNewMessage) {
                                                currentPage = messagesResponse.getData().page;
                                                totalPage = messagesResponse.getData().totalPager;
                                                getView().setMessages(messagesResponse.getData(), currentPage == totalPage);
                                            } else {
                                                getView().addNewMessage(messagesResponse.getData().messages.get(0));
                                            }
                                        }
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {

                                }
                            }));
    }


    public void initHandler(){
        if(handler==null) {
            messageItems = new ArrayList<>();
            handler = new Handler();
            updateRunnable = new Runnable() {
                @Override
                public void run() {
                    updateMessages(userId);
                    handler.postDelayed(updateRunnable, 10000);
                }
            };
            handler.post(updateRunnable);
        }
    }


    public void removeRunnable(){
        if(handler!=null)
            handler.removeCallbacks(updateRunnable);
    }









    public void updateMessages(final String userId){
        subscription.add(EbenRadioRetroWorker.getInstance().getMessages(GeneralUtil.getBasicAuthHeader(), PrefUtils.getCookies(), userId, page)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<MessagesResponse>() {
                    @Override
                    public void call(MessagesResponse messagesResponse) {
                        messageItems.addAll(messagesResponse.getData().messages);
                        page = page+1;

                        if(currentPage<page){
                            page=1;
                            if(isViewAttached())
                                try {
                                    getView().updateMessages(generateTimes(messageItems,currentPage==messagesResponse.getData().totalPager));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            messageItems.clear();
                        }else {
                            updateMessages(userId);
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                }));
    }






    public void sendMessage(final MessageBody messageBody){
        if(isViewAttached())
            getView().messageSent();
        subscription.add(EbenRadioRetroWorker.getInstance().writeClassAndMethod(this,messageBody).sendMessage(GeneralUtil.getBasicAuthHeader(), PrefUtils.getCookies(), messageBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<MessageResponse>() {
                    @Override
                    public void call(MessageResponse messageResponse) {
                        if(isViewAttached())
                            getView().progressMessageStop();
                            if(messageResponse.isStatusOk() && isViewAttached()) {
                                EventBus.getDefault().postSticky(new UpdateChatListEvent());
                                getView().addNewMessage(messageResponse.getMessageData().getMessageItem());
                            }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        if(isViewAttached())
                            getView().progressMessageStop();
                    }
                }));
    }


    @SuppressLint("SimpleDateFormat")
    public List<MessageItem> generateTimes(List<MessageItem> list, boolean isLast) throws ParseException {
        for(int i=0;i<list.size();i++) {
            if (i < list.size() - 1) {
                if (list.get(i).date == null && list.get(i + 1).date == null && i < list.size() - 1) {
                    Calendar calendarAfter = Calendar.getInstance();
                    calendarAfter.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(list.get(i).postDate));
                    Calendar calendarBefore = Calendar.getInstance();
                    calendarBefore.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(list.get(i + 1).postDate));
                    if (calendarAfter.get(Calendar.DAY_OF_YEAR) != calendarBefore.get(Calendar.DAY_OF_YEAR))
                        list.add(i + 1, new MessageItem(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(list.get(i).postDate)));
                }
            }
        }
        if(isLast)
            list.add(list.size(),new MessageItem(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(list.get(list.size()-1).postDate)));
        return list;
    }


    public void loadMore(int page){
        if(page<=totalPage){
            getMessages(userId, page, false);
        }
    }



}
