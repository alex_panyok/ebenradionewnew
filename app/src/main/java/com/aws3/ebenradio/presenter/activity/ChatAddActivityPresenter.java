package com.aws3.ebenradio.presenter.activity;

import android.content.Context;

import com.aws3.ebenradio.model.chat.UserChat;
import com.aws3.ebenradio.model.follow.FollowPerson;
import com.aws3.ebenradio.model.follow.FollowResponse;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.view.activity.ChatActivity_;
import com.aws3.ebenradio.view.views.activity.ChatAddActivityView;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 03.11.2016.
 */
public class ChatAddActivityPresenter extends BaseActivityPresenter<ChatAddActivityView> {
    public ChatAddActivityPresenter(Context context) {
        super(context);
    }


    public void getFollowing(){
        subscription.add(EbenRadioRetroWorker.getInstance().writeClassAndMethod(this).getFollowing(GeneralUtil.getBasicAuthHeader(), PrefUtils.getCookies())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<FollowResponse>() {
                    @Override
                    public void call(FollowResponse followResponse) {
                        if(followResponse.isStatusOk()){
                            if(isViewAttached()){
                                getView().setUpFollowing(followResponse);
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                }));
    }





    public void startChat(FollowPerson followPerson){
        ChatActivity_.intent(context).extra(ID,followPerson.id).extra(NAME, followPerson.name).start();
    }

}
