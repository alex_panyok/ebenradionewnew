package com.aws3.ebenradio.presenter.fragment;

import android.content.Context;
import android.media.audiofx.Equalizer;
import android.media.audiofx.PresetReverb;
import android.util.Log;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.radio.EqualizerUtil;
import com.aws3.ebenradio.view.views.fragment.FragmentEqualizerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 19.10.2016.
 */
public class FragmentEqualizerPresenter extends BaseFragmentPresenter<FragmentEqualizerView> {



    private Equalizer equalizer;

    private int upperEqualizer;

    private int lowerEqualizer;

    private List<String> presets;



    public FragmentEqualizerPresenter(Context context) {
        super(context);
    }



    public Equalizer getEqualizer() {
        return equalizer;
    }

    public void setEqualizer(Equalizer equalizer) {
        this.equalizer = equalizer;
        lowerEqualizer = EqualizerUtil.getLowerEqualizerLevel(equalizer);
        upperEqualizer = EqualizerUtil.getUpperEqualizerLevel(equalizer);
        setUpEqualizerSliders();
        setUpPresets();
        setUpEqualizerSwitch();
    }


    public void setUpEqualizerSliders(){
        if(isViewAttached()){
                getView().setUpEqualizerSliders(upperEqualizer-lowerEqualizer, new int[]{equalizer.getBandLevel((short)0)-lowerEqualizer,equalizer.getBandLevel((short)1)-lowerEqualizer,equalizer.getBandLevel((short)2)-lowerEqualizer,equalizer.getBandLevel((short)3)-lowerEqualizer,equalizer.getBandLevel((short)4)-lowerEqualizer});
        }
    }


    public void setUpPresets(){
        if(equalizer!=null){
            if(getView()!=null){
                presets = new ArrayList<>();
                presets.add(context.getString(R.string.custom));
                for(int i=0; i<equalizer.getNumberOfPresets();i++){
                    presets.add(equalizer.getPresetName((short)i));
                }
                getView().setUpPresets(presets);
            }
        }
    }


    public void setEqualizerLevel(int band, int level){
        if(equalizer!=null){
            equalizer.setBandLevel((short)band,(short)(level+lowerEqualizer));
        }
    }




    public void saveCustomPreset(){
        EqualizerUtil.saveEqualizerCustomPreset(equalizer);
    }

    public void saveCurrentLevels(){
        EqualizerUtil.saveEqualizerCurrentLevel(equalizer);
    }




    public void changeEqualizerStatus(boolean enable){
        if(equalizer!=null ){
            PrefUtils.setEqualizerEnabled(enable);
            try {
                equalizer.setEnabled(enable);
            } catch (IllegalStateException e) {
                e.printStackTrace();
            }
        }
    }

    public void usePreset(int i) {
        PrefUtils.setSelectedPreset(i);

        if (i > 0)
            equalizer.usePreset((short) (i - 1));
        else
            for(int n=0; n< PrefUtils.getCustomPreset().length;n++)
                setEqualizerLevel(n,PrefUtils.getCustomPreset()[n]);
        if (getView() != null)
            getView().setSlidersProgress(new int[]{equalizer.getBandLevel((short) 0) - lowerEqualizer, equalizer.getBandLevel((short) 1) - lowerEqualizer, equalizer.getBandLevel((short) 2) - lowerEqualizer, equalizer.getBandLevel((short) 3) - lowerEqualizer, equalizer.getBandLevel((short) 4) - lowerEqualizer});
        saveCurrentLevels();
    }

    public void setUpEqualizerSwitch(){
        if(isViewAttached()){
                getView().setUpEqualizerStatus(equalizer.getEnabled());
        }
    }
}
