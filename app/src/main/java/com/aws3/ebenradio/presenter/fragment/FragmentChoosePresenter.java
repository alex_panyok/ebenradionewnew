package com.aws3.ebenradio.presenter.fragment;

import android.app.Dialog;
import android.content.Context;

import com.aws3.ebenradio.model.chat.ChatResponse;
import com.aws3.ebenradio.model.chat.UserChat;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.view.activity.ChatActivity;
import com.aws3.ebenradio.view.activity.ChatActivity_;
import com.aws3.ebenradio.view.fragment.DialogDeleteChat;
import com.aws3.ebenradio.view.fragment.DialogDeleteChat_;
import com.aws3.ebenradio.view.views.fragment.FragmentChooseView;
import com.hannesdorfmann.mosby.mvp.MvpActivity;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.observers.SafeCompletableSubscriber;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 31.10.2016.
 */
public class FragmentChoosePresenter extends BaseFragmentPresenter<FragmentChooseView> {
    public FragmentChoosePresenter(Context context) {
        super(context);
    }



    public void getChats(){
        subscription.add(EbenRadioRetroWorker.getInstance().writeClassAndMethod(this).getChats(GeneralUtil.getBasicAuthHeader(), PrefUtils.getCookies())
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<ChatResponse>() {
                                @Override
                                public void call(ChatResponse chatResponse) {
                                    if(chatResponse.isStatusOk() && isViewAttached())
                                        getView().chooseFragment(chatResponse.chats);
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {

                                }
                            }));

    }


    public void startChat(UserChat userChat){
        userChat.unreadCount=0;
        if(isViewAttached())
            getView().selectChat(userChat);
        ChatActivity_.intent(context).extra(ID,userChat.userWithId).extra(NAME, userChat.userName).start();
    }


    public boolean deleteChat(UserChat userChat){
        DialogDeleteChat_.builder().arg(ID, userChat.userWithId).build().show(((MvpActivity)context).getSupportFragmentManager(), DialogDeleteChat.class.getName());
        return true;
    }
}
