package com.aws3.ebenradio.presenter.activity;

import android.content.Context;

import com.aws3.ebenradio.view.views.activity.SearchPeopleActivityView;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

/**
 * Created by Dell on 27.10.2016.
 */
public class SearchPeopleActivityPresenter extends BaseActivityPresenter<SearchPeopleActivityView> {
    public SearchPeopleActivityPresenter(Context context) {
        super(context);
    }
}
