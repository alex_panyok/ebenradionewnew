package com.aws3.ebenradio.presenter.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.net.Uri;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.radio.TogglePlayEvent;
import com.aws3.ebenradio.model.radio.RadioStation;
import com.aws3.ebenradio.service.RadioService;
import com.aws3.ebenradio.util.file.FileUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.radio.RadioStationUtil;
import com.aws3.ebenradio.util.skins.BackgroundUtil;
import com.aws3.ebenradio.view.activity.MainActivity;
import com.aws3.ebenradio.view.views.fragment.FragmentPlayerView;
import com.facebook.share.ShareApi;
import com.facebook.share.model.ShareContent;
import com.facebook.share.model.ShareHashtag;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareMediaContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.tweetcomposer.Card;
import com.twitter.sdk.android.tweetcomposer.ComposerActivity;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
/**
 * Created by Dell on 12.10.2016.
 */
public class FragmentPlayerPresenter extends BaseFragmentPresenter<FragmentPlayerView> {

    public FragmentPlayerPresenter(Context context) {
        super(context);
    }


    public void togglePlay(boolean play){

        serviceToggle();
    }

     private void serviceToggle(){
         Intent intent = new Intent(context, RadioService.class);
         intent.setAction(RadioService.ACTION_TOGGLE);
         context.startService(intent);
     }


    public void prevStation(){
        ((MainActivity)context).getPresenter().prevStation();
    }


    public void share(int type){
        if(isViewAttached())
            switch (type){
                case FACEBOOK:
                    getView().shareFacebook();
                    break;

                case TWITTER:
                    getView().shareTwitter();
                    break;

                case NORMAL:
                    getView().share();
                    break;
            }
    }


    public void nextStation(){
        ((MainActivity)context).getPresenter().nextStation();
    }

    public void togglePlayButton(TogglePlayEvent event){
        if(isViewAttached())
            getView().togglePlayButton(event.isPlay);
    }


    public void changeVolume(int value) {
        AudioManager audioManager =
                (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                value, 0);
    }

    public int getMaxValueVolume() {
        return ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getStreamMaxVolume(AudioManager.STREAM_MUSIC);
    }

    public int getCurrentValueVolume() {
        return ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).getStreamVolume(AudioManager.STREAM_MUSIC);
    }


    public void shareFacebook(String url, String title, String description, String imageUrl){
        ShareLinkContent content = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse(context.getString(R.string.url)))
                .setContentTitle(title)
                .setContentDescription(description)
                .setImageUrl(Uri.parse(imageUrl.isEmpty()?RadioStationUtil.getUrlImageStationById(PrefUtils.getStation().id):imageUrl))
                .setShareHashtag(new ShareHashtag.Builder()
                        .setHashtag(context.getString(R.string.hashtag))
                        .build())
                .build();

       new ShareDialog((MvpActivity)context).show(content);
    }


    public void shareTwitter(String url, String description, String imageUrl){
        try {
            new TweetComposer.Builder(context)
                    .text(description)
                    .image(imageUrl.isEmpty()? FileUtil.getUriToResource(context, BackgroundUtil.getRadioPlaceholder(context)):getImageUri(context, getBitmapFromURL(imageUrl)))
                    .url(new URL(url))
                    .show();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }


    private Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private Bitmap getBitmapFromURL(String src) {
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }





}
