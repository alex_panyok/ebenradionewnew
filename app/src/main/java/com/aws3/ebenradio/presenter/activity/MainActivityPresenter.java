package com.aws3.ebenradio.presenter.activity;

import android.content.Context;

import com.aws3.ebenradio.model.event_er.Banner;
import com.aws3.ebenradio.model.event_er.BannerResponse;
import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.model.user.UserPublicProfileResponse;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.view.activity.EventBannerActivity_;
import com.aws3.ebenradio.view.views.activity.BaseDrawerActivityView;
import com.aws3.ebenradio.view.views.activity.EventBannerActivityView;
import com.aws3.ebenradio.view.views.activity.MainActivityView;

import java.util.Date;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 11.10.2016.
 */
public class MainActivityPresenter extends BaseDrawerActivityPresenter<MainActivityView> {


    private static long WEEK = 604800000;

//    private static long WEEK = 10000;

    public MainActivityPresenter(Context context) {
        super(context);
    }


    public void getBanners(){
        if(PrefUtils.updateBannerCounter()%10==0) {
            PrefUtils.releaseBannerCount();
            subscription.add(EbenRadioRetroWorker.getInstance().getBanners(GeneralUtil.getDeviceId(context))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<BannerResponse>() {
                        @Override
                        public void call(BannerResponse bannerResponse) {
                            if (bannerResponse.isStatusOk())
                                startBannerActivity(bannerResponse.banners.get(0));
                        }
                    }, new Action1<Throwable>() {
                        @Override
                        public void call(Throwable throwable) {

                        }
                    }));
        }
    }


    private void startBannerActivity(Banner banner){
        EventBannerActivity_.intent(context).extra(BANNER, banner).start();
    }


    public void calculateFeedback(){
        if(!PrefUtils.getIsRateShown()) {
            if (PrefUtils.getRateTime() > 0) {
                if ((PrefUtils.getRateTime() + WEEK*2) <= (new Date().getTime())) {
                    PrefUtils.setRateTime(new Date().getTime());
                    if(isViewAttached())
                        getView().showRateDialog();
                }
            } else {
                PrefUtils.setRateTime(new Date().getTime());
            }
        }
    }




}
