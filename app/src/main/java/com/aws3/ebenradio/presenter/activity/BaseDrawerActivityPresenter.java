package com.aws3.ebenradio.presenter.activity;

import android.content.Context;

import com.aws3.ebenradio.model.event.player.ChangeStationPlayer;
import com.aws3.ebenradio.model.event.radio.ChangeStationEvent;
import com.aws3.ebenradio.model.radio.RadioStation;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.radio.RadioStationUtil;
import com.aws3.ebenradio.view.views.activity.BaseActivityView;
import com.aws3.ebenradio.view.views.activity.BaseDrawerActivityView;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby.mvp.MvpView;

import org.greenrobot.eventbus.EventBus;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by Dell on 05.10.2016.
 */
public class BaseDrawerActivityPresenter<V extends BaseDrawerActivityView> extends BaseActivityPresenter<V> {




    public BaseDrawerActivityPresenter(Context context) {
        super(context);
    }


    public void setStation(RadioStation radioStation){
        PrefUtils.setStation(radioStation);
        if(isViewAttached())
            getView().setStation(radioStation);
        EventBus.getDefault().postSticky(new ChangeStationPlayer());
        EventBus.getDefault().post(new ChangeStationEvent());
    }


    public void setStationUI(RadioStation radioStation){
        if(isViewAttached())
            getView().setStation(radioStation);
        EventBus.getDefault().postSticky(new ChangeStationPlayer());
    }


    public void prevStation(){
        setStation(PrefUtils.getStation().id==0? RadioStationUtil.getRadioStationItems(context).get(RadioStationUtil.getRadioStationItems(context).size()-1):RadioStationUtil.getRadioStationItems(context).get(PrefUtils.getStation().id-1));
    }

    public void nextStation(){
       setStation(PrefUtils.getStation().id==RadioStationUtil.getRadioStationItems(context).size()-1?RadioStationUtil.getRadioStationItems(context).get(0):RadioStationUtil.getRadioStationItems(context).get(PrefUtils.getStation().id+1));
    }


}
