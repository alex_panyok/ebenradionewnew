package com.aws3.ebenradio.presenter.fragment;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.view.activity.CreateSkinActivity_;
import com.aws3.ebenradio.view.fragment.DialogCreateSkin;
import com.aws3.ebenradio.view.fragment.DialogEditSkin;
import com.aws3.ebenradio.view.fragment.DialogEditSkin_;
import com.aws3.ebenradio.view.views.fragment.FragmentSkinsView;
import com.hannesdorfmann.mosby.mvp.MvpActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by Dell on 11.10.2016.
 */
public class FragmentSkinsPresenter extends BaseFragmentPresenter<FragmentSkinsView> {


    public FragmentSkinsPresenter(Context context) {
        super(context);
    }




    public void startCreateSkinActivity(){
        CreateSkinActivity_.intent(context).start();
    }


    public List<Skin> getSkins(){
       return SkinPrefUtils.getSkins();
    }

    public void applyNewSkin(Skin skin){
        SkinPrefUtils.setSkin(skin);
        EventBus.getDefault().postSticky(new ApplySkinEvent(skin));
    }


    public boolean editSkin(Fragment fragment, Skin skin, int pos){
        if(!skin.isDefault)
            DialogEditSkin_.builder().arg(SKIN, skin).arg(POSITION, pos).build().show(fragment.getChildFragmentManager(), DialogEditSkin.class.getName());
        return true;
    }


}
