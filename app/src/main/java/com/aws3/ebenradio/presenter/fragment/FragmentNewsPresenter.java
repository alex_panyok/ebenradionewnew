package com.aws3.ebenradio.presenter.fragment;

import android.content.Context;
import android.media.AudioManager;
import android.util.Log;

import com.aws3.ebenradio.model.rss.Feed;
import com.aws3.ebenradio.model.rss.Item;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.view.activity.DetailNewsActivity_;
import com.aws3.ebenradio.view.views.fragment.FragmentNewsView;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 28.10.2016.
 */
public class FragmentNewsPresenter extends BaseFragmentPresenter<FragmentNewsView> {

    public FragmentNewsPresenter(Context context) {
        super(context);
    }


    public void getNews(){
        subscription.add(EbenRadioRetroWorker.getInstance().writeClassAndMethod(this).getNews(GeneralUtil.getCurrentLocale(context).getLanguage().equals("fr")?"http://prod.ebenradio.com/rss_fr.xml?rand=9":"http://feeds.feedburner.com/hypebeast/feed")
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<Feed>() {
                                @Override
                                public void call(Feed feed) {
//                                    feed.channel.list.get(0).parseHtml();
                                    if(feed!=null){
                                        if(isViewAttached()){
                                            getView().setNews(feed.channel.parseHtml());
                                        }
                                    }

                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {
                                }
                            }));
    }



    public void startDetail(Item item){
        DetailNewsActivity_.intent(context).extra(ARTICLE, item).start();
    }
}
