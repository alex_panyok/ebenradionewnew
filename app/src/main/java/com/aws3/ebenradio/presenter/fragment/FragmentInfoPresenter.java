package com.aws3.ebenradio.presenter.fragment;

import android.content.Context;

import com.aws3.ebenradio.model.contact.Contact;
import com.aws3.ebenradio.model.networking.EbenResponse;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.view.views.fragment.FragmentInfoView;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 09.11.2016.
 */
public class FragmentInfoPresenter extends BaseFragmentPresenter<FragmentInfoView>{

    public FragmentInfoPresenter(Context context) {
        super(context);
    }



    public void sendMessage(String name, final String email, String message){
        subscription.add(EbenRadioRetroWorker.getInstance().writeClassAndMethod(this, name,email,message).sendContactMessage(GeneralUtil.getBasicAuthHeader(),new Contact(name, email, message))
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(new Action1<EbenResponse>() {
                                @Override
                                public void call(EbenResponse ebenResponse) {
                                    if(ebenResponse.isStatusOk() && isViewAttached())
                                        getView().messageSent(ebenResponse.message);
                                }
                            }, new Action1<Throwable>() {
                                @Override
                                public void call(Throwable throwable) {

                                }
                            }));

    }
}
