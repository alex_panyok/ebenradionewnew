package com.aws3.ebenradio.presenter.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.create_skin.attrs.Background;
import com.aws3.ebenradio.model.create_skin.attrs.Blur;
import com.aws3.ebenradio.model.create_skin.attrs.Color;
import com.aws3.ebenradio.model.create_skin.attrs.Font;
import com.aws3.ebenradio.model.create_skin.attrs.GraphicEQ;
import com.aws3.ebenradio.model.create_skin.attrs.Navigation;
import com.aws3.ebenradio.model.create_skin.attrs.Shape;
import com.aws3.ebenradio.model.create_skin.attrs.Volume;
import com.aws3.ebenradio.model.create_skin.categories.Category;
import com.aws3.ebenradio.model.event.skin.CreateSkinEvent;
import com.aws3.ebenradio.model.skin.PlayerControls;
import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.util.colors.ColorsUtil;
import com.aws3.ebenradio.util.file.FileUtil;
import com.aws3.ebenradio.util.skins.BlurUtil;
import com.aws3.ebenradio.util.skins.GraphicEQUtil;
import com.aws3.ebenradio.util.skins.ShapeUtil;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.view.adapter.create_skin.AdapterBackground;
import com.aws3.ebenradio.view.fragment.DialogCreateSkin;
import com.aws3.ebenradio.view.fragment.DialogCreateSkin_;
import com.aws3.ebenradio.view.views.activity.CreateSkinActivityView;
import com.hannesdorfmann.mosby.mvp.MvpActivity;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

/**
 * Created by Dell on 11.10.2016.
 */
public class CreateSkinActivityPresenter extends BaseActivityPresenter<CreateSkinActivityView> {


    private Skin createdSkin;

    private Category selectedCategory;



    private int index;

    public CreateSkinActivityPresenter(Context context) {
        super(context);
        createdSkin = SkinPrefUtils.getSkins().get(0);
        createdSkin.blur = BlurUtil.NO_BLUR_ID;

    }




    public void setAttrAdapter(Category category){
        selectedCategory = category;
        if(isViewAttached())
            getView().setAttrAdapter(category);
    }


    public void setCreatedSkin(Skin skin, int index){
        this.index = index;
        if(skin!=null) {
            createdSkin = skin;
        }
    }

    public void applyCreatedSkin(int selectedAttr){
          refreshSkinView(selectedAttr);
    }


    private void refreshSkinView(int id){
        if(isViewAttached())
            getView().refreshSkin(id,createdSkin);
    }

    public Skin getCreatedSkin() {
        return createdSkin;
    }


    public void pickImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        ((MvpActivity)context).startActivityForResult(intent, PICK_PHOTO);
    }


    public void applyBackground(Background background){
        selectedCategory.adapter.setSelectedItem(background.id);
        createdSkin.backgroundPath = background.pathId;
        createdSkin.backgroundRes = background.resId;
        refreshSkinView(background.id);
    }


    public void applyShape(Shape shape){
        selectedCategory.adapter.setSelectedItem(shape.id);
        createdSkin.shapeId = shape.shapeId;
        checkShape(shape.shapeId);
        refreshSkinView(shape.id);
    }


    public void applyVolume(Volume volume){
        selectedCategory.adapter.setSelectedItem(volume.id);
        createdSkin.volume = volume.volId;
        refreshSkinView(volume.id);
    }


    public void applyBlur(Blur blur){
        selectedCategory.adapter.setSelectedItem(blur.id);
        createdSkin.blur = blur.blurId;
        refreshSkinView(blur.id);
    }


    private void checkShape(int shapeId){
        if(shapeId!= ShapeUtil.RECTANGLE){
            Toast.makeText(context, context.getString(R.string.graphic_error),Toast.LENGTH_SHORT).show();
            createdSkin.visualizer = GraphicEQUtil.NO_GEQ_ID;
            if(isViewAttached())
                getView().changeGraphicEQAdapterNone();
        }
    }



    public void cancel(){
        ((MvpActivity)context).onBackPressed();
    }

    public void done(){
        if(index==-1) {
            DialogCreateSkin dialogCreateSkin = new DialogCreateSkin_();
            dialogCreateSkin.show(((MvpActivity) context).getSupportFragmentManager(), DialogCreateSkin.class.getName());
        }else
            if(isViewAttached())
                getView().getScreenShotView(createdSkin.name,String.valueOf(System.currentTimeMillis()));
    }


    public void getScreenShotView(String name, String filename){
        if(isViewAttached())
            getView().getScreenShotView(name, filename);
    }

    public void create(String name, String fileName, View view){
        createdSkin.name = name;
        createdSkin.isDefault = false;
        createdSkin.previewPath = FileUtil.saveBitmapToDisk(FileUtil.loadBitmapFromView(view,view.getWidth(),view.getHeight()), fileName);
        List<Skin> skins = SkinPrefUtils.getSkins();
        if(index!=-1 && !checkName(name))
            skins.set(index,createdSkin);
        else
            skins.add(createdSkin);
        SkinPrefUtils.setSkins(skins);
        EventBus.getDefault().postSticky(new CreateSkinEvent(createdSkin));
        ((MvpActivity)context).onBackPressed();
    }

    private boolean checkName(String name){
        for(Skin skin : SkinPrefUtils.getSkins()){
            if(skin.name.equals(name) )
                return false;
        }
        return true;
    }

    public int getIndex() {
        return index;
    }

    public void applyColor(Color color){
        selectedCategory.adapter.setSelectedItem(color.id);
        createdSkin.color = color.color;
        refreshSkinView(color.id);
    }

    public void applyGraphicEQ(GraphicEQ graphicEQ){
        if(graphicEQ.eqId != GraphicEQUtil.NO_GEQ_ID && createdSkin.shapeId!=ShapeUtil.RECTANGLE )
            Toast.makeText(context, context.getString(R.string.graphic_error),Toast.LENGTH_SHORT).show();
        else
            selectedCategory.adapter.setSelectedItem(graphicEQ.id);
        createdSkin.visualizer = createdSkin.shapeId==ShapeUtil.RECTANGLE?graphicEQ.eqId:GraphicEQUtil.NO_GEQ_ID;
        refreshSkinView(graphicEQ.id);
    }


    public void applyTypeface(Font font){
        selectedCategory.adapter.setSelectedItem(font.id);
        createdSkin.typefaceId = font.fontId;
        refreshSkinView(font.id);
    }

    public void applyControls(Navigation navigation){
        selectedCategory.adapter.setSelectedItem(navigation.id);
        createdSkin.controls = navigation.controls;
        refreshSkinView(navigation.id);
    }
    public void addBackgroundAndApply(String path){
        applyBackground(((AdapterBackground)selectedCategory.adapter).addItemBackground(path));
    }

    public Category getSelectedCategory() {
        return selectedCategory;
    }
}
