package com.aws3.ebenradio.presenter.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.aws3.ebenradio.model.event_er.Banner;
import com.aws3.ebenradio.model.event_er.BannerResponse;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.view.views.activity.EventBannerActivityView;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 09.11.2016.
 */
public class EventBannerActivityPresenter extends BaseActivityPresenter<EventBannerActivityView> {
    public EventBannerActivityPresenter(Context context) {
        super(context);
    }


    public void viewBanner(final Banner banner) {
        subscription.add(EbenRadioRetroWorker.getInstance().writeClassAndMethod(this, banner).viewBanner(banner.id, GeneralUtil.getDeviceId(context))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<BannerResponse>() {
                    @Override
                    public void call(BannerResponse bannerResponse) {
                            if(isViewAttached())
                                getView().bannerViewed(banner);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                }));
    }




}
