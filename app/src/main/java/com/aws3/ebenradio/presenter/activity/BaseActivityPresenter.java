package com.aws3.ebenradio.presenter.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.media.AudioManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.colors.ColorsUtil;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.aws3.ebenradio.util.keys.EbenRetroKeys;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.skins.ColorUtil;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.util.skins.VolumeUtil;
import com.aws3.ebenradio.view.views.activity.BaseActivityView;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby.mvp.MvpView;

import org.greenrobot.eventbus.Subscribe;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import rx.subscriptions.CompositeSubscription;

/**
 * Created by Dell on 05.10.2016.
 */
public  class BaseActivityPresenter<V extends MvpView> extends MvpBasePresenter<V> implements EbenKeys, EbenRetroKeys {

    protected Context context;


    protected CompositeSubscription subscription;


    public BaseActivityPresenter(Context context) {
        this.context = context;
        subscription = new CompositeSubscription();
    }


    public void unsubscribeSubscription(){
        if(!subscription.isUnsubscribed()){
            subscription.unsubscribe();
            subscription = new CompositeSubscription();
        }
    }


    public void setUpColor(){
        if(isViewAttached())
        ((BaseActivityView)getView()).setUpColor(ColorUtil.getColorById(getSkin().color, context), ColorsUtil.makeLighter(getSkin().color,40,context));
    }



    public void volumeDown() {
        ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).adjustStreamVolume(AudioManager.STREAM_MUSIC,
                AudioManager.ADJUST_LOWER, SkinPrefUtils.getSkin().volume== VolumeUtil.NO_VOL_ID? AudioManager.FLAG_SHOW_UI:0);
    }

    public void volumeUp() {
        ((AudioManager) context.getSystemService(Context.AUDIO_SERVICE)).adjustStreamVolume(AudioManager.STREAM_MUSIC,
                AudioManager.ADJUST_RAISE, SkinPrefUtils.getSkin().volume== VolumeUtil.NO_VOL_ID? AudioManager.FLAG_SHOW_UI:0);
    }


    public void retryLastMethod(){
        Method method = null;
        try {
            if(EbenRadioRetroWorker.getInstance().lastPresenter!=null) {
                Class[] classes = new Class[EbenRadioRetroWorker.getInstance().lastParams.length];
                for(int i = 0; i<classes.length;i++){
                    classes[i] = EbenRadioRetroWorker.getInstance().lastParams[i].getClass();
                }
                method = EbenRadioRetroWorker.getInstance().lastPresenter.getClass().getMethod(EbenRadioRetroWorker.getInstance().lastCalledMethod, classes);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        if (method != null ) {
            try {
                method.invoke(EbenRadioRetroWorker.getInstance().lastPresenter, EbenRadioRetroWorker.getInstance().lastParams);
            } catch (IllegalAccessException ignored) {
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }




    public Skin getSkin() {
        return SkinPrefUtils.getSkin();
    }
}
