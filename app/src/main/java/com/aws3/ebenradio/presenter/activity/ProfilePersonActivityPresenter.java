package com.aws3.ebenradio.presenter.activity;

import android.content.Context;

import com.aws3.ebenradio.model.follow.FollowPerson;
import com.aws3.ebenradio.model.follow.FollowPersonEvent;
import com.aws3.ebenradio.model.follow.FollowResponse;
import com.aws3.ebenradio.model.follow.FollowUnfollowBody;
import com.aws3.ebenradio.model.user.UserPublicProfileResponse;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.view.views.activity.ProfilePersonActivityView;
import com.hannesdorfmann.mosby.mvp.MvpActivity;

import org.greenrobot.eventbus.EventBus;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;


/**
 * Created by Dell on 26.10.2016.
 */
public class ProfilePersonActivityPresenter extends BaseActivityPresenter<ProfilePersonActivityView> {

    private String userId;


    public ProfilePersonActivityPresenter(Context context, String userId) {
        super(context);
        unsubscribeSubscription();
        this.userId = userId;
    }




    public void getUserProfile(final String userId){
        subscription.add(EbenRadioRetroWorker.getInstance().writeClassAndMethod(this,userId).getUser(GeneralUtil.getBasicAuthHeader(), PrefUtils.getCookies(), userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<UserPublicProfileResponse>() {
                    @Override
                    public void call(UserPublicProfileResponse userPublicProfileResponse) {
                        if (userPublicProfileResponse.isStatusOk()) {
                            if (isViewAttached()) {
                                getView().setUpProfile(userPublicProfileResponse.user);
                            }
                        }
//                        if (checkView())
//                            getView().endLoadingData();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
//                        if (checkView())
//                            getView().endLoadingData();
                    }
                }));
    }


    public void getFollowing(){
        subscription.add(EbenRadioRetroWorker.getInstance().getFollowing(GeneralUtil.getBasicAuthHeader(),PrefUtils.getCookies())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<FollowResponse>() {
                    @Override
                    public void call(FollowResponse followResponse) {
                        if(followResponse.isStatusOk()){
                            if(isViewAttached()){
                                for(FollowPerson person : followResponse.followingList) {
                                    if (userId.equals(person.id)) {
                                        getView().setUpFollowButton(true);
                                        return;
                                    }
                                }
                                getView().setUpFollowButton(false);
                            }
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                }));
    }




    public void back(){
        ((MvpActivity)context).onBackPressed();
    }

    public void follow(String followId){
        subscription.add(EbenRadioRetroWorker.getInstance().writeClassAndMethod(this,followId).followUnfollow(GeneralUtil.getBasicAuthHeader(), PrefUtils.getCookies(), new FollowUnfollowBody(followId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<FollowResponse>() {
                    @Override
                    public void call(FollowResponse followResponse) {
                        if(followResponse.isStatusOk()){
                            EventBus.getDefault().postSticky(new FollowPersonEvent());
                            if(isViewAttached())
                                getView().followUser(followResponse.message);
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                }));
    }
}
