package com.aws3.ebenradio.view.custom_views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Dell on 16.08.2016.
 */
public class SquareImageEventView extends ImageView {
    public SquareImageEventView(Context context) {
        super(context);
    }

    public SquareImageEventView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareImageEventView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(heightMeasureSpec, heightMeasureSpec);
    }
}
