package com.aws3.ebenradio.view.adapter.create_skin;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ItemGraphiceqBinding;
import com.aws3.ebenradio.model.create_skin.attrs.GraphicEQ;
import com.aws3.ebenradio.presenter.activity.CreateSkinActivityPresenter;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Dell on 17.10.2016.
 */
public class AdapterGraphicEQ extends AdapterAttr<AdapterGraphicEQ.GraphicEQHolder> {

    private List<GraphicEQ> list;

    private MvpBasePresenter presenter;

    public AdapterGraphicEQ(List<GraphicEQ> list, MvpBasePresenter presenter, int selectedItem) {
        super(selectedItem);
        this.list = list;
        this.presenter = presenter;
    }

    @Override
    public GraphicEQHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemGraphiceqBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_graphiceq, parent, false);
        binding.setPresenter((CreateSkinActivityPresenter)presenter);
        return new GraphicEQHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(GraphicEQHolder holder, int position) {
        holder.binding.setItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class GraphicEQHolder extends RecyclerView.ViewHolder{

        ItemGraphiceqBinding binding;

        public GraphicEQHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }


}
