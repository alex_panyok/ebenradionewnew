package com.aws3.ebenradio.view.fragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.model.event_er.Event;
import com.aws3.ebenradio.presenter.fragment.FragmentEventPresenter;
import com.aws3.ebenradio.view.adapter.AdapterEvents;
import com.aws3.ebenradio.view.views.fragment.FragmentEventView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 03.11.2016.
 */
@EFragment(R.layout.fragment_event)
public class FragmentEvent extends BaseFragment<FragmentEventView, FragmentEventPresenter> implements FragmentEventView {



    public static FragmentEvent getInstance(FragmentEvent fragmentEvent){
        return fragmentEvent;
    }



    @ViewById(R.id.rv_events)
    RecyclerView mRecyclerViewEvents;

    private AdapterEvents adapterEvents;


    @AfterViews
    void after(){
        initRv();
        presenter.getEvents();
    }



    @Override
    protected void skinEvent(ApplySkinEvent event) {

    }

    @Override
    public void setUpColor(int color, int colorLight) {

    }


    private void initRv(){
        mRecyclerViewEvents.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapterEvents = new AdapterEvents(new ArrayList<Event>(), presenter);
        mRecyclerViewEvents.setAdapter(adapterEvents);
    }

    @NonNull
    @Override
    public FragmentEventPresenter createPresenter() {
        return new FragmentEventPresenter(getActivity());
    }

    @Override
    public void setEvents(List<Event> list) {
        adapterEvents.setData(list);
    }
}
