package com.aws3.ebenradio.view.adapter.create_skin;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ItemBlurBinding;
import com.aws3.ebenradio.model.create_skin.attrs.Blur;
import com.aws3.ebenradio.presenter.activity.CreateSkinActivityPresenter;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Dell on 17.10.2016.
 */
public class AdapterBlur extends AdapterAttr<AdapterBlur.BlurHolder> {

    private List<Blur> list;

    private MvpBasePresenter presenter;

    public AdapterBlur(List<Blur> list, MvpBasePresenter presenter, int selectedItem) {
        super(selectedItem);
        this.list = list;
        this.presenter = presenter;
    }

    @Override
    public BlurHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemBlurBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_blur, parent, false);
        binding.setPresenter((CreateSkinActivityPresenter)presenter);
        return new BlurHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(BlurHolder holder, int position) {
        holder.binding.setItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class BlurHolder extends RecyclerView.ViewHolder{

        ItemBlurBinding binding;

        public BlurHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }


}
