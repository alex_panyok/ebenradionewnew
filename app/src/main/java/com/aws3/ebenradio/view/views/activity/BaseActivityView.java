package com.aws3.ebenradio.view.views.activity;

import com.hannesdorfmann.mosby.mvp.MvpView;


public interface BaseActivityView extends MvpView {
    void setUpColor(int color, int colorLight);
}
