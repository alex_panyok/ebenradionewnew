package com.aws3.ebenradio.view.views.fragment;

import com.aws3.ebenradio.model.ads.Ad;
import com.aws3.ebenradio.view.views.activity.BaseActivityView;

/**
 * Created by Dell on 04.11.2016.
 */
public interface FragmentAdView extends BaseFragmentView {

    void setAd(Ad item);

}
