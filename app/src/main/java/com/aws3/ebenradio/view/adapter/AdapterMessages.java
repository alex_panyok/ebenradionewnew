package com.aws3.ebenradio.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ItemMessageBinding;
import com.aws3.ebenradio.databinding.ItemMessageDateBinding;
import com.aws3.ebenradio.databinding.ItemMessageMeBinding;
import com.aws3.ebenradio.databinding.ItemTimerBinding;
import com.aws3.ebenradio.model.chat.Message;
import com.aws3.ebenradio.model.chat.MessageItem;
import com.aws3.ebenradio.model.user.UserFromSearch;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Dell on 01.11.2016.
 */
public class AdapterMessages extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements EbenKeys {

    private List<MessageItem> list;

    private MvpBasePresenter presenter;

    private String userId;


    public AdapterMessages(List<MessageItem> list, MvpBasePresenter presenter, String userId) {
        this.list = list;
        this.presenter = presenter;
        this.userId = userId;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemMessageMeBinding itemMessageMeBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_message_me, parent, false);
        ItemMessageBinding itemMessageBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_message, parent, false);
        ItemMessageDateBinding itemMessageDateBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_message_date, parent, false);
        if(viewType == TYPE_MESSAGE_FROM_ME)
            return new MessagesHolderMe(itemMessageMeBinding.getRoot());
        else if(viewType == TYPE_MESSAGE)
            return new MessagesHolder(itemMessageBinding.getRoot());
        else if(viewType == TYPE_DATE_MESSAGE)
            return new MessageItemDate(itemMessageDateBinding.getRoot());
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof MessagesHolder) {
            ((MessagesHolder) holder).binding.setMessage( list.get(position));
            ((MessagesHolder) holder).bind( list.get(position));
        }else if(holder instanceof MessagesHolderMe) {
            ((MessagesHolderMe) holder).binding.setMessage( list.get(position));
            ((MessagesHolderMe) holder).bind(list.get(position));
        } else if(holder instanceof MessageItemDate){
            ((MessageItemDate) holder).binding.setItem( list.get(position));
        }

    }


    public void addNewMessage(MessageItem item){

        this.list.add(0,item);
        notifyItemInserted(0);

    }



    public void addData(List<MessageItem> list){
        int size = this.list.size();
        this.list.addAll(list);
//        if(!list.isEmpty())
            notifyItemRangeChanged(0, this.list.size());
//        else
//            notifyItemRangeRemoved(0,this.list.size());
//        notifyDataSetChanged();

    }


    public void clearData(){
        this.list.clear();
    }



    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public int getItemViewType(int position) {
        return list.get(position).date==null? list.get(position).userId.equals(userId)?TYPE_MESSAGE:TYPE_MESSAGE_FROM_ME:TYPE_DATE_MESSAGE;
    }

    public static class MessagesHolder extends RecyclerView.ViewHolder{
        TextView mTextViewMessage;
        ItemMessageBinding binding;
        public MessagesHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            mTextViewMessage = (TextView)itemView.findViewById(R.id.message);
        }

        public void bind(MessageItem messageItem){
            mTextViewMessage.setText(messageItem.message);
        }
    }


    public static class MessagesHolderMe extends RecyclerView.ViewHolder{
        ItemMessageMeBinding binding;
        TextView mTextViewMessage;
        public MessagesHolderMe(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            mTextViewMessage = (TextView)itemView.findViewById(R.id.message);
        }


        public void bind(MessageItem messageItem){
            mTextViewMessage.setText(messageItem.message);
        }
    }


    public static class MessageItemDate extends RecyclerView.ViewHolder{

        ItemMessageDateBinding binding;

        public MessageItemDate(View itemView) {
            super(itemView);
           binding = DataBindingUtil.bind(itemView);

        }
    }


}
