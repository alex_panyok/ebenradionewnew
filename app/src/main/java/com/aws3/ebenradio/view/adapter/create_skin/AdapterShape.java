package com.aws3.ebenradio.view.adapter.create_skin;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ItemShapeBinding;
import com.aws3.ebenradio.model.create_skin.attrs.Shape;
import com.aws3.ebenradio.presenter.activity.CreateSkinActivityPresenter;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Dell on 17.10.2016.
 */
public class AdapterShape extends AdapterAttr<AdapterShape.ShapeHolder> {

    private List<Shape> list;

    private MvpBasePresenter presenter;

    public AdapterShape(List<Shape> list, MvpBasePresenter presenter, int selectedItem) {
        super(selectedItem);
        this.list = list;
        this.presenter = presenter;
    }

    @Override
    public ShapeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemShapeBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_shape, parent, false);
        binding.setPresenter((CreateSkinActivityPresenter)presenter);
        return new ShapeHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ShapeHolder holder, int position) {
        holder.binding.setItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ShapeHolder extends RecyclerView.ViewHolder{

        ItemShapeBinding binding;

        public ShapeHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }


}
