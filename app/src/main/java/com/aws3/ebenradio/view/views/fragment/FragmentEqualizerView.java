package com.aws3.ebenradio.view.views.fragment;

import java.util.List;

/**
 * Created by Dell on 19.10.2016.
 */
public interface FragmentEqualizerView extends BaseFragmentView {
    void setUpEqualizerSliders(int max,int[] progresses);

    void setUpEqualizerStatus(boolean isEnabled);

    void setUpPresets(List<String> presets);

    void setSlidersProgress(int[] progresses);
}
