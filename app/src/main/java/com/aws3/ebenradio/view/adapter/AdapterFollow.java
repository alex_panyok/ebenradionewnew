package com.aws3.ebenradio.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.FollowItemBinding;
import com.aws3.ebenradio.model.follow.FollowPerson;
import com.aws3.ebenradio.presenter.fragment.FragmentProfilePresenter;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Dell on 26.10.2016.
 */
public class AdapterFollow extends RecyclerView.Adapter<AdapterFollow.FollowHolder> {


    private List<FollowPerson> list;

    private MvpBasePresenter presenter;

    public AdapterFollow(List<FollowPerson> list, MvpBasePresenter presenter) {
        this.list = list;
        this.presenter = presenter;
    }


    @Override
    public FollowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        FollowItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.follow_item, parent, false);
        binding.setPresenter((FragmentProfilePresenter)presenter);
        return new FollowHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(FollowHolder holder, int position) {
        holder.binding.setPerson(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @Override
    public long getItemId(int position) {
        return Long.parseLong(list.get(position).id);
    }

//    public void setData(List<FollowPerson> list){
//        this.list.clear();
//        this.list.addAll(list);
//        notifyItemRangeChanged(0,list.size());
//
//    }

    public static class FollowHolder extends RecyclerView.ViewHolder{

        FollowItemBinding binding;

        public FollowHolder(View itemView) {
            super(itemView);
            binding  = DataBindingUtil.bind(itemView);
        }
    }

}
