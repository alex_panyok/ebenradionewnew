package com.aws3.ebenradio.view.adapter.create_skin;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ItemVolumeBinding;
import com.aws3.ebenradio.model.create_skin.attrs.Volume;
import com.aws3.ebenradio.presenter.activity.CreateSkinActivityPresenter;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Dell on 17.10.2016.
 */
public class AdapterVolume extends AdapterAttr<AdapterVolume.VolumeHolder> {

    private List<Volume> list;

    private MvpBasePresenter presenter;

    public AdapterVolume(List<Volume> list, MvpBasePresenter presenter, int selectedItem) {
        super(selectedItem);
        this.list = list;
        this.presenter = presenter;
    }

    @Override
    public VolumeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemVolumeBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_volume, parent, false);
        binding.setPresenter((CreateSkinActivityPresenter)presenter);
        return new VolumeHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(VolumeHolder holder, int position) {
        holder.binding.setItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class VolumeHolder extends RecyclerView.ViewHolder{

        ItemVolumeBinding binding;

        public VolumeHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }


}
