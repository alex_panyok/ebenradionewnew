package com.aws3.ebenradio.view.views.activity;

import com.aws3.ebenradio.model.event_er.Event;

/**
 * Created by Dell on 03.11.2016.
 */
public interface SingleEventActivityView extends BaseActivityView {

    void getEvent(Event event);
}
