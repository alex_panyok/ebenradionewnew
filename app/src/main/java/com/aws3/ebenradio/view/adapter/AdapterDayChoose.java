package com.aws3.ebenradio.view.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ItemMultiChooseBinding;
import com.aws3.ebenradio.model.alarm.WeekDay;


import java.util.List;

/**
 * Created by Dell on 19.09.2016.
 */
public class AdapterDayChoose extends RecyclerView.Adapter<AdapterDayChoose.Holder> {



    private List<WeekDay> list;




    public AdapterDayChoose(List<WeekDay> list) {
        this.list = list;
//        this.context = context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(context).inflate(R.layout.item_muti_choose,parent,false);
        ItemMultiChooseBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.item_multi_choose,parent,false);
        binding.setAdapter(this);
        return new Holder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        final WeekDay day = list.get(position);
        holder.binding.setDay(day);
//        holder.bind(day);
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(onSelectListener!=null){
//                    onSelectListener.select(day);
//                }
//            }
//        });
    }




    public void selectDay(int id){
        list.get(id).isChecked = !list.get(id).isChecked;
        notifyItemChanged(id);
    }



    public List<WeekDay> getList() {
        return list;
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class Holder extends RecyclerView.ViewHolder{

        ItemMultiChooseBinding binding;



        public Holder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }


    }
}
