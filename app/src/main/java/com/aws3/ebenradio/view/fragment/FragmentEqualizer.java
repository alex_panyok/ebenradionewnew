package com.aws3.ebenradio.view.fragment;

import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.radio.EqualizerSetupEvent;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.presenter.fragment.FragmentEqualizerPresenter;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.view.custom_views.VerticalSeekBar;
import com.aws3.ebenradio.view.views.fragment.FragmentEqualizerView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

/**
 * Created by Dell on 19.10.2016.
 */
@EFragment(R.layout.fragment_equalizer)
public class FragmentEqualizer extends BaseFragment<FragmentEqualizerView, FragmentEqualizerPresenter> implements FragmentEqualizerView {


    int[] sliderIds = new int[]{R.id.slider_1, R.id.slider_2, R.id.slider_3, R.id.slider_4, R.id.slider_5};




    public static FragmentEqualizer getInstance(FragmentEqualizer fragmentEqualizer){
        return fragmentEqualizer;
    }


    @ViewById(R.id.switch_equalizer)
    SwitchCompat mSwitchEqualizer;

    @ViewById(R.id.spinner_presets)
    AppCompatSpinner mSpinnerPresets;


    @ViewById(R.id.disabled_view)
    View mViewDisabled;

    private ArrayAdapter<String> adapterPresets;

    private boolean isWasFirst;


    @Override
    protected void skinEvent(ApplySkinEvent event) {

    }

    @Override
    public void setUpColor(int color, int colorLight) {
        mSpinnerPresets.setPopupBackgroundDrawable(new ColorDrawable(color));
    }

    @NonNull
    @Override
    public FragmentEqualizerPresenter createPresenter() {
        return new FragmentEqualizerPresenter(getActivity());
    }


    @AfterViews
    void after(){
        initListenerToSwitch();
        isWasFirst = false;
    }

    private void setListenerPresets() {
        mSpinnerPresets.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
                if(isWasFirst && PrefUtils.getSelectedPreset()!=position)
                    presenter.usePreset(position);
                isWasFirst = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }


        });
    }




    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onSetUpEqualizer(final EqualizerSetupEvent event){
                //mSpinnerPresets.setSelection(PrefUtils.getSelectedPreset());
                presenter.setEqualizer(event.equalizer);

                setListenersToSliders();


//        EventBus.getDefault().removeStickyEvent(event);
    }

    @Override
    public void setUpEqualizerSliders(int max, int[] progresses) {
        for (int i = 0; i < sliderIds.length; i++) {
            if (getView() != null) {
                ((VerticalSeekBar) getView().findViewById(sliderIds[i])).setMax(max);
                ((VerticalSeekBar) getView().findViewById(sliderIds[i])).setProgressProgram(progresses[i]);
                // updateThumbOnMainThread(i);

            }
        }
    }

    @Override
    public void setUpEqualizerStatus(boolean isEnabled) {
        mSwitchEqualizer.setChecked(isEnabled);
    }

    @Override
    public void setUpPresets(List<String> presets) {
        if(mSpinnerPresets.getAdapter()==null) {
            adapterPresets = new ArrayAdapter<>(getActivity(), R.layout.tv_spinner, presets);
            adapterPresets.setDropDownViewResource(R.layout.row_spinner);
            mSpinnerPresets.setAdapter(adapterPresets);
            setListenerPresets();
        }
        mSpinnerPresets.setSelection(PrefUtils.getSelectedPreset());
    }

    @Override
    public void setSlidersProgress(int[] progresses) {
        for (int i = 0; i < sliderIds.length; i++) {
            if (getView() != null) {
                ((VerticalSeekBar) getView().findViewById(sliderIds[i])).setProgressProgram(progresses[i]);
            }
        }
    }


    private void initListenerToSwitch() {
        mSwitchEqualizer.setOnCheckedChangeListener(new SwitchCompat.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {

                    presenter.changeEqualizerStatus(checked);
                    mViewDisabled.setVisibility(checked?View.GONE:View.VISIBLE);
            }

        });
    }


    private void setListenersToSliders() {
        for (int i = 0; i < sliderIds.length; i++) {
            final int band = i;
            if (getView() != null)
                ((VerticalSeekBar) getView().findViewById(sliderIds[i])).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        presenter.setEqualizerLevel(band, progress);
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

//                        ((EbenRadioTextViewMaterial) (mSpinnerPresets.findViewById(R.id.row_spn_tv))).setText("");
//                        mSpinnerPresets.setAdapter(adapterPresets);
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                        if(mSpinnerPresets.getSelectedItemPosition()==0)
                            presenter.saveCustomPreset();
                        presenter.saveCurrentLevels();
                    }
                });
        }
    }
}
