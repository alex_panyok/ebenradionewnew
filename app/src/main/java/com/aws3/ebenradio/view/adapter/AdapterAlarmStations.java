package com.aws3.ebenradio.view.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ItemAlarmStationBinding;
import com.aws3.ebenradio.model.radio.RadioStation;
import com.aws3.ebenradio.presenter.fragment.FragmentAlarmPresenter;
import com.aws3.ebenradio.presenter.fragment.FragmentSleepTimerPresenter;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


import java.util.List;

/**
 * Created by Alex on 21.09.2016.
 */
public class AdapterAlarmStations extends RecyclerView.Adapter<AdapterAlarmStations.Holder> {
//
//    private Context context;

    private List<RadioStation> list;

//    private MvpBasePresenter presenter;

//    OnSelectListener onSelectListener;


    private MvpBasePresenter presenter;

    public AdapterAlarmStations(List<RadioStation> list, MvpBasePresenter presenter) {
        this.list = list;
        this.presenter = presenter;
//        this.presenter = presenter;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
//        View view = LayoutInflater.from(context).inflate(R.layout.item_alarm_station, parent, false);
        ItemAlarmStationBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),R.layout.item_alarm_station, parent, false);
        binding.setPresenter((FragmentAlarmPresenter)presenter);
        return new Holder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        final RadioStation radioStation = list.get(position);
        holder.binding.setItem(radioStation);
//        holder.bind(radioStation);
//        holder.mTextViewStation.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.i("STATION",radioStation.title);
//                if(onSelectListener!=null){
//                    onSelectListener.select(radioStation);
//                }
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

//    public void setOnSelectListener(OnSelectListener onSelectListener){
//        this.onSelectListener = onSelectListener;
//    }
//
//
//    public interface OnSelectListener {
//        void select(RadioStation radioStation);
//    }
    public static class Holder  extends RecyclerView.ViewHolder{
//
//        TextView mTextViewStation;
        ItemAlarmStationBinding binding;
        public Holder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
//            mTextViewStation = (TextView)itemView.findViewById(R.id.station);
        }

//        public void bind(RadioStation radioStation){
//            mTextViewStation.setText(radioStation.title.replaceAll("\n"," "));
//        }
    }
}
