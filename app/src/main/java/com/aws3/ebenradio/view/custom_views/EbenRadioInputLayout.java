package com.aws3.ebenradio.view.custom_views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.design.widget.TextInputLayout;
import android.util.AttributeSet;
import android.widget.EditText;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.util.skins.TypeFaceUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Dell on 10.10.2016.
 */
public class EbenRadioInputLayout extends TextInputLayout {


    private int font;
    public EbenRadioInputLayout(Context context) {
        super(context);
    }

    public EbenRadioInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs, R.styleable.EbenRadioTextView, 0, 0);
        try {
            font = a.getInteger(R.styleable.EbenRadioTextView_font,0);
            initFont(font);
        } finally {
            a.recycle();
        }
    }

    public EbenRadioInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs, R.styleable.EbenRadioTextView, defStyleAttr, 0);
        try {
            font = a.getInteger(R.styleable.EbenRadioTextView_font,0);
            initFont(font);
        } finally {
            a.recycle();
        }
    }




    private void initFont(int fontStyle) {

        this.setTypeface(fontStyle==0? TypeFaceUtil.getTypefaceById(SkinPrefUtils.getSkin().typefaceId)[0]: TypeFaceUtil.getTypefaceById(SkinPrefUtils.getSkin().typefaceId)[1]);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true)
    public void onApplySkinEvent(ApplySkinEvent event){
        initFont(font);
        EventBus.getDefault().removeStickyEvent(event);
    }

    public int getFont() {
        return font;
    }


}
