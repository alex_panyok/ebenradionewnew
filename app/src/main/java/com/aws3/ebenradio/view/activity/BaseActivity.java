package com.aws3.ebenradio.view.activity;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.ImageView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.create_skin.attrs.Color;
import com.aws3.ebenradio.model.event.networking.NoNetworkEvent;
import com.aws3.ebenradio.model.event.radio.AudioSessionEvent;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.presenter.activity.BaseActivityPresenter;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.aws3.ebenradio.util.keys.EbenRetroKeys;
import com.aws3.ebenradio.util.skins.BackgroundUtil;
import com.aws3.ebenradio.util.skins.ColorUtil;
import com.hannesdorfmann.mosby.mvp.MvpActivity;
import com.hannesdorfmann.mosby.mvp.MvpView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Dell on 05.10.2016.
 */
public abstract class BaseActivity<V extends MvpView, P extends BaseActivityPresenter<V>> extends MvpActivity<V,P> implements EbenKeys, EbenRetroKeys,EbenRadioRetroWorker.OnLostInternetConnection {

    private AlertDialog dialogNoInternet;


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        EbenRadioRetroWorker.getInstance().setOnLostConnectionListener(this);
        applySkin();

    }

    @TargetApi(21)
    protected void setUpStatusBarColor(int color){
        getWindow().setStatusBarColor(color);
    }


    public void applySkin(){
        presenter.setUpColor();
        setUpActionBarColor(presenter.getSkin());
        BackgroundUtil.loadImageBackground(this,presenter.getSkin(),((ImageView)findViewById(R.id.background)));
    }


    @Subscribe(sticky = true)
    public void onApplySkinEvent(ApplySkinEvent event){
        applySkin();
        EventBus.getDefault().removeStickyEvent(event);
    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_VOLUME_UP:
                presenter.volumeUp();
                return true;
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                presenter.volumeDown();
                return true;
            default:
                return super.onKeyDown(keyCode, event);
        }

    }





    protected void setUpActionBarColor(Skin skin){
        if(findViewById(R.id.action_bar)!=null){
            findViewById(R.id.action_bar).setBackgroundColor(ColorUtil.getColorById(skin.color,this));
        }
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.LOLLIPOP){
            setUpStatusBarColor(ColorUtil.getColorById(presenter.getSkin().color,this));
        }
    }


    @Override
    protected void onResume() {
        EbenRadioRetroWorker.getInstance().setOnLostConnectionListener(this);
        EventBus.getDefault().register(this);
        super.onResume();

    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }


    @Subscribe
    public void onNoNetworkConnection(NoNetworkEvent event){
    }

    @Override
    public void noInternet() {
        final AlertDialog.Builder adb = new AlertDialog.Builder(this, R.style.NoInetAlertDialogStyle);
        adb.setTitle("No internet");
        adb.setMessage("Check your internet connection, please");
        adb.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                presenter.retryLastMethod();
            }
        });
        adb.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                EbenRadioRetroWorker.getInstance().lastPresenter = null;
                dialogInterface.dismiss();
            }
        });
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if(dialogNoInternet==null || !dialogNoInternet.isShowing()) {
                    if(EbenRadioRetroWorker.getInstance().lastPresenter!=null) {
                        if(dialogNoInternet==null) {
                            dialogNoInternet = adb.create();
                            dialogNoInternet.setCanceledOnTouchOutside(false);
                            dialogNoInternet.setCancelable(false);
                        }
                        dialogNoInternet.show();
                    }
                }
            }
        });
    }
}
