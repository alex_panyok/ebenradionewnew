package com.aws3.ebenradio.view.custom_views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.model.event_er.Event;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.util.skins.TypeFaceUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Dell on 10.10.2016.
 */
public class EbenRadioTextView extends TextView {


    private int font;
    public EbenRadioTextView(Context context) {
        super(context);
        initFont(0);
    }

    public EbenRadioTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs, R.styleable.EbenRadioTextView, 0, 0);
        try {
            font = a.getInteger(R.styleable.EbenRadioTextView_font,0);
            initFont(font);
        } finally {
            a.recycle();
        }
    }

    public EbenRadioTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs, R.styleable.EbenRadioTextView, defStyleAttr, 0);
        try {
            font = a.getInteger(R.styleable.EbenRadioTextView_font,0);
            initFont(font);
        } finally {
            a.recycle();
        }
    }




    private void initFont(int fontStyle) {
        this.setTypeface(fontStyle==0? TypeFaceUtil.getTypefaceById(SkinPrefUtils.getSkin().typefaceId)[0]: TypeFaceUtil.getTypefaceById(SkinPrefUtils.getSkin().typefaceId)[1]);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if(!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true)
    public void onApplySkinEvent(ApplySkinEvent event){
        initFont(font);
        EventBus.getDefault().removeStickyEvent(event);
    }

    public int getFont() {
        return font;
    }


}
