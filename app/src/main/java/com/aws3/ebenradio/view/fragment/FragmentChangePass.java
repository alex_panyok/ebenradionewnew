package com.aws3.ebenradio.view.fragment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.presenter.fragment.FragmentChangePassPresenter;
import com.aws3.ebenradio.util.general.EditTextUtil;
import com.aws3.ebenradio.view.views.fragment.FragmentChangePassView;
import com.mobsandgeeks.saripaar.QuickRule;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

/**
 * Created by Dell on 08.11.2016.
 */
@EFragment(R.layout.fragment_change_pass)
public class FragmentChangePass extends BaseFragment<FragmentChangePassView, FragmentChangePassPresenter> implements FragmentChangePassView, Validator.ValidationListener{

    @Password(messageResId = R.string.password_min)
    @ViewById(R.id.old_password)
    EditText mEditTextOldPassword;

    @ViewById(R.id.old_password_input)
    TextInputLayout mInputLayoutOldPassword;


    @Password(messageResId = R.string.password_min)
    @ViewById(R.id.password)
    EditText mEditTextPassword;

    @ViewById(R.id.password_input)
    TextInputLayout mInputLayoutPassword;



    @ViewById(R.id.re_password)
    EditText mEditTextRePassword;

    @ViewById(R.id.re_password_input)
    TextInputLayout mInputLayoutRePassword;


    private Validator validator;




    @AfterViews
    void after(){

        initValidator();
    }

    private void initValidator(){
        validator = new Validator(this);
        validator.setValidationListener(this);
        validator.put(mEditTextRePassword, new QuickRule<EditText>() {
            @Override
            public boolean isValid(EditText view) {
                return view.getText().toString().equals(mEditTextPassword.getText().toString());
            }

            @Override
            public String getMessage(Context context) {
                return getString(R.string.password_not_match);
            }
        });
    }







    @Click
    void change(){
        EditTextUtil.setErrorNull(mInputLayoutOldPassword, mInputLayoutPassword, mInputLayoutRePassword);
        validator.validate();
    }




    @Override
    protected void skinEvent(ApplySkinEvent event) {

    }

    @Override
    public void setUpColor(int color, int colorLight) {

    }

    @NonNull
    @Override
    public FragmentChangePassPresenter createPresenter() {
        return new FragmentChangePassPresenter(getActivity());
    }

    @Override
    public void onValidationSucceeded() {
        EditTextUtil.setErrorNull(mInputLayoutOldPassword, mInputLayoutPassword, mInputLayoutRePassword);
        presenter.changePassword(mEditTextOldPassword.getText().toString(), mEditTextPassword.getText().toString());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());
            // Display error messages ;)
            switch (view.getId()){
                case R.id.password:
                    mInputLayoutPassword.setError(message);
                    break;
                case R.id.re_password:
                    mInputLayoutRePassword.setError(message);
                    break;
                case R.id.old_password:
                    mInputLayoutOldPassword.setError(message);
                    break;
            }
        }
    }

    @Override
    public void changePass() {
        EditTextUtil.clearFields(getActivity(), mEditTextOldPassword, mEditTextPassword, mEditTextRePassword);
    }
}
