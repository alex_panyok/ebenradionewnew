package com.aws3.ebenradio.view.fragment;



import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.networking.EbenResponse;
import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.model.user.ForgetPasswordBody;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.util.general.PixelUtil;
import com.aws3.ebenradio.util.general.ToastUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.skins.ColorUtil;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.view.activity.CreateSkinActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 18.10.2016.
 */
@EFragment(R.layout.dialog_forgot_password)
public class DialogForgotPassword extends DialogFragment {

    @ViewById(R.id.et_email)
    EditText mEditTextEmail;
    @ViewById(R.id.background)
    View mViewBackground;

    @Click
    void ok(){
        forgotPassword(mEditTextEmail.getText().toString());
    }

    @AfterViews
    void after(){
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mViewBackground.setBackgroundColor(ColorUtil.getColorById(SkinPrefUtils.getSkin().color, getActivity()));
    }


    @Click
    void cancel(){
        getDialog().cancel();
    }


    private boolean validate(String email){
        if(email.isEmpty()) {
            ToastUtil.toast(getActivity(),getString(R.string.fill_field));
            return false;
        }

        return true;

    }


    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow().setLayout(PixelUtil.getScreenWidth(getActivity()), ViewGroup.LayoutParams.WRAP_CONTENT);
    }


    private void forgotPassword(final String email){
        EbenRadioRetroWorker.getInstance().forgotPassword(GeneralUtil.getBasicAuthHeader(), PrefUtils.getCookies(), new ForgetPasswordBody(email))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<EbenResponse>() {
                    @Override
                    public void call(EbenResponse ebenResponse) {
                        ToastUtil.toast(getActivity(), ebenResponse.message);
                        dismiss();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                });
    }



}
