package com.aws3.ebenradio.view.adapter.create_skin;

import android.support.v7.widget.RecyclerView;

import com.aws3.ebenradio.model.create_skin.attrs.AttrItem;

import java.util.List;

/**
 * Created by Dell on 17.10.2016.
 */
public abstract class AdapterAttr<H extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<H>{


    protected int selectedItem;

    public AdapterAttr(int selectedItem) {
        this.selectedItem = selectedItem;
    }


    public int getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(int selectedItem) {
        this.selectedItem = selectedItem;
    }






}
