package com.aws3.ebenradio.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.presenter.fragment.FragmentLoginPresenter;
import com.aws3.ebenradio.util.general.EditTextUtil;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.view.views.fragment.FragmentLoginView;
import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.mobsandgeeks.saripaar.QuickRule;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Dell on 25.10.2016.
 */
@EFragment(R.layout.fragment_login)
public class FragmentLogin  extends BaseFragment<FragmentLoginView, FragmentLoginPresenter> implements FragmentLoginView, Validator.ValidationListener{



    public static FragmentLogin getInstance(FragmentLogin fragmentLogin){
        return fragmentLogin;
    }


    @ViewById(R.id.email_input)
    TextInputLayout mTextInputLayoutEmail;


    @ViewById(R.id.password_input)
    TextInputLayout mTextInputLayoutPassword;

    @ViewById(R.id.facebook)
    View mViewFacebook;


    @ViewById(R.id.email)
    EditText mEditTextEmail;


    @Password(messageResId = R.string.password_min)
    @ViewById(R.id.password)
    EditText mEditTextPassword;



    @Click(R.id.log_in)
    void logIn(){
        presenter.validate();
    }


    @Click
    void forgot(){
        DialogForgotPassword_.builder().build().show(getActivity().getSupportFragmentManager(),DialogForgotPassword.class.getName());
    }


    @Click(R.id.facebook)
    void facebook(){
        LoginManager.getInstance().logOut();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"));
    }

    private Validator validator;

    private CallbackManager callbackManager;


    @AfterViews
    void after(){
        callbackManager = presenter.initFacebook();
        initValidator();
//        EditTextUtil.setFocusListeners(mTextInputLayoutEmail, mTextInputLayoutPassword);
    }


    @Override
    protected void skinEvent(ApplySkinEvent event) {
    }

    @Override
    public void setUpColor(int color, int colorLight) {

    }

    @NonNull
    @Override
    public FragmentLoginPresenter createPresenter() {
        return new FragmentLoginPresenter(getActivity());
    }



    private void initValidator(){
        validator = new Validator(this);
        validator.setValidationListener(this);
        validator.put(mEditTextEmail, new QuickRule<EditText>() {
            @Override
            public boolean isValid(EditText view) {
                return GeneralUtil.checkEmail(view.getText().toString());
            }

            @Override
            public String getMessage(Context context) {
                return getString(R.string.invalid_email);
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        EditTextUtil.setErrorNull(mTextInputLayoutEmail, mTextInputLayoutPassword);
        presenter.logIn(mEditTextEmail.getText().toString(), mEditTextPassword.getText().toString());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());
            // Display error messages ;)
            switch (view.getId()){
                case R.id.email:
                    mTextInputLayoutEmail.setError(message);
                    break;
                case R.id.password:
                    mTextInputLayoutPassword.setError(message);
                    break;
            }
        }
    }

    @Override
    public void validate() {
        validator.validate();
    }

    @Override
    public void performFacebookClick() {
        mViewFacebook.performClick();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
