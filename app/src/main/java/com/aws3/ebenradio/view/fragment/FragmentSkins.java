package com.aws3.ebenradio.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.FragmentSkinsBinding;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.model.event.skin.CreateSkinEvent;
import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.presenter.fragment.FragmentSkinsPresenter;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.view.adapter.SkinsAdapter;
import com.aws3.ebenradio.view.views.fragment.FragmentSkinsView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.List;

/**
 * Created by Dell on 11.10.2016.
 */
@EFragment(R.layout.fragment_skins)
public class FragmentSkins extends BaseFragment<FragmentSkinsView, FragmentSkinsPresenter> implements FragmentSkinsView, SkinsAdapter.OnSelectListener, SkinsAdapter.OnLongSelectListener {

    public static FragmentSkins getInstance(FragmentSkins fragmentSkins){
        return fragmentSkins;
    }

    @ViewById(R.id.rv_skins)
    RecyclerView mRecyclerViewSkins;


    @NonNull
    @Override
    public FragmentSkinsPresenter createPresenter() {
        return new FragmentSkinsPresenter(getActivity());
    }

    @AfterViews
    void after(){
        FragmentSkinsBinding fragmentSkinsBinding = FragmentSkinsBinding.bind(getView());
        fragmentSkinsBinding.setPresenter(presenter);
        initRv(mRecyclerViewSkins,presenter.getSkins());
    }




    @Override
    protected void skinEvent(ApplySkinEvent event) {
        mRecyclerViewSkins.getAdapter().notifyItemRangeChanged(0,mRecyclerViewSkins.getAdapter().getItemCount());
    }



    @Subscribe(sticky = true)
    public void createSkinEvent(CreateSkinEvent event){
        ((SkinsAdapter)mRecyclerViewSkins.getAdapter()).setData(SkinPrefUtils.getSkins());
        presenter.applyNewSkin(event.createdSkin);
        mRecyclerViewSkins.scrollToPosition(mRecyclerViewSkins.getAdapter().getItemCount());
        EventBus.getDefault().removeStickyEvent(event);

    }


    private void initRv(RecyclerView recyclerView, List<Skin> skins){
        SkinsAdapter adapter = new SkinsAdapter(skins, presenter);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),3,GridLayoutManager.VERTICAL,false));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setItemAnimator(null);
        recyclerView.setAdapter(adapter);
        adapter.setData(skins);
        adapter.setOnSelectListener(this);
        adapter.setOnLongSelectListener(this);
    }

    @Override
    public void setUpColor(int color, int colorLight) {

    }

    public void deleteSkin(int index){
        ((SkinsAdapter)mRecyclerViewSkins.getAdapter()).removeSkin(index);
    }

    @Override
    public void select(int pos, Skin skin) {
        presenter.applyNewSkin(skin);
    }

    @Override
    public void longSelect(int pos, Skin skin) {
        presenter.editSkin(this,skin, pos);
    }
}
