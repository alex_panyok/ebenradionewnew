package com.aws3.ebenradio.view.views.activity;

import com.aws3.ebenradio.model.chat.Message;
import com.aws3.ebenradio.model.chat.MessageItem;
import com.aws3.ebenradio.model.chat.MessagesData;

import java.util.List;

/**
 * Created by Dell on 31.10.2016.
 */
public interface ChatActivityView extends BaseActivityView {


    void messageSent();

    void messageReceived();

    void setMessages(MessagesData data, boolean isLastPage);

    void addNewMessage(MessageItem item);

    void progressMessageStop();

    void updateMessages(List<MessageItem> messageItems);
}
