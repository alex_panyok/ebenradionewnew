package com.aws3.ebenradio.view.views.fragment;

/**
 * Created by Dell on 08.11.2016.
 */
public interface FragmentChangePassView extends BaseFragmentView {
    void changePass();
}
