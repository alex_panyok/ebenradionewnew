package com.aws3.ebenradio.view.fragment;

import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.model.rss.Item;
import com.aws3.ebenradio.presenter.fragment.FragmentNewsPresenter;
import com.aws3.ebenradio.view.adapter.AdapterNews;
import com.aws3.ebenradio.view.views.fragment.FragmentNewsView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 28.10.2016.
 */
@EFragment(R.layout.fragment_news)
public class FragmentsNews extends BaseFragment<FragmentNewsView, FragmentNewsPresenter> implements FragmentNewsView {


    public static FragmentsNews getInstance(FragmentsNews fragmentsNews){
        return fragmentsNews;
    }


    @ViewById(R.id.rv_news)
    RecyclerView mRecyclerViewNews;

    private AdapterNews adapterNews;


    @AfterViews
    void after(){
        presenter.getNews();
        initRv();
    }


    private void initRv(){
        mRecyclerViewNews.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterNews = new AdapterNews(new ArrayList<Item>(), presenter);
        mRecyclerViewNews.setAdapter(adapterNews);

    }



    @Override
    protected void skinEvent(ApplySkinEvent event) {

    }

    @Override
    public void setUpColor(int color, int colorLight) {

    }

    @NonNull
    @Override
    public FragmentNewsPresenter createPresenter() {
        return new FragmentNewsPresenter(getContext());
    }

    @Override
    public void setNews(List<Item> list) {
        adapterNews.setData(list);
    }
}
