package com.aws3.ebenradio.view.fragment;

import android.animation.ArgbEvaluator;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.CompoundButtonCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import android.widget.CompoundButton;
import android.widget.TextView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.alarm.TimerUpdateEvent;
import com.aws3.ebenradio.model.event.alarm.TurnOffMusicEvent;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.presenter.fragment.FragmentSleepTimerPresenter;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.view.adapter.AdapterChooseTimer;
import com.aws3.ebenradio.view.adapter.AdapterViewPager;
import com.aws3.ebenradio.view.views.fragment.FragmentSleepTimerView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 24.10.2016.
 */
@EFragment(R.layout.fragment_sleep_timer)
public class FragmentSleepTimer extends BaseFragment<FragmentSleepTimerView, FragmentSleepTimerPresenter> implements FragmentSleepTimerView {

//    @ViewById(R.id.background)
//    View mViewBackground;
//
//    @ViewById(R.id.time)
//    TextView mTextViewTime;

    @ViewById(R.id.switch_timer)
    SwitchCompat mSwitchTimer;

    @ViewById(R.id.time_left)
    TextView mTextViewTimeLeft;

    @ViewById(R.id.rv_timer)
    RecyclerView mRecyclerViewTimer;

    private AdapterChooseTimer adapterChooseTimer;



    @AfterViews
    void after(){

        initRvTimer();
        presenter.setUpTime(PrefUtils.getTimer());
        mSwitchTimer.setChecked(PrefUtils.getTimerStatus());
        initListenerToSwitch();
    }


    @Override
    protected void skinEvent(ApplySkinEvent event) {

    }

    @Override
    public void setUpColor(int color, int colorLight) {

    }

    @NonNull
    @Override
    public FragmentSleepTimerPresenter createPresenter() {
        return new FragmentSleepTimerPresenter(getContext());
    }


    private void initRvTimer(){
        mRecyclerViewTimer.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerViewTimer.setItemAnimator(null);
        adapterChooseTimer = new AdapterChooseTimer(presenter.generateListTimer(), presenter);
        mRecyclerViewTimer.setAdapter(adapterChooseTimer);
//        initListenerToAdapter();

    }

    @Override
    public void changeStatusTimer(boolean status) {
        mSwitchTimer.setChecked(status);
        mTextViewTimeLeft.setText("");
    }

    @Override
    public void updateTimer(int timeLeft) {
        mTextViewTimeLeft.setText(String.format(getString(R.string.time_left_s), timeLeft));
    }

    @Override
    public void newTime(int time) {
        adapterChooseTimer.notifyDataSetChanged();
    }


    @Subscribe(sticky = true)
    public void onUpdateTimer(TimerUpdateEvent event) {
        if(event.leftTime>0 && PrefUtils.getTimerStatus())
            mTextViewTimeLeft.setText(String.format(getString(R.string.time_left_s), event.leftTime));
        else
            mTextViewTimeLeft.setText("");
    }


    @Subscribe(sticky = true)
    public void onTurnOffMusic(TurnOffMusicEvent event) {
        presenter.turnOffTimer();
        changeStatusTimer(false);
        adapterChooseTimer.setActive(!PrefUtils.getTimerStatus());
        EventBus.getDefault().removeStickyEvent(event);
    }



    private void initListenerToSwitch(){
        mSwitchTimer.setOnCheckedChangeListener(new SwitchCompat.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                if(checked && !PrefUtils.getTimerStatus()){
                    presenter.turnOnTimer();
                    adapterChooseTimer.setActive(!PrefUtils.getTimerStatus());
                } else if(!checked && PrefUtils.getTimerStatus()) {
                    presenter.turnOffTimer();
                    mTextViewTimeLeft.setText("");
                    adapterChooseTimer.setActive(!PrefUtils.getTimerStatus());
                }

            }
        });
    }

//    private void initListenerToAdapter(){
//        adapterChooseTimer.setOnSelectListener(new AdapterChooseTimer.OnSelectListener() {
//            @Override
//            public void select(Time time) {
//                presenter.setUpTime(time.time);
//            }
//        });
//    }




}
