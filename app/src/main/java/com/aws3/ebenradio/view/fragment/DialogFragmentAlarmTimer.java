package com.aws3.ebenradio.view.fragment;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.alarm.Time;
import com.aws3.ebenradio.model.alarm.WeekDay;
import com.aws3.ebenradio.util.general.PixelUtil;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.skins.ColorUtil;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.view.adapter.AdapterDayChoose;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Dell on 19.09.2016.
 */
@EFragment(R.layout.dialog_timer)
public class DialogFragmentAlarmTimer extends DialogFragment implements EbenKeys {

    @ViewById(R.id.rv_choose)
    RecyclerView mRecyclerView;

    @ViewById(R.id.background)
    View mViewBackground;

    private RecyclerView.Adapter adapter;

    private int timeValue;






    @Click
    void cancel(){
        getDialog().cancel();
    }

    @Click
    void apply(){
//        if(getArguments().getInt(TYPE)==ALARM){
            PrefUtils.setWeekDays(((AdapterDayChoose)adapter).getList());
            PrefUtils.setRepeatAlarm(checkIfReapeat());
            ((FragmentAlarm)getParentFragment()).getPresenter().setUpWeekDays(((AdapterDayChoose)adapter).getList());
//        }else {
//            ((FragmentTimer)getParentFragment()).getPresenter().setUpTime(timeValue);
//        }
        getDialog().cancel();
    }


    public static DialogFragmentAlarmTimer getInstance(DialogFragmentAlarmTimer dialogFragmentAlarmTimer){
        return dialogFragmentAlarmTimer;
    }

    @AfterViews
    void after(){
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            initRv();
        mViewBackground.setBackgroundColor(ColorUtil.getColorById(SkinPrefUtils.getSkin().color, getActivity()));
    }

    private void initRv(){
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new AdapterDayChoose(generateWeekDaysList());
        mRecyclerView.setItemAnimator(null);
        mRecyclerView.setAdapter(adapter);
    }


    private List<WeekDay> generateWeekDaysList(){
        List<WeekDay> list = new ArrayList<>();
        List<String> listWeekDays = Arrays.asList(getResources().getStringArray(R.array.week_days));
        List<String> listWeekDayCodes = Arrays.asList(getResources().getStringArray(R.array.week_day_code));
        List<WeekDay> selectedDays = PrefUtils.getWeekDays();
        if(selectedDays==null || selectedDays.isEmpty()) {
            for (int i = 0; i < listWeekDays.size(); i++) {
                list.add(new WeekDay(i, listWeekDays.get(i), listWeekDayCodes.get(i), false));
            }
        }else {
             return selectedDays;
        }
        return list;
    }


    private boolean checkIfReapeat(){
       for(WeekDay day : ((AdapterDayChoose)adapter).getList()){
           if(day.isChecked)
               return true;
       }
        return false;
    }



    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow().setLayout(PixelUtil.getScreenWidth(getActivity())/3*2, ViewGroup.LayoutParams.WRAP_CONTENT);
    }
}
