package com.aws3.ebenradio.view.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by Dell on 12.09.2016.
 */
public class AdapterViewPager extends FragmentPagerAdapter {

    private Context context;

    private List<Fragment> fragmentList;

    public AdapterViewPager(Context context, FragmentManager fm, List<Fragment> fragmentList) {
        super(fm);
        this.context = context;
        this.fragmentList = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }



    @Override
    public int getCount() {
        return fragmentList.size();
    }
}
