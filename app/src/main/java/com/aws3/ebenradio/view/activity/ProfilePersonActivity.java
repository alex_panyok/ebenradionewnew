package com.aws3.ebenradio.view.activity;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ActivityPersonProfileBinding;
import com.aws3.ebenradio.model.follow.FollowPersonEvent;
import com.aws3.ebenradio.model.user.UserPublicProfile;
import com.aws3.ebenradio.presenter.activity.ProfilePersonActivityPresenter;
import com.aws3.ebenradio.view.adapter.create_skin.AdapterAttr;
import com.aws3.ebenradio.view.views.activity.ProfilePersonActivityView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

@EActivity(R.layout.activity_person_profile)
public class ProfilePersonActivity extends BaseActivity<ProfilePersonActivityView, ProfilePersonActivityPresenter> implements ProfilePersonActivityView {



    private ActivityPersonProfileBinding binding;

    @ViewById(R.id.follow)
    View mViewFollow;

    @ViewById(R.id.ll_following)
    View mViewFollowing;

    @ViewById(R.id.add_follow)
    TextView mTextViewAddFollow;

    @NonNull
    @Override
    public ProfilePersonActivityPresenter createPresenter() {
        return new ProfilePersonActivityPresenter(this, getIntent().getStringExtra(ID));
    }

    @AfterViews
    void after(){
    }

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        Log.i("AFTER","ceate");
//        initBinding();
//    }


    @Override
    protected void onStart() {
        super.onStart();
        initBinding();
    }

    private void initBinding(){
        if(binding==null){
            binding = DataBindingUtil.setContentView(this, R.layout.activity_person_profile);
            binding.setPresenter(presenter);
            presenter.getUserProfile(getIntent().getStringExtra(ID));
            presenter.getFollowing();
//            presenter.applyCreatedSkin(((AdapterAttr)mRecycleViewAttrs.getAdapter()).getSelectedItem());
        }
    }

    @Override
    public void setUpColor(int color, int colorLight) {

    }

    @Override
    public void setUpProfile(UserPublicProfile user) {
        binding.setPerson(user);
    }

    @Override
    public void followUser(String message) {
        if(message.equals(getString(R.string.added_follow_s))){
            mTextViewAddFollow.setTextColor(Color.TRANSPARENT);
            mViewFollowing.setVisibility(View.VISIBLE);
            mViewFollow.setBackground(getResources().getDrawable(R.drawable.follow_shape_button));

        }else {
            mTextViewAddFollow.setTextColor(Color.WHITE);
            mViewFollowing.setVisibility(View.INVISIBLE);
            mViewFollow.setBackground(getResources().getDrawable(R.drawable.login_shape_button));

        }

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setUpFollowButton(boolean isFollowing) {
        if(isFollowing){
            mTextViewAddFollow.setTextColor(Color.TRANSPARENT);
            mViewFollowing.setVisibility(View.VISIBLE);
            mViewFollow.setBackground(getResources().getDrawable(R.drawable.follow_shape_button));
        }
    }
}
