package com.aws3.ebenradio.view.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.Toast;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.alarm.ChangeStationAlarm;
import com.aws3.ebenradio.model.event.event_er.OpenEventsEvent;
import com.aws3.ebenradio.model.event.radio.ChangeStationEvent;
import com.aws3.ebenradio.model.event_er.Banner;
import com.aws3.ebenradio.presenter.activity.EventBannerActivityPresenter;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.radio.RadioStationUtil;
import com.aws3.ebenradio.view.views.activity.EventBannerActivityView;
import com.bumptech.glide.Glide;
import com.hannesdorfmann.mosby.mvp.MvpActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

@EActivity(R.layout.activity_layout_banner)
public class EventBannerActivity extends MvpActivity<EventBannerActivityView, EventBannerActivityPresenter> implements EventBannerActivityView, EbenKeys {


    @ViewById(R.id.imageView)
    ImageView mImageView;


    @Click(R.id.imageView)
    void click(){
        presenter.viewBanner(((Banner) getIntent().getSerializableExtra(BANNER)));
        chooseType(((Banner) getIntent().getSerializableExtra(BANNER)));
    }


    @Click(R.id.cancel)
    void cancel(){
         presenter.viewBanner(((Banner) getIntent().getSerializableExtra(BANNER)));
        onBackPressed();
    }




    @AfterViews
    void after(){
        Glide.with(this).load("http://api.ebenradio.com/files/banner/image/"+((Banner) getIntent().getSerializableExtra(BANNER)).image).
                into(mImageView);
    }






    @NonNull
    @Override
    public EventBannerActivityPresenter createPresenter() {
        return new EventBannerActivityPresenter(this);
    }






    @Override
    public void onBackPressed() {
        presenter.viewBanner(((Banner) getIntent().getSerializableExtra(BANNER)));
        finish();
    }

    @Override
    public void setUpColor(int color, int colorLight) {

    }

    @Override
    public void bannerViewed(Banner banner) {

    }


    private void chooseType(Banner banner){
        switch (banner.type){
            case 1:
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URLUtil.isValidUrl(banner.url)?banner.url:"http://"+banner.url)));
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(this, "Can`t open this url", Toast.LENGTH_SHORT).show();
                }
                finish();
                break;

            case 2:
                EventBus.getDefault().postSticky(new OpenEventsEvent());
                finish();
                break;

            case 3:
                PrefUtils.setStation(RadioStationUtil.getRadioStationItems(this).get(banner.stream_id));
                EventBus.getDefault().postSticky(new ChangeStationEvent());
                EventBus.getDefault().postSticky(new ChangeStationAlarm());
                finish();
                break;
        }
    }


//    private void startMainActivityEvent(){
//        Intent intent = new Intent( this, MainActivity_.class );
//        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
//        intent.setAction(EVENT);
//        startActivity(intent);
//    }
}
