package com.aws3.ebenradio.view.adapter.create_skin;

import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ItemBackgroundAddBinding;
import com.aws3.ebenradio.databinding.ItemBackgroundBinding;
import com.aws3.ebenradio.model.create_skin.attrs.Background;
import com.aws3.ebenradio.presenter.activity.CreateSkinActivityPresenter;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Alex on 16.10.2016.
 */
 public class AdapterBackground extends AdapterAttr<RecyclerView.ViewHolder> {

    private List<Background> list;

    private MvpBasePresenter presenter;


    public static final int ADD_HOLDER = 1;

    public static final int BACKGROUND_HOLDER = 0;

    public AdapterBackground(List<Background> list, MvpBasePresenter presenter, int selectedItem) {
        super(selectedItem);
        this.list = list;
        this.presenter = presenter;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemBackgroundBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_background, parent,false);
        ItemBackgroundAddBinding addBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_background_add, parent,false);
        binding.setPresenter((CreateSkinActivityPresenter)presenter);
        addBinding.setPresenter((CreateSkinActivityPresenter)presenter);
        return viewType==0?new BackgroundHolder(binding.getRoot()):new AddBackgroundHolder(addBinding.getRoot());
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof BackgroundHolder)
            ((BackgroundHolder)holder).binding.setItem(list.get(position));
    }


    @Override
    public int getItemViewType(int position) {
        // Just as an example, return 0 or 2 depending on position
        // Note that unlike in ListView adapters, types don't have to be contiguous
        return position==getItemCount()-1?ADD_HOLDER:BACKGROUND_HOLDER;
    }


    public Background addItemBackground(String path){
        Background background = new Background(list.size(),0,path);
        list.add(background);
        notifyItemInserted(list.size()-1);
        return background;
    }




    @Override
    public int getItemCount() {
        return list.size()+1;
    }

    static class BackgroundHolder extends RecyclerView.ViewHolder{

        ItemBackgroundBinding binding;

        BackgroundHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }

    static class AddBackgroundHolder extends RecyclerView.ViewHolder{

        ItemBackgroundAddBinding binding;

        AddBackgroundHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }



}
