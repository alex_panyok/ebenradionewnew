package com.aws3.ebenradio.view.fragment;

import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.presenter.fragment.FragmentInfoPresenter;
import com.aws3.ebenradio.util.general.EditTextUtil;
import com.aws3.ebenradio.util.general.ToastUtil;
import com.aws3.ebenradio.view.views.fragment.FragmentInfoView;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

/**
 * Created by Dell on 09.11.2016.
 */
@EFragment(R.layout.fragment_info)
public class FragmentInfo extends BaseFragment<FragmentInfoView, FragmentInfoPresenter> implements FragmentInfoView, Validator.ValidationListener {


    public static FragmentInfo getInstance(FragmentInfo fragmentInfo){
        return fragmentInfo;
    }

    @NotEmpty
    @ViewById(R.id.name)
    EditText mEditTextName;

    @ViewById(R.id.name_input)
    TextInputLayout mInputName;


    @Email
    @ViewById(R.id.email)
    EditText mEditTextEmail;

    @ViewById(R.id.email_input)
    TextInputLayout mInputEmail;


    @NotEmpty
    @ViewById(R.id.message)
    EditText mEditTextMessage;

    @ViewById(R.id.message_input)
    TextInputLayout mInputMessage;

    private Validator validator;


    @AfterViews
    void after(){
        initValidator();
    }



    @Click
    void send(){
        EditTextUtil.setErrorNull(mInputName, mInputEmail, mInputMessage);
        validator.validate();
    }


    private void initValidator(){
        validator = new Validator(this);
        validator.setValidationListener(this);
    }


    @Override
    protected void skinEvent(ApplySkinEvent event) {

    }

    @Override
    public void setUpColor(int color, int colorLight) {

    }

    @NonNull
    @Override
    public FragmentInfoPresenter createPresenter() {
        return new FragmentInfoPresenter(getActivity());
    }

    @Override
    public void onValidationSucceeded() {
        EditTextUtil.setErrorNull(mInputName, mInputEmail, mInputMessage);
        presenter.sendMessage(mEditTextName.getText().toString(), mEditTextEmail.getText().toString(), mEditTextMessage.getText().toString());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());
            switch (view.getId()){
                case R.id.email:
                    mInputEmail.setError(message);
                    break;
                case R.id.name:
                    mInputName.setError(message);
                    break;
                case R.id.message:
                    mInputMessage.setError(message);
                    break;
            }
        }
    }

    @Override
    public void messageSent(String message) {
        EditTextUtil.clearFields(getActivity(),mEditTextName, mEditTextEmail, mEditTextMessage);
        ToastUtil.toast(getActivity(), message);
    }
}
