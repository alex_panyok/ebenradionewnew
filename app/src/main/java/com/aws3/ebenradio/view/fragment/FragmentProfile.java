package com.aws3.ebenradio.view.fragment;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.FragmentProfileBinding;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.model.event.user.UpdateProfileEvent;
import com.aws3.ebenradio.model.follow.FollowPersonEvent;
import com.aws3.ebenradio.model.follow.FollowResponse;
import com.aws3.ebenradio.presenter.fragment.FragmentProfilePresenter;
import com.aws3.ebenradio.util.user.UserDataPref;
import com.aws3.ebenradio.view.adapter.AdapterFollow;
import com.aws3.ebenradio.view.custom_views.FixedGridLayoutManager;
import com.aws3.ebenradio.view.views.fragment.FragmentProfileView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Dell on 26.10.2016.
 */
@EFragment(R.layout.fragment_profile)
public class FragmentProfile extends BaseFragment<FragmentProfileView, FragmentProfilePresenter> implements FragmentProfileView {



    private FragmentProfileBinding binding;


    public static FragmentProfile getInstance(FragmentProfile fragmentProfile){
        return fragmentProfile;
    }

    @ViewById(R.id.rv_follow)
    RecyclerView mRecyclerViewFollow;


    @ViewById(R.id.observable_scrollview)
    ObservableScrollView mObservableScrollView;

    @ViewById(R.id.ll_sticky)
    View mViewSticky;

    @ViewById(R.id.ll_sticky_1)
    View mViewSticky1;

    @ViewById(R.id.main_ll)
    View mViewMainLL;

    @ViewById(R.id.ll_header)
    View mHeaderLL;

    private int mViewStickTop;

    private AdapterFollow adapterFollowers;

    private AdapterFollow adapterFollowing;

    private int followIds[] = {R.id.followers, R.id.following};

    private int followid = R.id.following;


    @Override
    protected void skinEvent(ApplySkinEvent event) {

    }

    @Override
    public void setUpColor(int color, int colorLight) {

    }

    @AfterViews
    void after(){
        initRv();
        binding = FragmentProfileBinding.bind(getView());
        if(binding!=null)
        binding.setUser(UserDataPref.getUserFromPrefs(getContext()));
        binding.setPresenter(presenter);
        presenter.getFollow();
        initListenerToScrollView();
    }







    private void initRv(){
        mRecyclerViewFollow.setLayoutManager(new FixedGridLayoutManager(getContext(), 4));
        mRecyclerViewFollow.setItemAnimator(null);
        mRecyclerViewFollow.setNestedScrollingEnabled(false);

    }


    private void initListenerToScrollView(){
        mObservableScrollView.setScrollViewCallbacks(new ObservableScrollViewCallbacks() {
            @Override
            public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
                if (mViewStickTop == 0) {
                    mViewStickTop = mViewSticky.getTop();
                    LinearLayout.LayoutParams lp =
                            new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, mViewMainLL.getHeight() - mViewSticky1.getHeight());
                    mRecyclerViewFollow.setLayoutParams(lp);
                }
                if (scrollY >= mViewStickTop) {
                    mViewSticky.setTranslationY(scrollY - mViewStickTop);
                    mObservableScrollView.setEnabled(false);
                    mRecyclerViewFollow.setNestedScrollingEnabled(true);
                } else {
                    mRecyclerViewFollow.setNestedScrollingEnabled(false);
                }
            }

            @Override
            public void onDownMotionEvent() {

            }

            @Override
            public void onUpOrCancelMotionEvent(ScrollState scrollState) {

            }
        });
    }


    @NonNull
    @Override
    public FragmentProfilePresenter createPresenter() {
        return new FragmentProfilePresenter(getActivity());
    }

    @Subscribe(sticky = true)
    public void onFollowPersonEvent(FollowPersonEvent event){
        presenter.getFollowers();
        presenter.getFollowing();
        mRecyclerViewFollow.getAdapter().notifyItemRangeChanged(0, mRecyclerViewFollow.getAdapter().getItemCount());
        EventBus.getDefault().removeStickyEvent(event);
    }


    @Subscribe(sticky = true)
    public void onUpdateProfileEvent(UpdateProfileEvent event){
        if(binding!=null)
            binding.setUser(event.user);
        EventBus.getDefault().removeStickyEvent(event);
    }


    private void chooseSelected(int followid){
        this.followid = followid;
        for(int id:followIds){
            ((TextView)getView().findViewById(id)).setTextColor(followid==id?ContextCompat.getColor(getContext(),R.color.colorAccent):ContextCompat.getColor(getContext(),R.color.colorWhiteHalf));
        }
    }

    @Override
    public void setUpFollowers(FollowResponse followers) {
        binding.setFollowersResponse(followers);
        adapterFollowers = new AdapterFollow(followers.followersList, presenter);
        if(followid == R.id.followers)
            selectFollowers();
    }

    @Override
    public void setUpFollowing(FollowResponse following) {
        binding.setFollowingResponse(following);
        adapterFollowing = new AdapterFollow(following.followingList, presenter);
        if(followid == R.id.following)
            selectFollowing();


    }

    @Override
    public void selectFollowers() {
        chooseSelected(R.id.followers);
//        adapterFollow.setData(list);
        if(adapterFollowers!=null)
            mRecyclerViewFollow.setAdapter(adapterFollowers);

    }

    @Override
    public void selectFollowing() {
        chooseSelected(R.id.following);
//        adapterFollow.setData(list);
        if(adapterFollowing!=null)
        mRecyclerViewFollow.setAdapter(adapterFollowing);
    }
}
