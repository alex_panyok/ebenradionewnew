package com.aws3.ebenradio.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.RightMenuItemBinding;
import com.aws3.ebenradio.model.radio.RadioStation;
import com.aws3.ebenradio.presenter.activity.BaseDrawerActivityPresenter;
import com.aws3.ebenradio.presenter.activity.MainActivityPresenter;
import com.aws3.ebenradio.view.activity.BaseDrawerActivity;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Dell on 14.10.2016.
 */
public class RightMenuAdapter extends RecyclerView.Adapter<RightMenuAdapter.RightMenuHolder> {

    private BaseDrawerActivityPresenter presenter;
    private List<RadioStation> list;

    public RightMenuAdapter(BaseDrawerActivityPresenter presenter, List<RadioStation> list) {
        this.presenter = presenter;
        this.list = list;
    }

    @Override
    public RightMenuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RightMenuItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.right_menu_item,parent,false);
        binding.setPresenter(presenter);
        return new RightMenuHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(RightMenuHolder holder, int position) {
        holder.binding.setItem(list.get(position));
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).id;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class RightMenuHolder extends RecyclerView.ViewHolder{

        RightMenuItemBinding binding;



        public RightMenuHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
