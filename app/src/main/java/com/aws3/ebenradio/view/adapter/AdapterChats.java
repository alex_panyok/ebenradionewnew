package com.aws3.ebenradio.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ItemChatBinding;
import com.aws3.ebenradio.model.chat.UserChat;
import com.aws3.ebenradio.presenter.fragment.FragmentChoosePresenter;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Dell on 14.09.2016.
 */
public class AdapterChats extends RecyclerView.Adapter<AdapterChats.Holder> {


    private List<UserChat> list;

    private MvpBasePresenter presenter;

    public AdapterChats(List<UserChat> list, MvpBasePresenter presenter) {
        this.presenter = presenter;
        this.list = list;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemChatBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_chat, parent, false);
//        View view = LayoutInflater.from(context).inflate(R.layout.search_people_item, parent, false);
        binding.setPresenter((FragmentChoosePresenter) presenter);
        return new Holder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.binding.setItem(list.get(position));
    }

    public void setData(List<UserChat> list){
        int size = this.list.size();
        this.list = list;
        if(!list.isEmpty())
            notifyItemRangeChanged(0, list.size());
        else
            notifyItemRangeRemoved(0,size);

    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(list.get(position).userWithId);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {

        ItemChatBinding binding;

        public Holder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }


    }
}
