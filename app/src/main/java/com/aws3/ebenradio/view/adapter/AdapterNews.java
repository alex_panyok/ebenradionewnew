package com.aws3.ebenradio.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ItemNewsBinding;
import com.aws3.ebenradio.model.rss.Item;
import com.aws3.ebenradio.presenter.fragment.FragmentNewsPresenter;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Alex on 29.10.2016.
 */
public class AdapterNews extends RecyclerView.Adapter<AdapterNews.NewsHolder> {

    private List<Item> list;

    private MvpBasePresenter presenter;

    public AdapterNews(List<Item> list, MvpBasePresenter presenter) {
        this.list = list;
        this.presenter = presenter;
    }


    @Override
    public NewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemNewsBinding  binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_news, parent,false );
        binding.setPresenter((FragmentNewsPresenter) presenter);
        return new NewsHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(NewsHolder holder, int position) {
        holder.binding.setItem(list.get(position));
    }

    public void setData(List<Item> list){
        this.list = list;
        notifyItemRangeChanged(0, list.size());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class NewsHolder extends RecyclerView.ViewHolder{


        ItemNewsBinding binding;


        public NewsHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
