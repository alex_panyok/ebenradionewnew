package com.aws3.ebenradio.view.custom_views.visualizer_renderes;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.aws3.ebenradio.model.visualizer.AudioData;
import com.aws3.ebenradio.model.visualizer.FFTData;
import com.google.common.primitives.Bytes;
import com.google.common.primitives.Chars;
import com.google.common.primitives.Ints;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

/**
 * Created by Dell on 15.11.2016.
 */
public class WaveFormView extends Renderer {

    private static final float defaultFrequency = 8f;
    private static final float defaultAmplitude = 0.5f;
    private static final float defaultIdleAmplitude = 0f;
    private static final float defaultNumberOfWaves = 3.0f;
    private static final float defaultPhaseShift = -0.4f;
    private static final float defaultDensity = 5.0f;
    private static final float defaultPrimaryLineWidth = 6.0f;
    private static final float defaultSecondaryLineWidth = 2.0f;

    private float phase;
    private float amplitude;
    private float frequency;
    private float idleAmplitude;
    private float numberOfWaves;
    private float phaseShift;
    private float density;
    private float primaryWaveLineWidth;
    private float secondaryWaveLineWidth;
    Paint mPaintColor;
    Rect rect1;
    boolean isStraightLine = false;
    boolean isUp = true;
    public WaveFormView(Context context) {
        super();
        setUp();
    }


    private void setUp() {
        this.frequency = defaultFrequency;

        this.amplitude = defaultAmplitude;
        this.idleAmplitude = defaultIdleAmplitude;

        this.numberOfWaves = defaultNumberOfWaves;
        this.phaseShift = defaultPhaseShift;
        this.density = defaultDensity;

        this.primaryWaveLineWidth = defaultPrimaryLineWidth;
        this.secondaryWaveLineWidth = defaultSecondaryLineWidth;
        mPaintColor = new Paint();
        mPaintColor.setColor(Color.WHITE);
    }

    public void updateAmplitude(float ampli, boolean isSpeaking) {
        this.amplitude = Math.max(ampli, idleAmplitude);
        isStraightLine = isSpeaking;
    }


    private float averageByteLow(byte[] bytes){
        int sum = 0;
        for(int i=0; i<bytes.length/2;i++){
            sum +=bytes[i];
        }
        return Math.abs(((float)sum)/bytes.length);
    }



    private float averageByteHigh(byte[] bytes){
        int sum = 0;
        for(int i=bytes.length/2; i<bytes.length;i++){
            sum +=bytes[i];
        }
        return Math.abs(((float)sum)/bytes.length);
    }


    private void downFreq(){
        if(!isUp) {
            if (frequency > 0.5f) {
                frequency = frequency - 0.1f;
            } else {
                isUp = true;
            }
        }

    }

    private void riseFreq(){
        if(isUp) {
            if (frequency < 6f) {
                frequency = frequency + 0.1f;
            } else {
                isUp = false;
            }
        }else {
            downFreq();
        }

    }


    private float sumLow(byte[] bytes){
        int sum = 0;
        for(byte b : bytes){
            if(gmp_sign((int)b)==-1)
                sum +=(int)b;
        }
        return Math.abs(sum);
    }

    private int sumHigh(byte[] bytes){
        int sum = 0;
        for(byte b : bytes){
            if(gmp_sign((int)b)==1)
                sum +=(int)b;
        }
        return sum;
    }


    private int gmp_sign(int i){
        return i==0?0:i<0?-1:1;
    }

    private boolean gmp_sign_equal(int i, int i2){
        return gmp_sign(i)==gmp_sign(i2);
    }


    @Override
    public void onRender(Canvas canvas, AudioData data, Rect rect) {
        amplitude = defaultAmplitude*sumLow(data.bytes)/sumHigh(data.bytes);
//            riseFreq();


        for (int i = 0; i < numberOfWaves; i++) {
            mPaintColor.setStrokeWidth(i == 0 ? primaryWaveLineWidth : secondaryWaveLineWidth);
            float halfHeight = canvas.getHeight() / 2;
            float width1 = canvas.getWidth();
            float mid = canvas.getWidth() / 2;
            float maxAmplitude = halfHeight - 4.0f;
            float progress = 1.0f - (float) i / this.numberOfWaves;
            float normedAmplitude = (1.5f * progress - 0.5f) * this.amplitude;
            Path path = new Path();

            float multiplier = Math.min(1.0f, (progress / 3.0f * 2.0f) + (1.0f / 3.0f));

            for (float x = 0; x < width1 + density; x += density) {
                // We use a parable to scale the sinus wave, that has its peak in the middle of the view.
                float scaling = (float) (-Math.pow(1 / mid * (x - mid), 2) + 1);

                float y = (float) (scaling * maxAmplitude * normedAmplitude * Math.sin(2 * Math.PI * (x / width1) * frequency + phase) + halfHeight);

                if (x == 0) {
                    path.moveTo(x, y);
                } else {
                    path.lineTo(x, y);
                }
            }
            mPaintColor.setStyle(Paint.Style.STROKE);
            mPaintColor.setAntiAlias(true);
            canvas.drawPath(path, mPaintColor);

        }
        this.phase += phaseShift;



    }

    @Override
    public void onRender(Canvas canvas, FFTData data, Rect rect, float width) {
//        canvas.drawColor(Color.BLUE);
              /*canvas.drawRect(rect, mPaintColor);*/
//        if(isStraightLine) {;


    }
}
