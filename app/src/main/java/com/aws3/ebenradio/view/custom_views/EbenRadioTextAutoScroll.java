package com.aws3.ebenradio.view.custom_views;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;

public class EbenRadioTextAutoScroll extends EbenRadioTextView {
    public EbenRadioTextAutoScroll(Context context, AttributeSet attrs,
                                   int defStyle) {
        super(context, attrs, defStyle);
    }

    public EbenRadioTextAutoScroll(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EbenRadioTextAutoScroll(Context context) {
        super(context);
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction,
                                  Rect previouslyFocusedRect) {
        if (focused) {
            super.onFocusChanged(focused, direction, previouslyFocusedRect);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean focused) {
        if (focused) {
            super.onWindowFocusChanged(focused);
        }
    }

    @Override
    public boolean isFocused() {
        return true;
    }
}
