package com.aws3.ebenradio.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;


import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ActivityCreateSkinBinding;
import com.aws3.ebenradio.databinding.CreateSkinPlayerLayoutBinding;
import com.aws3.ebenradio.model.create_skin.categories.Category;
import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.presenter.activity.CreateSkinActivityPresenter;

import com.aws3.ebenradio.util.file.RealPathUtil;
import com.aws3.ebenradio.util.keys.PrefKeys;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.skins.CategoryUtil;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.view.adapter.create_skin.AdapterAttr;
import com.aws3.ebenradio.view.adapter.create_skin.AdapterCategory;
import com.aws3.ebenradio.view.views.activity.CreateSkinActivityView;
import com.bumptech.glide.Glide;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Arrays;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Dell on 11.10.2016.
 */
@EActivity(R.layout.activity_create_skin)
public class CreateSkinActivity extends BaseActivity<CreateSkinActivityView, CreateSkinActivityPresenter> implements CreateSkinActivityView {

    @ViewById(R.id.rv_categories)
    RecyclerView mRecyclerViewCategories;

    @ViewById(R.id.rv_attrs)
    RecyclerView mRecycleViewAttrs;

    @ViewById(R.id.category_ll)
    View mCategoryLL;

    @ViewById(R.id.ll_skin)
    View mViewSkin;

    @ViewById(R.id.create_skin)
    View mViewCreateSkin;

    ActivityCreateSkinBinding binding;


    CreateSkinPlayerLayoutBinding playerLayoutBinding;

    @Override
    protected void onStart() {
        super.onStart();
        if(binding==null) {
            binding = DataBindingUtil.setContentView(this, R.layout.activity_create_skin);
            playerLayoutBinding = DataBindingUtil.bind(mViewCreateSkin);
            binding.setPresenter(presenter);
            presenter.applyCreatedSkin(((AdapterAttr)mRecycleViewAttrs.getAdapter()).getSelectedItem());
        }
    }

    @AfterViews
    void after(){
        presenter.setCreatedSkin((Skin) getIntent().getSerializableExtra(SKIN), getIntent().getIntExtra(POSITION,-1));
        initRvAttrs();
        initRvCategories();
        presenter.setAttrAdapter(((AdapterCategory)mRecyclerViewCategories.getAdapter()).getList().get(0));
    }



    @NonNull
    @Override
    public CreateSkinActivityPresenter createPresenter() {
        return new CreateSkinActivityPresenter(this);
    }

    @Override
    public void setUpColor(int color, int colorLight) {
        mCategoryLL.setBackgroundColor(color);
    }

    private void initRvAttrs(){
        mRecycleViewAttrs.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
        mRecycleViewAttrs.setItemAnimator(null);
    }

    private void initRvCategories(){
        mRecyclerViewCategories.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false));
        mRecyclerViewCategories.setItemAnimator(null);
        mRecyclerViewCategories.setAdapter(new AdapterCategory(CategoryUtil.generateCategories(this,presenter,presenter.getCreatedSkin()),presenter));
    }



    @Override
    public void setAttrAdapter(Category category) {
        mRecyclerViewCategories.getAdapter().notifyItemRangeChanged(0, mRecyclerViewCategories.getAdapter().getItemCount());
        mRecyclerViewCategories.scrollToPosition(category.id);
        mRecycleViewAttrs.setAdapter(category.adapter);
        mRecycleViewAttrs.scrollToPosition(((AdapterAttr)mRecycleViewAttrs.getAdapter()).getSelectedItem());
    }

    @Override
    public void refreshSkin(int id, Skin skin) {
        binding.setSkin(skin);
        playerLayoutBinding.setSkin(skin);
        mRecycleViewAttrs.getAdapter().notifyItemRangeChanged(0,mRecycleViewAttrs.getAdapter().getItemCount());
        mRecycleViewAttrs.scrollToPosition(id);
    }

    @Override
    public void changeGraphicEQAdapterNone() {
        ((AdapterCategory)mRecyclerViewCategories.getAdapter()).getList().get(Arrays.asList(getResources().getStringArray(R.array.category_items)).indexOf(getString(R.string.graphic_eq))).adapter.setSelectedItem(1);
    }

    @Override
    public void getScreenShotView(String name, String fileName) {
        presenter.create(name, fileName, mViewSkin);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO && resultCode == Activity.RESULT_OK ) {
            if (data == null) {
                return;
            }
            presenter.addBackgroundAndApply(RealPathUtil.getRealPath(this,data.getData()));
        }
    }
}
