package com.aws3.ebenradio.view.views.fragment;

import com.aws3.ebenradio.model.event_er.Event;

import java.util.List;

/**
 * Created by Dell on 03.11.2016.
 */
public interface FragmentEventView extends BaseFragmentView{
    void setEvents(List<Event> list);
}
