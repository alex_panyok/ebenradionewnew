package com.aws3.ebenradio.view.views.fragment;

import com.aws3.ebenradio.model.follow.FollowPerson;
import com.aws3.ebenradio.model.follow.FollowResponse;
import com.google.common.cache.ForwardingLoadingCache;

import java.util.List;

/**
 * Created by Dell on 26.10.2016.
 */
public interface FragmentProfileView extends BaseFragmentView {

    void setUpFollowers(FollowResponse followers);

    void setUpFollowing(FollowResponse following);

    void selectFollowers();

    void selectFollowing();


}
