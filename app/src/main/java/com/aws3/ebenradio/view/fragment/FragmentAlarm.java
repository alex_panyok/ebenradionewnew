package com.aws3.ebenradio.view.fragment;

import android.support.annotation.NonNull;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.alarm.TimeAlarm;
import com.aws3.ebenradio.model.event.alarm.TurnOffAlarmEvent;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.presenter.fragment.FragmentAlarmPresenter;
import com.aws3.ebenradio.presenter.fragment.FragmentAlarmTimerPresenter;
import com.aws3.ebenradio.presenter.fragment.FragmentSleepTimerPresenter;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.view.views.fragment.FragmentAlarmView;
import com.aws3.ebenradio.view.views.fragment.FragmentSleepTimerView;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.Subscribe;

import java.util.Calendar;

/**
 * Created by Dell on 24.10.2016.
 */
@EFragment(R.layout.fragment_alarm)
public class FragmentAlarm extends BaseFragment<FragmentAlarmView, FragmentAlarmPresenter> implements FragmentAlarmView, TimePickerDialog.OnTimeSetListener {




    private TimePickerDialog tpd;


    @ViewById(R.id.tv_repeat)
    TextView mTextViewAlarm;

    @ViewById(R.id.tv_stream)
    TextView mTextViewStream;

    @ViewById(R.id.switch_alarm)
    SwitchCompat mSwitchAlarm;


    @ViewById(R.id.time)
    TextView mTextViewTime;

    DialogAlarmStations dialogAlarmStations;


    @Click
    void time(){
        tpd.show(getActivity().getFragmentManager(),"time");
    }



    @AfterViews
    void after(){
        initTimePicker(PrefUtils.getAlarmTime());
        presenter.setUpWeekDays(PrefUtils.getWeekDays());
        presenter.setUpStream(PrefUtils.getAlarmStation(getActivity()));
//        initListenerToAdapter();
        mSwitchAlarm.setChecked(PrefUtils.getAlarmStatus());
        initListenerToSwitch();
    }

    @Click
    void stream(){
        dialogAlarmStations =  DialogAlarmStations.getInstance(new DialogAlarmStations_(), presenter.fillStationsArray());
        dialogAlarmStations.show(getChildFragmentManager(),"stream");
    }
//
    @Click
    void repeat(){
        DialogFragmentAlarmTimer.getInstance(new DialogFragmentAlarmTimer_()).show(getChildFragmentManager(), "repeat");
    }


    @Override
    protected void skinEvent(ApplySkinEvent event) {

    }

    @Override
    public void setUpColor(int color, int colorLight) {
        tpd.setAccentColor(color);
    }

    @NonNull
    @Override
    public FragmentAlarmPresenter createPresenter() {
        return new FragmentAlarmPresenter(getContext());
    }


    @Subscribe
    public void onTurnOffAlarmEvent(TurnOffAlarmEvent event) {
        mSwitchAlarm.setChecked(false);
    }





    private void initTimePicker(TimeAlarm timeAlarm) {
        Calendar calendar= Calendar.getInstance();
        if(timeAlarm==null) {
            calendar.set(Calendar.HOUR_OF_DAY, 8);
            calendar.set(Calendar.MINUTE, 0);
        }else{
            calendar.set(Calendar.HOUR_OF_DAY, timeAlarm.hour);
            calendar.set(Calendar.MINUTE, timeAlarm.minute);}
        tpd = TimePickerDialog.newInstance(
                this,
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true
        );
        presenter.setHourAndMinute(calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE));
        mTextViewTime.setText(String.format("%02d:%02d",calendar.get(Calendar.HOUR_OF_DAY),calendar.get(Calendar.MINUTE)));
    }



    private void initListenerToSwitch(){
        mSwitchAlarm.setOnCheckedChangeListener(new SwitchCompat.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                presenter.setUpBroadcastAndSend(PrefUtils.getRepeatAlarm(), checked);
            }
        });
    }

    @Override
    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute, int second) {
        presenter.setHourAndMinute(hourOfDay,minute);
        mTextViewTime.setText(String.format("%02d:%02d", hourOfDay, minute));
        presenter.setUpBroadcastAndSend(PrefUtils.getRepeatAlarm(), PrefUtils.getAlarmStatus());
    }

    @Override
    public void setUpWeekDays(String weekDays) {
        mTextViewAlarm.setText(weekDays);
        presenter.setUpBroadcastAndSend(PrefUtils.getRepeatAlarm(), PrefUtils.getAlarmStatus());
    }

    @Override
    public void setUpStream(String title) {
        mTextViewStream.setText(title);
        if(dialogAlarmStations!=null)
        dialogAlarmStations.dismiss();
    }
}
