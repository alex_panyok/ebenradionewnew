package com.aws3.ebenradio.view.views.fragment;

import com.aws3.ebenradio.model.login.Male;

import java.util.List;

/**
 * Created by Dell on 25.10.2016.
 */
public interface FragmentSignupView extends BaseFragmentView {


    void setUpMaleSpinner(List<Male> list);

    void setUpYearSpinner(List<String> list);

    void registrationComplete();
}
