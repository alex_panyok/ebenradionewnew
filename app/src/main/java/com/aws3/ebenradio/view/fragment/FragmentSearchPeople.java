package com.aws3.ebenradio.view.fragment;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.FragmentSearchPeopleBinding;
import com.aws3.ebenradio.model.event.search.SearchPeopleEvent;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.model.user.UserFromSearch;
import com.aws3.ebenradio.model.user.UsersFromSearchResponse;
import com.aws3.ebenradio.presenter.fragment.FragmentSearchPeoplePresenter;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.aws3.ebenradio.view.activity.ProfilePersonActivity_;
import com.aws3.ebenradio.view.adapter.AdapterSearchPeople;
import com.aws3.ebenradio.view.custom_views.FixedLayoutManager;
import com.aws3.ebenradio.view.views.fragment.FragmentSearchPeopleView;


import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;


@EFragment(R.layout.fragment_search_people)
public class FragmentSearchPeople extends BaseFragment<FragmentSearchPeopleView, FragmentSearchPeoplePresenter> implements FragmentSearchPeopleView {

    @ViewById(R.id.rv_search_people)
    RecyclerView mRecyclerViewSearchPeople;


    @ViewById(R.id.people)
    TextView mTextViewPeople;


    private AdapterSearchPeople adapterSearchPeople;

    private FragmentSearchPeopleBinding binding;



    public static FragmentSearchPeople getInstance(FragmentSearchPeople fragmentSearchPeople, int distance){
        Bundle bundle = new Bundle();
        bundle.putInt(DISTANCE, distance);
        fragmentSearchPeople.setArguments(bundle);
        return fragmentSearchPeople;
    }


    @AfterViews
    void after(){
        initRecycleView();
        presenter.getPeople(getArguments().getInt(DISTANCE));
        binding = DataBindingUtil.bind(getView());

    }


    private void initRecycleView(){
        mRecyclerViewSearchPeople.setLayoutManager(new FixedLayoutManager(getActivity()));
        adapterSearchPeople = new AdapterSearchPeople(new ArrayList<UserFromSearch>(), presenter);
        adapterSearchPeople.setHasStableIds(true);
        mRecyclerViewSearchPeople.setAdapter(adapterSearchPeople);
    }

//
    @Subscribe
    public void onSearchEvent(SearchPeopleEvent event) {
        presenter.searchPeople(event.searchText);
    }

//
//    @Subscribe
//    public void onNetworkChange(NetworkChangeEvent event) {
//        if(event.isConnected)
//        presenter.getData(getArguments().getInt(DISTANCE));
//    }



//    @Override
//    public void onResume() {
//        super.onResume();
////        if(adapterSearchPeople==null){
////            presenter.getPeople(getArguments().getBoolean(ISALL), getArguments().getInt(DISTANCE));
////        }
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
////        presenter.unsubscribeSubscription();
//    }

    @Override
    protected void skinEvent(ApplySkinEvent event) {

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.unsubscribeSubscription();
    }

    @NonNull
    @Override
    public FragmentSearchPeoplePresenter createPresenter() {
        return new FragmentSearchPeoplePresenter(getActivity());
    }

//
    private void setAdapter(List<UserFromSearch> list){

        adapterSearchPeople.setData(list);
    }
//
//
//
//
//
//    private void initListenerToAdapter(){
//        adapterSearchPeople.setOnSelectListener(new AdapterSearchPeople.OnSelectListener() {
//            @Override
//            public void select(UserFromSearch user) {
//                startPersonProfileActivity(user.id);
//            }
//        });
//    }



    @Override
    public void setUpSearchPeople(UsersFromSearchResponse response) {
        binding.setResponse(response);
        setAdapter(response.users);
        mTextViewPeople.setText(getArguments().getInt(DISTANCE)==0?String.format(getString(R.string.people), "30000+"):String.format(getString(R.string.people),String.valueOf(response.users.size())));
    }

    @Override
    public void setUpColor(int color, int colorLight) {

    }
}
