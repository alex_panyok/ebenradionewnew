package com.aws3.ebenradio.view.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;



import java.util.List;

/**
 * Created by Dell on 30.09.2016.
 */
public class AdapterFragment extends FragmentStatePagerAdapter {

    private List<Fragment> fragmentList;

    private int[] titles;
    private Context context;

    public AdapterFragment(Context context, FragmentManager fm, List<Fragment> fragmentList, int []titles) {
        super(fm);
        this.fragmentList = fragmentList;
        this.titles = titles;
        this.context = context;
    }


    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return context.getString(titles[position]);
    }
}
