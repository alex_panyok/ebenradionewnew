package com.aws3.ebenradio.view.fragment;

import android.support.v4.app.Fragment;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.view.activity.LoginActivity_;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

/**
 * Created by Dell on 28.10.2016.
 */
@EFragment(R.layout.fragment_login_first)
public class FragmentLoginFirst extends Fragment {


    public static FragmentLoginFirst getInstance(FragmentLoginFirst fragmentLoginFirst){
        return fragmentLoginFirst;
    }


    @Click(R.id.log_in)
    void logIn(){
        LoginActivity_.intent(getContext()).start();
    }



}
