package com.aws3.ebenradio.view.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.ImageView;

import com.aws3.ebenradio.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.WindowFeature;

/**
 * Created by Dell on 16.12.2016.
 */
@Fullscreen
@WindowFeature({Window.FEATURE_NO_TITLE})
@EActivity(R.layout.activity_lancher)
public class LauncherActivity extends AppCompatActivity {


    private Handler handler;

    private Runnable runnable;

    @AfterViews
    void after(){
        handler  = new Handler();
        runnable =  new Runnable() {
            @Override
            public void run() {
                finish();
                MainActivity_.intent(LauncherActivity.this).flags(Intent.FLAG_ACTIVITY_SINGLE_TOP).start();
            }
        };
        handler.postDelayed(runnable, 1000);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        handler.removeCallbacks(runnable);
    }
}
