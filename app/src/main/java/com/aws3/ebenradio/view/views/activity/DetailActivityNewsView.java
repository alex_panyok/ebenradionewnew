package com.aws3.ebenradio.view.views.activity;

import com.aws3.ebenradio.model.rss.Item;

/**
 * Created by Alex on 29.10.2016.
 */
public interface DetailActivityNewsView extends BaseActivityView {

    void setItem(Item item);

    void back();
}
