package com.aws3.ebenradio.view.views.activity;

import com.aws3.ebenradio.model.login.Male;
import com.aws3.ebenradio.model.user.User;

import java.util.List;

/**
 * Created by Dell on 28.10.2016.
 */
public interface EditProfileActivityView extends BaseActivityView {

    void setUpUser(User user, int posCountry, int posGender, int posYear);

    void setUpMaleSpinner(List<Male> list);

    void setUpYearSpinner(List<String> list);
}
