package com.aws3.ebenradio.view.activity;

import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.alarm.ChangeStationAlarm;
import com.aws3.ebenradio.model.event.chat.NewMessageEvent;
import com.aws3.ebenradio.model.event.chat.RemoveUnreadDotEvent;
import com.aws3.ebenradio.model.event.user.UnreadCountEvent;
import com.aws3.ebenradio.model.radio.RadioStation;
import com.aws3.ebenradio.presenter.activity.BaseDrawerActivityPresenter;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.radio.RadioStationUtil;
import com.aws3.ebenradio.util.skins.BackgroundUtil;
import com.aws3.ebenradio.view.adapter.RightMenuAdapter;
import com.aws3.ebenradio.view.custom_views.DividerItemDecoration;
import com.aws3.ebenradio.view.fragment.FragmentAlarmTimer;
import com.aws3.ebenradio.view.fragment.FragmentAlarmTimer_;
import com.aws3.ebenradio.view.fragment.FragmentEqualizer;
import com.aws3.ebenradio.view.fragment.FragmentEqualizer_;
import com.aws3.ebenradio.view.fragment.FragmentEvent;
import com.aws3.ebenradio.view.fragment.FragmentEvent_;
import com.aws3.ebenradio.view.fragment.FragmentInfo;
import com.aws3.ebenradio.view.fragment.FragmentInfo_;
import com.aws3.ebenradio.view.fragment.FragmentMusic;
import com.aws3.ebenradio.view.fragment.FragmentMusic_;
import com.aws3.ebenradio.view.fragment.FragmentProfileContainer;
import com.aws3.ebenradio.view.fragment.FragmentProfileContainer_;
import com.aws3.ebenradio.view.fragment.FragmentSkins;
import com.aws3.ebenradio.view.fragment.FragmentSkins_;
import com.aws3.ebenradio.view.fragment.FragmentsNews;
import com.aws3.ebenradio.view.fragment.FragmentsNews_;
import com.aws3.ebenradio.view.views.activity.BaseDrawerActivityView;
import com.bumptech.glide.Glide;
import com.google.common.primitives.Ints;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by Dell on 10.10.2016.
 */
public abstract class BaseDrawerActivity<V extends BaseDrawerActivityView, P extends BaseDrawerActivityPresenter<V>> extends BaseActivity<V,P> implements BaseDrawerActivityView {

    private Fragment[] fragments = {FragmentMusic.getInstance(new FragmentMusic_()),
            FragmentProfileContainer.getInstance(new FragmentProfileContainer_(), PROF_FRAG_ID),
            FragmentProfileContainer.getInstance(new FragmentProfileContainer_(), CHAT_FRAG_ID),
            FragmentSkins.getInstance(new FragmentSkins_()),
            FragmentEvent.getInstance(new FragmentEvent_()),
            FragmentsNews.getInstance(new FragmentsNews_()),
            FragmentEqualizer.getInstance(new FragmentEqualizer_()),
            FragmentAlarmTimer.getInstance(new FragmentAlarmTimer_()),
            FragmentInfo.getInstance(new FragmentInfo_())};

    private int[] leftMenuIds = {R.id.music,R.id.profile,R.id.chats,R.id.skins, R.id.events, R.id.news, R.id.equalizer, R.id.timer_alarm, R.id.info_menu};


    private RecyclerView mRecycleViewRight;


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        initRecycleViewRightMenu();
        setUpDrawerControls();
        startFragmentByViewId(leftMenuIds[0]);
        updateStationIcon(PrefUtils.getStation());
    }

    public void setUpDrawerControls(){
        if(findViewById(R.id.menu)!=null){
            findViewById(R.id.menu).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    toggleDrawerMenu(Gravity.START);
                }
            });
            for(int id: leftMenuIds){
                findViewById(id).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startFragmentByViewId(view.getId());
                    }
                });
            }
        }
        if(findViewById(R.id.station)!=null){
            findViewById(R.id.station).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    toggleDrawerMenu(Gravity.END);
                }
            });
        }
    }




    @Override
    public void setStation(RadioStation radioStation){
        mRecycleViewRight.getAdapter().notifyDataSetChanged();
        if(getSupportFragmentManager().findFragmentByTag(FragmentMusic_.class.getName())!=null)
            ((TextView) findViewById(R.id.main_title)).setText(radioStation.title);
        updateStationIcon(radioStation);
        closeDrawer(Gravity.END);

    }



    private void updateStationIcon(RadioStation radioStation){
//        Glide.with(this).load(RadioStationUtil.getStationIconById(radioStation.imageRes)).dontAnimate().into((ImageView)findViewById(R.id.station));
        ((ImageView)findViewById(R.id.station)).setImageResource(RadioStationUtil.getStationIconById(radioStation.imageRes));
    }


    @Subscribe(sticky = true)
    public void onChangeStationAlarm(ChangeStationAlarm event){
        presenter.setStationUI(PrefUtils.getStation());
        EventBus.getDefault().removeStickyEvent(event);
    }

    protected void startEventFragment(){
        startFragmentByViewId(R.id.events);
    }





    private void initRecycleViewRightMenu(){
        mRecycleViewRight = (RecyclerView)findViewById(R.id.rv_station);
        mRecycleViewRight.setLayoutManager(new LinearLayoutManager(this));
        mRecycleViewRight.addItemDecoration(new DividerItemDecoration(this, R.drawable.divider, DividerItemDecoration.VERTICAL_LIST));
        mRecycleViewRight.setItemAnimator(null);
        mRecycleViewRight.setNestedScrollingEnabled(false);
        RightMenuAdapter adapter = new RightMenuAdapter(presenter, RadioStationUtil.getRadioStationItems(this));
        adapter.setHasStableIds(true);
        mRecycleViewRight.setAdapter(adapter);

    }

    private void startFragmentByViewId(int id){
        if(findViewById(R.id.main_title)!=null) {
            if(id!=R.id.music)
                ((TextView) findViewById(R.id.main_title)).setText(((TextView) ((ViewGroup) findViewById(id)).getChildAt(1)).getText().toString());
            else
                ((TextView) findViewById(R.id.main_title)).setText(getResources().getStringArray(R.array.station_titles)[PrefUtils.getStation().id]);
        }
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragments[Ints.asList(leftMenuIds).indexOf(id)], fragments[Ints.asList(leftMenuIds).indexOf(id)].getClass().getName()).commit();
        resetLeftMenuAndApply(id);
        closeDrawer(Gravity.START);
        if(id == R.id.chats)
            if(findViewById(R.id.dot_new_message)!=null){
                findViewById(R.id.dot_new_message).setVisibility(View.GONE);
            }
        if(id==R.id.music)
            openPlayer();
        else
            closePlayer();
    }


    private void resetLeftMenuAndApply(int id){
        TypedArray icons = getResources().obtainTypedArray(R.array.left_menu_icons);
        TypedArray icons_active = getResources().obtainTypedArray(R.array.left_menu_icons_active);
        for(int i  = 0; i<leftMenuIds.length;i++) {
            changeLeftMenuItem((ViewGroup) findViewById(leftMenuIds[i]), R.color.colorWhiteHalf, icons.getResourceId(i,0));
        }
        changeLeftMenuItem((ViewGroup) findViewById(id),R.color.colorWhite,icons_active.getResourceId(Ints.asList(leftMenuIds).indexOf(id),0));
        icons.recycle();
        icons_active.recycle();

    }


    private void changeLeftMenuItem(ViewGroup view, int colorId, int res){
        ((TextView)view.getChildAt(1)).setTextColor(ContextCompat.getColor(this,colorId));
        ((ImageView)view.getChildAt(0)).setImageResource(res);
    }



    public abstract void openPlayer();

    public abstract void closePlayer();

    private void openDrawer(int gravity){
        ((DrawerLayout)findViewById(R.id.drawer)).openDrawer(gravity);
    }


    private void closeDrawer(int gravity){
        ((DrawerLayout)findViewById(R.id.drawer)).closeDrawer(gravity);
    }
    private void toggleDrawerMenu(int gravity){
        if(((DrawerLayout)findViewById(R.id.drawer)).isDrawerOpen(gravity))
           closeDrawer(gravity);
        else
            openDrawer(gravity);
    }


    @Subscribe(sticky = true)
    public void onNewMessageEvent(NewMessageEvent event){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if(findViewById(R.id.dot_new_message)!=null){
                    findViewById(R.id.dot_new_message).setVisibility(View.VISIBLE);
                }
            }
        });
//        EventBus.getDefault().removeStickyEvent(event);
    }


    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onUnredCountEvent(UnreadCountEvent event){
                if(findViewById(R.id.dot_new_message)!=null){
                    findViewById(R.id.dot_new_message).setVisibility(View.VISIBLE);
                }
        EventBus.getDefault().removeStickyEvent(event);
    }


    @Subscribe(sticky = true)
    public void onRemoveUnreadDot(RemoveUnreadDotEvent event){
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                if(findViewById(R.id.dot_new_message)!=null){
                    findViewById(R.id.dot_new_message).setVisibility(View.GONE);
                }
            }
        });
        EventBus.getDefault().removeStickyEvent(event);
    }
}
