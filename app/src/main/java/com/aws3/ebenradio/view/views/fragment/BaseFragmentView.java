package com.aws3.ebenradio.view.views.fragment;

import com.hannesdorfmann.mosby.mvp.MvpView;

/**
 * Created by Dell on 11.10.2016.
 */
public interface BaseFragmentView extends MvpView {

    void setUpColor(int color, int colorLight);

}
