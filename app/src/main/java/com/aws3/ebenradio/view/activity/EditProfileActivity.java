package com.aws3.ebenradio.view.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.login.Male;
import com.aws3.ebenradio.model.user.User;
import com.aws3.ebenradio.presenter.activity.EdiProfileActivityPresenter;
import com.aws3.ebenradio.util.general.EditTextUtil;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.util.user.UserDataPref;
import com.aws3.ebenradio.view.views.activity.EditProfileActivityView;
import com.bumptech.glide.Glide;
import com.mobsandgeeks.saripaar.QuickRule;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Dell on 28.10.2016.
 */
@EActivity(R.layout.activity_edit_profile)
public class EditProfileActivity extends BaseActivity<EditProfileActivityView, EdiProfileActivityPresenter> implements EditProfileActivityView,  View.OnClickListener, DatePickerDialog.OnDateSetListener {

    @ViewById(R.id.spinner_country)
    AppCompatSpinner mSpinnerCountry;

    @ViewById(R.id.birthday)
    TextView mTextViewBirthday;

    @ViewById(R.id.spinner_gender)
    AppCompatSpinner mSpinnerGender;

    @ViewById(R.id.iv_profile)
    ImageView mImageViewProfile;

    @ViewById(R.id.name)
    EditText mEditTextName;

    @ViewById(R.id.name_input)
    TextInputLayout mTextInputLayoutName;

    @ViewById(R.id.email)
    EditText mEditTextEmail;

    @ViewById(R.id.email_input)
    TextInputLayout mTextInputLayoutEmail;



    @ViewById(R.id.about)
    EditText mEditTextAbout;



    @ViewById(R.id.photo)
    View mViewPhoto;




    private ArrayAdapter<String> adapterCountries;

    private ArrayAdapter<Male> adapterMale;

    private ArrayAdapter<String> adapterYear;


    private Validator validatorUser;

    private PopupWindow popupWindow;

    private DatePickerDialog datePickerDialog;



    @Click
    void done() {
        EditTextUtil.setErrorNull(mTextInputLayoutName, mTextInputLayoutEmail);
        validatorUser.validate();
    }


    @Click
    void photo() {
        popupWindow.showAsDropDown(mViewPhoto);
    }

    @Click
    void cancel() {
        onBackPressed();
    }


    @Click
    void birthday(){
        datePickerDialog.show(getFragmentManager(), datePickerDialog.getClass().getName());
    }




    @AfterViews
    void after(){
        initDatePicker();
        checkFacebook();
        initSpinnerCountries();
        presenter.setUpMaleArray();
        presenter.setUpYearArray();
        presenter.setUpUserData();
        initValidator();
        initPopupWindow();
    }


    private void checkFacebook(){
        if(UserDataPref.getUserFromPrefs(this).isFacebook)
            getSupportFragmentManager().beginTransaction().hide(getSupportFragmentManager().findFragmentById(R.id.change_pass)).commit();
    }



    @NonNull
    @Override
    public EdiProfileActivityPresenter createPresenter() {
        return new EdiProfileActivityPresenter(this);
    }


    @Override
    public void setUpColor(int color, int colorLight) {
        mSpinnerCountry.setPopupBackgroundDrawable(new ColorDrawable(color));
//        mSpinnerYear.setPopupBackgroundDrawable(new ColorDrawable(color));
        datePickerDialog.setAccentColor(color);
        mSpinnerGender.setPopupBackgroundDrawable(new ColorDrawable(color));
    }

    @Override
    public void setUpUser(User user, int posCountry, int posGender, int posYear) {
        mSpinnerCountry.setSelection(posCountry);
        mSpinnerGender.setSelection(posGender);
//        mSpinnerYear.setSelection(posYear);
        mTextViewBirthday.setText(user.birthday_year);
        mEditTextName.setText(user.full_name);
        mEditTextEmail.setText(user.email);
        mEditTextAbout.setText(user.about_you);
        Glide.with(this).load(user.photo).bitmapTransform(new CropCircleTransformation(this)).error(R.drawable.no_photo_bg).into(mImageViewProfile);
    }

    @Override
    public void setUpMaleSpinner(List<Male> list) {
        initSpinnerMale(list);
    }

    @Override
    public void setUpYearSpinner(List<String> list) {
//        initSpinnerYear(list);
    }



    private void initSpinnerCountries() {
        adapterCountries = new ArrayAdapter<>(this, R.layout.tv_spinner, Arrays.asList(getResources().getStringArray(R.array.countries)));
        adapterCountries.setDropDownViewResource(R.layout.row_spinner);
        mSpinnerCountry.setAdapter(adapterCountries);
    }

    private void initSpinnerMale(List<Male> list) {
        adapterMale = new ArrayAdapter<>(this, R.layout.tv_spinner, list);
        adapterMale.setDropDownViewResource(R.layout.row_spinner);
        mSpinnerGender.setAdapter(adapterMale);
    }


//    private void initSpinnerYear(List<String> list) {
//        adapterYear = new ArrayAdapter<>(this, R.layout.tv_spinner, list);
//        adapterYear.setDropDownViewResource(R.layout.row_spinner);
//        mSpinnerYear.setAdapter(adapterYear);
//    }


    private void initValidator() {
        validatorUser = new Validator(this);
        validatorUser.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                EditTextUtil.setErrorNull(mTextInputLayoutName, mTextInputLayoutEmail);
                presenter.updateUserProfile(mEditTextName.getText().toString()
                        ,mEditTextEmail.getText().toString()
                        ,mSpinnerCountry.getSelectedItemPosition()!=0?mSpinnerCountry.getSelectedItem().toString():""
                        ,mEditTextAbout.getText().toString()
                        ,((Male)mSpinnerGender.getSelectedItem()).type
                        , mTextViewBirthday.getText().toString());
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                for (ValidationError error : errors) {
                    View view = error.getView();
                    String message = error.getCollatedErrorMessage(EditProfileActivity.this);
                    // Display error messages ;)
                    switch (view.getId()){
                        case R.id.email:
                            mTextInputLayoutEmail.setError(message);
                            break;
                        case R.id.name:
                            mTextInputLayoutName.setError(message);
                            break;
                    }
                }
            }
        });
        validatorUser.put(mEditTextName, new QuickRule<EditText>() {
            @Override
            public boolean isValid(EditText view) {
                return view.getText().toString().replace(" ","").length()>=2;
            }

            @Override
            public String getMessage(Context context) {
                    return getString(R.string.name_min);
            }
        });
        validatorUser.put(mEditTextEmail, new QuickRule<EditText>() {
            @Override
            public boolean isValid(EditText view) {
                return GeneralUtil.checkEmail(view.getText().toString());
            }

            @Override
            public String getMessage(Context context) {
                return getString(R.string.invalid_email);
            }
        });
    }



    private void initPopupWindow(){
        popupWindow = new PopupWindow(initPopupView(), ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);
        popupWindow.setFocusable(true);

    }


    private View initPopupView(){
        View popupView = getLayoutInflater().inflate(R.layout.popup_select_image, null);
        popupView.findViewById(R.id.gallery).setOnClickListener(this);
        popupView.findViewById(R.id.photo).setOnClickListener(this);
        popupView.findViewById(R.id.delete).setOnClickListener(this);
        return popupView;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_PHOTO && resultCode == Activity.RESULT_OK || requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK) {
            if (data == null && requestCode == PICK_PHOTO) {
                return;
            }
            try {
                Uri imageUri = requestCode==TAKE_PHOTO_CODE? presenter.getTakePhotoUri() : data.getData();
                presenter.getPhoto(imageUri);
                Glide.with(this).load(imageUri).bitmapTransform(new CropCircleTransformation(this)).error(R.drawable.no_photo_bg).into(mImageViewProfile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.gallery:
                presenter.pickImage();
                break;
            case R.id.photo:
                presenter.takePhoto();
                break;
            case R.id.delete:
                presenter.deletePhoto();
                Glide.with(this).load(R.drawable.no_photo_bg).bitmapTransform(new CropCircleTransformation(this)).into(mImageViewProfile);
                break;
        }
        popupWindow.dismiss();
    }


    private void initDatePicker() {
        Calendar calendar= Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setMaxDate(calendar);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        mTextViewBirthday.setText(String.format(getString(R.string.birthday), monthOfYear<=8?String.format(getString(R.string.birthday_format), monthOfYear+1):monthOfYear+1, dayOfMonth<=9?String.format(getString(R.string.birthday_format), dayOfMonth):dayOfMonth, year));
    }
}
