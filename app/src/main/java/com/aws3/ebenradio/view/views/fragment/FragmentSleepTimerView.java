package com.aws3.ebenradio.view.views.fragment;

/**
 * Created by Dell on 24.10.2016.
 */
public interface FragmentSleepTimerView  extends  BaseFragmentView{

    void changeStatusTimer(boolean status);

    void updateTimer(int timeLeft);

    void newTime(int time);
}
