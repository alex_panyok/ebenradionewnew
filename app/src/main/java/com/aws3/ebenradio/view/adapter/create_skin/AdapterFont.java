package com.aws3.ebenradio.view.adapter.create_skin;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ItemFontBinding;
import com.aws3.ebenradio.model.create_skin.attrs.Font;
import com.aws3.ebenradio.presenter.activity.CreateSkinActivityPresenter;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Dell on 17.10.2016.
 */
public class AdapterFont extends AdapterAttr<AdapterFont.FontHolder> {

    private List<Font> list;

    private MvpBasePresenter presenter;

    public AdapterFont(List<Font> list, MvpBasePresenter presenter, int selectedItem) {
        super(selectedItem);
        this.list = list;
        this.presenter = presenter;
    }

    @Override
    public FontHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemFontBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_font, parent, false);
        binding.setPresenter((CreateSkinActivityPresenter)presenter);
        return new FontHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(FontHolder holder, int position) {
        holder.binding.setItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class FontHolder extends RecyclerView.ViewHolder{

        ItemFontBinding binding;

        public FontHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }


}
