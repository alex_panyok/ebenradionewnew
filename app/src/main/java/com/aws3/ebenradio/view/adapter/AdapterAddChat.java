package com.aws3.ebenradio.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ItemAddChatBinding;
import com.aws3.ebenradio.databinding.SearchPeopleItemBinding;
import com.aws3.ebenradio.model.follow.FollowPerson;
import com.aws3.ebenradio.model.user.UserFromSearch;
import com.aws3.ebenradio.presenter.activity.ChatAddActivityPresenter;
import com.aws3.ebenradio.presenter.fragment.FragmentSearchPeoplePresenter;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Dell on 14.09.2016.
 */
public class AdapterAddChat extends RecyclerView.Adapter<AdapterAddChat.Holder> {


    private List<FollowPerson> list;

    private MvpBasePresenter presenter;

    public AdapterAddChat(List<FollowPerson> list, MvpBasePresenter presenter) {
        this.presenter = presenter;
        this.list = list;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemAddChatBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_add_chat, parent, false);
//        View view = LayoutInflater.from(context).inflate(R.layout.search_people_item, parent, false);
        binding.setPresenter((ChatAddActivityPresenter) presenter);
        return new Holder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.binding.setItem(list.get(position));
    }

    public void setData(List<FollowPerson> list){
        int size = this.list.size();
        this.list = list;
        if(!list.isEmpty())
            notifyItemRangeChanged(0, list.size());
        else
            notifyItemRangeRemoved(0,size);

    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(list.get(position).id);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {

        ItemAddChatBinding binding;

        public Holder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }


    }
}
