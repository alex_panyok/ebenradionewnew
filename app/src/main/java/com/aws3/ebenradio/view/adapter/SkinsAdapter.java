package com.aws3.ebenradio.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.SkinItemBinding;
import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.presenter.fragment.FragmentSkinsPresenter;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Dell on 13.10.2016.
 */
public class SkinsAdapter extends RecyclerView.Adapter<SkinsAdapter.SkinsHolder> {

    private List<Skin> skins;

    private MvpBasePresenter presenter;

    OnSelectListener onSelectListener;

    OnLongSelectListener onLongSelectListener;
    public SkinsAdapter(List<Skin> skins, MvpBasePresenter presenter) {
        this.skins = skins;
        this.presenter = presenter;
    }

    @Override
    public SkinsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        SkinItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.skin_item,parent,false);
        binding.setPresenter((FragmentSkinsPresenter)presenter);
        return new SkinsHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(final SkinsHolder holder, int position) {
        holder.binding.setSkin(skins.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(onSelectListener!=null){
                    onSelectListener.select(holder.getAdapterPosition(), skins.get(holder.getAdapterPosition()));
                }
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(onLongSelectListener!=null){
                    onLongSelectListener.longSelect(holder.getAdapterPosition(), skins.get(holder.getAdapterPosition()));
                }
                return true;
            }
        });
    }


    public void setOnSelectListener(OnSelectListener onSelectListener){
        this.onSelectListener = onSelectListener;
    }

    public void setOnLongSelectListener(OnLongSelectListener onLongSelectListener){
        this.onLongSelectListener = onLongSelectListener;
    }


    public void removeSkin(int pos){
        skins.remove(pos);
        notifyItemRemoved(pos);
    }

    public interface OnSelectListener{
        void select(int pos, Skin skin);
    }

    public interface OnLongSelectListener{
        void longSelect(int pos, Skin skin);
    }

    public void setData(List<Skin> list){
        this.skins = list;
        notifyItemRangeChanged(0, list.size());
    }

    @Override
    public int getItemCount() {
        return skins.size();
    }

    public static class SkinsHolder extends RecyclerView.ViewHolder{

        SkinItemBinding binding;

        public SkinsHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
