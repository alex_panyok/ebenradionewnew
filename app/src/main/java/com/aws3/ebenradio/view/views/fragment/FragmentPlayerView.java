package com.aws3.ebenradio.view.views.fragment;

/**
 * Created by Dell on 12.10.2016.
 */
public interface FragmentPlayerView extends BaseFragmentView {
    void togglePlayButton(boolean isPlay);

    void share();

    void shareFacebook();

    void shareTwitter();

}
