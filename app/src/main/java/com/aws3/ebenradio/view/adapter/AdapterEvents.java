package com.aws3.ebenradio.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ItemEventBinding;
import com.aws3.ebenradio.model.event_er.Event;
import com.aws3.ebenradio.model.follow.FollowPerson;
import com.aws3.ebenradio.presenter.fragment.FragmentEventPresenter;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Dell on 03.11.2016.
 */
public class AdapterEvents extends RecyclerView.Adapter<AdapterEvents.EventsHolder> {

    private List<Event> list;

    private MvpBasePresenter presenter;

    public AdapterEvents(List<Event> list, MvpBasePresenter presenter) {
        this.list = list;
        this.presenter = presenter;
    }

    @Override
    public EventsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemEventBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_event, parent, false);
        binding.setPresenter((FragmentEventPresenter)presenter);
        return new EventsHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(EventsHolder holder, int position) {
        holder.binding.setItem(list.get(position));
    }

    public void setData(List<Event> list){
        int size = this.list.size();
        this.list = list;
        if(!list.isEmpty())
            notifyItemRangeChanged(0, list.size());
        else
            notifyItemRangeRemoved(0,size);

    }



    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class EventsHolder extends RecyclerView.ViewHolder{
        ItemEventBinding binding;
        public EventsHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
