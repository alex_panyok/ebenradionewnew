package com.aws3.ebenradio.view.fragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ShareCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.FragmentPlayerBinding;
import com.aws3.ebenradio.model.create_skin.attrs.Background;
import com.aws3.ebenradio.model.create_skin.attrs.GraphicEQ;
import com.aws3.ebenradio.model.event.player.ChangeStationPlayer;
import com.aws3.ebenradio.model.event.radio.AudioSessionEvent;
import com.aws3.ebenradio.model.event.radio.ChangeVolumeEvent;
import com.aws3.ebenradio.model.event.radio.TogglePlayEvent;
import com.aws3.ebenradio.model.event.radio.UpdatePlayerInfoEvent;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.presenter.fragment.FragmentPlayerPresenter;
import com.aws3.ebenradio.util.general.PixelUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.radio.AudioManagerUtil;
import com.aws3.ebenradio.util.radio.RadioPlayerState;
import com.aws3.ebenradio.util.radio.RadioStationUtil;
import com.aws3.ebenradio.util.skins.BackgroundUtil;
import com.aws3.ebenradio.util.skins.BlurUtil;
import com.aws3.ebenradio.util.skins.GraphicEQUtil;
import com.aws3.ebenradio.util.skins.NavigationUtil;
import com.aws3.ebenradio.util.skins.ShapeUtil;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.view.activity.MainActivity;
import com.aws3.ebenradio.view.custom_views.BlurTransformationPicassoV2;
import com.aws3.ebenradio.view.custom_views.BlurTransformationV2;
import com.aws3.ebenradio.view.custom_views.visualizer.VisualizerView;
import com.aws3.ebenradio.view.custom_views.visualizer_renderes.BarGraphRenderer;
import com.aws3.ebenradio.view.custom_views.visualizer_renderes.WaveFormView;
import com.aws3.ebenradio.view.views.fragment.FragmentPlayerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import jp.wasabeef.glide.transformations.BlurTransformation;

import static android.widget.RelativeLayout.TRUE;


@EFragment(R.layout.fragment_player)
public class FragmentPlayer extends BaseFragment<FragmentPlayerView, FragmentPlayerPresenter> implements FragmentPlayerView, SeekBar.OnSeekBarChangeListener {

    @ViewById(R.id.imageView_album)
    ImageView mImageViewAlbum;

    @ViewById(R.id.imageView_album_mini)
    ImageView mImageViewAlbumMini;

    @ViewById(R.id.blur)
    ImageView mImageViewBlur;

    @ViewById(R.id.title_song)
    TextView mTextViewTitle;

    @ViewById(R.id.play_mini)
    ImageView mImageViewPlayMini;

    @ViewById(R.id.play)
    ImageView mImageViewPlay;

    @ViewById(R.id.seek_bar)
    SeekBar mSeekBarVolume;



    @ViewById(R.id.title_song_mini)
    TextView mTextViewTitleSongMini;

    @ViewById(R.id.station_title_mini)
    TextView mTextViewTitleStationMini;

    @ViewById(R.id.rl_album_art)
    RelativeLayout mRelativeLayoutAlbumArt;



    private FragmentPlayerBinding binding;


    private UpdatePlayerInfoEvent songInfo;


    @AfterViews
    void after(){
        binding = FragmentPlayerBinding.bind(getView());
        binding.setSkin(presenter.getSkin());
        binding.setPresenter(presenter);
        applyStation();
        initSeekBarVolume();
        if(RadioPlayerState.getState() == RadioPlayerState.STATE_PLAY){
            setUpVisualizer();
        }
    }


    private void initSeekBarVolume(){
        mSeekBarVolume.setMax(presenter.getMaxValueVolume());
        mSeekBarVolume.setProgress(presenter.getCurrentValueVolume());
        mSeekBarVolume.setOnSeekBarChangeListener(this);
    }


    @NonNull
    @Override
    public FragmentPlayerPresenter createPresenter() {
        return new FragmentPlayerPresenter(getActivity());
    }

    @Override
    public void setUpColor(int color, int colorLight) {

    }




    @Override
    protected void skinEvent(ApplySkinEvent event) {
        EventBus.getDefault().removeStickyEvent(event);
        binding.setSkin(presenter.getSkin());
        removeAddVisualizer(event.skin);
        updateSongInfo(songInfo);
        addShinGrapichEQ(RadioPlayerState.getState() == RadioPlayerState.STATE_PLAY);
    }


    private void removeAddVisualizer(Skin skin){
        VisualizerView visualizerView = (VisualizerView) getView().findViewById(R.id.visualizer);
        RelativeLayout relativeLayout = (RelativeLayout) visualizerView.getParent();
        relativeLayout.removeView(relativeLayout.findViewById(R.id.visualizer));
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, PixelUtil.dpToPx(getActivity(), 60));
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, TRUE);
        visualizerView.setLayoutParams(params);
        relativeLayout.addView(visualizerView);
    }

//
    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onAudioSessionId(final AudioSessionEvent event) {
        setUpVisualizer();
        EventBus.getDefault().removeStickyEvent(event);
    }


    private void setUpVisualizer(){
        ((VisualizerView)getView().findViewById(R.id.visualizer)).link(AudioManagerUtil.getInstance().getSessionId());
        addShinGrapichEQ(RadioPlayerState.getState() == RadioPlayerState.STATE_PLAY);
    }






    @Subscribe(sticky = true)
    public void onTogglePlayEvent(TogglePlayEvent event){
        presenter.togglePlayButton(event);
        EventBus.getDefault().removeStickyEvent(event);
        addShinGrapichEQ(event.isPlay);
    }


    @Subscribe(sticky = true)
    public void onChangeVolumeEvent(ChangeVolumeEvent event){
        mSeekBarVolume.setProgress(presenter.getCurrentValueVolume());
        EventBus.getDefault().removeStickyEvent(event);
    }

    @Subscribe(sticky = true)
    public void onChangeStationPlayerEvent(ChangeStationPlayer event){
       applyStation();
        ((VisualizerView)getView().findViewById(R.id.visualizer)).clearRenderers();
        if(RadioPlayerState.getState() == RadioPlayerState.STATE_STOP)
            ((MainActivity)getActivity()).startRadioServicePlay();
        EventBus.getDefault().removeStickyEvent(event);
    }


    private void applyStation(){
        mTextViewTitle.setText("");
        mTextViewTitleSongMini.setText("");
        mTextViewTitleStationMini.setText(PrefUtils.getStation().title);
        Picasso.with(getActivity()).cancelRequest(mImageViewAlbum);
        Picasso.with(getActivity()).cancelRequest(mImageViewAlbumMini);
        Picasso.with(getActivity()).cancelRequest(mImageViewBlur);
        Picasso.with(getActivity()).load(RadioStationUtil.getUrlImageStationById(PrefUtils.getStation().id)).resize(300,300)
                .centerCrop()
                .into(mImageViewAlbum);
        Picasso.with(getActivity()).load(RadioStationUtil.getUrlImageStationById(PrefUtils.getStation().id)).resize(300,300)
                .centerCrop()
                .into(mImageViewAlbumMini);
        if (presenter.getSkin().blur == BlurUtil.BLUR)
            Picasso.with(getActivity()).load(RadioStationUtil.getUrlImageStationById(PrefUtils.getStation().id)).transform(new BlurTransformationPicassoV2(getActivity())).into(mImageViewBlur);
    }



    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onUpdateSongInfo(UpdatePlayerInfoEvent event){
        this.songInfo = event;
        if(!mTextViewTitle.getText().toString().equals(event.titleSong) || event.update) {
            updateMetadata(event);
        }
        EventBus.getDefault().removeStickyEvent(event);
    }



    private void updateSongInfo(UpdatePlayerInfoEvent event){
        if(event!=null) {
            updateMetadata(event);
        }
    }

    private void updateMetadata(UpdatePlayerInfoEvent event){
        mTextViewTitle.setText(event.titleSong);
        mTextViewTitleSongMini.setText(event.titleSong);

        Picasso.with(getActivity()).load(event.coverSong.isEmpty()?"image":event.coverSong).resize(300,300)
                .centerCrop()
                .placeholder(BackgroundUtil.getRadioPlaceholder(getActivity()))
                .error(BackgroundUtil.getRadioPlaceholder(getActivity()))
                .into(mImageViewAlbum);
        Picasso.with(getActivity()).load(event.coverSong.isEmpty()?"image":event.coverSong).resize(300,300)
                .centerCrop()
                .placeholder(BackgroundUtil.getRadioPlaceholder(getActivity()))
                .error(BackgroundUtil.getRadioPlaceholder(getActivity()))
                .into(mImageViewAlbumMini);
        if (presenter.getSkin().blur== BlurUtil.BLUR)
            Picasso.with(getActivity()).load(event.coverSong.isEmpty()?"image":event.coverSong).transform(new BlurTransformationPicassoV2(getActivity())).into(mImageViewBlur, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(getActivity()).load(RadioStationUtil.getUrlImageStationById(PrefUtils.getStation().id)).noFade().transform(new BlurTransformationPicassoV2(getActivity())).into(mImageViewBlur);
                }
            });

    }




    @Override
    public void togglePlayButton(boolean isPlay) {
        mImageViewPlay.setImageResource(!isPlay? NavigationUtil.getControls(presenter.getSkin().controls.playId, getActivity()).playId:NavigationUtil.getControls(presenter.getSkin().controls.playId, getActivity()).pauseId);
        mImageViewPlayMini.setImageResource(!isPlay? NavigationUtil.getControls(presenter.getSkin().controls.playId, getActivity()).playId:NavigationUtil.getControls(presenter.getSkin().controls.playId, getActivity()).pauseId);
    }






    public void shareBackground(){
                    Glide.with(getActivity())
                                    .load(songInfo.coverSong)
                                    .asBitmap()
                                    .into(new SimpleTarget<Bitmap>(300, 300) {
                                        @Override
                                        public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                            bitmapShare(resource);
                                        }

                                        @Override
                                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                            super.onLoadFailed(e, errorDrawable);
                                            bitmapShare(BitmapFactory.decodeResource(getResources(), BackgroundUtil.getRadioPlaceholder(getActivity())));
                                            Glide.clear(this);
                                        }
                                    });


    }


    public void bitmapShare(Bitmap bitmap){
        shareMainThread(Uri.parse(MediaStore.Images.Media.insertImage(getActivity().getContentResolver(), bitmap, "Nur", null)));
    }

    @UiThread
    public void shareMainThread(Uri uri){
        ShareCompat.IntentBuilder
                .from(getActivity())
                .setText(String.format(getString(R.string.share_text), songInfo.titleSong, PrefUtils.getStation().title))
                .setType("image/jpeg")
                .addStream(uri)
                .startChooser();
    }

    @Override
    public void share() {
        if(songInfo!=null)
            shareBackground();
    }


    @Override
    public void shareFacebook() {
        if(songInfo!=null)
            presenter.shareFacebook(getString(R.string.play_market_url),getString(R.string.app_name), String.format(getString(R.string.share_desc), songInfo.titleSong,PrefUtils.getStation().title), songInfo.coverSong);
    }

    @Override
    public void shareTwitter() {
        if(songInfo!=null)
            presenter.shareTwitter(getString(R.string.url),String.format(getString(R.string.share_twitter), songInfo.titleSong,PrefUtils.getStation().title), songInfo.coverSong);
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
        presenter.changeVolume(i);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }


    private void addShinGrapichEQ(boolean isPlay){
        ((VisualizerView)getView().findViewById(R.id.visualizer)).clearRenderers();
        if(isPlay)
            switch (SkinPrefUtils.getSkin().visualizer){
                case GraphicEQUtil.CLASSIC:
                    addBarGraphRenderers();
                    break;

                case  GraphicEQUtil.WAVES:
                    addWaveFromView();
                    break;
        }
    }


    private void addBarGraphRenderers()
    {
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.argb(255, 255, 255, 255));
        BarGraphRenderer barGraphRendererBottom = new BarGraphRenderer(getContext(), paint, false);
        ((VisualizerView)getView().findViewById(R.id.visualizer)).addRenderer(barGraphRendererBottom);
    }
//
//
    private void addWaveFromView(){
        ((VisualizerView)getView().findViewById(R.id.visualizer)).addRenderer(new WaveFormView(getContext()));
    }
}
