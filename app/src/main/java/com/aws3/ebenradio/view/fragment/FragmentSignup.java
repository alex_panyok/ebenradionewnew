package com.aws3.ebenradio.view.fragment;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatSpinner;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.model.event.user.LoginEvent;
import com.aws3.ebenradio.model.login.Male;
import com.aws3.ebenradio.presenter.fragment.FragmentSignupPresenter;
import com.aws3.ebenradio.util.general.EditTextUtil;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.view.views.fragment.FragmentSignupView;
import com.mobsandgeeks.saripaar.QuickRule;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Max;
import com.mobsandgeeks.saripaar.annotation.Min;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Dell on 25.10.2016.
 */
@EFragment(R.layout.fragment_signup)
public class FragmentSignup extends BaseFragment<FragmentSignupView, FragmentSignupPresenter> implements FragmentSignupView, Validator.ValidationListener, DatePickerDialog.OnDateSetListener{





    public static FragmentSignup getInstance(FragmentSignup fragmentSignup){
        return fragmentSignup;
    }



    @ViewById(R.id.spinner_country)
    AppCompatSpinner mSpinnerCountry;


    @ViewById(R.id.spinner_gender)
    AppCompatSpinner mSpinnerGender;

    @ViewById(R.id.birthday)
    TextView mTextViewBirthday;




    @ViewById(R.id.name)
    EditText mEditTextName;

    @ViewById(R.id.name_input)
    TextInputLayout mTextInputLayoutName;



    @ViewById(R.id.email)
    EditText mEditTextEmail;

    @ViewById(R.id.email_input)
    TextInputLayout mTextInputLayoutEmail;



    @Password(messageResId = R.string.password_min)
    @ViewById(R.id.password)
    EditText mEditTextPassword;

    @ViewById(R.id.password_input)
    TextInputLayout mTextInputLayoutPass;



    @ConfirmPassword(messageResId = R.string.password_not_match)
    @ViewById(R.id.re_password)
    EditText mEditTextRepass;

    @ViewById(R.id.re_password_input)
    TextInputLayout mTextInputLayoutRepass;




    private Validator validator;


    private DatePickerDialog datePickerDialog;

    @Click(R.id.sign_up)
    void signUp(){
        EditTextUtil.setErrorNull(mTextInputLayoutName, mTextInputLayoutEmail, mTextInputLayoutPass, mTextInputLayoutRepass);
        validator.validate();
    }

    @Click
    void birthday(){
        datePickerDialog.show(getActivity().getFragmentManager(), datePickerDialog.getClass().getName());
    }






    private ArrayAdapter<String> adapterCountries;

    private ArrayAdapter<Male> adapterMale;

    private ArrayAdapter<String> adapterYear;



    @AfterViews
    void after(){
        initDatePicker();
        initValidator();
        initSpinnerCountries();
        presenter.setUpYearArray();
        presenter.setUpMaleArray();
    }


    private void initValidator(){
        validator = new Validator(this);
        validator.setValidationListener(this);
        validator.put(mEditTextName, new QuickRule<EditText>() {
            @Override
            public boolean isValid(EditText view) {
                return view.getText().toString().replace(" ","").length()>=2;
            }

            @Override
            public String getMessage(Context context) {
                return getString(R.string.name_min);
            }
        });

        validator.put(mEditTextEmail, new QuickRule<EditText>() {
            @Override
            public boolean isValid(EditText view) {
                return GeneralUtil.checkEmail(view.getText().toString());
            }

            @Override
            public String getMessage(Context context) {
                return getString(R.string.invalid_email);
            }
        });
    }


    @Override
    protected void skinEvent(ApplySkinEvent event) {

    }

    @Override
    public void setUpColor(int color, int colorLight) {
        mSpinnerCountry.setPopupBackgroundDrawable(new ColorDrawable(color));
//        mSpinnerYear.setPopupBackgroundDrawable(new ColorDrawable(color));
        mSpinnerGender.setPopupBackgroundDrawable(new ColorDrawable(color));
        datePickerDialog.setAccentColor(color);
    }



    private void initSpinnerCountries(){
        adapterCountries = new ArrayAdapter<>(getActivity(), R.layout.tv_spinner, Arrays.asList(getResources().getStringArray(R.array.countries)));
        adapterCountries.setDropDownViewResource(R.layout.row_spinner);
        mSpinnerCountry.setAdapter(adapterCountries);
    }


    private void initSpinnerMale(List<Male> list){
        adapterMale = new ArrayAdapter<>(getActivity(), R.layout.tv_spinner, list);
        adapterMale.setDropDownViewResource(R.layout.row_spinner);
        mSpinnerGender.setAdapter(adapterMale);
    }


    private void initSpinnerYear(List<String> list){
        adapterYear = new ArrayAdapter<>(getActivity(), R.layout.tv_spinner, list);
        adapterYear.setDropDownViewResource(R.layout.row_spinner);
//        mSpinnerYear.setAdapter(adapterYear);
    }

    @NonNull
    @Override
    public FragmentSignupPresenter createPresenter() {
        return new FragmentSignupPresenter(getActivity());
    }

    @Override
    public void setUpMaleSpinner(List<Male> list) {
        initSpinnerMale(list);
    }

    @Override
    public void setUpYearSpinner(List<String> list) {
        initSpinnerYear(list);
    }

    @Override
    public void registrationComplete() {
        EventBus.getDefault().postSticky(new LoginEvent());
        getActivity().onBackPressed();
    }

    @Override
    public void onValidationSucceeded() {
        EditTextUtil.setErrorNull(mTextInputLayoutName, mTextInputLayoutEmail, mTextInputLayoutPass, mTextInputLayoutRepass);
        presenter.signUp(mEditTextEmail.getText().toString()
                ,mEditTextName.getText().toString()
                ,mTextViewBirthday.getText().toString()
                ,mSpinnerCountry.getSelectedItemPosition()!=0?mSpinnerCountry.getSelectedItem().toString():""
                ,String.valueOf(((Male)mSpinnerGender.getSelectedItem()).type)
                ,mEditTextPassword.getText().toString());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());
            // Display error messages ;)
            switch (view.getId()){
                case R.id.email:
                    mTextInputLayoutEmail.setError(message);
                    break;
                case R.id.name:
                    mTextInputLayoutName.setError(message);
                    break;
                case R.id.re_password:
                    mTextInputLayoutRepass.setError(message);
                    break;
                case R.id.password:
                    mTextInputLayoutPass.setError(message);
                    break;
            }
        }
    }

    private void initDatePicker() {
        Calendar calendar= Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.setMaxDate(calendar);

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        mTextViewBirthday.setText(String.format(getString(R.string.birthday), monthOfYear<=9?String.format(getString(R.string.birthday_format), monthOfYear+1):monthOfYear+1, dayOfMonth<=9?String.format(getString(R.string.birthday_format), dayOfMonth):dayOfMonth, year));
    }
}
