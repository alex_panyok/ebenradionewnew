package com.aws3.ebenradio.view.views.fragment;

/**
 * Created by Dell on 09.11.2016.
 */
public interface FragmentInfoView extends BaseFragmentView{
    void messageSent(String message);
}
