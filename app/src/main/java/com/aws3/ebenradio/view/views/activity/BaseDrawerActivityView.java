package com.aws3.ebenradio.view.views.activity;


import com.aws3.ebenradio.model.radio.RadioStation;

public interface BaseDrawerActivityView extends BaseActivityView {

    void setStation(RadioStation radioStation);
}
