package com.aws3.ebenradio.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.presenter.fragment.BaseFragmentPresenter;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.aws3.ebenradio.util.keys.EbenRetroKeys;
import com.aws3.ebenradio.view.views.fragment.BaseFragmentView;
import com.hannesdorfmann.mosby.mvp.MvpFragment;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Dell on 11.10.2016.
 */
public abstract class BaseFragment<V extends BaseFragmentView, P extends BaseFragmentPresenter<V>> extends MvpFragment<V,P> implements EbenKeys, EbenRetroKeys {


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void onResume() {
        super.onResume();

        EventBus.getDefault().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }



    @Override
    public void onStart() {
        super.onStart();
        presenter.setUpColor();
    }

    @Subscribe(sticky = true)
    public void onApplySkinEvent(ApplySkinEvent event){
        skinEvent(event);
        EventBus.getDefault().removeStickyEvent(event);
    }


    protected abstract void skinEvent(ApplySkinEvent event);

}
