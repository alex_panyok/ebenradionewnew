package com.aws3.ebenradio.view.fragment;

import android.support.annotation.NonNull;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.FragmentAdBinding;
import com.aws3.ebenradio.model.ads.Ad;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.presenter.fragment.FragmentAdPresenter;
import com.aws3.ebenradio.util.general.PixelUtil;
import com.aws3.ebenradio.view.views.fragment.FragmentAdView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Dell on 04.11.2016.
 */
@EFragment(R.layout.fragment_ad)
public class FragmentAd extends BaseFragment<FragmentAdView, FragmentAdPresenter> implements FragmentAdView  {

    FragmentAdBinding binding;

    @ViewById(R.id.ad_image)
    ImageView mImageView;

    @Override
    protected void skinEvent(ApplySkinEvent event) {
    }

    @Override
    public void setUpColor(int color, int colorLight) {
    }


    @AfterViews
    void after(){
        binding = FragmentAdBinding.bind(getView());
        binding.setPresenter(presenter);
        presenter.getAds();
        ViewGroup.LayoutParams params = mImageView.getLayoutParams();
        params.height = 5*PixelUtil.getScreenWidth(getActivity())/32;
        mImageView.setLayoutParams(params);
    }



    @NonNull
    @Override
    public FragmentAdPresenter createPresenter() {
        return new FragmentAdPresenter(getActivity());
    }


    @Override
    public void onPause() {
        super.onPause();
        presenter.unsubscribeSubscription();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.getAds();
    }

    @Override
    public void setAd(Ad item) {
        binding.setItem(item);
    }
}
