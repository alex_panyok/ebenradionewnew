package com.aws3.ebenradio.view.activity;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ActivityDetailNewBinding;
import com.aws3.ebenradio.model.rss.Item;
import com.aws3.ebenradio.presenter.activity.DetailNewsActivityPresenter;
import com.aws3.ebenradio.view.views.activity.DetailActivityNewsView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

/**
 * Created by Alex on 29.10.2016.
 */
@EActivity(R.layout.activity_detail_new)
public class DetailNewsActivity extends BaseActivity<DetailActivityNewsView, DetailNewsActivityPresenter> implements DetailActivityNewsView {


    private ActivityDetailNewBinding binding;


    @AfterViews
    void after(){


    }


    @Override
    protected void onStart() {
        super.onStart();
        if(binding==null)
            binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_new);
            binding.setPresenter(presenter);
            presenter.getItem(getIntent());
    }

    @Override
    public void setItem(Item item) {
        binding.setItem(item);
    }

    @Override
    public void back() {
        onBackPressed();
    }

    @Override
    public void setUpColor(int color, int colorLight) {

    }

    @NonNull
    @Override
    public DetailNewsActivityPresenter createPresenter() {
        return new DetailNewsActivityPresenter(this);
    }
}
