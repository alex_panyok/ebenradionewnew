package com.aws3.ebenradio.view.views.activity;

import com.aws3.ebenradio.model.event_er.Banner;

/**
 * Created by Dell on 09.11.2016.
 */
public interface EventBannerActivityView extends BaseActivityView {

    void bannerViewed(Banner banner);
}
