/**
 * Copyright 2011, Felix Palmer
 *
 * Licensed under the MIT license:
 * http://creativecommons.org/licenses/MIT/
 */
package com.aws3.ebenradio.view.custom_views.visualizer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.media.audiofx.Visualizer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import com.aws3.ebenradio.model.visualizer.AudioData;
import com.aws3.ebenradio.model.visualizer.FFTData;
import com.aws3.ebenradio.util.general.PixelUtil;
import com.aws3.ebenradio.view.custom_views.visualizer_renderes.Renderer;

import java.util.HashSet;
import java.util.Set;

/**
 * A class that draws visualizations of data received from a
 * {@link Visualizer.OnDataCaptureListener#onWaveFormDataCapture } and
 * {@link Visualizer.OnDataCaptureListener#onFftDataCapture }
 */
public class VisualizerView extends View {
  private static final String TAG = "VisualizerView";
  private ScaleGestureDetector mScaleDetector;
  private byte[] mBytes;
  private byte[] mFFTBytes;
  private Rect mRect = new Rect();



  private Visualizer mVisualizer;


  private Set<Renderer> mRenderers;

  private Paint mFlashPaint = new Paint();
  private Paint mFadePaint = new Paint();
  private Paint mBackgroundPaint = new Paint();


  private float width = 8f*(PixelUtil.getScreenHeight(getContext())/2560f);


  public VisualizerView(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs);
    init();
//    mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());

  }

  public VisualizerView(Context context, AttributeSet attrs) {
    this(context, attrs, 0);
  }

  public VisualizerView(Context context) {
    this(context, null, 0);
  }

  private void init() {
    mBytes = null;
    mFFTBytes = null;

    mFlashPaint.setColor(Color.argb(255, 0, 0, 0));
    mFadePaint.setColor(Color.argb(255, 255, 255, 255)); // Adjust alpha to change how quickly the image fades
    mFadePaint.setXfermode(new PorterDuffXfermode(Mode.MULTIPLY));

    mRenderers = new HashSet<>();
  }

  public Set<Renderer> getRenderers() {
    return mRenderers;
  }


//  @Override
//  public boolean onTouchEvent(MotionEvent event) {
////    mScaleDetector.onTouchEvent(event);
//    return true;
//  }


  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    super.onMeasure(widthMeasureSpec, heightMeasureSpec);

  }

  public void link(int id) {
  if(mVisualizer!=null) {
    mVisualizer.setEnabled(false);
  }
    // Create the Visualizer object and attach it to our media player.
    mVisualizer = new Visualizer(id);
    mVisualizer.setScalingMode(Visualizer.SCALING_MODE_NORMALIZED);
    mVisualizer.setEnabled(false);
    mVisualizer.setCaptureSize(Visualizer.getCaptureSizeRange()[1]);

    // Pass through Visualizer data to VisualizerView
    Visualizer.OnDataCaptureListener captureListener = new Visualizer.OnDataCaptureListener() {
      @Override
      public void onWaveFormDataCapture(Visualizer visualizer, byte[] bytes,
                                        int samplingRate) {
//        Log.i("LOW_HIGH", sumHigh(bytes) + "  "+sumLow(bytes));
        updateVisualizer(bytes);
      }

      @Override
      public void onFftDataCapture(Visualizer visualizer, byte[] bytes,
                                   int samplingRate) {
        updateVisualizerFFT(bytes);
      }
    };

    mVisualizer.setDataCaptureListener(captureListener,
            Visualizer.getMaxCaptureRate() / 2, true, true);

    // Enabled Visualizer and disable when we're done with the stream
    mVisualizer.setEnabled(true);

  }

  private float averageByte(byte[] bytes){
    int sum = 0;
    for(byte b : bytes){
      sum +=b;
    }
    return Math.abs(((float)sum)/bytes.length);
  }




  public void addRenderer(Renderer renderer) {
    if (renderer != null) {
      mRenderers.add(renderer);
    }
  }

  public Visualizer getmVisualizer() {
    return mVisualizer;
  }

  public void clearRenderers() {
    mRenderers.clear();
  }

  /**
   * Call to release the resources used by VisualizerView. Like with the
   * MediaPlayer it is good practice to call this method
   */
  public void release() {
    if(mVisualizer!=null) {
      mVisualizer.release();
    }
  }

  /**
   * Pass data to the visualizer. Typically this will be obtained from the
   * Android Visualizer.OnDataCaptureListener call back. See
   * {@link Visualizer.OnDataCaptureListener#onWaveFormDataCapture }
   *
   * @param bytes
   */
  public void updateVisualizer(byte[] bytes) {
    mBytes = bytes;
    invalidate();
  }

  /**
   * Pass FFT data to the visualizer. Typically this will be obtained from the
   * Android Visualizer.OnDataCaptureListener call back. See
   * {@link Visualizer.OnDataCaptureListener#onFftDataCapture }
   *
   * @param bytes
   */
  public void updateVisualizerFFT(byte[] bytes) {
    mFFTBytes = bytes;
    invalidate();
  }

  boolean mFlash = false;

  /**
   * Call this to make the visualizer flash. Useful for flashing at the start
   * of a song/loop etc...
   */
  public void flash() {
    mFlash = true;
    invalidate();
  }

  Bitmap mCanvasBitmap;
  Canvas mCanvas;


  @Override
  protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);

    // Create canvas once we're ready to draw
    mRect.set(0, 0, getWidth(), getHeight());

    if (mCanvasBitmap == null) {
      mCanvasBitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Config.ARGB_8888);
    }
    if (mCanvas == null) {
      mCanvas = new Canvas(mCanvasBitmap);
    }
    mCanvas.drawColor(Color.TRANSPARENT, Mode.CLEAR);

    mCanvas.drawColor(Color.argb(128, 0, 0, 0));
    if (mBytes != null) {
      // Render all audio renderers
      AudioData audioData = new AudioData(mBytes);
      for(Renderer r : mRenderers)
      {
        r.render(mCanvas, audioData, mRect);
      }
    }

    if (mFFTBytes != null) {

      // Render all FFT renderers
      FFTData fftData = new FFTData(mFFTBytes);
      for (Renderer r : mRenderers) {
        r.render(mCanvas, fftData, mRect, width);
      }
    }

//     Fade out old contents


    if (mFlash) {
      mFlash = false;
      mCanvas.drawPaint(mFlashPaint);
    }
//    mBackgroundPaint.setAlpha(50);
    canvas.drawBitmap(mCanvasBitmap, new Matrix(), mBackgroundPaint);
    invalidate();
  }


  private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
    @Override
    public boolean onScale(ScaleGestureDetector detector) {
        if(width * (detector.getScaleFactor())>=4 && width * (detector.getScaleFactor())<=32) {
          width = width * (detector.getScaleFactor());
        }else if(width * (detector.getScaleFactor())<4){
          width = 4;
        }else if(width * (detector.getScaleFactor())>32){
          width = 32;
        }

      return true;
    }

    @Override
    public boolean onScaleBegin(ScaleGestureDetector detector) {
      return true;
    }

    @Override
    public void onScaleEnd(ScaleGestureDetector detector) {
      super.onScaleEnd(detector);

    }
  }
}