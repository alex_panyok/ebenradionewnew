package com.aws3.ebenradio.view.views.fragment;


import com.aws3.ebenradio.model.user.UserFromSearch;
import com.aws3.ebenradio.model.user.UsersFromSearchResponse;

import java.util.List;

public interface FragmentSearchPeopleView extends BaseFragmentView {


    void setUpSearchPeople(UsersFromSearchResponse response);

}
