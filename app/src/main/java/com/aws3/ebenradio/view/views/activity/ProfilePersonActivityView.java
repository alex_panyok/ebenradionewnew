package com.aws3.ebenradio.view.views.activity;

import com.aws3.ebenradio.model.user.User;
import com.aws3.ebenradio.model.user.UserPublicProfile;

/**
 * Created by Dell on 26.10.2016.
 */
public interface ProfilePersonActivityView extends BaseActivityView{

    void setUpProfile(UserPublicProfile user);

    void followUser(String message);

    void setUpFollowButton(boolean isFollowing);
}
