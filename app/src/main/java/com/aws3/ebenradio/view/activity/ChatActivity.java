package com.aws3.ebenradio.view.activity;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.chat.Message;
import com.aws3.ebenradio.model.chat.MessageBody;
import com.aws3.ebenradio.model.chat.MessageItem;
import com.aws3.ebenradio.model.chat.MessagesData;
import com.aws3.ebenradio.model.chat.NewMessageChat;
import com.aws3.ebenradio.model.event.chat.NewMessageEvent;
import com.aws3.ebenradio.presenter.activity.ChatActivityPresenter;
import com.aws3.ebenradio.service.ChatService;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.view.adapter.AdapterMessages;
import com.aws3.ebenradio.view.custom_views.EndlessRecyclerOnScrollListener;
import com.aws3.ebenradio.view.views.activity.ChatActivityView;
import com.github.rahatarmanahmed.cpv.CircularProgressView;
import com.paginate.Paginate;


import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


/**
 * Created by Dell on 31.10.2016.
 */
@EActivity(R.layout.activity_chat)
public class ChatActivity extends BaseActivity<ChatActivityView, ChatActivityPresenter> implements ChatActivityView {


    @ViewById(R.id.main_title)
    TextView mTextViewMainTitle;

    @ViewById(R.id.et_chat)
    EditText mEditTextChat;

    @ViewById(R.id.rv_chat)
    RecyclerView mRecyclerViewChat;

    @ViewById(R.id.progress_view)
    CircularProgressView mProgressView;

    @ViewById(R.id.send)
    View mViewSend;

    private MediaPlayer mMediaPlayerOut;

    private MediaPlayer mMediaPlayerIn;

    @Click
    void back(){
        onBackPressed();
    }

    private AdapterMessages adapterMessages;

    @Click
    void send(){
        if(!mEditTextChat.getText().toString().isEmpty()) {
            mProgressView.setVisibility(View.VISIBLE);
            mProgressView.startAnimation();
            mViewSend.setVisibility(View.GONE);
            presenter.sendMessage(new MessageBody(getIntent().getStringExtra(ID), mEditTextChat.getText().toString().trim()));
        }
    }


    private void initPlayers(){
        mMediaPlayerOut = MediaPlayer.create(this, R.raw.send);
        mMediaPlayerIn = MediaPlayer.create(this, R.raw.receive);
    }


    @Override
    protected void onStart() {
        super.onStart();
        cancelNotification(this, Integer.parseInt(getIntent().getStringExtra(ID)));
        ChatService.isChatAcitivityRunning = true;
        ChatService.idChat = Integer.parseInt(getIntent().getStringExtra(ID));
//        Log.i("ChAT_ID1",getIntent().getStringExtra(ID)+"");

    }



    @Override
    protected void onStop() {
        super.onStop();
        ChatService.isChatAcitivityRunning = false;
        if(ChatService.idChat == Integer.parseInt(getIntent().getStringExtra(ID))) {
            ChatService.idChat = -1;
        }
    }








    @AfterViews
    void after(){
        startChatService();
        initRv();
        initPlayers();
        presenter.getMessages(getIntent().getStringExtra(ID), 1, false);
        mTextViewMainTitle.setText(String.format(getString(R.string.chat_with),getIntent().getStringExtra(NAME)));
    }





    private void initRv(){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setReverseLayout(true);

        mRecyclerViewChat.setLayoutManager(linearLayoutManager);
        adapterMessages = new AdapterMessages(new ArrayList<MessageItem>(), presenter, getIntent().getStringExtra(ID));
        mRecyclerViewChat.setAdapter(adapterMessages);
        mRecyclerViewChat.setItemAnimator(null);
        mRecyclerViewChat.addOnScrollListener(new EndlessRecyclerOnScrollListener((LinearLayoutManager) mRecyclerViewChat.getLayoutManager()) {
            @Override
            public void onLoadMore(int current_page) {
                presenter.loadMore(current_page);
            }
        });

    }





    @Subscribe
    public void onNewMessageEvent(NewMessageEvent event){
        if(event.messageChat.from_user_id==Integer.parseInt(getIntent().getStringExtra(ID))) {
            mMediaPlayerIn.start();
            presenter.getMessages(getIntent().getStringExtra(ID), 1, true);
        }
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if(intent.getAction().equals("CHAT")) {
            adapterMessages.clearData();
            mTextViewMainTitle.setText(String.format(getString(R.string.chat_with),getIntent().getStringExtra(NAME)));
            presenter.getMessages(intent.getStringExtra(ID), 1, false);
        }
    }

    private   void cancelNotification(Context ctx, int notifyId) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancel(notifyId);
    }



    @Override
    protected void onDestroy() {
        resetPlayer();
        presenter.removeRunnable();
        stopChatService();
        super.onDestroy();
    }

    private void resetPlayer(){
        mMediaPlayerIn.release();
        mMediaPlayerOut.release();
    }


    private void startChatService(){
        startService(new Intent(this, ChatService.class));
    }

    private void stopChatService(){
        stopService(new Intent(this, ChatService.class));
    }

    @NonNull
    @Override
    public ChatActivityPresenter createPresenter() {
        return new ChatActivityPresenter(this);
    }

    @Override
    public void setUpColor(int color, int colorLight) {
        mProgressView.setColor(color);
    }

    @Override
    public void messageSent() {
        mEditTextChat.setText("");
    }

    @Override
    public void messageReceived() {

    }

    @Override
    public void setMessages(MessagesData data, boolean isLast) {
        try {
//            Log.d("TIMES", presenter.generateTimes(data.messages).size()+"");
            adapterMessages.addData(presenter.generateTimes(data.messages, isLast));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addNewMessage(MessageItem item) {
        adapterMessages.addNewMessage(item);
        mRecyclerViewChat.smoothScrollToPosition(0);
    }


    @Override
    public void progressMessageStop() {
        mMediaPlayerOut.start();
        mProgressView.setVisibility(View.GONE);
        mViewSend.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateMessages(List<MessageItem> messageItems) {
//        Log.i("UPDATE_MESSAGES",messageItems.size()+"");
        adapterMessages.clearData();
        adapterMessages.addData(messageItems);
    }


}
