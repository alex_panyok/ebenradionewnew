package com.aws3.ebenradio.view.adapter;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ItemTimerBinding;
import com.aws3.ebenradio.model.alarm.Time;
import com.aws3.ebenradio.presenter.fragment.FragmentSleepTimerPresenter;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;


/**
 * Created by Dell on 19.09.2016.
 */
public class AdapterChooseTimer extends RecyclerView.Adapter<AdapterChooseTimer.TimerHolder> {

    private List<Time> list;


    private MvpBasePresenter presenter;

//    OnSelectListener onSelectListener;

    public AdapterChooseTimer(List<Time> list, MvpBasePresenter presenter) {
        this.list = list;
        this.presenter  = presenter;

    }

    @Override
    public TimerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemTimerBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_timer, parent, false);
        binding.setPresenter((FragmentSleepTimerPresenter)presenter);
        return new TimerHolder(binding.getRoot());
    }



    @Override
    public void onBindViewHolder(TimerHolder holder, int position) {
        Time timeItem = list.get(position);

        holder.binding.setItem(timeItem);
//        timerHolder.bind(context,timeItem);
//        timerHolder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                resetAllRadioButtons();
//                timeItem.isChecked = true;
//                if(onSelectListener!=null){
//                    onSelectListener.select(timeItem);
//                }
////                timeItem.isChecked = true;
//
//            }
//        });
    }

//    public interface OnSelectListener{
//        void select(Time time);
//    }
//
//    public void setOnSelectListener(OnSelectListener onSelectListener){
//        this.onSelectListener = onSelectListener;
//    }


//    private void resetAllRadioButtons(){
//        for(Time time : this.list){
//            time.isChecked = false;
//        }
//        notifyItemRangeChanged(0, list.size());
//    }


    public void setActive(boolean isActive){
        for(Time time : list){
            time.isActive = isActive;
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class TimerHolder extends RecyclerView.ViewHolder{

        ItemTimerBinding binding;

        public TimerHolder(View itemView) {
            super(itemView);

            binding = DataBindingUtil.bind(itemView);

        }


    }
}
