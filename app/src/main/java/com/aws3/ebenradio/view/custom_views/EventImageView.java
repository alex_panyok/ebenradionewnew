package com.aws3.ebenradio.view.custom_views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Dell on 28.02.2017.
 */

public class EventImageView extends ImageView {

    public EventImageView(Context context) {
        super(context);
    }

    public EventImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(),getMeasuredWidth()/2);
    }
}
