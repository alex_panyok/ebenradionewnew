package com.aws3.ebenradio.view.fragment;

import android.support.v4.app.Fragment;

import com.aws3.ebenradio.R;

import org.androidannotations.annotations.EFragment;

/**
 * Created by Dell on 19.10.2016.
 */
@EFragment(R.layout.fragment_music)
public class FragmentMusic extends Fragment {


    public static FragmentMusic getInstance(FragmentMusic fragmentMusic){
        return fragmentMusic;
    }

}
