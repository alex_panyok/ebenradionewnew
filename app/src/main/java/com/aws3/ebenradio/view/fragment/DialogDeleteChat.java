package com.aws3.ebenradio.view.fragment;

import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.chat.DeleteChatBody;
import com.aws3.ebenradio.model.event.chat.ChatDeleteEvent;
import com.aws3.ebenradio.model.event_er.Event;
import com.aws3.ebenradio.model.networking.EbenResponse;
import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.util.general.PixelUtil;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.aws3.ebenradio.util.keys.EbenRetroKeys;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.skins.ColorUtil;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.view.activity.CreateSkinActivity_;
import com.aws3.ebenradio.view.activity.EventBannerActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 17.11.2016.
 */
@EFragment(R.layout.delete_cht_dialog)
public class DialogDeleteChat extends DialogFragment implements EbenKeys, EbenRetroKeys{


    @ViewById(R.id.background)
    View mViewBackground;

    @Click
    void delete(){
        deleteChat(new DeleteChatBody(getArguments().getString(ID)));
        dismiss();
    }

    @AfterViews
    void after(){
        mViewBackground.setBackgroundColor(ColorUtil.getColorById(SkinPrefUtils.getSkin().color, getActivity()));
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow().setLayout(PixelUtil.getScreenWidth(getActivity()), ViewGroup.LayoutParams.WRAP_CONTENT);
    }



    private void deleteChat(DeleteChatBody body){
        EbenRadioRetroWorker.getInstance().deleteChat(GeneralUtil.getBasicAuthHeader(), PrefUtils.getCookies(), body)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<EbenResponse>() {
                    @Override
                    public void call(EbenResponse ebenResponse) {
                        EventBus.getDefault().post(new ChatDeleteEvent());
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                });

    }



}
