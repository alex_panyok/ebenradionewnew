package com.aws3.ebenradio.view.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.SearchPeopleItemBinding;
import com.aws3.ebenradio.model.user.UserFromSearch;
import com.aws3.ebenradio.presenter.fragment.FragmentSearchPeoplePresenter;
import com.bumptech.glide.Glide;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

/**
 * Created by Dell on 14.09.2016.
 */
public class AdapterSearchPeople extends RecyclerView.Adapter<AdapterSearchPeople.Holder> {


    private List<UserFromSearch> list;

    private MvpBasePresenter presenter;

    public AdapterSearchPeople(List<UserFromSearch> list, MvpBasePresenter presenter) {
        this.presenter = presenter;
        this.list = list;
    }


    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        SearchPeopleItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.search_people_item, parent, false);
//        View view = LayoutInflater.from(context).inflate(R.layout.search_people_item, parent, false);
        binding.setPresenter((FragmentSearchPeoplePresenter)presenter);
        return new Holder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.binding.setItem(list.get(position));
    }

    public void setData(List<UserFromSearch> list){
        int size = this.list.size();
        this.list = list;
        if(!list.isEmpty())
            notifyItemRangeChanged(0, list.size());
        else
            notifyItemRangeRemoved(0,size);

    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(list.get(position).id);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class Holder extends RecyclerView.ViewHolder {

        SearchPeopleItemBinding binding;

        public Holder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }


    }
}
