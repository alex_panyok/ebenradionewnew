package com.aws3.ebenradio.view.activity;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.follow.FollowPerson;
import com.aws3.ebenradio.model.follow.FollowPersonEvent;
import com.aws3.ebenradio.model.follow.FollowResponse;
import com.aws3.ebenradio.presenter.activity.ChatAddActivityPresenter;
import com.aws3.ebenradio.view.adapter.AdapterAddChat;
import com.aws3.ebenradio.view.views.activity.ChatAddActivityView;
import com.google.android.exoplayer2.BaseRenderer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;

/**
 * Created by Dell on 03.11.2016.
 */
@EActivity(R.layout.activity_add_chat)
public class ChatAddActivity extends BaseActivity<ChatAddActivityView, ChatAddActivityPresenter> implements ChatAddActivityView {

    @ViewById(R.id.rv_add_chat)
    RecyclerView mRecyclerViewAddChat;

    @ViewById(R.id.search)
    View mViewSearch;

    @Click
    void search(){
        SearchPeopleActivity_.intent(this).start();
    }

    private AdapterAddChat adapterAddChat;


    @Click
    void back(){
        onBackPressed();
    }

    @AfterViews
    void after(){
        initRv();
        presenter.getFollowing();

    }



    private void initRv(){
        mRecyclerViewAddChat.setLayoutManager(new LinearLayoutManager(this));
        adapterAddChat= new AdapterAddChat(new ArrayList<FollowPerson>(), presenter);
        mRecyclerViewAddChat.setAdapter(adapterAddChat);
    }




    @Override
    public void setUpColor(int color, int colorLight) {

    }

    @NonNull
    @Override
    public ChatAddActivityPresenter createPresenter() {
        return new ChatAddActivityPresenter(this);
    }

    @Override
    public void setUpFollowing(FollowResponse followResponse) {
        if(!followResponse.followingList.isEmpty()) {
            adapterAddChat.setData(followResponse.followingList);
            mViewSearch.setVisibility(View.GONE);
        }else
            mViewSearch.setVisibility(View.VISIBLE);
    }


    @Subscribe(sticky = true)
    public void onFollowPersonEvent(FollowPersonEvent event){
        presenter.getFollowing();
        EventBus.getDefault().removeStickyEvent(event);
    }
}
