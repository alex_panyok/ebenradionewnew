package com.aws3.ebenradio.view.views.activity;

import android.support.v7.widget.RecyclerView;

import com.aws3.ebenradio.model.create_skin.categories.Category;
import com.aws3.ebenradio.model.skin.Skin;

/**
 * Created by Dell on 11.10.2016.
 */
public interface CreateSkinActivityView extends BaseActivityView {

    void setAttrAdapter(Category category);

    void refreshSkin(int attrSelectedId, Skin skin);

    void changeGraphicEQAdapterNone();

    void getScreenShotView(String name, String fileName);
}
