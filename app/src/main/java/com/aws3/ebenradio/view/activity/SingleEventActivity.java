package com.aws3.ebenradio.view.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event_er.Event;
import com.aws3.ebenradio.presenter.activity.SingleEventActivityPresenter;
import com.aws3.ebenradio.util.data_binding.DataBinder;
import com.aws3.ebenradio.view.views.activity.SingleEventActivityView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Dell on 03.11.2016.
 */
@EActivity(R.layout.activity_single_event)
public class SingleEventActivity extends BaseActivity<SingleEventActivityView, SingleEventActivityPresenter> implements SingleEventActivityView {


    @ViewById(R.id.imageView)
    ImageView mImageView;

    @ViewById(R.id.location)
    TextView mTextViewLocation;

    @ViewById(R.id.date)
    TextView mTextViewDate;

    @ViewById(R.id.description)
    TextView mTextViewDescription;

    @ViewById(R.id.main_title)
    TextView mTextViewTitle;

    @ViewById(R.id.background_ll)
    View mViewBackground;

    @ViewById(R.id.scroll_view)
    ScrollView mScrollView;

    @Click
    void back(){
        onBackPressed();
    }


    @Click(R.id.more_info)
    void info(){
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URLUtil.isValidUrl(presenter.getEvent().url)?presenter.getEvent().url:"http://"+presenter.getEvent().url)));
        } catch (ActivityNotFoundException e) {
            Toast.makeText(this, "Can`t open this url", Toast.LENGTH_SHORT).show();
        }
    }


    @AfterViews
    void after(){
        presenter.getEvent(getIntent().getStringExtra(ID));
    }



    @Override
    public void getEvent(Event event) {
        mScrollView.animate().alpha(1).setDuration(200).start();
        DataBinder.ebenRadioImageEvent(mImageView, event.image);
        mTextViewLocation.setText(event.location);
        mTextViewDate.setText(event.date_event);
        mTextViewDescription.setText(event.description);
        mTextViewTitle.setText(event.title);
    }

    @Override
    public void setUpColor(int color, int colorLight) {
        mViewBackground.setBackgroundColor(colorLight);
    }

    @NonNull
    @Override
    public SingleEventActivityPresenter createPresenter() {
        return new SingleEventActivityPresenter(this);
    }
}
