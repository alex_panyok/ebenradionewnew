package com.aws3.ebenradio.view.views.fragment;

import com.aws3.ebenradio.model.chat.UserChat;

import java.util.List;

/**
 * Created by Dell on 31.10.2016.
 */
public interface FragmentChooseView extends BaseFragmentView {

    void chooseFragment(List<UserChat> list);

    void selectChat(UserChat userChat);
}
