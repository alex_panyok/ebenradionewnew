package com.aws3.ebenradio.view.views.fragment;

/**
 * Created by Dell on 24.10.2016.
 */
public interface FragmentAlarmView extends BaseFragmentView {

    void setUpWeekDays(String weekDays);

    void setUpStream(String title);
}
