package com.aws3.ebenradio.view.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Toast;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.alarm.ChangeStationAlarm;
import com.aws3.ebenradio.model.event.banner.BannerEventEvents;
import com.aws3.ebenradio.model.event.banner.BannerEventStation;
import com.aws3.ebenradio.model.event.event_er.OpenEventsEvent;
import com.aws3.ebenradio.model.event.player.ChangeStationPlayer;
import com.aws3.ebenradio.model.event.radio.ChangeStationEvent;
import com.aws3.ebenradio.presenter.activity.MainActivityPresenter;
import com.aws3.ebenradio.service.ChatService;
import com.aws3.ebenradio.service.RadioService;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.radio.RadioPlayerState;
import com.aws3.ebenradio.util.radio.RadioStationUtil;
import com.aws3.ebenradio.util.user.UserDataPref;
import com.aws3.ebenradio.view.custom_views.WABottomSheetBehavior;
import com.aws3.ebenradio.view.fragment.DialogRate;
import com.aws3.ebenradio.view.fragment.DialogRate_;
import com.aws3.ebenradio.view.views.activity.MainActivityView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseDrawerActivity<MainActivityView, MainActivityPresenter> implements MainActivityView {

    @ViewById(R.id.bottom_sheet)
    View mBottomSheetView;

    private WABottomSheetBehavior bottomSheetBehavior;


    @NonNull
    @Override
    public MainActivityPresenter createPresenter() {
        return new MainActivityPresenter(this);
    }


    @AfterViews
    void after(){
        presenter.calculateFeedback();
//        if(UserDataPref.isAuthorized(this))
//            startService(new Intent(this, ChatService.class));
        startService(new Intent(this, RadioService.class));
        initBottomSheetBehavior();


        //\setUpDrawerControls();
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        checkIntent(getIntent());
    }

    @Override
    protected void onResume() {
        presenter.getBanners();
        super.onResume();
    }





    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
       checkIntent(intent);
    }

    private void checkIntent(Intent intent){
        if(intent==null){
            return;
        }
        if(intent.getAction()!=null && intent.getAction().equals(EVENT_PUSH)){
            switch (Integer.parseInt(intent.getStringExtra(TYPE))){
                case 1:
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(URLUtil.isValidUrl(intent.getStringExtra(URL))?intent.getStringExtra(URL):"http://"+intent.getStringExtra(URL))));
                    } catch (ActivityNotFoundException e) {
                        Toast.makeText(this, "Can`t open this url", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case 2:
                    startEventFragment();
                    break;

                case 3:
                    PrefUtils.setStation(RadioStationUtil.getRadioStationItems(this).get(intent.getIntExtra(STREAM_ID,0)));
                    EventBus.getDefault().postSticky(new ChangeStationEvent());
                    EventBus.getDefault().postSticky(new ChangeStationAlarm());
                    EventBus.getDefault().postSticky(new ChangeStationPlayer());
                    break;
            }
        }
    }


    @Subscribe(sticky = true)
    public void onEventEvents(BannerEventEvents bannerEventEvents){

    }

    @Subscribe(sticky = true)
    public void onEventStation(BannerEventStation bannerEventStation){

    }

    @Override
    protected void onDestroy() {
        if(RadioPlayerState.getState() != RadioPlayerState.STATE_PLAY){
            Intent intent = new Intent(this, RadioService.class);
            intent.setAction(RadioService.ACTION_STOP);
            startService(intent);
        }
        super.onDestroy();
    }


    public void startRadioServicePlay(){
        Intent intent = new Intent(this, RadioService.class);
        intent.setAction(RadioService.ACTION_PLAY);
        startService(intent);
    }



    @Subscribe(sticky = true)
    public void onOpenEventsEvent(OpenEventsEvent event){
        startEventFragment();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    public void setUpColor(int color, int colorLight) {
        findViewById(R.id.left_menu).setBackgroundColor(colorLight);
        findViewById(R.id.right_menu_background).setBackgroundColor(colorLight);
    }


    @Override
    public void openPlayer() {
        setPlayerStateExpanded();
    }

    @Override
    public void closePlayer() {
        setPlayerStateCollapsed();
    }

    private void setPlayerStateCollapsed(){
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setAllowUserDragging(true);
    }

    private void setPlayerStateExpanded(){
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetBehavior.setAllowUserDragging(false);
    }

    private void initBottomSheetBehavior() {
        bottomSheetBehavior = (WABottomSheetBehavior)BottomSheetBehavior.from(mBottomSheetView);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {

            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                findViewById(R.id.mini).setAlpha(Math.abs(1-slideOffset));
                findViewById(R.id.action_bar).setAlpha(1-slideOffset);
            }
        });
    }

    @Override
    public void showRateDialog() {
                DialogRate dialogRate = new DialogRate_();
                    dialogRate.show(getSupportFragmentManager(), "contact");
    }
}
