package com.aws3.ebenradio.view.adapter.create_skin;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ItemCategoryBinding;
import com.aws3.ebenradio.model.create_skin.categories.Category;
import com.aws3.ebenradio.presenter.activity.CreateSkinActivityPresenter;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Alex on 16.10.2016.
 */

public class AdapterCategory extends RecyclerView.Adapter<AdapterCategory.CategoryHolder> {


    private List<Category> list;

    private MvpBasePresenter presenter;

    public AdapterCategory(List<Category> list, MvpBasePresenter presenter) {
        this.list = list;
        this.presenter = presenter;
    }

    @Override
    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemCategoryBinding binding  = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_category,parent,false);
        binding.setPresenter((CreateSkinActivityPresenter)presenter);
        return new CategoryHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(CategoryHolder holder, int position) {
        holder.binding.setItem(list.get(position));
    }

    public List<Category> getList() {
        return list;
    }


    @Override
    public int getItemCount() {
        return list.size();
    }

     static class CategoryHolder extends RecyclerView.ViewHolder{

        ItemCategoryBinding binding;

         CategoryHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
