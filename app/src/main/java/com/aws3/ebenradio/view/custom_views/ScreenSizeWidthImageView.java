package com.aws3.ebenradio.view.custom_views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.aws3.ebenradio.util.general.PixelUtil;

/**
 * Created by Dell on 13.10.2016.
 */
public class ScreenSizeWidthImageView extends ImageView {
    public ScreenSizeWidthImageView(Context context) {
        super(context);
    }

    public ScreenSizeWidthImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ScreenSizeWidthImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int width = getMeasuredWidth();
        setMeasuredDimension(width, (int)(width*(float)PixelUtil.getScreenHeight(getContext())/PixelUtil.getScreenWidth(getContext())));

    }
}
