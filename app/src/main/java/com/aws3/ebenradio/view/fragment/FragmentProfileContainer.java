package com.aws3.ebenradio.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.user.LoginEvent;
import com.aws3.ebenradio.service.ChatService;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.aws3.ebenradio.util.user.UserDataPref;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * Created by Dell on 27.10.2016.
 */
@EFragment(R.layout.fragment_container)
public class FragmentProfileContainer extends Fragment implements EbenKeys {



    public static FragmentProfileContainer getInstance(FragmentProfileContainer fragmentProfileContainer, int fragId){
        Bundle bundle = new Bundle();
        bundle.putInt(FRAG_ID, fragId);
        fragmentProfileContainer.setArguments(bundle);
        return fragmentProfileContainer;
    }




    @AfterViews
    void after(){
      check();
    }

    @Override
    public void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }


    @Override
    public void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    private Fragment chooseFragment(int fragId){
        switch (fragId){
            case PROF_FRAG_ID:
                return FragmentProfile.getInstance(new FragmentProfile_());
            case CHAT_FRAG_ID:
                return FragmentChatChoose.getInstance(new FragmentChatChoose_());
        }
        return null;
    }

    private void check(){
        replaceFragment(UserDataPref.isAuthorized(getContext())?chooseFragment(getArguments().getInt(FRAG_ID)):FragmentLoginFirst.getInstance(new FragmentLoginFirst_()));
    }


    private void replaceFragment(Fragment fragment){
        getChildFragmentManager().beginTransaction().replace(R.id.container, fragment).commit();
    }



    @Subscribe(sticky = true)
    public void onLoginEvent(LoginEvent event){
//        if(UserDataPref.isAuthorized(getContext()))
//            getActivity().startService(new Intent(getActivity(), ChatService.class));
        check();
        EventBus.getDefault().removeStickyEvent(event);
    }











}
