package com.aws3.ebenradio.view.fragment;

import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.util.general.PixelUtil;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.aws3.ebenradio.util.skins.ColorUtil;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.view.activity.CreateSkinActivity_;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.List;

/**
 * Created by Dell on 17.11.2016.
 */
@EFragment(R.layout.edit_skin_dialog)
public class DialogEditSkin  extends DialogFragment implements EbenKeys{


    @ViewById(R.id.background)
    View mViewBackground;

    @Click
    void edit(){
        CreateSkinActivity_.intent(getActivity()).extra(SKIN, getArguments().getSerializable(SKIN)).extra(POSITION, getArguments().getInt(POSITION)).start();
        dismiss();
    }
    @Click
    void delete(){
        if(((Skin)getArguments().getSerializable(SKIN)).name.equals(SkinPrefUtils.getSkin().name))
            ((FragmentSkins)getParentFragment()).getPresenter().applyNewSkin(SkinPrefUtils.getSkins().get(0));
        List<Skin> skins = SkinPrefUtils.getSkins();
        skins.remove(getArguments().getInt(POSITION));
        SkinPrefUtils.setSkins(skins);
        ((FragmentSkins)getParentFragment()).deleteSkin( getArguments().getInt(POSITION));
        dismiss();
    }

    @AfterViews
    void after(){
        mViewBackground.setBackgroundColor(ColorUtil.getColorById(SkinPrefUtils.getSkin().color, getActivity()));
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow().setLayout(PixelUtil.getScreenWidth(getActivity()), ViewGroup.LayoutParams.WRAP_CONTENT);
    }



}
