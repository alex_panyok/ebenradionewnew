package com.aws3.ebenradio.view.fragment;



import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.util.general.PixelUtil;
import com.aws3.ebenradio.util.general.ToastUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.skins.ColorUtil;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.view.activity.CreateSkinActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Dell on 18.10.2016.
 */
@EFragment(R.layout.dialog_rate)
public class DialogRate extends DialogFragment {


    @ViewById(R.id.background)
    View mViewBackground;

    @Click
    void ok(){
        startActivity(new Intent(Intent.ACTION_VIEW).setData(Uri.parse(getString(R.string.play_market_url))));
        PrefUtils.setIsRateShown(true);
        dismiss();
    }

    @AfterViews
    void after(){
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);
        mViewBackground.setBackgroundColor(ColorUtil.getColorById(SkinPrefUtils.getSkin().color, getActivity()));

    }


    @Click
    void cancel(){
        getDialog().cancel();
    }





    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow().setLayout(PixelUtil.getScreenWidth(getActivity()), ViewGroup.LayoutParams.WRAP_CONTENT);
    }



}
