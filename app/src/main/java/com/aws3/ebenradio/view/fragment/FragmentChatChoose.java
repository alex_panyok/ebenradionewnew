package com.aws3.ebenradio.view.fragment;

import android.content.res.ColorStateList;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.chat.UserChat;
import com.aws3.ebenradio.model.event.chat.ChatDeleteEvent;
import com.aws3.ebenradio.model.event.chat.NewMessageEvent;
import com.aws3.ebenradio.model.event.chat.RemoveUnreadDotEvent;
import com.aws3.ebenradio.model.event.chat.UpdateChatListEvent;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.presenter.fragment.FragmentChoosePresenter;
import com.aws3.ebenradio.view.activity.ChatAddActivity;
import com.aws3.ebenradio.view.activity.ChatAddActivity_;
import com.aws3.ebenradio.view.adapter.AdapterChats;
import com.aws3.ebenradio.view.views.fragment.FragmentChooseView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Dell on 31.10.2016.
 */
@EFragment(R.layout.fragment_add_chat)
public class FragmentChatChoose extends BaseFragment<FragmentChooseView, FragmentChoosePresenter> implements FragmentChooseView {

    @ViewById(R.id.ll_no_chat)
    View mViewNoChat;


    @ViewById(R.id.rv_chats)
    RecyclerView mRecyclerViewChats;


    private AdapterChats adapterChats;

    @ViewById(R.id.add)
    FloatingActionButton mFloatingActionButtonAdd;

    @Click
    void add(){
        ChatAddActivity_.intent(this).start();
    }



    public static FragmentChatChoose getInstance(FragmentChatChoose fragmentChatChoose){
        return fragmentChatChoose;
    }


    @AfterViews
    void after(){
        initRv();
        presenter.getChats();
    }


    private void initRv(){
        mRecyclerViewChats.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterChats = new AdapterChats(new ArrayList<UserChat>(), presenter);
        mRecyclerViewChats.setAdapter(adapterChats);
        mRecyclerViewChats.setItemAnimator(null);
    }




    @Override
    protected void skinEvent(ApplySkinEvent event) {

    }

    @Override
    public void setUpColor(int color, int colorLight) {
        mFloatingActionButtonAdd.setBackgroundTintList(ColorStateList.valueOf(color));
        mFloatingActionButtonAdd.setRippleColor(colorLight);
    }

    @NonNull
    @Override
    public FragmentChoosePresenter createPresenter() {
        return new FragmentChoosePresenter(getActivity());
    }


    @Override
    public void chooseFragment(List<UserChat> list) {
//        replaceFragment(list.isEmpty());
        mViewNoChat.setVisibility(list.isEmpty()?View.VISIBLE:View.GONE);
        mRecyclerViewChats.setVisibility(list.isEmpty()?View.GONE:View.VISIBLE);
        adapterChats.setData(list);

    }

    @Override
    public void selectChat(UserChat userChat) {
        adapterChats.notifyItemRangeChanged(0,adapterChats.getItemCount());
    }


    @Subscribe(sticky = true)
    public void onNewMessageEvent(NewMessageEvent event){
        presenter.getChats();
        EventBus.getDefault().removeStickyEvent(event);
        EventBus.getDefault().post(new RemoveUnreadDotEvent());
    }



    @Subscribe(sticky = true)
    public void onUpdateChatListEvent(UpdateChatListEvent event){

        presenter.getChats();
        EventBus.getDefault().removeStickyEvent(event);

    }

    @Subscribe
    public void onDeleteChat(ChatDeleteEvent event){
        presenter.getChats();
    }


}
