package com.aws3.ebenradio.view.views.activity;

import com.aws3.ebenradio.model.skin.Skin;

/**
 * Created by Dell on 11.10.2016.
 */
public interface MainActivityView extends BaseDrawerActivityView {

    void showRateDialog();
}
