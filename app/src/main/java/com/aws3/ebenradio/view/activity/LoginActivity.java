package com.aws3.ebenradio.view.activity;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.presenter.activity.LoginActivityPresenter;
import com.aws3.ebenradio.view.adapter.AdapterFragment;
import com.aws3.ebenradio.view.custom_views.SlidingTabLayout;
import com.aws3.ebenradio.view.fragment.FragmentLogin;
import com.aws3.ebenradio.view.fragment.FragmentLogin_;
import com.aws3.ebenradio.view.fragment.FragmentSignup;
import com.aws3.ebenradio.view.fragment.FragmentSignup_;
import com.aws3.ebenradio.view.views.activity.LoginActivityView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.Arrays;

/**
 * Created by Dell on 25.10.2016.
 */
@EActivity(R.layout.activity_login)
public class LoginActivity extends BaseActivity<LoginActivityView, LoginActivityPresenter> implements LoginActivityView {

    @ViewById(R.id.view_pager)
    ViewPager mViewPager;

    @ViewById(R.id.tab_layout)
    SlidingTabLayout mTabLayout;

    private AdapterFragment adapterFragment;


    Fragment[] listFragments = {FragmentLogin.getInstance(new FragmentLogin_()), FragmentSignup.getInstance(new FragmentSignup_())};


    int[] titles = {
            R.string.log_in,
            R.string.sign_up
    };


    @Click
    void cancel(){
        onBackPressed();
    }


    @AfterViews
    void after(){
        initViewPager();
    }

    @Override
    public void setUpColor(int color, int colorLight) {

    }


    private void initViewPager(){
        mViewPager.setOffscreenPageLimit(listFragments.length);
        adapterFragment = new AdapterFragment(this,getSupportFragmentManager(), Arrays.asList(listFragments), titles);
        mViewPager.setAdapter(adapterFragment);
        //mTabLayout.setCustomTabView(R.layout.tab_layout, R.id.tv_tab);
        mTabLayout.setDistributeEvenly(true);
        mTabLayout.setViewPager(mViewPager);
        mTabLayout.setSelectedIndicatorColors(ContextCompat.getColor(this,R.color.colorAccent));

    }

    @NonNull
    @Override
    public LoginActivityPresenter createPresenter() {
        return new LoginActivityPresenter(this);
    }
}
