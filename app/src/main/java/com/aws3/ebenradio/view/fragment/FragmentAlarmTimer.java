package com.aws3.ebenradio.view.fragment;

import android.animation.ArgbEvaluator;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.presenter.fragment.FragmentAlarmTimerPresenter;
import com.aws3.ebenradio.view.adapter.AdapterViewPager;
import com.aws3.ebenradio.view.views.fragment.FragmentAlarmTimerView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 24.10.2016.
 */
@EFragment(R.layout.fragment_alarm_timer)
public class FragmentAlarmTimer extends BaseFragment<FragmentAlarmTimerView, FragmentAlarmTimerPresenter> implements FragmentAlarmTimerView {


    @ViewById(R.id.view_pager)
    ViewPager mViewPager;

    private AdapterViewPager adapterViewPager;

    private int[] tabIds = new int[]{R.id.timer, R.id.alarm};


    @Click({R.id.timer, R.id.alarm})
    void tabClick(View v){
        switch (v.getId()){
            case R.id.timer:
                mViewPager.setCurrentItem(0, true);
                break;

            case R.id.alarm:
                mViewPager.setCurrentItem(1, true);
                break;
        }
    }

    @AfterViews
    void after(){
        setUpViewPager();
    }

    @Override
    protected void skinEvent(ApplySkinEvent event) {

    }

    @Override
    public void setUpColor(int color, int colorLight) {

    }

    public static FragmentAlarmTimer getInstance(FragmentAlarmTimer fragmentAlarmTimer){
        return fragmentAlarmTimer;
    }




    @NonNull
    @Override
    public FragmentAlarmTimerPresenter createPresenter() {
        return new FragmentAlarmTimerPresenter(getContext());
    }


    private void setUpViewPager(){

        List<Fragment> list = new ArrayList<>();
        list.add(new FragmentSleepTimer_());
        list.add(new FragmentAlarm_());
        adapterViewPager = new AdapterViewPager(getActivity(), getChildFragmentManager(), list);
        mViewPager.setAdapter(adapterViewPager);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                for(int i = 0; i<tabIds.length;i++) {
                    //тернарнарные операторы лучше но впадло
                    if(position==i) {
                        ((ViewGroup) ((ViewGroup) getView().findViewById(tabIds[i])).getChildAt(0)).getChildAt(1).setAlpha(1 - positionOffset);
                        ((ViewGroup) ((ViewGroup) getView().findViewById(tabIds[i])).getChildAt(0)).getChildAt(0).setAlpha(positionOffset);
                        ((TextView)((ViewGroup) getView().findViewById(tabIds[i])).getChildAt(1)).setTextColor((Integer) new ArgbEvaluator().evaluate(positionOffset,getResources().getColor(android.R.color.white),getResources().getColor(R.color.colorWhiteHalf)));
                    }
                    else {
                        ((ViewGroup) ((ViewGroup) getView().findViewById(tabIds[i])).getChildAt(0)).getChildAt(1).setAlpha(positionOffset);
                        ((ViewGroup) ((ViewGroup) getView().findViewById(tabIds[i])).getChildAt(0)).getChildAt(0).setAlpha(1 - positionOffset);
                        ((TextView)((ViewGroup) getView().findViewById(tabIds[i])).getChildAt(1)).setTextColor((Integer) new ArgbEvaluator().evaluate(positionOffset,getResources().getColor(R.color.colorWhiteHalf),getResources().getColor(android.R.color.white)));
                    }
                }
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}
