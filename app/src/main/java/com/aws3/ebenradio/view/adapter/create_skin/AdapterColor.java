package com.aws3.ebenradio.view.adapter.create_skin;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ItemColorBinding;
import com.aws3.ebenradio.model.create_skin.attrs.Color;
import com.aws3.ebenradio.presenter.activity.CreateSkinActivityPresenter;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Dell on 17.10.2016.
 */
public class AdapterColor extends AdapterAttr<AdapterColor.ColorHolder> {

    private List<Color> list;

    private MvpBasePresenter presenter;

    public AdapterColor(List<Color> list, MvpBasePresenter presenter, int selectedItem) {
        super(selectedItem);
        this.list = list;
        this.presenter = presenter;
    }

    @Override
    public ColorHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemColorBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_color, parent, false);
        binding.setPresenter((CreateSkinActivityPresenter)presenter);
        return new ColorHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(ColorHolder holder, int position) {
        holder.binding.setItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ColorHolder extends RecyclerView.ViewHolder{

        ItemColorBinding binding;

        public ColorHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }


}
