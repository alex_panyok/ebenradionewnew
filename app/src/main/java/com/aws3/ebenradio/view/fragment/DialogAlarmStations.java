package com.aws3.ebenradio.view.fragment;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.radio.RadioStation;
import com.aws3.ebenradio.util.general.PixelUtil;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.aws3.ebenradio.util.skins.ColorUtil;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.view.adapter.AdapterAlarmStations;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;


import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Alex on 21.09.2016.
 */
@EFragment(R.layout.dialog_alarm_stations)
public class DialogAlarmStations extends DialogFragment implements EbenKeys {

    @ViewById(R.id.rv_stations)
    RecyclerView mRecyclerViewStations;

    @ViewById(R.id.background)
    View mViewBackground;




    private AdapterAlarmStations adapterAlarmStations;



    public static DialogAlarmStations getInstance(DialogAlarmStations dialogAlarmStations, List<RadioStation> radioStations){
        Bundle bundle = new Bundle();
        bundle.putSerializable(STATIONS, (Serializable) radioStations);
        dialogAlarmStations.setArguments(bundle);
        return dialogAlarmStations;
    }


    @AfterViews
    void after(){
        mViewBackground.setBackgroundColor(ColorUtil.getColorById(SkinPrefUtils.getSkin().color, getActivity()));
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        initRv((List<RadioStation>)getArguments().getSerializable(STATIONS));
    }

    private void initRv(List<RadioStation> stations){
        mRecyclerViewStations.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapterAlarmStations = new AdapterAlarmStations(stations,((FragmentAlarm)getParentFragment()).getPresenter() );
        mRecyclerViewStations.setAdapter(adapterAlarmStations);
//        adapterAlarmStations.setOnSelectListener(new AdapterAlarmStations.OnSelectListener() {
//            @Override
//            public void select(RadioStation radioStation) {
//                ((FragmentAlarm)getParentFragment()).getPresenter().setUpStream(radioStation);
//                getDialog().cancel();
//            }
//        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow().setLayout(PixelUtil.getScreenWidth(getActivity())/3*2, PixelUtil.getScreenHeight(getActivity())/3*2);
    }
}
