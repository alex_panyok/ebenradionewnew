package com.aws3.ebenradio.view.fragment;



import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.util.general.PixelUtil;
import com.aws3.ebenradio.util.general.ToastUtil;
import com.aws3.ebenradio.util.skins.ColorUtil;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.view.activity.CreateSkinActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Dell on 18.10.2016.
 */
@EFragment(R.layout.dialog_create_skin)
public class DialogCreateSkin extends DialogFragment {

    @ViewById(R.id.et_name)
    EditText mEditTextName;

    @ViewById(R.id.background)
    View mViewBackground;

    @Click
    void ok(){
        if(validate(mEditTextName.getText().toString()))
            ((CreateSkinActivity)getActivity()).getPresenter().getScreenShotView(mEditTextName.getText().toString(),String.valueOf(System.currentTimeMillis()));
    }

    @AfterViews
    void after(){
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mViewBackground.setBackgroundColor(ColorUtil.getColorById(SkinPrefUtils.getSkin().color, getActivity()));
        if(((CreateSkinActivity)getActivity()).getPresenter().getIndex()!=-1){
            mEditTextName.setText(SkinPrefUtils.getSkins().get(((CreateSkinActivity)getActivity()).getPresenter().getIndex()).name);
        }
    }


    @Click
    void cancel(){
        getDialog().cancel();
    }


    private boolean validate(String name){
        if(name.isEmpty()) {
            ToastUtil.toast(getActivity(),getString(R.string.fill_field));
            return false;
        }
        if(((CreateSkinActivity)getActivity()).getPresenter().getIndex()==-1)
            if(!checkName(name)) {
                ToastUtil.toast(getActivity(),getString(R.string.created_name));
                return false;
        }

        return true;

    }


    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow().setLayout(PixelUtil.getScreenWidth(getActivity()), ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    private boolean checkName(String name){
        for(Skin skin : SkinPrefUtils.getSkins()){
            if(skin.name.equals(name))
            return false;
        }
        return true;
    }


}
