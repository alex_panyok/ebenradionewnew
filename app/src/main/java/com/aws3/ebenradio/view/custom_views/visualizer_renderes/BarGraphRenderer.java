/**
 * Copyright 2011, Felix Palmer
 *
 * Licensed under the MIT license:
 * http://creativecommons.org/licenses/MIT/
 */
package com.aws3.ebenradio.view.custom_views.visualizer_renderes;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;

import com.aws3.ebenradio.model.visualizer.AudioData;
import com.aws3.ebenradio.model.visualizer.FFTData;
import com.aws3.ebenradio.util.general.PixelUtil;

//import com.pheelicks.visualizer.AudioData;
//import com.pheelicks.visualizer.FFTData;

public class BarGraphRenderer extends Renderer
{
  private int mDivisions;
  private Paint mPaint;
  private boolean mTop;
  private Context context;
  /**
   * Renders the FFT data as a series of lines, in histogram form
//   * @param divisions - must be a power of 2. Controls how many lines to draw
   * @param paint - Paint to draw lines with
   * @param top - whether to draw the lines at the top of the canvas, or the bottom
   */
  public BarGraphRenderer(Context context, Paint paint,
                          boolean top)
  {
    super();
//    mDivisions = divisions;
    mPaint = paint;
    mTop = top;
    this.context  = context;
  }

//  @Override
//  public void onRender(Canvas canvas, AudioData data, Rect rect)
//  {
//    // Do nothing, we only display FFT data
//  }

  @Override
  public void onRender(Canvas canvas, AudioData data, Rect rect) {

  }

  @Override
  public void onRender(Canvas canvas, FFTData data, Rect rect, float width) {
    mPaint.setStrokeWidth(width/(width/8));
    mDivisions = (int)((width/2-width/2%2))>=4?(int)((width/2-width/2%2)):4;
    for (int i = 0; i < data.bytes.length/mDivisions; i++) {
//      Log.d("WAVE_DATA",  mFFTPoints[i]+" "+data.bytes[i]);
      mFFTPoints[i * 4] = i  * 4 * mDivisions;
      mFFTPoints[i * 4 + 2] = i * 4 * mDivisions;
      byte rfk = data.bytes[i];
      byte ifk = data.bytes[i + 1];
      float magnitude = (rect.height()/20+rfk * rfk + ifk * ifk);
      int dbValue = (int) (10 * Math.log10(magnitude));
        mFFTPoints[i * 4+ 1] = rect.height();
        mFFTPoints[i * 4 + 3] = rect.height()-(dbValue * 5 - 10)*(width/8);
//      mPaint.setColor(Color.argb(255,255,(int)(255*(((float)(rect.height()-((dbValue * 5 - 10))))/rect.height())),(int)(255*(((float)(rect.height()-((dbValue * 5 - 10))))/rect.height()))));
      canvas.drawLine( mFFTPoints[i * 4], mFFTPoints[i * 4+ 1],mFFTPoints[i * 4 + 2], mFFTPoints[i * 4 + 3],mPaint);
    }
//    (int)(255*(((float)(rect.height()-(dbValue * 5 - 10)))/rect.height())))
  }
}
