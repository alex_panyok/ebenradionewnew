package com.aws3.ebenradio.view.adapter.create_skin;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.databinding.ItemNavigationBinding;
import com.aws3.ebenradio.model.create_skin.attrs.Navigation;
import com.aws3.ebenradio.presenter.activity.CreateSkinActivityPresenter;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.List;

/**
 * Created by Alex on 16.10.2016.
 */
 public class AdapterNavigation extends AdapterAttr<AdapterNavigation.NavigationHolder> {

    private List<Navigation> list;

    private MvpBasePresenter presenter;

    public AdapterNavigation(List<Navigation> list, MvpBasePresenter presenter, int selectedItem) {
        super(selectedItem);
        this.list = list;
        this.presenter = presenter;
    }

    @Override
    public NavigationHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemNavigationBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_navigation, parent,false);
        binding.setPresenter((CreateSkinActivityPresenter)presenter);
        return new NavigationHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(NavigationHolder holder, int position) {
        holder.binding.setItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class NavigationHolder extends RecyclerView.ViewHolder{

        ItemNavigationBinding binding;

        NavigationHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }





}
