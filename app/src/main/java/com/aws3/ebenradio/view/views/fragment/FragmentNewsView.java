package com.aws3.ebenradio.view.views.fragment;

import com.aws3.ebenradio.model.rss.Item;

import java.util.List;

/**
 * Created by Dell on 28.10.2016.
 */
public interface FragmentNewsView extends BaseFragmentView {

    void setNews(List<Item> list);
}
