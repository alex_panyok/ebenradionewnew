package com.aws3.ebenradio.view.views.activity;

import com.aws3.ebenradio.model.follow.FollowResponse;

/**
 * Created by Dell on 03.11.2016.
 */
public interface ChatAddActivityView extends BaseActivityView {

    void setUpFollowing(FollowResponse followResponse);
}
