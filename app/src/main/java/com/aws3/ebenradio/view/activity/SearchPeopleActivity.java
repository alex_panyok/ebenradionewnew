package com.aws3.ebenradio.view.activity;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.search.SearchPeopleEvent;
import com.aws3.ebenradio.presenter.activity.SearchPeopleActivityPresenter;
import com.aws3.ebenradio.presenter.fragment.FragmentSearchPeoplePresenter;
import com.aws3.ebenradio.util.general.EditTextUtil;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.util.general.PixelUtil;
import com.aws3.ebenradio.view.adapter.AdapterFragment;
import com.aws3.ebenradio.view.custom_views.SlidingTabLayout;
import com.aws3.ebenradio.view.fragment.FragmentSearchPeople;
import com.aws3.ebenradio.view.fragment.FragmentSearchPeople_;
import com.aws3.ebenradio.view.views.activity.SearchPeopleActivityView;
import com.jakewharton.rxbinding.widget.RxTextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.greenrobot.eventbus.EventBus;

import java.util.Arrays;

import io.codetail.animation.SupportAnimator;
import io.codetail.animation.ViewAnimationUtils;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Dell on 27.10.2016.
 */
@EActivity(R.layout.activity_search_people)
public class SearchPeopleActivity extends BaseActivity<SearchPeopleActivityView, SearchPeopleActivityPresenter> implements SearchPeopleActivityView {




    private Fragment[] fragments = {FragmentSearchPeople.getInstance(new FragmentSearchPeople_(),0),
            FragmentSearchPeople.getInstance(new FragmentSearchPeople_(),5),
            FragmentSearchPeople.getInstance(new FragmentSearchPeople_(),10),
            FragmentSearchPeople.getInstance(new FragmentSearchPeople_(),15),
            FragmentSearchPeople.getInstance(new FragmentSearchPeople_(),20)};

    private int[] titles = new int[]{R.string.all, R.string.five_miles, R.string.ten_miles, R.string.fifteen_miles, R.string.twenty_miles};

    private AdapterFragment adapterFragment;

    @ViewById(R.id.view_pager)
    ViewPager mViewPager;

    @ViewById(R.id.tab_layout)
    SlidingTabLayout mTabLayout;

    @ViewById(R.id.et_search)
    EditText mEditTextSearch;


    @ViewById(R.id.search)
    View mViewSearch;

    @ViewById(R.id.search_view)
    View mSearchView;

    private SupportAnimator animator;

    @Click
    void search(){
        for(Fragment fragment : fragments){
            if(!(((FragmentSearchPeople)fragment).getPresenter()).loaded)
                return;
        }
        int[] locations = new int[2];
        mViewSearch.getLocationOnScreen(locations);
        animator = initAnimator(locations[0]+mViewSearch.getWidth()/2, locations[1]);
        animator.start();
    }


    @Click(R.id.close_search)
    void closeSearch(){
        initReverseAnimator(animator).start();
    }

    @AfterViews
    void after(){
        initViewPager();
        RxTextView.textChanges(mEditTextSearch)
                .subscribe(new Action1<CharSequence>() {
                    @Override
                    public void call(CharSequence s) {
                        EventBus.getDefault().post(new SearchPeopleEvent(s.toString()));
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                    }
                });
    }


    private void initViewPager(){
        mViewPager.setOffscreenPageLimit(fragments.length);
        adapterFragment = new AdapterFragment(this,getSupportFragmentManager(), Arrays.asList(fragments), titles);
        mViewPager.setAdapter(adapterFragment);
        //mTabLayout.setCustomTabView(R.layout.tab_layout, R.id.tv_tab);
        mTabLayout.setDistributeEvenly(true);
        mTabLayout.setViewPager(mViewPager);
        mTabLayout.setSelectedIndicatorColors(ContextCompat.getColor(this,R.color.colorAccent));
    }


    @Override
    public void setUpColor(int color, int colorLight) {

    }

    @NonNull
    @Override
    public SearchPeopleActivityPresenter createPresenter() {
        return new SearchPeopleActivityPresenter(this);
    }



    private SupportAnimator initAnimator(final int posX, int posY){
        SupportAnimator animator =
                ViewAnimationUtils.createCircularReveal(mSearchView, posX, posY, 0, PixelUtil.getScreenWidth(this));
        animator.setInterpolator(new AccelerateInterpolator());
        animator.setDuration(400);
        animator.addListener(new SupportAnimator.AnimatorListener() {
            @Override
            public void onAnimationStart() {
                mSearchView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd() {
                EditTextUtil.setFocus(SearchPeopleActivity.this, mEditTextSearch);
            }

            @Override
            public void onAnimationCancel() {

            }

            @Override
            public void onAnimationRepeat() {

            }
        });
        return animator;
    }


    private SupportAnimator initReverseAnimator(SupportAnimator animator){
        SupportAnimator animatorReverse = animator.reverse();
        animatorReverse.setInterpolator(new DecelerateInterpolator());
        animatorReverse.setDuration(400);
        animatorReverse.addListener(new SupportAnimator.AnimatorListener() {
            @Override
            public void onAnimationStart() {
                EditTextUtil.clearFocus(SearchPeopleActivity.this,mEditTextSearch);
                mEditTextSearch.setText("");
            }

            @Override
            public void onAnimationEnd() {
                mSearchView.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationCancel() {

            }

            @Override
            public void onAnimationRepeat() {

            }
        });
        return animatorReverse;
    }
}
