package com.aws3.ebenradio.view.views.fragment;

/**
 * Created by Dell on 25.10.2016.
 */
public interface FragmentLoginView extends BaseFragmentView {

    void validate();

    void performFacebookClick();
}
