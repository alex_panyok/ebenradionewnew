package com.aws3.ebenradio.networking;


import com.aws3.ebenradio.BuildConfig;
import com.aws3.ebenradio.model.chat.ChatResponse;
import com.aws3.ebenradio.model.chat.DeleteChatBody;
import com.aws3.ebenradio.model.chat.MessageBody;
import com.aws3.ebenradio.model.chat.MessageResponse;
import com.aws3.ebenradio.model.chat.MessagesResponse;
import com.aws3.ebenradio.model.contact.Contact;
import com.aws3.ebenradio.model.device.SaveDeviceBody;
import com.aws3.ebenradio.model.event_er.BannerResponse;
import com.aws3.ebenradio.model.event_er.EventResponse;
import com.aws3.ebenradio.model.event_er.SingleEventResponse;
import com.aws3.ebenradio.model.follow.FollowResponse;
import com.aws3.ebenradio.model.follow.FollowUnfollowBody;
import com.aws3.ebenradio.model.login.LoginBody;
import com.aws3.ebenradio.model.login.LogoutBody;
import com.aws3.ebenradio.model.login.SignUpBody;
import com.aws3.ebenradio.model.login.SignUpResponse;
import com.aws3.ebenradio.model.login.SocialLoginBody;
import com.aws3.ebenradio.model.networking.EbenResponse;
import com.aws3.ebenradio.model.player.SongStreamResponse;
import com.aws3.ebenradio.model.rss.Channel;
import com.aws3.ebenradio.model.rss.Feed;
import com.aws3.ebenradio.model.user.ChangePasswordBody;
import com.aws3.ebenradio.model.user.ForgetPasswordBody;
import com.aws3.ebenradio.model.user.User;
import com.aws3.ebenradio.model.user.UserPublicProfileResponse;
import com.aws3.ebenradio.model.user.UserUpdatedProfileResponse;
import com.aws3.ebenradio.model.user.UsersFromSearchResponse;
import com.google.android.exoplayer2.BaseRenderer;


import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;
import rx.Observable;


public interface EbenRadioRetroService {

    String USER_AGENT_EBENRADIO_ANDROID = "User-Agent: ebenradio, android, " + BuildConfig.VERSION_NAME;

    @Scalar
    @GET
    Observable<String> getSongTitle(@Url String url);


    @GET
    Observable<SongStreamResponse> getSongCoverImage(@Url String url);

    @GET
    Observable<ResponseBody> getAd(@Url String url);


    @Xml
    @GET
    Observable<Feed> getNews(@Url String url);


    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @PUT("api/user/login")
    Observable<User> login(@Header("Authorization") String authorization,
                           @Body LoginBody body);


    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @PUT("api/user/logout")
    Observable<EbenResponse> logout(@Header("Authorization") String authorization,
                                    @Header("Cookie") String cookie,
                                   @Body LogoutBody body);



    //
    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @PUT("api/user/login")
    Observable<User> socialLogin(@Header("Authorization") String authorization,
                                 @Body SocialLoginBody body);
//
    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @POST("api/users")
    Observable<SignUpResponse> createAccount(@Header("Authorization") String authorization,
                                             @Body SignUpBody body);
//
    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @GET("api/user/followers")
    Observable<FollowResponse> getFollowers(@Header("Authorization") String authorization,
                                            @Header("Cookie") String cookies);

    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @GET("api/user/following")
    Observable<FollowResponse> getFollowing(@Header("Authorization") String authorization,
                                            @Header("Cookie") String cookies);
//
    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @GET("api/user/geosearch-ext")
    Observable<UsersFromSearchResponse> getUsersByDistance(@Header("Authorization") String authorization,
                                                           @Header("Cookie") String cookies,
                                                           @Query("distance") String distance,
                                                           @Query("limit") String limit);
//
//    @Headers({USER_AGENT_EBENRADIO_ANDROID})
//    @GET("api/user/geosearch-ext")
//    Observable<UsersFromSearchResponse> getAllUsers(@Header("Authorization") String authorization,
//                                                    @Header("Cookie") String cookies,
//                                                    @Query("limit") String limit);
//
//
    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @PUT("api/user")
    Observable<UserUpdatedProfileResponse> updateUserInfo(@Header("Authorization") String authorization,
                                                          @Header("Cookie") String cookies,
                                                          @Body User body);
//
//
    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @GET("api/user/public_profile")
    Observable<UserPublicProfileResponse> getUserPublicProfile(@Header("Authorization") String authorization,
                                                               @Header("Cookie") String cookies,
                                                               @Query("user_id") String userId);
//
//
    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @POST("api/user/follow")
    Observable<FollowResponse> followUnfollow(@Header("Authorization") String authorization,
                                              @Header("Cookie") String cookies,
                                              @Body FollowUnfollowBody body);


    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @GET("api/user/chats")
    Observable<ChatResponse> getChats(@Header("Authorization") String authorization,
                                      @Header("Cookie") String cookies);


    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @POST("api/user/save_device")
    Observable<EbenResponse> saveDevice(@Header("Authorization") String authorization,
                                        @Header("Cookie") String cookies,
                                        @Body SaveDeviceBody body);


    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @POST("api/user/message")
    Observable<MessageResponse> sendMessage(@Header("Authorization") String authorization,
                                            @Header("Cookie") String cookies,
                                            @Body MessageBody body);


    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @POST("api/user/delete_chat")
    Observable<EbenResponse> deleteChat(@Header("Authorization") String authorization,
                                            @Header("Cookie") String cookies,
                                            @Body DeleteChatBody body);


    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @POST("api/user/forgot_password")
    Observable<EbenResponse> forgetPassword(@Header("Authorization") String authorization,
                                            @Header("Cookie") String cookies,
                                            @Body ForgetPasswordBody body);



    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @GET("api/user/messages")
    Observable<MessagesResponse> getMessages(@Header("Authorization") String authorization,
                                             @Header("Cookie") String cookies, @Query("from_user_id") String userWithId,
                                             @Query("page") int page);

    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @PUT("api/user/change_password")
    Observable<EbenResponse> changePassword(@Header("Authorization") String authorization,
                                             @Header("Cookie") String cookies,
                                             @Body ChangePasswordBody body);


    @Headers({USER_AGENT_EBENRADIO_ANDROID})
    @POST("api/support/contact")
    Observable<EbenResponse> sendContactMessage(@Header("Authorization") String authorization,
                                                   @Body Contact contact);


    @FormUrlEncoded
    @POST("/api/er_banners")
    Observable<BannerResponse> getBanners(@Field("device_id") String device_id);

    @FormUrlEncoded
    @POST("/api/banner_viewed")
    Observable<BannerResponse> viewBanner(@Field("banner_id") String id, @Field("device_id") String device_id);


    @FormUrlEncoded
    @POST("/api/save_device")
    Observable<EventResponse> saveDevice(@Field("device_id") String device_id, @Field("device_token") String device_token, @Field("type") String type);

    @FormUrlEncoded
    @POST("/api/event_list")
    Observable<EventResponse> getEvents(@Field("device_id") String device_id);

    @FormUrlEncoded
    @POST("/api/event_view/{id}")
    Observable<SingleEventResponse> getEvent(@Path("id") String id, @Field("device_id") String device_id);






}
