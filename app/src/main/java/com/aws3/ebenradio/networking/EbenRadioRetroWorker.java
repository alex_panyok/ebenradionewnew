package com.aws3.ebenradio.networking;


import android.util.Log;

import com.aws3.ebenradio.model.chat.ChatResponse;
import com.aws3.ebenradio.model.chat.DeleteChatBody;
import com.aws3.ebenradio.model.chat.MessageBody;
import com.aws3.ebenradio.model.chat.MessageResponse;
import com.aws3.ebenradio.model.chat.MessagesResponse;
import com.aws3.ebenradio.model.contact.Contact;
import com.aws3.ebenradio.model.device.SaveDeviceBody;
import com.aws3.ebenradio.model.event.networking.NoNetworkEvent;
import com.aws3.ebenradio.model.event_er.BannerResponse;
import com.aws3.ebenradio.model.event_er.Event;
import com.aws3.ebenradio.model.event_er.EventResponse;
import com.aws3.ebenradio.model.event_er.SingleEventResponse;
import com.aws3.ebenradio.model.follow.FollowResponse;
import com.aws3.ebenradio.model.follow.FollowUnfollowBody;
import com.aws3.ebenradio.model.login.LoginBody;
import com.aws3.ebenradio.model.login.LogoutBody;
import com.aws3.ebenradio.model.login.SignUpBody;
import com.aws3.ebenradio.model.login.SignUpResponse;
import com.aws3.ebenradio.model.login.SocialLoginBody;
import com.aws3.ebenradio.model.networking.EbenResponse;
import com.aws3.ebenradio.model.player.SongStreamResponse;
import com.aws3.ebenradio.model.rss.Channel;
import com.aws3.ebenradio.model.rss.Feed;
import com.aws3.ebenradio.model.user.ChangePasswordBody;
import com.aws3.ebenradio.model.user.ForgetPasswordBody;
import com.aws3.ebenradio.model.user.User;
import com.aws3.ebenradio.model.user.UserPublicProfileResponse;
import com.aws3.ebenradio.model.user.UserUpdatedProfileResponse;
import com.aws3.ebenradio.model.user.UsersFromSearchResponse;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;


import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;
import rx.Observable;


public class EbenRadioRetroWorker  {

    private final static String EBEN_URL = "http://ebenradio.com:8004";


    private final static String EBEN_URL_BANNER = "http://api.ebenradio.com";

    private static EbenRadioRetroWorker retroWorker;

    private static volatile EbenRadioRetroService restClient;

    private static volatile EbenRadioRetroService restClientBanner;


    public  String lastCallerClass;

    public  String lastCalledMethod;

    public  MvpBasePresenter lastPresenter;

    public  Object[] lastParams;

    OnLostInternetConnection onLostInternetConnection;


    private EbenRadioRetroWorker() {

            createRestWorker();

    }

    public static EbenRadioRetroWorker getInstance() {
        if (retroWorker == null) {
            synchronized (EbenRadioRetroWorker.class) {
                if (retroWorker == null) {
                    retroWorker = new EbenRadioRetroWorker();
                }
            }
        }
        return retroWorker;
    }

    private OkHttpClient getUnsafeOkHttpClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(60, TimeUnit.SECONDS);
        builder.writeTimeout(60, TimeUnit.SECONDS);
        builder.readTimeout(60, TimeUnit.SECONDS);
//        builder.addInterceptor(interceptor);
        builder.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                okhttp3.Response original = null;

                try {
                    original = chain.proceed(chain.request());
                        String userId = original.header("Set-Cookie");
                    if(!userId.isEmpty())
                        PrefUtils.setCookies(userId);
                    } catch (Exception e) {
                        e.printStackTrace();
                        if(e instanceof UnknownHostException || e instanceof SocketTimeoutException){
                          if(onLostInternetConnection!=null && lastPresenter!=null && lastParams!=null){
                              onLostInternetConnection.noInternet();
                          }
                            EventBus.getDefault().post(new NoNetworkEvent());
                            return null;
                        }
                    }
                lastParams = null;
                return original;
            }
        });
        return builder.build();

    }

    public interface OnLostInternetConnection{
        void noInternet();
    }


    public void setOnLostConnectionListener(OnLostInternetConnection onLostConnectionListener){
        this.onLostInternetConnection = onLostConnectionListener;
    }


    public void removeOnLostConnection(){
        this.onLostInternetConnection = null;
    }


    private void createRestWorker() {
        Retrofit restAdapter = new Retrofit.Builder()
                .baseUrl(EBEN_URL)
                .addConverterFactory(new QualifiedTypeConverterFactory(GsonConverterFactory.create(), SimpleXmlConverterFactory.create(), ScalarsConverterFactory.create()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(getUnsafeOkHttpClient())
                .build();


        Retrofit restAdapterBanner = new Retrofit.Builder()
                .baseUrl(EBEN_URL_BANNER)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(getUnsafeOkHttpClient())
                .build();


        restClient = restAdapter.create(EbenRadioRetroService.class);
        restClientBanner  = restAdapterBanner.create(EbenRadioRetroService.class);

    }



    public Observable<String> getSongTitle(String url){
        return restClient.getSongTitle(url);
    }

    public Observable<ResponseBody> getAds(String url){
        return restClient.getAd(url);
    }

    public Observable<SongStreamResponse> getSongCoverImage(String url){
        return restClient.getSongCoverImage(url);
    }

    public Observable<Feed> getNews(String url){
        return restClient.getNews(url);
    }

    public Observable<SignUpResponse> signUp(String authorization, SignUpBody signUpBody){
        return restClient.createAccount(authorization, signUpBody);
    }
//
    public Observable<User> logIn(String authorization, LoginBody loginBody){
        return restClient.login(authorization, loginBody);
    }

    public Observable<EbenResponse> logOut(String authorization, String cookie, LogoutBody logoutBody){
        return restClient.logout(authorization, cookie, logoutBody);
    }
//
    public Observable<User> socialLogin(String authorization, SocialLoginBody loginBody){
        return restClient.socialLogin(authorization, loginBody);
    }


    public Observable<FollowResponse> getFollowers(String authorization, String cookie){
        return restClient.getFollowers(authorization, cookie);
    }


    public Observable<FollowResponse> getFollowing(String authorization, String cookie){
        return restClient.getFollowing(authorization, cookie);
    }
//
//    public Observable<UsersFromSearchResponse> getAllPeople(String authorization, String cookie, String limit){
//        return restClient.getAllUsers(authorization, cookie, limit);
//    }
//
    public Observable<UsersFromSearchResponse> getUsersByDistance(String authorization, String cookie, String distance, String limit){
        return restClient.getUsersByDistance(authorization, cookie, distance, limit);
    }
//
    public Observable<UserUpdatedProfileResponse> updateUserProfile(String authorization, String cookie, User user){
        return restClient.updateUserInfo(authorization, cookie, user);
    }
//
    public Observable<UserPublicProfileResponse> getUser(String authorization, String cookie, String userId){
        return restClient.getUserPublicProfile(authorization, cookie, userId);
    }
//
    public Observable<FollowResponse> followUnfollow(String authorization, String cookie, FollowUnfollowBody body){
        return restClient.followUnfollow(authorization, cookie, body);
    }

    public Observable<ChatResponse> getChats(String authorization, String cookie){
        return restClient.getChats(authorization, cookie);
    }


    public Observable<EbenResponse> saveDevice(String authorization, String cookie, SaveDeviceBody body){
        return restClient.saveDevice(authorization, cookie, body);
    }


    public Observable<EventResponse> saveDeviceBanner(String device_id, String device_token, String type){
        return restClientBanner.saveDevice(device_id, device_token, type);
    }

    public Observable<MessageResponse> sendMessage(String authorization, String cookie, MessageBody body){
        return restClient.sendMessage(authorization, cookie, body);
    }

    public Observable<EbenResponse> sendContactMessage(String authorization, Contact contact){
        return restClient.sendContactMessage(authorization, contact);
    }


    public Observable<MessagesResponse> getMessages(String authorization, String cookie, String userId, int page){
        return restClient.getMessages(authorization, cookie, userId, page);
    }

    public Observable<EbenResponse> changePassword(String auth, String cookies, ChangePasswordBody body){
        return restClient.changePassword(auth,cookies,body);
    }

    public Observable<EventResponse> getEvents(String device_id){
        return restClientBanner.getEvents(device_id);
    }

    public Observable<SingleEventResponse> getEvent(String event_id,String device_id){
        return restClientBanner.getEvent(event_id, device_id);
    }

    public Observable<BannerResponse> getBanners(String device_id){
        return restClientBanner.getBanners(device_id);
    }

    public Observable<BannerResponse> viewBanner(String id,String device_id){
        return restClientBanner.viewBanner(id, device_id);
    }


    public Observable<EbenResponse> deleteChat(String auth, String cookies,DeleteChatBody body){
        return restClient.deleteChat(auth, cookies, body);
    }


    public Observable<EbenResponse> forgotPassword(String auth, String cookies,ForgetPasswordBody body){
        return restClient.forgetPassword(auth, cookies, body);
    }


    public EbenRadioRetroWorker writeClassAndMethod(MvpBasePresenter presenter, Object...lastParams){
        lastCalledMethod = Thread.currentThread().getStackTrace()[3].getMethodName();
        lastCallerClass = Thread.currentThread().getStackTrace()[3].getClassName();
        lastPresenter = presenter;
        this.lastParams = lastParams;
        return this;
    }
















}
