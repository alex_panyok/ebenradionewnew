package com.aws3.ebenradio.networking;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Dell on 28.10.2016.
 */
@Retention(RUNTIME)
public @interface Xml {

}
