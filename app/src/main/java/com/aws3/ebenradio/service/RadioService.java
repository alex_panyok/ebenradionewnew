package com.aws3.ebenradio.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.audiofx.Equalizer;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.media.MediaMetadataCompat;
import android.support.v4.media.session.MediaButtonReceiver;
import android.support.v4.media.session.MediaControllerCompat;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.support.v7.app.NotificationCompat;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.event.alarm.ChangeStationAlarm;
import com.aws3.ebenradio.model.event.alarm.TurnOffAlarmEvent;
import com.aws3.ebenradio.model.event.alarm.TurnOffMusicEvent;
import com.aws3.ebenradio.model.event.radio.AudioSessionEvent;
import com.aws3.ebenradio.model.event.radio.ChangeStationEvent;
import com.aws3.ebenradio.model.event.radio.EqualizerSetupEvent;
import com.aws3.ebenradio.model.event.radio.TogglePlayEvent;
import com.aws3.ebenradio.model.event.radio.UpdatePlayerInfoEvent;
import com.aws3.ebenradio.model.event.skin.ApplySkinEvent;
import com.aws3.ebenradio.model.player.SongStreamResponse;
import com.aws3.ebenradio.networking.EbenRadioRetroWorker;
import com.aws3.ebenradio.receiver.HeadsetPlugReceiver;
import com.aws3.ebenradio.util.alarm.AlarmUtil;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.radio.AudioManagerUtil;
import com.aws3.ebenradio.util.radio.EqualizerUtil;
import com.aws3.ebenradio.util.radio.RadioPlayerState;
import com.aws3.ebenradio.util.radio.RadioStationUtil;
import com.aws3.ebenradio.util.skins.BackgroundUtil;
import com.aws3.ebenradio.view.activity.MainActivity_;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Renderer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.audio.MediaCodecAudioRenderer;
import com.google.android.exoplayer2.ext.okhttp.OkHttpDataSourceFactory;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.extractor.mp3.Mp3Extractor;
import com.google.android.exoplayer2.extractor.mp4.Mp4Extractor;
import com.google.android.exoplayer2.extractor.ogg.OggExtractor;
import com.google.android.exoplayer2.extractor.ts.Ac3Extractor;
import com.google.android.exoplayer2.extractor.ts.AdtsExtractor;
import com.google.android.exoplayer2.extractor.ts.TsExtractor;
import com.google.android.exoplayer2.extractor.wav.WavExtractor;
import com.google.android.exoplayer2.mediacodec.MediaCodecSelector;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.FixedTrackSelection;

import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.util.Util;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


public class RadioService extends Service implements ExoPlayer.EventListener, AudioManager.OnAudioFocusChangeListener, EbenKeys {
    public static final String ACTION_TOGGLE = "action_toggle";
    public static final String ACTION_PLAY = "action_play";
    public static final String ACTION_PAUSE = "action_pause";
    public static final String ACTION_SET_SOURCE = "action_set_source";
    public static final String ACTION_ALARM_FIRE = "action_alarm_fire";
    public static final String ACTION_NEXT = "action_next";
    public static final String ACTION_PREVIOUS = "action_previous";
    public static final String ACTION_STOP = "action_stop";
    public int audioSession;
    public HeadsetPlugReceiver headsetPlugReceiver;
    ExoPlayer player;
    boolean updateInfo;
    private MediaControllerCompat mediaControllerCompat;
    private MediaSessionCompat mediaSession;
    private Notification status;
    private CompositeSubscription subscription;
    private String titleSong;
    private String songCover;
    private Bitmap coverBitmap;
    private AudioManager audioManager;
    private android.support.v4.app.NotificationCompat.Action currentAction;
    private String action;
    private Equalizer mEqualizer;
    private WifiManager.WifiLock mWifiLock;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        getSongTitle(PrefUtils.getStation().id);
        if (intent != null) {
            handleIntent(intent);
        }
        return START_NOT_STICKY;
    }

    private void resumePlayer() {
        if (requestAudioFocus()) {
            if (player != null) {
//                relaxResources(true);
                player.prepare(initRadioMediaSource(PrefUtils.getStation().id));
            } else
                initExoPlayer(true);
            player.setPlayWhenReady(true);
            mWifiLock.acquire();
        }
    }



//    public void relaxResources(boolean releaseMediaPlayer) {
//
//        // stop and release the Media Player, if it's available
//        if (releaseMediaPlayer && player != null) {
//            player.setPlayWhenReady(false);
//            player.stop();
////            player.release();
////            player = null;
//        }
//
//        // we can also release the Wifi lock, if we're holding it
//    }


    private void pausePlayer() {
        player.setPlayWhenReady(false);
        if (mWifiLock.isHeld()) {
            mWifiLock.release();
        }
    }


    private void initMediaSession() {
        ComponentName mRemoteControlResponder = new ComponentName(getPackageName(),
                MediaButtonReceiver.class.getName());
        final Intent mediaButtonIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
        mediaButtonIntent.setComponent(mRemoteControlResponder);
        mediaSession = new MediaSessionCompat(getApplicationContext(), getString(R.string.app_name), mRemoteControlResponder, null);

        mediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS | MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);
        try {
            mediaControllerCompat = new MediaControllerCompat(getApplicationContext(), mediaSession.getSessionToken());
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        mediaSession.setCallback(new MediaSessionCompat.Callback() {


                                     @Override
                                     public boolean onMediaButtonEvent(Intent mediaButtonEvent) {

                                         if (((KeyEvent) mediaButtonEvent.getParcelableExtra(Intent.EXTRA_KEY_EVENT)).getKeyCode() == KeyEvent.KEYCODE_HEADSETHOOK) {
                                             Intent intent = new Intent(RadioService.this, RadioService.class);
                                             intent.setAction(ACTION_TOGGLE);
                                             startService(intent);
                                         }
                                         return true;
                                     }


                                     @Override
                                     public void onPlay() {
                                         super.onPlay();
                                         RadioPlayerState.setStatePlay();
                                         currentAction = generateAction(android.R.drawable.ic_media_pause, "Pause", ACTION_PAUSE);
                                         buildNotification(currentAction);
                                         resumePlayer();
                                         startForeground(1, status);
                                     }

                                     @Override
                                     public void onPause() {
                                         super.onPause();
                                         EventBus.getDefault().postSticky(new AudioSessionEvent(audioSession));
                                         RadioPlayerState.setStatePause();
                                         currentAction = generateAction(android.R.drawable.ic_media_play, "Play", ACTION_PLAY);
                                         buildNotification(currentAction);
                                         pausePlayer();
                                         stopForeground(false);

                                     }

                                     @Override
                                     public void onStop() {
                                         super.onStop();
                                         //Stop media player her

                                         stopPlayer();
                                         stopService(new Intent(RadioService.this, RadioService.class));

                                     }


                                 }
        );
        mediaSession.setPlaybackState(new PlaybackStateCompat.Builder()
                .setActions(PlaybackStateCompat.ACTION_PLAY | PlaybackStateCompat.ACTION_PLAY_PAUSE |
                        PlaybackStateCompat.ACTION_PLAY_FROM_MEDIA_ID | PlaybackStateCompat.ACTION_PAUSE |
                        PlaybackStateCompat.ACTION_SKIP_TO_NEXT | PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS).build());
        mediaSession.setActive(true);
    }


    private NotificationCompat.Action generateAction(int icon, String title, String intentAction) {
        Intent intent = new Intent(getApplicationContext(), RadioService.class);
        intent.setAction(intentAction);
        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), 1, intent, 0);
        return new NotificationCompat.Action.Builder(icon, title, pendingIntent).build();
    }


    private void buildNotification(NotificationCompat.Action action) {
        NotificationCompat.MediaStyle style = new NotificationCompat.MediaStyle();
        Intent intent = new Intent(this, MainActivity_.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        Intent deleteIntent = new Intent(this, RadioService.class);
        deleteIntent.setAction(ACTION_STOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 1, intent, 0);
        PendingIntent pendingDeleteIntent = PendingIntent.getService(getApplicationContext(), 1001, deleteIntent, 0);
        NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_notify)
//                .setColor(PrefUtils.getColor())
                .setContentTitle(PrefUtils.getStation().title)
                .setContentText(titleSong)
                .setWhen(0)
                .setDeleteIntent(pendingDeleteIntent)
                .setLargeIcon(coverBitmap)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setContentIntent(pendingIntent)
                .setStyle(style);
        builder.addAction(action);
        style.setShowActionsInCompactView(0);
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        status = builder.build();
        notificationManager.notify(1, status);

    }


    private void handleIntent(Intent intent) {
        if (intent == null || intent.getAction() == null)
            return;

        action = intent.getAction();

        if (action.equalsIgnoreCase(ACTION_TOGGLE)) {
            if (!player.getPlayWhenReady() || player == null)
                mediaControllerCompat.getTransportControls().play();
            else
                mediaControllerCompat.getTransportControls().pause();
        } else if (action.equalsIgnoreCase(ACTION_PLAY)) {
            mediaControllerCompat.getTransportControls().play();
        } else if (action.equalsIgnoreCase(ACTION_PAUSE)) {
            mediaControllerCompat.getTransportControls().pause();
        } else if (action.equalsIgnoreCase(ACTION_SET_SOURCE)) {
//            handleChangeSourceAction((RadioStation)intent.getSerializableExtra(SELECTED_STATION));
        } else if (action.equalsIgnoreCase(ACTION_ALARM_FIRE)) {
            handleAlarmFireAction(intent);
        } else if (action.equalsIgnoreCase(ACTION_PREVIOUS)) {
            mediaControllerCompat.getTransportControls().skipToPrevious();
        } else if (action.equalsIgnoreCase(ACTION_NEXT)) {
            mediaControllerCompat.getTransportControls().skipToNext();
        } else if (action.equalsIgnoreCase(ACTION_STOP)) {
            mediaControllerCompat.getTransportControls().stop();
        }
    }


    private void handleAlarmFireAction(Intent intent) {
        if (PrefUtils.getAlarmTime().minute == Calendar.getInstance().get(Calendar.MINUTE)) {
            PrefUtils.setStation(PrefUtils.getAlarmStation(this));
            player.seekToDefaultPosition(PrefUtils.getStation().id);
            mediaControllerCompat.getTransportControls().play();
            EventBus.getDefault().postSticky(new ChangeStationAlarm());
            getSongTitle(PrefUtils.getStation().id);
        }
        AlarmUtil.startAlarmBroadcast(this, intent.getBooleanExtra(REPEAT, false), intent.getBooleanExtra(REPEAT, false));
        if (!intent.getBooleanExtra(REPEAT, false))
            EventBus.getDefault().post(new TurnOffAlarmEvent());
    }


    @Override
    public void onCreate() {
        super.onCreate();
        registerHeadphoneReceiver();
        if (mediaSession == null) {
            initMediaSession();
        }
        mWifiLock = ((WifiManager) getSystemService(Context.WIFI_SERVICE))
                .createWifiLock(WifiManager.WIFI_MODE_FULL, "eben_radio_lock");
        coverBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.eben_radio_logo_squere);
        subscription = new CompositeSubscription();
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        EventBus.getDefault().register(this);
        initExoPlayer(false);
    }


    private void setUpEqualizer(int sessionId) {
        if (mEqualizer != null) {
            mEqualizer.release();
        }
//            mEqualizer.setEnabled(false);

        mEqualizer = new Equalizer(0, sessionId);
        EventBus.getDefault().postSticky(new EqualizerSetupEvent(EqualizerUtil.restoreEqualizer(mEqualizer)));
        EventBus.getDefault().postSticky(new AudioSessionEvent(sessionId));

    }

    @Subscribe
    public void onTurnOffMusic(TurnOffMusicEvent event) {
        mediaControllerCompat.getTransportControls().stop();
    }

    private void releasePlayer() {
        RadioPlayerState.setStateStop();
        player.setPlayWhenReady(false);
        player.removeListener(this);
        player.release();
        if (mWifiLock.isHeld()) {
            mWifiLock.release();
        }
    }


    @Override
    public void onDestroy() {
        releasePlayer();
        unsubscribeSubscription();
        EventBus.getDefault().unregister(this);
        unregisterHeadphoneReceiver();
        super.onDestroy();
    }


    private void stopPlayer() {
        RadioPlayerState.setStateStop();
        abandonAudioFocus();
        player.setPlayWhenReady(false);
        player.stop();
        stopForeground(true);
        if (mWifiLock.isHeld()) {
            mWifiLock.release();
        }

    }






    private void initExoPlayer(boolean play) {
        RadioPlayerState.setStateStop();
        TrackSelection.Factory trackSelection =
                new FixedTrackSelection.Factory();
        TrackSelector trackSelector =
                new DefaultTrackSelector(trackSelection);
        MediaCodecAudioRenderer mediaCodecAudioRenderer = new MediaCodecAudioRenderer(MediaCodecSelector.DEFAULT) {
            @Override
            protected void onAudioSessionId(int audioSessionId) {
                super.onAudioSessionId(audioSessionId);
                audioSession = audioSessionId;
                AudioManagerUtil.getInstance().setSessionId(audioSessionId);
                setUpEqualizer(audioSession);
            }
        };
        player = ExoPlayerFactory.newInstance(new Renderer[]{mediaCodecAudioRenderer}, trackSelector, new DefaultLoadControl());
        player.addListener(this);
        preparePlayer(play);
    }

    private void preparePlayer(boolean play) {
        player.prepare(initRadioMediaSource(PrefUtils.getStation().id));
//        player.seekToDefaultPosition(PrefUtils.getStation().id);
        if (play)
            mediaControllerCompat.getTransportControls().play();
        getSongTitle(PrefUtils.getStation().id);
    }


    private class AudioExtractorsFactory implements ExtractorsFactory {

        @Override
        public Extractor[] createExtractors() {
            return new Extractor[]{
                    new OggExtractor(),
                    new WavExtractor(),
                    new Mp3Extractor(),
                    new Mp4Extractor(),
                    new Ac3Extractor(),
                    new AdtsExtractor(),
                    new TsExtractor()};
        }
    }


    private MediaSource initRadioMediaSource(int id) {
        OkHttpDataSourceFactory dataSourceFactory = new OkHttpDataSourceFactory(new OkHttpClient(), Util.getUserAgent(this, getString(R.string.app_name)), null, null);
//        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();
//        MediaSource[] sources = new MediaSource[getResources().getStringArray(R.array.station_titles).length];

        return new ExtractorMediaSource(Uri.parse(RadioStationUtil.getStationById(id)),
                    dataSourceFactory, new AudioExtractorsFactory(), null, null);

    }

    @Override
    public void onLoadingChanged(boolean isLoading) {
    }

    @Override
    public void onPlayerStateChanged(final boolean playWhenReady, int playbackState) {
        switch (playbackState) {
            case ExoPlayer.STATE_READY:
                EventBus.getDefault().postSticky(new AudioSessionEvent(audioSession));
                EventBus.getDefault().postSticky(new TogglePlayEvent(playWhenReady));
                break;
//            case ExoPlayer.STATE_BUFFERING:
//                RadioPlayerState.setStatePause();
//                EventBus.getDefault().postSticky(new TogglePlayEvent(true));
//                break;
            case ExoPlayer.STATE_IDLE:
                EventBus.getDefault().postSticky(new TogglePlayEvent(false));
                EventBus.getDefault().postSticky(new AudioSessionEvent(audioSession));
                break;
        }
    }

    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest) {
    }

    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

    }

    @Override
    public void onPlayerError(ExoPlaybackException error) {
        try {
            if (error.getSourceException() != null) {
                error.getSourceException().printStackTrace();
                stopPlayer();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPositionDiscontinuity() {

    }


    private boolean requestAudioFocus() {
        int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN);
        return result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED;
    }


    private void abandonAudioFocus() {
        int result = audioManager.abandonAudioFocus(this);
    }


    private void unsubscribeSubscription() {
        if (!subscription.isUnsubscribed()) {
            subscription.unsubscribe();
            subscription = new CompositeSubscription();
        }
    }

    @Subscribe(sticky = true)
    public void onApplySkinEvent(ApplySkinEvent event) {
        updateInfo = true;
        getSongTitle(PrefUtils.getStation().id);
        EventBus.getDefault().removeStickyEvent(event);
    }

    @Override
    public void onAudioFocusChange(int focusChange) {
        Intent intent = new Intent(this, RadioService.class);
        switch (focusChange) {

            case AudioManager.AUDIOFOCUS_GAIN:
                // resume playback
                //changePlayerVolume(1f);
                if (RadioPlayerState.getState() == RadioPlayerState.STATE_FOCUS_PAUSE)
                    mediaControllerCompat.getTransportControls().play();
                break;

            case AudioManager.AUDIOFOCUS_LOSS:
                // Lost focus for an unbounded amount of time: stop playback and release media player
                if (RadioPlayerState.getState() == RadioPlayerState.STATE_PLAY) {
                    RadioPlayerState.setStateFocusPause();
                    mediaControllerCompat.getTransportControls().pause();
                }
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                // Lost focus for a short time, but we have to stop
                // playback. We don't release the media player because playback
                // is likely to resume
                if (RadioPlayerState.getState() == RadioPlayerState.STATE_PLAY) {
                    RadioPlayerState.setStateFocusPause();
                    mediaControllerCompat.getTransportControls().pause();
                }
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                // Lost focus for a short time, but it's ok to keep playing
                // at an attenuated level
//                if (player.getPlayWhenReady()) {
////                    changePlayerVolume(0.1f);
//                }

                break;

        }
    }


    @Subscribe
    public void onChangeStation(ChangeStationEvent event) {
        mediaControllerCompat.getTransportControls().play();
        getSongTitle(PrefUtils.getStation().id);
    }


    private void getSongTitle(int stationId) {
        unsubscribeSubscription();
        subscription.add(EbenRadioRetroWorker.getInstance().getSongTitle(RadioStationUtil.getUrlTitleSongById(stationId))
                .repeatWhen(new Func1<Observable<? extends Void>, Observable<?>>() {
                    @Override
                    public Observable<?> call(Observable<? extends Void> completed) {
                        return completed.delay(20, TimeUnit.SECONDS);
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(new Action1<String>() {
                    @Override
                    public void call(String responseBody) {
                        unsubscribeSubscription();

                        getSongCover(Html.fromHtml(responseBody).toString(), updateInfo);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                    }
                }));
    }



    private void getSongCover(final String title, final boolean update) {
        subscription.add(EbenRadioRetroWorker.getInstance().getSongCoverImage(String.format(getString(R.string.url_cover), title))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<SongStreamResponse>() {
                    @Override
                    public void call(SongStreamResponse responseBody) {
                        updateInfo = false;
                        songCover = responseBody.results.isEmpty() ? "" : responseBody.results.get(0).artworkUrl100.replace("100x100", "300x300");
                        titleSong = title;
                        Glide.with(RadioService.this).load(songCover)
                                .asBitmap()
                                .into(new SimpleTarget<Bitmap>() {
                                    @Override
                                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                                        coverBitmap = resource;
                                        if (currentAction != null)
                                            buildNotification(currentAction);
                                        updateMediaSessionMetaData();
                                    }

                                    @Override
                                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                        super.onLoadFailed(e, errorDrawable);
                                        loadPlaceholder();
                                        Glide.clear(this);
                                    }
                                });

                        EventBus.getDefault().postSticky(new UpdatePlayerInfoEvent(title, songCover, update));
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                    }
                }));
    }

    private void loadPlaceholder(){
        Glide.with(this).load(BackgroundUtil.getRadioPlaceholder(RadioService.this))
                .asBitmap()
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                        coverBitmap = resource;
                        if (currentAction != null)
                            buildNotification(currentAction);
                        updateMediaSessionMetaData();
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                        Glide.clear(this);
                    }
                });
    }

    private void updateMediaSessionMetaData() {
        mediaSession.setMetadata(new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, getResources().getStringArray(R.array.station_titles)[PrefUtils.getStation().id])
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, titleSong)
                .putBitmap(MediaMetadataCompat.METADATA_KEY_ART, coverBitmap)
                .build());


    }


    private void registerHeadphoneReceiver() {
        headsetPlugReceiver = new HeadsetPlugReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.HEADSET_PLUG");
        registerReceiver(headsetPlugReceiver, intentFilter);
    }

    private void unregisterHeadphoneReceiver() {
        if (headsetPlugReceiver != null) {
            unregisterReceiver(headsetPlugReceiver);
            headsetPlugReceiver = null;
        }
    }

}
