package com.aws3.ebenradio.service;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;

import android.support.v4.app.NotificationCompat;
import android.support.v4.app.RemoteInput;
import android.util.Log;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.chat.JoinEmit;
import com.aws3.ebenradio.model.chat.NewMessageChat;
import com.aws3.ebenradio.model.event.chat.NewMessageEvent;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.aws3.ebenradio.util.keys.EbenRetroKeys;
import com.aws3.ebenradio.util.skins.BackgroundUtil;
import com.aws3.ebenradio.util.skins.ColorUtil;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.view.activity.ChatActivity_;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.net.URISyntaxException;
import java.util.List;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

/**
 * Created by Dell on 01.11.2016.
 */
public class ChatService extends Service implements EbenKeys, EbenRetroKeys {


    public static final String CHAT_URL = "http://ebenradio.com:8081";

    public static boolean isChatAcitivityRunning = false;

    public static int idChat;

    private Socket mSocket;
    {
        try {
            mSocket = IO.socket(CHAT_URL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }



    public void connect(){
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(NEW_MESSAGE, onNewMessage);
        mSocket.connect();
    }


    public void disconnect(){
        mSocket.disconnect();
        mSocket.close();
        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(NEW_MESSAGE, onNewMessage);
    }

//    private void sendJoinEvent(){
//        String JOIN_EMIT =  new Gson().toJson(new JoinEmit(GeneralUtil.getDeviceId(this)));
//        Log.d("CHAT", JOIN_EMIT);
//        mSocket.emit(JOIN,JOIN_EMIT);
//    }





    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            try {
                sendJoinEvent();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };



//    private Emitter.Listener onSongTitle = new Emitter.Listener() {
//        @Override
//        public void call(Object... args) {
//            Log.d(getClass().getName(),"Connected");
//            try {
//                sendJoinEvent();
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//    };


    private void sendJoinEvent() throws JSONException {
        JSONObject object = new JSONObject();
        object.put("deviceid", GeneralUtil.getDeviceId(this));
//        object.put("with_user_id", "29706");
//        object.put("cookie", PrefUtils.getCookies());
        mSocket.emit("join", object);
//        Log.d("ChatActvity", "join sent: " + object.toString());
    }


    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            NewMessageChat messageChat = new Gson().fromJson(args[0].toString(),new TypeToken<NewMessageChat>(){}.getType());
            EventBus.getDefault().postSticky(new NewMessageEvent(messageChat));;
        }
    };



    private void sendNotification(NewMessageChat messageChat){

//        if(ChatService.idChat !=messageChat.from_user_id){
            Intent intent;
        intent = new Intent(this, ChatActivity_.class);
        intent.putExtra(ID, String.valueOf(messageChat.from_user_id));
        intent.putExtra(NAME, messageChat.text.substring(0, messageChat.text.indexOf(":")));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,1, intent,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

//            RemoteInput remoteInput = new RemoteInput.Builder("key_text_reply")
//                    .setLabel(getString(R.string.type_message))
//                    .build();

//            NotificationCompat.Action action =
//                    new NotificationCompat.Action.Builder(R.drawable.send, getString(R.string.type_message), pendingIntent)
//                            .addRemoteInput(remoteInput)
//                            .setAllowGeneratedReplies(true)
//                            .build();
            NotificationCompat.Builder notificationBuilder;
            notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_stat_notify_letter)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("New message")
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setColor(ColorUtil.getColorById(SkinPrefUtils.getSkin().color, this))
                    .setVibrate(new long[]{100, 500, 300, 500})
                    .setLights(ColorUtil.getColorById(SkinPrefUtils.getSkin().color, this), 500, 200)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_header_notify))
                    .setContentIntent(pendingIntent)
//                    .addAction(action)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setStyle(new NotificationCompat.MessagingStyle(getString(R.string.type_message))
                            .addMessage(messageChat.text, System.currentTimeMillis(), "New message"))
            ;
        NotificationManager notificationManager =
                (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = notificationBuilder.build();
       // context.getSharedPreferences(SHARED_PREF,Context.MODE_PRIVATE).edit().putInt(NOTIFCOUNT,(context.getSharedPreferences(SHARED_PREF,Context.MODE_PRIVATE).getInt(NOTIFCOUNT,0)+1)).apply();
        notificationManager.notify(messageChat.from_user_id, notification);
//        }
    }



    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        connect();
    }


    @Override
    public void onDestroy() {
        disconnect();
        super.onDestroy();

    }




}
