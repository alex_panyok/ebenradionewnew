package com.aws3.ebenradio.app;

import android.content.Context;
import android.content.Intent;
import android.support.multidex.MultiDexApplication;

import com.aws3.ebenradio.gcm.RegistrationIntentService;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.radio.AudioManagerUtil;
import com.aws3.ebenradio.util.radio.RadioStationUtil;
import com.aws3.ebenradio.util.skins.SkinPrefUtils;
import com.aws3.ebenradio.util.skins.SkinUtils;
import com.aws3.ebenradio.util.skins.TypeFaceUtil;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Dell on 11.10.2016.
 */
public class EbenRadioApplication extends MultiDexApplication {


    private static final String TWITTER_KEY = "Rt7HxhqiKlxrWpQDDvarTuqOa";
    private static final String TWITTER_SECRET = "eD8dGptZS12Yn4Ze0YC1w4iTQU9eoI5FMSxRxFoPaTE4u0vgpB";

    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig), new Crashlytics());
        FacebookSdk.sdkInitialize(this);
        PrefUtils.initialize(this);
        RadioStationUtil.getRadioStationItems(this);
        SkinPrefUtils.initialize(this);
        SkinUtils.initDefaultSkins(this);
        TypeFaceUtil.initTypefaces(this);
        refreshToken(this);
    }


    public static void refreshToken(Context context) {
        Intent intent = new Intent(context, RegistrationIntentService.class);
        context.startService(intent);
    }




}
