package com.aws3.ebenradio.util.alarm;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;

import com.aws3.ebenradio.receiver.AlarmBroadcast;
import com.aws3.ebenradio.util.keys.EbenKeys;
import com.aws3.ebenradio.util.prefs.PrefUtils;

/**
 * Created by Alex on 22.09.2016.
 */
public class AlarmUtil implements EbenKeys {

    private static void enableReceiver(Context context){
        ComponentName receiver = new ComponentName(context, AlarmBroadcast.class);
        PackageManager pm = context.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }
//


    public static void startAlarmBroadcast(Context context, boolean isRepeat, boolean status){
        enableReceiver(context);
        Intent intent = new Intent(context, AlarmBroadcast.class);
        intent.putExtra(REPEAT, isRepeat);
        intent.setAction(status?AlarmBroadcast.ACTION_ON:AlarmBroadcast.ACTION_OFF);
        PrefUtils.setAlarmStatus(status);
        context.sendBroadcast(intent);
    }
}
