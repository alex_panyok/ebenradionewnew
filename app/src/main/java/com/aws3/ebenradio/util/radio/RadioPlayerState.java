package com.aws3.ebenradio.util.radio;

/**
 * Created by Dell on 14.10.2016.
 */
public class RadioPlayerState {

    public static final int STATE_PLAY = 1;

    public static final int STATE_PAUSE = 2;

    public static final int STATE_FOCUS_PAUSE = 4;

    public static final int STATE_STOP = 3;

    private static int currentState;


    public static int getState(){
        return currentState;
    }

    public static void setStatePlay(){
        currentState = STATE_PLAY;
    }

    public static void setStatePause(){
        if(currentState!=STATE_FOCUS_PAUSE)
            currentState = STATE_PAUSE;
    }

    public static void setStateFocusPause(){
            currentState = STATE_FOCUS_PAUSE;
    }

    public static void setStateStop(){
        currentState = STATE_STOP;
    }

}
