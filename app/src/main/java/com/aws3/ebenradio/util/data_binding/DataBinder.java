package com.aws3.ebenradio.util.data_binding;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.databinding.BindingAdapter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.alarm.WeekDay;
import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.model.user.User;
import com.aws3.ebenradio.model.user.UserFromSearch;
import com.aws3.ebenradio.model.user.UserPublicProfile;
import com.aws3.ebenradio.util.general.GeneralUtil;
import com.aws3.ebenradio.util.skins.BackgroundUtil;
import com.aws3.ebenradio.util.skins.BlurUtil;
import com.aws3.ebenradio.util.skins.GraphicEQUtil;
import com.aws3.ebenradio.util.skins.ShapeUtil;
import com.aws3.ebenradio.util.skins.TypeFaceUtil;
import com.aws3.ebenradio.util.user.UserDataPref;
import com.aws3.ebenradio.view.custom_views.EbenRadioTextView;
import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.makeramen.roundedimageview.RoundedImageView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import jp.wasabeef.glide.transformations.BlurTransformation;


public class DataBinder {

    @BindingAdapter("ebenRadioBackground")
    public static void ebenRadioBackground(ImageView imageView, Object path) {
        Context context = imageView.getContext();
        path = path instanceof Integer? BackgroundUtil.getBackground((Integer)path, imageView.getContext()):path;
        Glide.with(context).load(path).into(imageView);
    }


    @BindingAdapter("ebenRadioPreview")
    public static void ebenRadioPreview(ImageView imageView, Object path) {
        Context context = imageView.getContext();
        path = path instanceof Integer? BackgroundUtil.getPreview((Integer)path, imageView.getContext()):path;
        Glide.with(context).load(path).into(imageView);
    }


    @BindingAdapter("ebenRadioBackgroundAttr")
    public static void ebenRadioBackgroundAttr(ImageView imageView, Integer path) {
        Context context = imageView.getContext();
        Glide.with(context).load(path).into(imageView);
    }


    @BindingAdapter("ebenRadioCreateSkinLoadArt")
    public static void ebenRadioCreateSkinLoadArt(ImageView imageView, int shapeId) {
        Context context = imageView.getContext();
        Glide.with(context).load(R.drawable.cover_0).bitmapTransform(ShapeUtil.shapeById(context,shapeId)).into(imageView);
    }

    @BindingAdapter("ebenRadioLoadDrawable")
    public static void ebenRadioLoadDrawable(ImageView imageView, Drawable image) {
        Context context = imageView.getContext();
        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imageView);
        Glide.with(context).load(R.drawable.gif_eben).into(imageViewTarget);
    }


    @BindingAdapter({"ebenRadioImage","shapeId"})
    public static void ebenRadioImage(ImageView imageView,String url, int shapeId) {
        Context context = imageView.getContext();
        DrawableRequestBuilder<String> requestBuilder = Glide.with(context).load(url);
        if (shapeId == ShapeUtil.CIRCLE) {
            requestBuilder.placeholder(R.drawable.no_photo_bg).error(R.drawable.no_photo_bg);
        }
        requestBuilder.bitmapTransform(ShapeUtil.shapeById(context, shapeId)).into(imageView);
    }


    @BindingAdapter("ebenRadioImageNoShape")
    public static void ebenRadioImageNoShape(ImageView imageView,String url) {
        Context context = imageView.getContext();
        Glide.with(context).load(url).into(imageView);
    }

    @BindingAdapter("ebenRadioImageEvent")
    public static void ebenRadioImageEvent(ImageView imageView, String url) {
        Context context = imageView.getContext();
        Glide.with(context).load("http://api.ebenradio.com//files/event/image/"+url).into(imageView);
    }

    @BindingAdapter("ebenRadioPreviewEvent")
    public static void ebenRadioPreviewEvent(ImageView imageView, String url) {
        Context context = imageView.getContext();
        Glide.with(context).load("http://api.ebenradio.com//files/event/preview/"+url).into(imageView);
    }

    @BindingAdapter("ebenRadioImageAd")
    public static void ebenRadioImageAd(ImageView imageView,String url) {
        Context context = imageView.getContext();
        Glide.with(context).load(url).into(imageView);
    }


    @BindingAdapter("ebenRadioBlurCreateSkin")
    public static void ebenRadioBlurCreateSkin(ImageView imageView, int blurId) {
        Context context = imageView.getContext();
        if(blurId == BlurUtil.BLUR) {
            imageView.setVisibility(View.VISIBLE);
            Glide.with(context).load(R.drawable.cover_0).bitmapTransform(new BlurTransformation(imageView.getContext())).into(imageView);
        }else {
            imageView.setVisibility(View.GONE);
        }
    }


    @BindingAdapter("setInfoUserText")
    public static void setInfoUserText(TextView textView, UserPublicProfile person){
        Context context = textView.getContext();
        if(person!=null)
        textView.setText(person.age!=0 && !person.country.isEmpty()?String.format(context.getString(R.string.info_user),person.country,person.age):person.age==0 && person.country.isEmpty()?"":!person.country.isEmpty()?person.country:String.format(context.getString(R.string.years), person.age));
    }


    @BindingAdapter("imageRoundedSkin")
    public static void  imageRoundedSkin(final RoundedImageView imageView, Skin skin){
        switch (skin.shapeId){
            case ShapeUtil.CIRCLE:
                imageView.setOval(true);
                imageView.setCornerRadius(300/2);
                break;

            case ShapeUtil.RECTANGLE:
                imageView.setOval(false);
                imageView.setCornerRadius(0);
                break;

            case ShapeUtil.ROUNDED:
                imageView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        imageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        imageView.setOval(false);
                        imageView.setCornerRadius(imageView.getMeasuredHeight()/6);
                    }
                });

                break;
        }
    }


    @SuppressLint("SimpleDateFormat")
    @BindingAdapter("setInfoUserMeText")
    public static void setInfoUserMeText(TextView textView, User person){
        Context context = textView.getContext();
        if(person!=null) {
            Calendar calendar = Calendar.getInstance();
            try {
                calendar.setTime(new Date(new SimpleDateFormat("MM/dd/yyyy").parse(person.birthday_year).getTime()));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String age = person.birthday_year.length()>4? GeneralUtil.getAge(calendar): person.birthday_year.length()>0?String.valueOf(Calendar.getInstance().get(Calendar.YEAR) - Integer.parseInt(person.birthday_year)):"";
            age = age.equals("")?"":Integer.parseInt(age)>0?age:"";
            textView.setText(!age.equals("") && !person.country.isEmpty() ? String.format(context.getString(R.string.info_user), person.country, age) : age.equals("") && person.country.isEmpty() ? "" : !person.country.isEmpty() ? person.country : String.format(context.getString(R.string.years), age));
        }
    }


    @BindingAdapter("weekDayName")
    public static void weekDayName(TextView textView, WeekDay day) {
        textView.setText(Arrays.asList(textView.getResources().getStringArray(R.array.week_days)).get(day.id));
    }


    @BindingAdapter("ebenSrc")
    public static void ebenSrc(ImageView imageView, int path) {
        Glide.with(imageView.getContext()).load(path).dontAnimate().into(imageView);
    }

    @BindingAdapter("ebenSrcCreateSkinEq")
    public static void ebenSrcCreateSkinEq(ImageView imageView, int id) {
        TypedArray resIds = imageView.getContext().getResources().obtainTypedArray(R.array.graphic_eq_screen_ids);
        switch (id){
            case GraphicEQUtil.CLASSIC:
                imageView.setImageResource(resIds.getResourceId(Arrays.asList(GraphicEQUtil.eqIds).indexOf(GraphicEQUtil.CLASSIC),0));
                break;
            case GraphicEQUtil.WAVES:
                imageView.setImageResource(resIds.getResourceId(Arrays.asList(GraphicEQUtil.eqIds).indexOf(GraphicEQUtil.WAVES),0));
                break;
        }
        resIds.recycle();
    }

    @BindingAdapter("setTypefaceCreateSkin")
    public static void setTypefaceCreateSkin(EbenRadioTextView textView, int typefaceId) {

        textView.setTypeface(textView.getFont()==0? TypeFaceUtil.getTypefaceById(typefaceId)[0]: TypeFaceUtil.getTypefaceById(typefaceId)[1]);
    }

    @BindingAdapter("setTypeface")
    public static void setTypeface(EbenRadioTextView textView, Typeface[] typeface) {
        textView.setTypeface(textView.getFont()==0? typeface[0]: typeface[1]);
    }

    @BindingAdapter("setTypefaceDefault")
    public static void setTypefaceDefault(TextView textView, int typefaceId) {
        textView.setTypeface(TypeFaceUtil.getTypefaceById(typefaceId)[0]);
    }



    @BindingAdapter("visible")
    public static void visible(View view, boolean isVisible) {
       view.setVisibility(isVisible?View.VISIBLE:View.GONE);
    }


    @SuppressLint("SimpleDateFormat")
    @BindingAdapter("formatDate")
    public static void formatDate(TextView textView, Date date){
        textView.setText(new SimpleDateFormat("dd MMM yyyy").format(date));
    }


    @SuppressLint("SimpleDateFormat")
    @BindingAdapter("formatTime")
    public static void formatTime(TextView textView, String date){
        try {
            Date date1 = new Date(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date).getTime()+ TimeZone.getDefault().getRawOffset());
            textView.setText(new SimpleDateFormat("HH:mm").format(date1));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("SimpleDateFormat")
    @BindingAdapter("formatTimeEvent")
    public static void formatTimeEvent(TextView textView, String date){
        try {
            Date date1 = new Date(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(date).getTime()+ TimeZone.getDefault().getRawOffset());
            textView.setText(new SimpleDateFormat("EEE MMM dd/MM, hh:mm a").format(date1));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }




    @SuppressLint("SimpleDateFormat")
    @BindingAdapter("formatTimeNews")
    public static void formatTimeNews(TextView textView, String date){
        try {
            textView.setText(new SimpleDateFormat("dd MMM yyyy").format(new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss", Locale.ENGLISH).parse(date.substring(0,date.indexOf("+")-1))));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }






    @BindingAdapter("distance")
    public static void distance(TextView textView, UserFromSearch user){
        Location myLocation = new Location("");
        Location userLocation = new Location("");
        myLocation.setLatitude(UserDataPref.getUserFromPrefs(textView.getContext()).lat);
        myLocation.setLongitude(UserDataPref.getUserFromPrefs(textView.getContext()).lng);
        userLocation.setLatitude(user.lat);
        userLocation.setLatitude(user.lng);
            textView.setText(String.valueOf(myLocation.distanceTo(userLocation)));

    }










}
