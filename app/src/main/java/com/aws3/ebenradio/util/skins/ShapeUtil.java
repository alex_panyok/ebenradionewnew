package com.aws3.ebenradio.util.skins;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;


import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.create_skin.attrs.Shape;
import com.aws3.ebenradio.model.skin.Skin;
import com.bumptech.glide.load.Transformation;


import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.CropCircleTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;


public class ShapeUtil {

    public static final int RECTANGLE = 1;

    public static final int ROUNDED = 2;

    public static final int CIRCLE = 3;

    public static Integer[] shapeIds = {RECTANGLE,ROUNDED,CIRCLE};

    public static int selectedItem = -1;

    public static Transformation<Bitmap> shapeById(Context context, int shape){
        switch (shape){
            case RECTANGLE:
                return new RoundedCornersTransformation(context,0,0);
            case ROUNDED:
                return new RoundedCornersTransformation(context,50,0);
            case CIRCLE:
                return new CropCircleTransformation(context);
        }
        return new RoundedCornersTransformation(context,0,0);
    }



    public static com.squareup.picasso.Transformation shapePicassoById(Context context, int shape){
        switch (shape){
            case RECTANGLE:
                return new jp.wasabeef.picasso.transformations.RoundedCornersTransformation(0,0, jp.wasabeef.picasso.transformations.RoundedCornersTransformation.CornerType.ALL);
            case ROUNDED:
                return new jp.wasabeef.picasso.transformations.RoundedCornersTransformation(50,0, jp.wasabeef.picasso.transformations.RoundedCornersTransformation.CornerType.ALL);
            case CIRCLE:
                return new jp.wasabeef.picasso.transformations.CropCircleTransformation();
        }
        return new jp.wasabeef.picasso.transformations.RoundedCornersTransformation(0,0, jp.wasabeef.picasso.transformations.RoundedCornersTransformation.CornerType.ALL);
    }

    public static List<Shape> generateList(Context context, Skin skin){
        selectedItem = -1;
        List<Shape> list = new ArrayList<>();
        TypedArray resIds = context.getResources().obtainTypedArray(R.array.shape_ids);
        for(int i = 0; i<resIds.length();i++){
//            list.add(new Color(i,resIds.getResourceId(i,0),context.getResources().getIntArray(R.array.colors_skin)[i],i==isSelected));
            if(selectedItem==-1)
                selectedItem = skin.shapeId==shapeIds[i]?i:-1;
            list.add(new Shape(i,shapeIds[i],resIds.getResourceId(i,0)));
        }
        resIds.recycle();
        return list;
    }

}
