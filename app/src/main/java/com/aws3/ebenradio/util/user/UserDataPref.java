package com.aws3.ebenradio.util.user;

import android.content.Context;
import android.preference.PreferenceManager;

import com.aws3.ebenradio.model.user.User;


public class UserDataPref {

    public static void setAuthorized(Context context) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putBoolean(Constants.AUTHORIZED, true).apply();
    }

    public static void setNotAuthorized(Context context) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putBoolean(Constants.AUTHORIZED, false).apply();
//        PreferenceManager.getDefaultSharedPreferences(context).edit().clear().apply();
    }



    public static boolean isAuthorized(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(Constants.AUTHORIZED, false);
    }

    public static boolean isShownIntro(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(Constants.INTRO, false);
    }


    public static void setShownIntro(Context context){
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putBoolean(Constants.INTRO, true).apply();
    }


    public static void saveUserData(Context context, User user) {


        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putString(Constants.UserData.USER_EMAIL, user.email).apply();

        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putString(Constants.UserData.USER_NAME, user.full_name).apply();

        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putString(Constants.UserData.USER_COUNTRY, user.country).apply();

        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putString(Constants.UserData.USER_BIRTHDAY, user.birthday_year).apply();

        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putInt(Constants.UserData.USER_GENDER, user.gender).apply();

        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putString(Constants.UserData.USER_ABOUT_YOU, user.about_you).apply();

        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putString(Constants.UserData.USER_PHOTO, user.photo).apply();

        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putString(Constants.UserData.USER_SOCIAL_ID, user.social_id).apply();

        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putFloat(Constants.UserData.USER_LAT, user.lat).apply();

        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putFloat(Constants.UserData.USER_LNG, user.lng).apply();

        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putBoolean(Constants.UserData.USER_IS_FACEBOOK, user.isFacebook).apply();
    }




    public static User getUserFromPrefs(Context context) {
        User user = new User();



        user.email = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(Constants.UserData.USER_EMAIL, "");

        user.full_name = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(Constants.UserData.USER_NAME, "");

        user.country = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(Constants.UserData.USER_COUNTRY, "");

        user.birthday_year = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(Constants.UserData.USER_BIRTHDAY, "0");

        user.photo = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(Constants.UserData.USER_PHOTO, "");

        user.gender = PreferenceManager.getDefaultSharedPreferences(context)
                .getInt(Constants.UserData.USER_GENDER, 1);

        user.social_id = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(Constants.UserData.USER_SOCIAL_ID, "");

        user.lat = PreferenceManager.getDefaultSharedPreferences(context)
                .getFloat(Constants.UserData.USER_LAT, 0f);


        user.lng = PreferenceManager.getDefaultSharedPreferences(context)
                .getFloat(Constants.UserData.USER_LNG, 0f);

        user.about_you = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(Constants.UserData.USER_ABOUT_YOU, "");


        user.isFacebook = PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(Constants.UserData.USER_IS_FACEBOOK, false);



        return user;
    }

    public interface Constants {
        String AUTHORIZED = "authorized";
        String INTRO = "intro";
        interface UserData {

            String USER_IS_FACEBOOK = "is_facebook";
            String USER_NAME = "user_name";
            String USER_EMAIL = "user_email";
            String USER_COUNTRY  = "user_country";
            String USER_SOCIAL_ID = "user_social_id";
            String USER_PHOTO = "user_photo";
            String USER_ABOUT_YOU  = "user_about_you";
            String USER_BIRTHDAY = "user_birthday";
            String USER_GENDER = "user_gender";
            String USER_LAT = "user_lat";
            String USER_LNG = "user_lng";

        }

    }

}
