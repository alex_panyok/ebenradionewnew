package com.aws3.ebenradio.util.skins;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.Log;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.create_skin.attrs.Navigation;
import com.aws3.ebenradio.model.skin.PlayerControls;
import com.aws3.ebenradio.model.skin.Skin;

import java.util.ArrayList;
import java.util.List;



public class NavigationUtil {

    public static int selectedItem = -1;

    public static List<Navigation> generateList(Context context, Skin skin){
        selectedItem = -1;
        List<Navigation> list = new ArrayList<>();
        TypedArray resIds = context.getResources().obtainTypedArray(R.array.navigation_ids);
        for(int i = 0; i<resIds.length();i++){
            PlayerControls playerControls = new PlayerControls(i,i,i,i);
            if(selectedItem==-1)
                selectedItem = skin.controls.equals(playerControls)?i:-1;
            list.add(new Navigation(i,resIds.getResourceId(i,0),playerControls));
        }
        resIds.recycle();

        return list;
    }



    public static PlayerControls getControls(int id, Context context){
        TypedArray prevIds = context.getResources().obtainTypedArray(R.array.navigation_prev_ids);
        TypedArray nextIds = context.getResources().obtainTypedArray(R.array.navigation_next_ids);
        TypedArray playIds = context.getResources().obtainTypedArray(R.array.navigation_play_ids);
        TypedArray pauseIds = context.getResources().obtainTypedArray(R.array.navigation_pause_ids);
        PlayerControls playerControls = new PlayerControls(prevIds.getResourceId(id,0),nextIds.getResourceId(id,0),playIds.getResourceId(id,0),pauseIds.getResourceId(id,0));
        prevIds.recycle();
        nextIds.recycle();
        playIds.recycle();
        pauseIds.recycle();
        return playerControls;
    }
}
