package com.aws3.ebenradio.util.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.CursorIndexOutOfBoundsException;
import android.preference.PreferenceManager;
import android.util.Log;


import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.alarm.TimeAlarm;
import com.aws3.ebenradio.model.alarm.WeekDay;
import com.aws3.ebenradio.model.radio.RadioStation;
import com.aws3.ebenradio.util.keys.PrefKeys;
import com.aws3.ebenradio.util.radio.RadioStationUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Dell on 13.09.2016.
 */
public class PrefUtils implements PrefKeys {


    private static PrefUtils sInstance;
    private Context mContext;

    private SharedPreferences mPreferences;

    private PrefUtils(Context context) {
        mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mContext = context;
    }

    public static void initialize(Context context) {
        if (sInstance != null) {
            throw new IllegalStateException("");
        }
        sInstance = new PrefUtils(context);
    }

    private static PrefUtils getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException("");
        }
        return sInstance;
    }

    private static SharedPreferences.Editor getEditor() {
        return getPrefs().edit();
    }

    private static SharedPreferences getPrefs() {
        return getInstance().mPreferences;
    }


    public static RadioStation getStation(){
        Type type = new TypeToken<RadioStation>(){}.getType();
        return new Gson().fromJson(getPrefs().getString(SELECTED_STATION,""),type);
    }

    public static void setStation(RadioStation station){
        getEditor().putString(SELECTED_STATION, new Gson().toJson(station)).apply();
    }


    public static void setSelectedPreset(int selectedPreset){
        getEditor().putInt(SELECTED_PRESET, selectedPreset).apply();
    }

    public static boolean getEqualizerEnabled(){
        return getPrefs().getBoolean(IS_ENABLED, false);
    }

    public static void setEqualizerEnabled(boolean enabled){
        getEditor().putBoolean(IS_ENABLED, enabled).apply();
    }

    public static int getSelectedPreset(){
       return getPrefs().getInt(SELECTED_PRESET,0);
    }



    public static void setCustomPreset(int[] levels){
        getEditor().putString(CUSTOM_PRESET,new Gson().toJson(levels)).apply();
    }


    public static int[] getCustomPreset(){
        Type type = new TypeToken<int[]>(){}.getType();
        return new Gson().fromJson(getPrefs().getString(CUSTOM_PRESET,new Gson().toJson(new int[]{1500,1500,1500,1500,1500})),type);
    }


    public static void setCurrentLevels(int[] levels){
        getEditor().putString(CURRENT_LEVELS,new Gson().toJson(levels)).apply();
    }

    public static int[] getCurrentLevels(){
        Type type = new TypeToken<int[]>(){}.getType();
        return new Gson().fromJson(getPrefs().getString(CURRENT_LEVELS,new Gson().toJson(getCustomPreset())),type);
    }

    public static int getTimer(){
        return getPrefs().getInt(SELECTED_TIME, 5);
    }

    public static void setTimer(int time){
        getEditor().putInt(SELECTED_TIME, time).apply();
    }

    public static boolean getTimerStatus(){
        return getPrefs().getBoolean(TIMER_STATUS, false);
    }

    public static void setTimerStatus(boolean status){
        getEditor().putBoolean(TIMER_STATUS, status).apply();
    }


    public static void setAlarmTime(TimeAlarm alarmTime){
        getEditor().putString(ALARM_TIME,new Gson().toJson(alarmTime)).apply();
    }

    public static TimeAlarm getAlarmTime(){
        Type type = new TypeToken<TimeAlarm>(){}.getType();
        return new Gson().fromJson(getPrefs().getString(ALARM_TIME, ""), type);
    }


    public static void setAlarmStatus(boolean status){
        getEditor().putBoolean(ALARM_STATUS, status).apply();
    }

    public static boolean getAlarmStatus(){
        return getPrefs().getBoolean(ALARM_STATUS, false);
    }


    public static List<WeekDay> getWeekDays(){
        Type type = new TypeToken<List<WeekDay>>(){}.getType();
        return new Gson().fromJson(getPrefs().getString(SELECTED_DAYS, new Gson().toJson(new ArrayList<WeekDay>())), type);
    }


    public static void setWeekDays(List<WeekDay> weekDays){
        getEditor().putString(SELECTED_DAYS, new Gson().toJson(weekDays)).apply();
    }


    public static void setRepeatAlarm(boolean isRepeat){
        getEditor().putBoolean(REPEAT, isRepeat).apply();
    }

    public static boolean  getRepeatAlarm(){
        return getPrefs().getBoolean(REPEAT, false);
    }

    public static void setAlarmStation(RadioStation radioStation){
        getEditor().putString(ALARM_STATION,new Gson().toJson(radioStation)).apply();
    }
    public static RadioStation getAlarmStation(Context context){
        Type type = new TypeToken<RadioStation>(){}.getType();
        return new Gson().fromJson(getPrefs().getString(ALARM_STATION, new Gson().toJson(RadioStationUtil.getRadioStationItems(context).get(0))), type);
    }


    public static String getCookies() {
        return getPrefs().getString(COOKIES, "");
    }

    public static void setCookies(String cookies) {
        getEditor().putString(COOKIES, cookies).apply();
    }


    public static boolean getIsRateShown() {
        return getPrefs().getBoolean(ISRATESHOWN, false);
    }

    public static void setIsRateShown(boolean isRateShown) {
        getEditor().putBoolean(ISRATESHOWN, isRateShown).apply();
    }

    public static void setRateTime(long time){
        getEditor().putLong(TIME, time).apply();
    }

    public static long getRateTime(){
        return getPrefs().getLong(TIME, 0);
    }

    public static int updateBannerCounter(){
        int count = getPrefs().getInt(BANNER_COUNT,0)+1;
        getPrefs().edit().putInt(BANNER_COUNT, count).apply();
        return count;
    }

    public  static  void releaseBannerCount(){
        getPrefs().edit().putInt(BANNER_COUNT, 0).apply();
    }

//
//
//    public static void setColors(int colorOriginal, int colorLighter, int colorDarker){
//         getEditor().putInt(SELECTED_COLOR, colorOriginal).apply();
//        getEditor().putInt(SELECTED_COLOR_LIGHTER, colorLighter).apply();
//
//        getEditor().putInt(SELECTED_COLOR_DARKER, colorDarker).apply();
//    }
//
//
//    public static int getColor(){
//        return getPrefs().getInt(SELECTED_COLOR, Color.parseColor(DEFAULT_COLOR));
//    }
//
//

//


//
//    public static void setTimerStatus(boolean status){
//        getEditor().putBoolean(TIMER_STATUS, status).apply();
//    }
//
//    public static void setAlarmTime(TimeAlarm alarmTime){
//        Log.i("TIME_ALARM", new Gson().toJson(alarmTime));
//        getEditor().putString(ALARM_TIME,new Gson().toJson(alarmTime)).apply();
//    }
//
//    public static void setAlarmStation(RadioStation radioStation){
//
//        getEditor().putString(ALARM_STATION,new Gson().toJson(radioStation)).apply();
//    }
//    public static RadioStation getAlarmStation(Context context){
//        Type type = new TypeToken<RadioStation>(){}.getType();
//        return new Gson().fromJson(getPrefs().getString(ALARM_STATION, new Gson().toJson(new RadioStation(0, RadioStationUtil.getStationIconById(0, false), RadioStationUtil.getStationIconById(0, true), context.getResources().getStringArray(R.array.station_titles)[0], RadioStationUtil.getStationById(0), true))), type);
//    }
//
//    public static void setNoNetworkShown(boolean status){
//        getEditor().putBoolean(NO_NWTWORK_SHOWN, status).apply();
//    }
//
//    public static boolean  getNoNetworkShown(){
//       return getPrefs().getBoolean(NO_NWTWORK_SHOWN, false);
//    }
//
//    public static TimeAlarm getAlarmTime(){
//        Type type = new TypeToken<TimeAlarm>(){}.getType();
//        Log.i("TIME_ALARM2",getPrefs().getString(ALARM_TIME, "123"));
//        return new Gson().fromJson(getPrefs().getString(ALARM_TIME, ""), type);
//    }
//
//
//    public static void setAlarmStatus(boolean status){
//        getEditor().putBoolean(ALARM_STATUS, status).apply();
//    }
//
//    public static boolean getAlarmStatus(){
//        return getPrefs().getBoolean(ALARM_STATUS, false);
//    }
//
//
//    public static boolean getTimerStatus(){
//        return getPrefs().getBoolean(TIMER_STATUS, false);
//    }
//

//
//    public static List<WeekDay> getWeekDays(){
//        Type type = new TypeToken<List<WeekDay>>(){}.getType();
//        return new Gson().fromJson(getPrefs().getString(SELECTED_DAYS, new Gson().toJson(new ArrayList<WeekDay>())), type);
//    }
//
//
//    public static void setWeekDays(List<WeekDay> weekDays){
//      getEditor().putString(SELECTED_DAYS, new Gson().toJson(weekDays)).apply();
//    }
//
//
//    public static int getStation(){
//        return getPrefs().getInt(SELECTED_STATION, 0);
//    }
//
//    public static int[] getColors(){
//        return new int[]{getPrefs().getInt(SELECTED_COLOR, Color.parseColor(DEFAULT_COLOR)), getPrefs().getInt(SELECTED_COLOR_LIGHTER, Color.parseColor(DEFAULT_COLOR)), getPrefs().getInt(SELECTED_COLOR_DARKER, Color.parseColor(DEFAULT_COLOR))};
//    }
//


}
