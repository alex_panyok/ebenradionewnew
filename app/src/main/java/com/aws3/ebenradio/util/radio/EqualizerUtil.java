package com.aws3.ebenradio.util.radio;

import android.media.audiofx.Equalizer;

import com.aws3.ebenradio.util.prefs.PrefUtils;

/**
 * Created by Dell on 20.10.2016.
 */
public class EqualizerUtil {



    public static Equalizer restoreEqualizer(Equalizer equalizer){
        for(int i=0;i<PrefUtils.getCurrentLevels().length;i++) {
            setEqualizerLevel(equalizer,i,PrefUtils.getCurrentLevels()[i]);
        }
          equalizer.setEnabled(PrefUtils.getEqualizerEnabled());
        return equalizer;
    }

    public static void saveEqualizerCurrentLevel(Equalizer equalizer){

        PrefUtils.setCurrentLevels(getCurrentLevels(equalizer));
    }

    public static void saveEqualizerCustomPreset(Equalizer equalizer){

        PrefUtils.setCustomPreset(getCurrentLevels(equalizer));
    }


    public static int[] getCurrentLevels(Equalizer equalizer){
        return new int[]{getLevel(equalizer,0),getLevel(equalizer,1),getLevel(equalizer,2),getLevel(equalizer,3),getLevel(equalizer,4)};
    }

    public static int getLowerEqualizerLevel(Equalizer equalizer){
        return equalizer.getBandLevelRange()[0];
    }

    public static int getUpperEqualizerLevel(Equalizer equalizer){
        return equalizer.getBandLevelRange()[1];
    }
    private static int getLevel(Equalizer equalizer,int band){
        return equalizer.getBandLevel((short) band)-getLowerEqualizerLevel(equalizer);
    }

    public static void setEqualizerLevel(Equalizer equalizer,int band, int level){

            equalizer.setBandLevel((short)band,(short)(level+getLowerEqualizerLevel(equalizer)));
        }
    }


