package com.aws3.ebenradio.util.skins;

import android.content.Context;
import android.content.res.TypedArray;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.create_skin.attrs.Navigation;
import com.aws3.ebenradio.model.create_skin.attrs.Volume;
import com.aws3.ebenradio.model.skin.PlayerControls;
import com.aws3.ebenradio.model.skin.Skin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Dell on 17.10.2016.
 */
public class VolumeUtil {


    public static int selectedItem = -1;

    public static final int CLASSIC = 1;

    public static final int NO_VOL_ID = -1;

    public static Integer[] volIds = {CLASSIC,NO_VOL_ID};


    public static List<Volume> generateList(Context context, Skin skin){
        selectedItem = -1;
        List<Volume> list = new ArrayList<>();
        TypedArray resIds = context.getResources().obtainTypedArray(R.array.volume_ids);
        for(int i = 0; i<resIds.length();i++){
                if(selectedItem==-1)
                    selectedItem = Arrays.asList(volIds).indexOf(skin.volume);
            list.add(new Volume(i,volIds[i],resIds.getResourceId(i,0)));
        }
        resIds.recycle();
        return list;
    }
}
