package com.aws3.ebenradio.util.skins;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.util.keys.PrefKeys;
import com.aws3.ebenradio.util.keys.SkinKeys;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;


/**
 * Created by Dell on 13.09.2016.
 */
public class SkinPrefUtils implements SkinKeys {


    private static SkinPrefUtils sInstance;
    private final Context mContext;

    private SharedPreferences mPreferences;
    private static Gson gsonSkinUriSerializer;
    private static Gson gsonSkinUriDeserializer;
    private SkinPrefUtils(Context context) {
        mPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mContext = context;
    }

    public static void initialize(Context context) {
        if (sInstance != null) {
            throw new IllegalStateException("PrefsUtil have already been initialized");
        }
        gsonSkinUriSerializer = new GsonBuilder()
                .registerTypeAdapter(Uri.class, new UriSerializer())
                .create();
        gsonSkinUriDeserializer = new GsonBuilder()
                .registerTypeAdapter(Uri.class, new UriDeserializer())
                .create();
        sInstance = new SkinPrefUtils(context);
    }

    private static SkinPrefUtils getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException("PrefsUtil should be initialized first");
        }
        return sInstance;
    }

    private static SharedPreferences.Editor getEditor() {
        return getPrefs().edit();
    }

    private static SharedPreferences getPrefs() {
        return getInstance().mPreferences;
    }

    public static void setSkin(Skin skin){
        getEditor().putString(SKIN, gsonSkinUriSerializer.toJson(skin)).commit();
    }

    public static Skin getSkin(){
        Type type = new TypeToken<Skin>(){}.getType();
        return gsonSkinUriDeserializer.fromJson(getPrefs().getString(SKIN,""), type);
    }


    public static void setSkins(List<Skin> skins){
        getEditor().putString(SKINS, gsonSkinUriSerializer.toJson(skins)).commit();
    }


    public static List<Skin> getSkins(){
        Type type = new TypeToken<List<Skin>>(){}.getType();
        return gsonSkinUriDeserializer.fromJson(getPrefs().getString(SKINS,""), type);
    }



    public static void setDefaultSkins(List<Skin> skins){
        getEditor().putString(DEFAULT_SKINS, gsonSkinUriSerializer.toJson(skins)).commit();
    }


    public static List<Skin> getDefaultSkins(){
        Type type = new TypeToken<List<Skin>>(){}.getType();
        return gsonSkinUriDeserializer.fromJson(getPrefs().getString(DEFAULT_SKINS,""), type);
    }

//    public static void setBackgroundPath(Object object){
//        getEditor().putString(BACKGROUND, new Gson().toJson(object)).apply();
//    }
//
//    public static Object getBackgroundPath(){
//        Type type = new TypeToken<Object>(){}.getType();
////        Object background = new Gson().fromJson(getPrefs().getString(BACKGROUND,""), type);
////        return background instanceof String?String.valueOf(background):(int)background
//        return new Gson().fromJson(getPrefs().getString(BACKGROUND,""), type);
//    }

//
//    public static void setCookies(String cookies) {
//        getEditor().putString(COOKIES, cookies).apply();
//    }
//
//
//    public static void setColors(int colorOriginal, int colorLighter, int colorDarker){
//         getEditor().putInt(SELECTED_COLOR, colorOriginal).apply();
//        getEditor().putInt(SELECTED_COLOR_LIGHTER, colorLighter).apply();
//
//        getEditor().putInt(SELECTED_COLOR_DARKER, colorDarker).apply();
//    }
//
//
//    public static int getColor(){
//        return getPrefs().getInt(SELECTED_COLOR, Color.parseColor(DEFAULT_COLOR));
//    }
//
//
//    public static void setStation(int station){
//        getEditor().putInt(SELECTED_STATION, station).apply();
//    }
//
//    public static void setTimer(int time){
//        getEditor().putInt(SELECTED_TIME, time).apply();
//    }
//
//    public static void setTimerStatus(boolean status){
//        getEditor().putBoolean(TIMER_STATUS, status).apply();
//    }
//
//    public static void setAlarmTime(TimeAlarm alarmTime){
//        Log.i("TIME_ALARM", new Gson().toJson(alarmTime));
//        getEditor().putString(ALARM_TIME,new Gson().toJson(alarmTime)).apply();
//    }
//
//    public static void setAlarmStation(RadioStation radioStation){
//
//        getEditor().putString(ALARM_STATION,new Gson().toJson(radioStation)).apply();
//    }
//    public static RadioStation getAlarmStation(Context context){
//        Type type = new TypeToken<RadioStation>(){}.getType();
//        return new Gson().fromJson(getPrefs().getString(ALARM_STATION, new Gson().toJson(new RadioStation(0, RadioStationUtil.getStationIconById(0, false), RadioStationUtil.getStationIconById(0, true), context.getResources().getStringArray(R.array.station_titles)[0], RadioStationUtil.getStationById(0), true))), type);
//    }
//
//    public static void setNoNetworkShown(boolean status){
//        getEditor().putBoolean(NO_NWTWORK_SHOWN, status).apply();
//    }
//
//    public static boolean  getNoNetworkShown(){
//       return getPrefs().getBoolean(NO_NWTWORK_SHOWN, false);
//    }
//
//    public static TimeAlarm getAlarmTime(){
//        Type type = new TypeToken<TimeAlarm>(){}.getType();
//        Log.i("TIME_ALARM2",getPrefs().getString(ALARM_TIME, "123"));
//        return new Gson().fromJson(getPrefs().getString(ALARM_TIME, ""), type);
//    }
//
//
//    public static void setAlarmStatus(boolean status){
//        getEditor().putBoolean(ALARM_STATUS, status).apply();
//    }
//
//    public static boolean getAlarmStatus(){
//        return getPrefs().getBoolean(ALARM_STATUS, false);
//    }
//
//
//    public static boolean getTimerStatus(){
//        return getPrefs().getBoolean(TIMER_STATUS, false);
//    }
//
//    public static int getTimer(){
//        return getPrefs().getInt(SELECTED_TIME, 5);
//    }
//
//    public static List<WeekDay> getWeekDays(){
//        Type type = new TypeToken<List<WeekDay>>(){}.getType();
//        return new Gson().fromJson(getPrefs().getString(SELECTED_DAYS, new Gson().toJson(new ArrayList<WeekDay>())), type);
//    }
//
//
//    public static void setWeekDays(List<WeekDay> weekDays){
//      getEditor().putString(SELECTED_DAYS, new Gson().toJson(weekDays)).apply();
//    }
//
//
//    public static int getStation(){
//        return getPrefs().getInt(SELECTED_STATION, 0);
//    }
//
//    public static int[] getColors(){
//        return new int[]{getPrefs().getInt(SELECTED_COLOR, Color.parseColor(DEFAULT_COLOR)), getPrefs().getInt(SELECTED_COLOR_LIGHTER, Color.parseColor(DEFAULT_COLOR)), getPrefs().getInt(SELECTED_COLOR_DARKER, Color.parseColor(DEFAULT_COLOR))};
//    }
//
//    public static String getCookies() {
//        return getPrefs().getString(COOKIES, "");
//    }
    public static class UriSerializer implements JsonSerializer<Uri> {
        public JsonElement serialize(Uri src, Type typeOfSrc, JsonSerializationContext context) {
            return new JsonPrimitive(src.toString());
        }
    }

    public static class UriDeserializer implements JsonDeserializer<Uri> {
        @Override
        public Uri deserialize(final JsonElement src, final Type srcType,
                               final JsonDeserializationContext context) throws JsonParseException {
            return Uri.parse(src.getAsString());
        }
    }
}
