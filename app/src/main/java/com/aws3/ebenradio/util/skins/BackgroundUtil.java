package com.aws3.ebenradio.util.skins;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.ImageView;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.create_skin.attrs.Background;
import com.aws3.ebenradio.model.radio.RadioStation;
import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.util.general.PixelUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;
import com.aws3.ebenradio.util.radio.RadioStationUtil;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.glide.transformations.BlurTransformation;

/**
 * Created by Dell on 12.10.2016.
 */
public class BackgroundUtil {


    public static void loadImageBackground(Context context, Skin skin, ImageView imageView){
        Glide.with(context).load(skin.backgroundPath==null?BackgroundUtil.getBackground(skin.backgroundRes,context):skin.backgroundPath).centerCrop().into(imageView);
    }

    public static int selectedItem = -1;

    public static List<Background> generateList(Context context, Skin skin){
        selectedItem = -1;
        TypedArray resIds = context.getResources().obtainTypedArray(R.array.background_ids);
        List<Background> list = new ArrayList<>();
        for(int i = 0;i<resIds.length();i++){
            if(selectedItem==-1)
                selectedItem = skin.backgroundRes==i?i:-1;
            list.add(new Background(i,i, null));
        }
        resIds.recycle();
        return list;

    }


    public static int getRadioPlaceholder(Context context){
        TypedArray resIds = context.getResources().obtainTypedArray(R.array.radio_cover);
        int drawable = resIds.getResourceId(PrefUtils.getStation().id,0);
        resIds.recycle();
       return  drawable;
    }


    public static BitmapDrawable radioPlaceholder(Context context){
        TypedArray resIds = context.getResources().obtainTypedArray(R.array.radio_cover);
        int drawable = resIds.getResourceId(PrefUtils.getStation().id,0);
        resIds.recycle();
        return PixelUtil.transformDrawable(context, ContextCompat.getDrawable(context, drawable), ShapeUtil.shapeById(context, SkinPrefUtils.getSkin().shapeId),300);
    }


    public static BitmapDrawable radioPlaceholderBlur(Context context){
        TypedArray resIds = context.getResources().obtainTypedArray(R.array.radio_cover);
        int drawable = resIds.getResourceId(PrefUtils.getStation().id,0);
        resIds.recycle();
        return PixelUtil.transformDrawable(context, ContextCompat.getDrawable(context, drawable), new BlurTransformation(context),400);
    }


    public static int radioPlaceholderId(Context context){
        TypedArray resIds = context.getResources().obtainTypedArray(R.array.radio_cover);
        int drawable = resIds.getResourceId(PrefUtils.getStation().id,0);
        resIds.recycle();
        return drawable;
    }


    public static Integer getPreview(int id, Context context){
        TypedArray resIds = context.getResources().obtainTypedArray(R.array.preview_ids);
        int drawable = resIds.getResourceId(id,0);
        resIds.recycle();
        return  drawable;

    }


    public static Integer getBackground(int id, Context context){
        TypedArray resIds = context.getResources().obtainTypedArray(R.array.background_ids);
        int drawable = resIds.getResourceId(id,0);
        resIds.recycle();
        return  drawable;

    }





}
