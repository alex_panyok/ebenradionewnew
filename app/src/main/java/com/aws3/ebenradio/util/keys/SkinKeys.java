package com.aws3.ebenradio.util.keys;

/**
 * Created by Dell on 12.10.2016.
 */
public interface SkinKeys {
    String SKIN = "skin";
    String SKINS = "skins";
    String DEFAULT_SKINS = "default_skins";
    String TYPEFACE_ID = "typeface_id";
}
