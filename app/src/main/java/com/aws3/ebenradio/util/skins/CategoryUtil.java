package com.aws3.ebenradio.util.skins;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ShareActionProvider;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.create_skin.categories.Category;
import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.util.colors.ColorsUtil;
import com.aws3.ebenradio.view.adapter.create_skin.AdapterAttr;
import com.aws3.ebenradio.view.adapter.create_skin.AdapterBackground;
import com.aws3.ebenradio.view.adapter.create_skin.AdapterBlur;
import com.aws3.ebenradio.view.adapter.create_skin.AdapterColor;
import com.aws3.ebenradio.view.adapter.create_skin.AdapterFont;
import com.aws3.ebenradio.view.adapter.create_skin.AdapterGraphicEQ;
import com.aws3.ebenradio.view.adapter.create_skin.AdapterNavigation;
import com.aws3.ebenradio.view.adapter.create_skin.AdapterShape;
import com.aws3.ebenradio.view.adapter.create_skin.AdapterVolume;
import com.google.common.primitives.Ints;
import com.hannesdorfmann.mosby.mvp.MvpBasePresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 15.10.2016.
 */

public class CategoryUtil {




    public static List<Category> generateCategories(Context context, MvpBasePresenter presenter,Skin skin){
        List<Category> list = new ArrayList<>();
        List<AdapterAttr> listAdapters = generateListAdapters(context,presenter,skin);
        for(int i = 0; i<listAdapters.size();i++){
            list.add(new Category(i,context.getResources().getStringArray(R.array.category_items)[i],listAdapters.get(i)));
        }
        return list;
    }



    private static List<AdapterAttr> generateListAdapters(Context context, MvpBasePresenter presenter, Skin skin){
        List<AdapterAttr> list = new ArrayList<>();
        list.add(new AdapterBackground(BackgroundUtil.generateList(context, skin),presenter, BackgroundUtil.selectedItem));
        list.add(new AdapterBlur(BlurUtil.generateList(context, skin),presenter, BlurUtil.selectedItem));
        list.add(new AdapterNavigation(NavigationUtil.generateList(context,skin), presenter, NavigationUtil.selectedItem));
        list.add(new AdapterVolume(VolumeUtil.generateList(context,skin), presenter,VolumeUtil.selectedItem));
        list.add(new AdapterGraphicEQ(GraphicEQUtil.generateList(context,skin),presenter, GraphicEQUtil.selectedItem));
        list.add(new AdapterColor(ColorUtil.generateList(context,skin), presenter, ColorUtil.selectedItem));
        list.add(new AdapterShape(ShapeUtil.generateList(context, skin), presenter, ShapeUtil.selectedItem));
        list.add(new AdapterFont(TypeFaceUtil.generateList(context,skin),presenter, TypeFaceUtil.selectedItem));
        return list;
    }

}
