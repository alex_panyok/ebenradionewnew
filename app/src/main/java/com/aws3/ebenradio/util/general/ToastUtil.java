package com.aws3.ebenradio.util.general;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Dell on 18.10.2016.
 */
public class ToastUtil {


    public static void toast(Context context, String message){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }
}
