package com.aws3.ebenradio.util.skins;

import android.content.Context;
import android.content.res.TypedArray;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.create_skin.attrs.Blur;
import com.aws3.ebenradio.model.create_skin.attrs.Volume;
import com.aws3.ebenradio.model.skin.Skin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Dell on 18.10.2016.
 */
public class BlurUtil {
    public static int selectedItem = -1;

    public static final int BLUR = 1;

    public static final int NO_BLUR_ID = -1;

    public static Integer[] blurIds = {BLUR,NO_BLUR_ID};


    public static List<Blur> generateList(Context context, Skin skin){
        selectedItem = -1;
        List<Blur> list = new ArrayList<>();
        TypedArray resIds = context.getResources().obtainTypedArray(R.array.blur_ids);
        for(int i = 0; i<resIds.length();i++){
            if(selectedItem==-1)
                selectedItem = Arrays.asList(blurIds).indexOf(skin.blur);
            list.add(new Blur(i,resIds.getResourceId(i,0),blurIds[i]));
        }
        resIds.recycle();
        return list;
    }
}
