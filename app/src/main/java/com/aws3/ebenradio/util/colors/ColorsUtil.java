package com.aws3.ebenradio.util.colors;

import android.content.Context;
import android.support.v4.graphics.ColorUtils;

import com.aws3.ebenradio.util.skins.ColorUtil;


//Color modifications utilities
public class ColorsUtil {

    public static final int LIGHT_PERCENT = 40;


    public static int makeLighter(int color, int percent, Context context){
        float[]hsv = new float[3];
        ColorUtils.colorToHSL(ColorUtil.getColorById(color, context), hsv);
        hsv[2] = hsv[2]*(1f+percent/100f);
        return ColorUtils.HSLToColor(hsv);
    }

    public static int makeLighterColor(int color, int percent){
        float[]hsv = new float[3];
        ColorUtils.colorToHSL(color, hsv);
        hsv[2] = hsv[2]*(1f+percent/100f);
        return ColorUtils.HSLToColor(hsv);
    }

    public static int makeDarker(int color, int percent,Context context){
        float[]hsv = new float[3];
        ColorUtils.colorToHSL(ColorUtil.getColorById(color, context), hsv);
        hsv[2] = hsv[2]/(1f+percent/100f);
        return ColorUtils.HSLToColor(hsv);
    }



}


