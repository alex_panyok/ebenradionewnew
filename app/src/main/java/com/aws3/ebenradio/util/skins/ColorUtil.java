package com.aws3.ebenradio.util.skins;

import android.content.Context;
import android.content.res.TypedArray;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.create_skin.attrs.Color;
import com.aws3.ebenradio.model.create_skin.attrs.Volume;
import com.aws3.ebenradio.model.skin.Skin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 17.10.2016.
 */
public class ColorUtil {

    public static int selectedItem = -1;

    public static List<Color> generateList(Context context, Skin skin){
        selectedItem = -1;
        List<Color> list = new ArrayList<>();
        TypedArray resIds = context.getResources().obtainTypedArray(R.array.color_ids);
        for(int i = 0; i<resIds.length();i++){
            if(selectedItem==-1)
                selectedItem = skin.color==i?i:-1;
            list.add(new Color(i,resIds.getResourceId(i,0),i));
        }
        resIds.recycle();
        return list;
    }




    public static Integer getColorById(int id,Context context){
        TypedArray resIds = context.getResources().obtainTypedArray(R.array.colors_ids);
        Integer resId = resIds.getColor(id,0);
        resIds.recycle();
        return resId;
    }







}
