package com.aws3.ebenradio.util.keys;

/**
 * Created by Dell on 06.10.2016.
 */
public interface PrefKeys {
    String BACKGROUND = "background";
    String SELECTED_STATION= "selected_station";
    String STATIONS = "stations";
    String CUSTOM_PRESET = "custom_preset";
    String CURRENT_LEVELS = "current_levels";
    String SELECTED_PRESET = "selected_preset";
    String IS_ENABLED = "is_enabled";
    String SELECTED_TIME = "selected_time";
    String TIMER_STATUS = "timer_status";
    String ALARM_TIME = "alarm_time";
    String ALARM_STATUS = "alarm_status";
    String SELECTED_DAYS = "selected_days";
    String REPEAT = "repeat";
    String ALARM_STATION = "alarm_station";
    String COOKIES = "cookies";
    String ISRATESHOWN = "is_rate_shown";
    String TIME = "time";
    String BANNER_COUNT = "banner_count";
}
