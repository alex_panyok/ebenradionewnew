package com.aws3.ebenradio.util.skins;

import android.animation.TypeEvaluator;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.icu.text.DisplayContext;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.create_skin.attrs.Color;
import com.aws3.ebenradio.model.create_skin.attrs.Font;
import com.aws3.ebenradio.model.skin.Skin;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 12.10.2016.
 */
public class TypeFaceUtil {

   public static final int OPEN_SANS = 1;

   public static final int COMFORTAA = 2;

   public static final int EXO = 3;

   public static final int BIRYANI = 4;

   public static final int MITR = 5;

   public static final int MERIENDA = 6;


   public static Integer[] fontsIds = {OPEN_SANS,COMFORTAA,EXO,BIRYANI, MITR, MERIENDA};

   public static int selectedItem = -1;

   public static Typeface[] typefaceOpensans = new Typeface[2];
   public static Typeface[] typefaceComfortaa = new Typeface[2];
   public static Typeface[] typefaceExo = new Typeface[2];
   public static Typeface[] typefaceBiryani = new Typeface[2];
   public static Typeface[] typefaceMitr = new Typeface[2];
   public static Typeface[] typefaceMerienda = new Typeface[2];

   public static void initTypefaces(Context context){
      initTypeFace(typefaceOpensans,Typeface.createFromAsset(context.getAssets(), "open_sans_regular.ttf"),Typeface.createFromAsset(context.getAssets(), "open_sans_bold.ttf"));
      initTypeFace(typefaceComfortaa, Typeface.createFromAsset(context.getAssets(), "comfortaa_regular.ttf"),Typeface.createFromAsset(context.getAssets(), "comfortaa_bold.ttf"));
      initTypeFace(typefaceExo, Typeface.createFromAsset(context.getAssets(), "exo_regular.ttf"),Typeface.createFromAsset(context.getAssets(), "exo_bold.ttf"));
      initTypeFace(typefaceBiryani, Typeface.createFromAsset(context.getAssets(), "biryani_regular.ttf"),Typeface.createFromAsset(context.getAssets(), "biryani_bold.ttf"));
      initTypeFace(typefaceMitr, Typeface.createFromAsset(context.getAssets(), "mitr_regular.ttf"),Typeface.createFromAsset(context.getAssets(), "mitr_bold.ttf"));
      initTypeFace(typefaceMitr, Typeface.createFromAsset(context.getAssets(), "mitr_regular.ttf"),Typeface.createFromAsset(context.getAssets(), "mitr_bold.ttf"));
      initTypeFace(typefaceMerienda, Typeface.createFromAsset(context.getAssets(), "merienda_regular.ttf"),Typeface.createFromAsset(context.getAssets(), "merienda_bold.ttf"));
   }


   private static void  initTypeFace(Typeface[] typefaces, Typeface typefaceRegular, Typeface typefaceBold){
      typefaces[0] = typefaceRegular;
      typefaces[1] = typefaceBold;
   }



   public static Typeface[] getTypefaceById(int id){
      switch (id){
         case OPEN_SANS:
            return typefaceOpensans;
         case COMFORTAA:
            return typefaceComfortaa;
         case EXO:
            return typefaceExo;
         case BIRYANI:
            return typefaceBiryani;
         case MITR:
            return typefaceMitr;
         case MERIENDA:
            return typefaceMerienda;
      }
      return typefaceOpensans;
   }


    public static List<Font> generateList(Context context, Skin skin){
       selectedItem = -1;
        List<Font> list = new ArrayList<>();
        TypedArray resIds = context.getResources().obtainTypedArray(R.array.font_ids);
        for(int i = 0; i<resIds.length();i++){
           if(selectedItem==-1)
              selectedItem = skin.typefaceId==fontsIds[i]?i:-1;
            list.add(new Font(i,fontsIds[i],resIds.getResourceId(i,0)));
        }
        resIds.recycle();
        return list;
    }


}
