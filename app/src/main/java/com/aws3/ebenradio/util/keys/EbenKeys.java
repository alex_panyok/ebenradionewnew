package com.aws3.ebenradio.util.keys;

/**
 * Created by Dell on 18.10.2016.
 */
public interface EbenKeys {

    ////////REQUEST ACTIVITY KEYS//////
    int PICK_PHOTO = 1000;
    int TAKE_PHOTO_CODE = 1001;


    String REPEAT = "repeat";
    String STATIONS = "stations";
    String TYPE = "type";
    String DISTANCE = "distance";
    String ARTICLE = "article";


    //Fragment Profile Keys

    String FRAG_ID = "fragId";
    int PROF_FRAG_ID = 1;
    int CHAT_FRAG_ID = 2;


    //Events Chat
    String NEW_MESSAGE = "newmessage";
    String JOIN = "join";
    int TYPE_MESSAGE_FROM_ME = 1;
    int TYPE_MESSAGE = 2;
    int TYPE_DATE_MESSAGE = 3;



    String EVENT = "event";

    String EVENT_PUSH = "event_push";

    String BANNER = "banner";

    String EVENT_ID = "event_id";

    String URL = "url";

    String STREAM_ID = "stream_id";

    String MESSAGE = "message";

    String SKIN = "skin";

    String POSITION = "pos";

    int FACEBOOK = 1;
    int TWITTER = 2;
    int NORMAL = 3;
}
