package com.aws3.ebenradio.util.general;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by Dell on 25.10.2016.
 */
public class EditTextUtil {





    public static void setFocusListeners(TextInputLayout...editTexts) {

        for (final TextInputLayout editText : editTexts) {
            editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus)
                        editText.setError(null);
                }
            });
        }

    }



    public static void clearFocus(Context context,EditText...editTexts){
        for(EditText editText : editTexts){
            editText.clearFocus();
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }

    public static void setErrorNull(TextInputLayout...layouts){
        for(TextInputLayout layout:layouts){
            layout.setError(null);
        }
    }

    public static void setFocus(Context context, EditText...editTexts) {

        for (EditText editText : editTexts) {
            editText.requestFocus();
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        }

    }


    public static void clearFields(Context context, EditText...editTexts){
        for (EditText editText : editTexts) {
            editText.setText("");
        }
        clearFocus(context, editTexts);
    }
}
