package com.aws3.ebenradio.util.skins;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.create_skin.attrs.GraphicEQ;
import com.aws3.ebenradio.model.skin.PlayerControls;
import com.aws3.ebenradio.model.skin.Skin;
import com.aws3.ebenradio.util.colors.ColorsUtil;
import com.aws3.ebenradio.util.prefs.PrefUtils;

import java.util.ArrayList;
import java.util.List;

public class SkinUtils {

    public static void initDefaultSkins(Context context){
        Skin defaultSkin_0 = new Skin(
                context.getString(R.string.skin_default),
                true,
                0,
                TypeFaceUtil.OPEN_SANS,
                null,
               0,
                new PlayerControls(0,
                       0,
                        0,
                        0),
                0,
                BlurUtil.BLUR,
                ShapeUtil.RECTANGLE,
                VolumeUtil.CLASSIC,
                GraphicEQUtil.CLASSIC);

        Skin defaultSkin_1 = new Skin(
                context.getString(R.string.skin_1),
                true,
               1,
                TypeFaceUtil.COMFORTAA,
                null,
                1,
                new PlayerControls(1,
                        1,
                        1,
                        1),
                1,
                BlurUtil.NO_BLUR_ID,
                ShapeUtil.CIRCLE,
                VolumeUtil.CLASSIC,
                GraphicEQUtil.NO_GEQ_ID);
        Skin defaultSkin_2 = new Skin(
                context.getString(R.string.skin_2),
                true,
                2,
                TypeFaceUtil.EXO,
                null,
                2,
                new PlayerControls(2,
                        2,
                        2,
                        2),
                2,
                BlurUtil.NO_BLUR_ID,
                ShapeUtil.CIRCLE,
                VolumeUtil.CLASSIC,
                GraphicEQUtil.NO_GEQ_ID);
        Skin defaultSkin_3 = new Skin(
                context.getString(R.string.skin_3),
                true,
               3,
                TypeFaceUtil.BIRYANI,
                null,
                3,
                new PlayerControls(3,
                        3,
                        3,
                        3),
                3,
                BlurUtil.NO_BLUR_ID,
                ShapeUtil.RECTANGLE,
                VolumeUtil.CLASSIC,
                GraphicEQUtil.CLASSIC);
        Skin defaultSkin_4 = new Skin(
                context.getString(R.string.skin_4),
                true,
                4,
                TypeFaceUtil.MITR,
                null,
                4,
                new PlayerControls(4,
                        4,
                        4,
                        4),
                4,
                BlurUtil.NO_BLUR_ID,
                ShapeUtil.RECTANGLE,
                VolumeUtil.CLASSIC,
                GraphicEQUtil.CLASSIC);



        Skin defaultSkin_5 = new Skin(
                context.getString(R.string.skin_5),
                true,
                5,
                TypeFaceUtil.MERIENDA,
                null,
               5,
                new PlayerControls(5,
                        5,
                        5,
                        5),
                5,
                BlurUtil.NO_BLUR_ID,
                ShapeUtil.ROUNDED,
                VolumeUtil.CLASSIC,
                GraphicEQUtil.NO_GEQ_ID);

        Skin defaultSkin_6 = new Skin(
                context.getString(R.string.skin_6),
                true,
                6,
                TypeFaceUtil.MITR,
                null,
                6,
                new PlayerControls(6,
                        6,
                        6,
                        6),
                10,
                BlurUtil.NO_BLUR_ID,
                ShapeUtil.RECTANGLE,
                VolumeUtil.CLASSIC,
                GraphicEQUtil.CLASSIC);

        Skin defaultSkin_7 = new Skin(
                context.getString(R.string.skin_7),
                true,
                7,
                TypeFaceUtil.OPEN_SANS,
                null,
                7,
                new PlayerControls(6,
                        6,
                        6,
                        6),
                11,
                BlurUtil.NO_BLUR_ID,
                ShapeUtil.RECTANGLE,
                VolumeUtil.CLASSIC,
                GraphicEQUtil.CLASSIC);



        List<Skin> tempSkins = new ArrayList<>();
        tempSkins.add(defaultSkin_0);
        tempSkins.add(defaultSkin_1);
        tempSkins.add(defaultSkin_2);
        tempSkins.add(defaultSkin_3);
        tempSkins.add(defaultSkin_4);
        tempSkins.add(defaultSkin_5);
        tempSkins.add(defaultSkin_6);
        tempSkins.add(defaultSkin_7);
        if(SkinPrefUtils.getSkins()==null || SkinPrefUtils.getSkins().isEmpty()){
            List<Skin> skins = new ArrayList<>();
            skins.addAll(tempSkins);
            SkinPrefUtils.setDefaultSkins(tempSkins);
            SkinPrefUtils.setSkins(skins);
        }else{
                removeDefaultSkins();
                addDefaultSkins(tempSkins);
                SkinPrefUtils.setDefaultSkins(tempSkins);
        }
        if(SkinPrefUtils.getSkin()==null){
            applySkin(defaultSkin_0);
        }
    }

    private static void removeDefaultSkins(){
        List<Skin> skins = SkinPrefUtils.getSkins();
        for(int i = 0; i<SkinPrefUtils.getDefaultSkins().size();i++){
            if(SkinPrefUtils.getSkins().size()!=0)
                skins.remove(0);
        }
        SkinPrefUtils.setSkins(skins);
    }


    private static void addDefaultSkins(List<Skin> tempSkins){
        List<Skin> skins = SkinPrefUtils.getSkins();
        for(int i = 0; i<tempSkins.size();i++){
            int version = 0;
            addSkinIfNameNotExist(skins, i, tempSkins.get(i), version);
        }
        SkinPrefUtils.setSkins(skins);
    }


    private static boolean checkName(String name){
        for(Skin skin : SkinPrefUtils.getSkins()){
            if(skin.name.equals(name))
                return false;
        }
        return true;
    }


    private static void addSkinIfNameNotExist(List<Skin> skins, int pos,  Skin skin, int version){
        if(checkName(skin.name))
            skins.add(pos, skin);
        else{
            version++;
            skin.name = version!=1?skin.name.substring(0, skin.name.length()-2):skin.name+" "+version;
            addSkinIfNameNotExist(skins, pos, skin, version);
        }

    }





    private static void applySkin(Skin skin){
        SkinPrefUtils.setSkin(skin);
    }
}
