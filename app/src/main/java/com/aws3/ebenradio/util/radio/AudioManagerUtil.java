package com.aws3.ebenradio.util.radio;

import android.content.Context;

/**
 * Created by Dell on 02.03.2017.
 */
public class AudioManagerUtil {
    private static AudioManagerUtil ourInstance = new AudioManagerUtil();

    public static AudioManagerUtil getInstance() {
        return ourInstance;
    }

    private int sessionId;


    private AudioManagerUtil() {
    }


    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }



}
