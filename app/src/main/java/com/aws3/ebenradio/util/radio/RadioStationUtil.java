package com.aws3.ebenradio.util.radio;

import android.content.Context;

import com.aws3.ebenradio.R;
import com.aws3.ebenradio.model.radio.RadioStation;
import com.aws3.ebenradio.util.prefs.PrefUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 31.08.2016.
 */
public class RadioStationUtil {



    public static int getStationIconById(int id){
        switch (id) {
            case 0:
                return R.drawable.rediscover_big;
            case 1:
                return R.drawable.r_n_b_big;
            case 2:
                return R.drawable.afro_pop_big;
            case 3:
                return R.drawable.tropical_big;
            case 4:
                return R.drawable.lounge_big;
            case 5:
                return R.drawable.oldies_big;
            case 6:
                return R.drawable.cristian_big;
            case 7:
                return R.drawable.afro_pop_big;
            case 8:
                return R.drawable.arabian_big;
            case 9:
                return R.drawable.asian_big;
            case 10:
                return R.drawable.classic_big;
            case 11:
                return R.drawable.hip_hop_big;
            case 12:
                return R.drawable.latin_big;
            case 13:
                return R.drawable.clubbing_big;
            case 14:
                return R.drawable.fitness_big;
            case 15:
                return R.drawable.spa_big;
            default:
                return R.drawable.rediscover_big;
        }
    }



    public static String getStationById(int id){
        switch (id) {
            case 0:
                return "http://s8.voscast.com:7558/;stream.mpg";
            case 1:
                return "http://s9.voscast.com:7334/;stream.mpg";
            case 2:
                return "http://s2.voscast.com:9022/;stream.mpg";
            case 3:
                return "http://s8.voscast.com:9784/;stream.mpg";
            case 4:
                return "http://s8.voscast.com:9786/;stream.mpg";
            case 5:
                return "http://s8.voscast.com:9788/;stream.mpg";
            case 6:
                return "http://s8.voscast.com:9790/;stream.mpg";
            case 7:
                return "http://s10.voscast.com:9972/;stream.mpg";
            case 8:
                return "http://s10.voscast.com:9974/;stream.mpg";
            case 9:
                return "http://s10.voscast.com:9976/;stream.mpg";
            case 10:
                return "http://s10.voscast.com:9978/;stream.mpg";
            case 11:
                return "http://s10.voscast.com:9980/;stream.mpg";
            case 12:
                return "http://s10.voscast.com:9982/;stream.mpg";
            case 13:
                return "http://s6.voscast.com:8760/;stream.mpg";
            case 14:
                return "http://s6.voscast.com:8762/;stream.mpg";
            case 15:
                return "http://s6.voscast.com:8764/;stream.mpg";
            default:
                return "http://s2.voscast.com:7558/;stream.mpg";
        }
    }



    public static List<RadioStation> getRadioStationItems(Context context){

            List<RadioStation> list = new ArrayList<>();
            for (int i = 0; i < context.getResources().getStringArray(R.array.station_titles).length; i++) {
                list.add(new RadioStation(i, context.getResources().getStringArray(R.array.station_titles)[i], getStationById(i), i, getUrlTitleSongById(i)));
            }
            if(PrefUtils.getStation()==null)
                PrefUtils.setStation(list.get(0));
            return list;

    }






    public static String getUrlTitleSongById(int id){
        switch (id) {
            case 0:
                return "http://s8.voscast.com:7558/currentsong?sid=1";
            case 1:
                return "http://s9.voscast.com:7334/currentsong?sid=1";
            case 2:
                return "http://s2.voscast.com:9022/currentsong?sid=1";
            case 3:
                return "http://s8.voscast.com:9784/currentsong?sid=1";
            case 4:
                return "http://s8.voscast.com:9786/currentsong?sid=1";
            case 5:
                return "http://s8.voscast.com:9788/currentsong?sid=1";
            case 6:
                return "http://s8.voscast.com:9790/currentsong?sid=1";
            case 7:
                return "http://s10.voscast.com:9972/currentsong?sid=1";
            case 8:
                return "http://s10.voscast.com:9974/currentsong?sid=1";
            case 9:
                return "http://s10.voscast.com:9976/currentsong?sid=1";
            case 10:
                return "http://s10.voscast.com:9978/currentsong?sid=1";
            case 11:
                return "http://s10.voscast.com:9980/currentsong?sid=1";
            case 12:
                return "http://s10.voscast.com:9982/currentsong?sid=1";
            case 13:
                return "http://s6.voscast.com:8760/currentsong?sid=1";
            case 14:
                return "http://s6.voscast.com:8762/currentsong?sid=1";
            case 15:
                return "http://s6.voscast.com:8764/currentsong?sid=1";
            default:
                return "http://s2.voscast.com:7558/currentsong?sid=1";
        }
    }


    public static String getUrlImageStationById(int id){
        switch (id) {
            case 0:
                return "http://api.ebenradio.com/covers/cover_0.png";
            case 1:
                return "http://api.ebenradio.com/covers/cover_1.png";
            case 2:
                return "http://api.ebenradio.com/covers/cover_2.png";
            case 3:
                return "http://api.ebenradio.com/covers/cover_3.png";
            case 4:
                return "http://api.ebenradio.com/covers/cover_4.png";
            case 5:
                return "http://api.ebenradio.com/covers/cover_5.png";
            case 6:
                return "http://api.ebenradio.com/covers/cover_6.png";
            case 7:
                return "http://api.ebenradio.com/covers/cover_7.png";
            case 8:
                return "http://api.ebenradio.com/covers/cover_8.png";
            case 9:
                return "http://api.ebenradio.com/covers/cover_9.jpeg";
            case 10:
                return "http://api.ebenradio.com/covers/cover_10.png";
            case 11:
                return "http://api.ebenradio.com/covers/cover_11.png";
            case 12:
                return "http://api.ebenradio.com/covers/cover_12.png";
            case 13:
                return "http://api.ebenradio.com/covers/cover_13.png";
            case 14:
                return "http://api.ebenradio.com/covers/cover_14.png";
            case 15:
                return "http://api.ebenradio.com/covers/cover_15.png";
            default:
                return "http://ebenradio.com/images/covers/rediscover.jpg";
        }
    }

}
